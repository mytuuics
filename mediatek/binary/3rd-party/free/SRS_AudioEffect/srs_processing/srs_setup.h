#ifndef ANDROID_SRS_SETUP
#define ANDROID_SRS_SETUP

// Path Stringizers

#define SRS_STR(s) DOSRS_STR(s)		// Trick to allow values of defines to become strings properly...
#define DOSRS_STR(s) #s

#ifdef _SRSCFG_ARCH_ARM
	#define SRSLIBINC(file) DOSRS_STR( srs_include_arm/file )
	#include SRSLIBINC(srs_fxp.h)
#endif	// _SRSCFG_ARCH_ARM

#ifdef _SRSCFG_ARCH_X86
	#define SRSLIBINC(file) DOSRS_STR( srs_include_x86/file )
	#define SRS_FXP16(val,iwl) val
	#define SRS_FXP32(val,iwl) val
#endif	// _SRSCFG_ARCH_X86

// Didn't allow Logging?
#ifndef SRS_VERBOSE

#ifdef LOG		// Mute Android's own LOG macro (add other macros to mute as needed)
	#undef LOG
	#define LOG(...)   ((void)0)
#endif

#endif

#endif	// ANDROID_SRS_SETUP

