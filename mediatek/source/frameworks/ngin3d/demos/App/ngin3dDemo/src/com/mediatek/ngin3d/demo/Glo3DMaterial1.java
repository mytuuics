package com.mediatek.ngin3d.demo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.mediatek.ngin3d.Ngin3d;
import com.mediatek.ngin3d.Glo3D;
import com.mediatek.ngin3d.Point;
import com.mediatek.ngin3d.Container;
import com.mediatek.ngin3d.Rotation;
import com.mediatek.ngin3d.Scale;
import com.mediatek.ngin3d.Stage;
import com.mediatek.ngin3d.android.StageActivity;
import com.mediatek.ngin3d.android.StageView;
import com.mediatek.ngin3d.demo.R;

import javax.microedition.khronos.opengles.GL10;

/**
 * A test bed for a Glo3D model with various materials.
 *
 * The object is whatever is defined by assets/simple_surface.glo but this
 * should be kept to something like a simple curve to ease of understanding
 * what is being rendered.
 */
public class Glo3DMaterial1 extends Activity {
    private static final String TAG = "Glo3DMaterial1";

    private static final float CAM_X = 0f;
    private static final float CAM_Y = 0f;
    private static final float CAM_Z = 1000f;
    // half-size of screen. Orientation fixed at landscape in manifest XML.
    private static final float HWIDTH = 400f;
    private static final float HHEIGHT = 240f;

    // The Surface object, position and scaling
    private static final float SURFX = HWIDTH;
    private static final float SURFY = HHEIGHT;
    private static final float SURFZ = 0.f;
    private static final float SURFSIZE = 5f;

    protected Stage mStage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Glo3D landscape = Glo3D.createFromAsset("simple_surface.glo");

        mMaterialDemoView = new MaterialDemoView(this);
        mStage = mMaterialDemoView.getStage();
        setContentView(mMaterialDemoView);

        Container scenario = new Container();

        scenario.add(landscape);

        scenario.setPosition(new Point(SURFX, SURFY, SURFZ));
        // Scale Y -1 as UI-Perspective is Y-down, but model is Y-up
        scenario.setScale(new Scale(SURFSIZE, -SURFSIZE, SURFSIZE));

        // Todo ... something with materials...

        mStage.add(scenario);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.glo_material_option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        // Menu is coded as 'reset' then a few on/off switches.
        // Exactly what is turned on/off is only defined here
        // - assumed to be material sets but could be animation etc.
        // 'reset' => everything 'off' and resets position of object & camera

        switch (itemId){
        case R.id.mat_00: // reset everything
            break;
        case R.id.mat_01: // Toggle 'set 1' on/off
            break;
        case R.id.mat_02: // Toggle 'set 2', etc.
            break;
        case R.id.mat_03:
            break;
        case R.id.mat_04:
            break;
        case R.id.mat_05:
            break;
        default:
            return false;
        }
        return true;
    }


    /* This demo uses a conventional 3D projection, different to the default
     * pseudo-orthogonal one in ngin3d.  Therefore it is necessary to override
     * the default camera set-up with our own view behaviour.
     *
     * MaterialDemoView extends the default StageView, allowing us to customize
     * the camera set-up on onSurfaceChanged.
     */

    private MaterialDemoView mMaterialDemoView;

    private class MaterialDemoView extends StageView {
        public MaterialDemoView(Context context) {
            super(context);
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height) {
            super.onSurfaceChanged(gl, width, height);
            // Position camera at its location but invert Y in order to
            // correct for the oddities of the coordinate system
            mStage.setCamera(
                new Point(CAM_X, -CAM_Y, CAM_Z),
                // Aim the camera at the location of the scene
                new Point(SURFX, SURFY, SURFZ) );
        }
    }
}
