/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Appearance class
 *
 */
#ifndef A3M_APPEARANCELINKER_H
#define A3M_APPEARANCELINKER_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/appearance.h> /* A3M base type defines */

namespace a3m
{
  /** \defgroup a3mAppearancelinker AppearanceLinker
   * \ingroup  a3mAppearance
   *
   * Utility class to perform linking of global uniforms with Appearances.
   *
   * @{
   */

  /**
   * Appearance linker class.
   * A convenience class which owns a list of uniforms, and implements the
   * appearance linker interface so that appearances can use the uniform values
   * set in this object.  Implementing this functionality in its own class
   * avoids having to duplicate it in classes which required linking
   * capabilities (e.g. renderers).  It also allows different parts of a system
   * to share the same list of global uniform values (e.g. if an application
   * uses several different renderers).
   */
  class AppearanceLinker : public Appearance::Linker
  {
  public:
    /** Returns a uniform with a specific ID.
     * Called by the appearance to get a handle on a global uniform value.
     */
    ShaderUniformBase::Ptr getUniform(
        A3M_CHAR8 const* id
        /**< Shader uniform ID */);

    /** Sets an existing uniform's value.
     */
    template<typename T>
    void addUniform(
        A3M_CHAR8 const* id,
        /**< Shader uniform ID */
        A3M_INT32 size = 1
        /**< Size of shader uniform */)
    {
      if (size <= 0)
      {
        A3M_LOG_ERROR("Size of uniform \"%s\" must be greater than zero.", id);
        return;
      }

      m_uniforms[id] = ShaderUniformBase::Ptr(new ShaderUniform<T>(size));
    }

    /** Sets an existing uniform's value.
     */
    template<typename T>
    A3M_BOOL setUniformValue(
        A3M_CHAR8 const* id,
        /**< Shader uniform ID */
        T const& value,
        /**< Value to set */
        A3M_INT32 index = 0
        /**< Index into shader uniform */)
    {
      ShaderUniformBase::Ptr uniform = getUniform(id);

      if (!uniform)
      {
        return A3M_FALSE;
      }

      return uniform->setValue(value, index);
    }

  private:
    typedef std::map<std::string, ShaderUniformBase::Ptr> ShaderUniformMap;

    ShaderUniformMap m_uniforms; /**< Uniforms mapped by ID */
  };

  /** @} */

} /* end of namespace */

#endif /* A3M_APPEARANCELINKER_H */
