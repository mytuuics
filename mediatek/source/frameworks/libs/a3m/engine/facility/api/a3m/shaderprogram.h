/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * ShaderProgram class
 *
 */
#ifndef A3M_SHADERPROGRAM_H
#define A3M_SHADERPROGRAM_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/detail/programresource.h>  /* for ProgramResource */
#include <a3m/detail/shaderresource.h>  /* for ShaderResource */
#include <a3m/assetcache.h> /* for AssetCache */
#include <a3m/pointer.h>          /* for SharedPtr */
#include <a3m/shaderuniform.h>   /* ShaderUniform */
#include <a3m/detail/resourcecache.h>   /* for ResourceCache */

namespace a3m
{
  /** \defgroup a3mShaderprogram Shader Program
   * \ingroup  a3mRefRender
   * \ingroup  a3mRefAssets
   *
   * The shader program class encapsulates a program running on the GPU
   * defined by a vertex shader and fragment shader pair.
   *
   * ShaderPrograms are Assets and are therefore managed by an AssetCache in the
   * AssetPool.
   *
   * \note
   * Generally, the client will want to use AssetCachePool to create and manage
   * asset caches, rather than creating them manually.
   *
   * \code
   * // Create a shader program cache and add a path from which to load programs
   * ShaderProgramCache::Ptr cache( new ShaderProgramCache() );
   * registerSource( *cache, ".\\assets\\shaders");
   *
   * // Register a loader for the cache to use
   * cache->registerLoader( myLoader );
   *
   * // Load ShaderProgram from ".\assets\shaders"
   * ShaderProgram::Ptr shaderProgram = cache->get( "phong.sp" );
   * \endcode
   *
   *  @{
   */

  /* Forward declarations */
  class ShaderProgramCache;
  class VertexBuffer;

  /** Shader Program class.
   * A ShaderProgram object contains an OpenGL shader program object. Upon
   * construction the supplied vertex and fragment shader source is compiled
   * and linked. The vertex attributes in a VertexBuffer can be bound to those
   * used in the shader program using the bind() method. All uniforms used by
   * the shader program can be obtained with the getUniforms() method. This
   * creates a linked list of uniforms which are used to store client-specific
   * values for each uniform. To "bind" a retrieved shader uniform, use
   * uniform->enable().
   */
  class ShaderProgram : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( ShaderProgram )

    /** Smart pointer type */
    typedef a3m::SharedPtr< ShaderProgram > Ptr;

    /** Cache type for this class */
    typedef ShaderProgramCache CacheType;

    /** Destructor. */
    ~ShaderProgram();

    /** Select this program for use. */
    void enable();

    /** Validate the shader program.
     * Warning: this may be slow and should be used for debugging purposes
     * only.
     * \return A3M_TRUE if the program was successfuly compiled, linked and
     * validated.
     */
    A3M_BOOL isValid();

    /** Bind the VertexBuffer object.
     * Associates any vertex attributes used in the shader program with
     * matching attributes defined in the VertexBuffer.
     */
    void bind( VertexBuffer &buffer /**< Vertex buffer containing
                                         matching vertex attributes */ );

    /** Enables shader uniform at a specified index.
     * The uniform will only be enabled if the new value is different from the
     * old value, or if force has been set to A3M_TRUE.
     */
    void enableUniform(
        A3M_INT32 i,
        /**< Uniform index */
        ShaderUniformBase::Ptr const& newValue,
        /**< Value to assign to uniform (pass null to keep current value) */
        A3M_BOOL force = A3M_FALSE
        /**< Set to A3M_TRUE to enable uniform even if it hasn't changed. */);

    /** Get the name of a shader uniform at a specified index.
     * \return Shader uniform name
     */
    A3M_CHAR8 const* getUniformName(A3M_INT32 i /**< Uniform index */) const;

    /** Gives the number of shader uniforms in this program.
     * \return The number of shader uniforms
     */
    A3M_INT32 getUniformCount() const;

    /** Get the compiled binary code for the shader.
     * Use this function to save the compiled shader program. The buffer
     * pointed to by the "binary" parameter should be freed using
     * delete [] when you are finished with it.
     * \return true if successful.
     */
    A3M_BOOL getBinary( A3M_UINT32 &size,
                        /**< Length of binary data */
                        A3M_UINT32 &format,
                        /**< Format of binary data */
                        A3M_CHAR8 *&binary
                        /**< Buffer containing precompiled shader */);

  private:
    friend class ShaderProgramCache; /* Is ShaderProgram's factory class */

    /*
     * Structure containing a uniform and its associated data.
     */
    struct ShaderUniformInfo
    {
      ShaderUniformInfo(
          A3M_CHAR8 const* name_,
          A3M_INT32 location_,
          A3M_INT32 texUnit_,
          ShaderUniformBase::Ptr uniform_) :
        name(name_),
        location(location_),
        texUnit(texUnit_),
        uniform(uniform_)
      {
      }

      std::string name;
      A3M_INT32 location;
      A3M_INT32 texUnit;
      ShaderUniformBase::Ptr uniform;
    };

    /**
     * Private constructor.
     * This constructor is called by ShaderProgramCache.
     */
    ShaderProgram(
        detail::ProgramResource::Ptr const& resource /**< Program resource */);

    detail::ProgramResource::Ptr m_resource; /**< OpenGL program resource */

    /* List of uniforms with the values that were last set in this program */
    std::vector<ShaderUniformInfo> m_uniforms;

    /* Utility function to create a shader uniform. */
    template< class T >
    void createUniform( A3M_CHAR8 const *name,
                        A3M_INT32 location,
                        A3M_INT32 texUnit,
                        A3M_INT32 size );

    /* Builds an internal list of all the uniforms defined by this program.
     */
    void getUniforms();
  };

  /**
   * AssetCache specialised for storing and creating ShaderProgram assets.
   */
  class ShaderProgramCache : public AssetCache<ShaderProgram>
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< ShaderProgramCache > Ptr;

    /** Creates a shader program from source.
     * Compiles and links the given vertex and fragment shader source code into
     * an OpenGL shader program object.
     * \return The shader program, or null if compilation or linking failed
     */
    ShaderProgram::Ptr create(
        A3M_CHAR8 const* vsSource, /**< Source code for vertex shader */
        A3M_CHAR8 const* fsSource, /**< Source code for fragment shader */
        A3M_CHAR8 const* name = 0 /**< Optional name to give the asset. If
                                    omitted, the asset will not be reachable
                                    via the AssetCache::get() function. */);

    /** Creates a shader program from precompiled binary.
     * Loads a precompiled shader program from memory. This function may not be
     * supported on certain platforms, and will return null if so.
     * \return The shader program, or null if loading failed
     */
    ShaderProgram::Ptr create(
        A3M_UINT32 size, /**< Length of binary data */
        A3M_UINT32 format, /**< Format of binary data */
        A3M_CHAR8 const* binary, /**< Buffer containing precompiled shader */
        A3M_CHAR8 const* name = 0 /**< Optional name to give the asset. If
                                    omitted, the asset will not be reachable
                                    via the AssetCache::get() function. */);
  };

  /** @} */

} /* end of namespace */

#endif /* A3M_SHADERPROGRAM_H */
