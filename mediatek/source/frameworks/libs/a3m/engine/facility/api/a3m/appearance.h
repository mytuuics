/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Appearance class
 *
 */
#ifndef A3M_APPEARANCE_H
#define A3M_APPEARANCE_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/base_types.h>       /* A3M base type defines      */
#include <a3m/pointer.h>          /* for SharedPtr              */
#include <a3m/noncopyable.h>      /* for NonCopyable            */
#include <a3m/shaderprogram.h>   /* for ShaderProgram::Ptr     */
#include <a3m/shaderuniform.h>   /* for ShaderUniformBase::Ptr */
#include <a3m/stream.h>          /* for Stream::Ptr */
#include <a3m/texture2d.h>       /* for Texture2DCache */
#include <a3m/polygonmode.h>     /* for PolygonMode            */
#include <a3m/compositingmode.h> /* for CompositingMode        */
#include <a3m/renderdevice.h>    /* for render()               */

#include <map> /* for std::map */
#include <string> /* for std::string */
#include <vector> /* for std::vector */

namespace a3m
{
  /** \defgroup a3mAppearance Appearance
   * \ingroup  a3mRefScene
   *
   * The appearance class specifies all aspects of how a primitive will be
   * rendered. It contains a ShaderProgram and a list of ShaderUniforms that
   * will be written to the ShaderProgram. It also specifies the polygon mode
   * (culling operations), alpha blending mode and depth testing/writing that
   * will be used.
   *
   * ShaderUniforms can be added to the Appearance one by one:
   * \code
   *  a3m::ShaderUniformBase::Ptr transform(
   *    new a3m::ShaderUniform< a3m::Matrix4f >( "u_invProjection" ) );
   *  appearance.addUniform( transform );
   * \endcode
   * or by telling an Appearance object to add all of the uniforms used by its
   * currently attached ShaderProgram:
   * \code
   * appearance.addUniformsFromProgram();
   * \endcode
   *
   * There is also a convenience function setUniformValue:
   * \code
   * frontAppearance.setUniformValue( "u_texture", texture );
   * \endcode
   * This function will only set the value of the uniform if the uniform has
   * already been added to the Appearance and the type of the value parameter
   * matches the type of the uniform.
   *  @{
   */

  /** Interface for uniform collector objects.
   * Classes should implement this interface to be able to collect uniform data
   * via the Appearance::collectUniforms() function.
   */
  class AppearanceUniformCollector
  {
  public:
    virtual ~AppearanceUniformCollector() {}

    /** Called by the appearance once per uniform to pass uniform information.
     * \return A3M_TRUE to continue collecting the uniforms, or A3M_FALSE to
     * terminate collection early.
     */
    virtual A3M_BOOL collect(
        ShaderUniformBase::Ptr const& uniform,
        /**< Uniform object */
        A3M_CHAR8 const* name,
        /**< Name of the uniform */
        A3M_CHAR8 const* id,
        /**< ID associated with the uniform (null if no ID is associated) */
        A3M_BOOL linkedToShaderProgram
        /**< A3M_TRUE if uniform exists in the shader program */) = 0;
  };

  /** Appearance class.
   * The Appearance class specifies all aspects of how a primitive will be
   * rendered. It contains a ShaderProgram and a list of ShaderUniforms that
   * will be written to the ShaderProgram. It also specifies the polygon mode
   * (culling operations), alpha blending mode and depth testing/writing that
   * will be used.
   */
  class Appearance : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( Appearance )

    /** Callback interface for linking with appearance uniforms.
     * While the value of many a uniform is local to the appearance which uses
     * it, some uniform values need to be broadcast globally to all appearances.
     * Linker objects return a uniform value when requested by an appearance,
     * so that it can acquire a referenced to this shared global value.  This
     * provides a convenient way of setting a single uniform value on multiple
     * appearances, without having to manually filter and update appearances
     * with the said value.
     *
     * Classes can implement this interface if they own shader uniforms that
     * they wish to link using Appearance::link().
     */
    class Linker
    {
    public:
      /** Virtual destructor.
       */
      virtual ~Linker() {}

      /** Returns a uniform with a specific ID.
       */
      virtual ShaderUniformBase::Ptr getUniform(
          A3M_CHAR8 const* id
          /**< ID of the uniform */) = 0;
    };

    /** Smart pointer type for this class */
    typedef SharedPtr< Appearance > Ptr;

    /** Default constructor.
     */
    Appearance() {}

    /**
     * Constructs an appearance from an appearance file.
     */
    Appearance( A3M_CHAR8 const* appearanceName,
                /**< Name of appearance file to read */
                Stream::Ptr const& stream,
                /**< Stream from which to load the appearance file */
                ShaderProgramCache& shaderProgramCache,
                /**< Cache with which to load shader programs */
                Texture2DCache& texture2DCache
                /**< Cache with which to load shader programs */);

    /** Set ShaderProgram.
     * Sets the ShaderProgram object used by this Appearance.
     */
    void setShaderProgram( ShaderProgram::Ptr const &shaderProgram
                           /**< [in] ShaderProgram */ );
    /** Get ShaderProgram.
     * \return the ShaderProgram object used by this Appearance.
     */
    ShaderProgram::Ptr const &getShaderProgram() const;

    /** Add ShaderUniform object.
     * Adds a ShaderUniform object to this object's list. Each ShaderUniform
     * will be enabled when this object is enabled.  It is essential that all
     * uniforms be added to the appearance before setting a shader program,
     * otherwise the shader applied to the appearance may not render correctly.
     * Warnings will be reported for each uniform which has not been added by
     * the time the shader is linked.
     */
    template< typename T >
    void addUniform(
        A3M_CHAR8 const *name,
        /**< Name of the uniform */
        A3M_CHAR8 const *id = 0,
        /**< Optional ID to allow automatic linking of the uniform to a
             specially defined parameter. */
        A3M_INT32 size = 1
        /**< Size of the uniform (should be greater than 1 for arrays) */)
    {
      ShaderUniformBase::Ptr uniform(new ShaderUniform<T>(size));
      m_uniforms[name] = uniform;

      if (id)
      {
        m_idToNameMap[id] = name;
      }

      // Mark uniform list as dirty for linker
      m_uniformsLinked = A3M_FALSE;
    }

    /** Passes data for each uniform in turn to a collector object.
     * This function can be used to acquire information about each of the
     * uniforms in the appearance via callbacks.
     * \note This function accesses internal information which is not
     * structured in a way that allows efficient lookup, so high performance
     * should not be expected.
     */
    void collectUniforms(
        AppearanceUniformCollector* collector
        /**< Collector to which the appearance will pass its uniforms */ ) const;

    /** Returns the name
     * \return ShaderUniform object with the matching name, or null pointer
     * if no uniform with a matching name was found.
     */
    ShaderUniformBase::Ptr getUniform( A3M_CHAR8 const *uniformName
                                       /**< [in] name of ShaderUniform to
                                                 return */ ) const;

    /** Set the value of a ShaderUniform
     * A convenience function to find the uniform with the given name and set
     * its value. If the uniform doesn't exist, function has no effect.
     * \return true if uniform value is set else false
     */
    template< typename T >
    A3M_BOOL setUniformValue( A3M_CHAR8 const *nameOrId,
                          /**< name or ID of uniform to find and set */
                          T const &value,
                          /**< new value for uniform */
                          A3M_INT32 i = 0
                          /**< Index into uniform array (should equal 0 for
                               non-arrays) */ )
    {
      ShaderUniformBase::Ptr ub = getUniform( nameOrId );
      if( ub )
      {
        return ub->setValue( value, i );
      }
      else
      {
        return A3M_FALSE;
      }
    }

    /** Links to an object defining the Linker interface.
     * This function acquires any unlinked uniforms from the linker that are
     * defined in the appearance, such that the Appearance shared uniforms with
     * the linker (and is hence "linked").
     */
    void link(Linker& linker /**< Linker to use */);

    /** Get PolygonMode.
     \return reference to the PolygonMode object used by this Appearance.
     */
    PolygonMode const &getPolygonMode() const;

    /** Set PolygonMode.
     * Set the PolygonMode object used by this Appearance.
     */
    void setPolygonMode( PolygonMode const &newPolygonMode
                         /**<[in] PolygonMode to be used */ );

    /** Get CompositingMode.
     \return reference to the CompositingMode object used by this Appearance.
     */
    CompositingMode const &getCompositingMode() const;

    /** Set CompositingMode.
     * Set the CompositingMode object used by this Appearance.
     */
    void setCompositingMode( CompositingMode const &newCompositingMode
                         /**<[in] CompositingMode to be used */ );

    /** Returns the Appearance's name.
     * \return name for this Appearance
     */
    A3M_CHAR8 const *getName() const { return m_name.c_str(); }

    /** Sets the Appearance's name.
     */
    void setName( A3M_CHAR8 const * name /**< New name for this Appearance */ )
    {
      m_name = name;
    }

  private:
    /*
     * Declaring a3m::RenderDevice::render() as friend of a3m::Appearance
     * class. This is to enable RenderDevice::render() to call the
     * Appearance::enable() methods.
     *
     */
    friend void RenderDevice::render( VertexBuffer &vb,
                                      IndexBuffer &ib,
                                      Appearance &app );
    /* Enable this Appearance.
     * Makes this Appearance current for future drawing operations.
     */
    void enable();

    /* Enable this Appearance.
     * Makes this Appearance current for future drawing operations. This
     * version minimises state changes by comparing appearance settings
     * with the supplied Appearance which reflects the current state.
     */
    void enable( Appearance &other /* [inout] Appearance object reflecting
                                      current render state */ );

    /* Enable shader uniforms which differ from those already set in the
     * shader program.
     */
    void applyUniformValues();

    /* Builds a list of uniforms parallel to those in the currently assigned
     * shader program.
     */
    void linkShaderProgram();

    typedef std::map<std::string, ShaderUniformBase::Ptr> ShaderUniformMap;
    typedef std::vector<ShaderUniformBase::Ptr> ShaderUniformVector;
    typedef std::map<std::string, std::string> StringMap;

    std::string            m_name;            /* Name of the appearance      */
    PolygonMode            m_polygonMode;     /* Culling mode                */
    CompositingMode        m_compositingMode; /* Compositing mode            */
    ShaderProgram::Ptr     m_shaderProgram;   /* Shared ShaderProgram object */
    ShaderUniformMap       m_uniforms;        /* Uniforms mapped by name     */
    ShaderUniformVector    m_programUniforms; /* Uniforms owned by program   */
    StringMap              m_idToNameMap;     /* Uniform names mapped by ID  */
    A3M_BOOL               m_uniformsLinked;  /* Have uniforms been linked?  */
    A3M_BOOL               m_uniformsSet;     /* Have uniforms been set?     */
  };
  /** @} */

} /* end of namespace */

#endif /* A3M_APPEARANCE_H */
