/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 **************************************************************************/
/** \file
 * Java chapters and document structure (Text only, no code).
 */

/*************************************************************************
 * USER MANUAL Top Level - JAVA Perspective
 *
 * What things does a Java user NEED to know?  The contents of internal
 * classes is not one of them.
 *************************************************************************/

/** \defgroup a3mJava A3M, a Java Users Guide.
    \ingroup a3m

What a user needs to know to write a 3D graphics application in
Java on Android, using the A3M Java interface.

A3M is a 'native' graphics engine library, i.e. it is written in C++ and
precompiled into a library rather than interpreted. It has a C++ interface but
that is wrapped in a "Java Native Interface" (JNI) which then presents a Java
API for application developers.  This section describes the use and function of
that interface. */

/* Subsections are the User Guide (just below) and the Java Reference
(defined in a3m.java) */


/** \defgroup a3mJusr User Guide
    \ingroup a3mJava

Here a [new] user is guided through the process of using A3M at the Java
API (just above JNI).

\todo Everything

Lorem ipsum dolor sit amet, a3mJusr adipiscing elit. Praesent dolor
enim, vestibulum sed adipiscing ut, sollicitudin a urna. Morbi sed leo
eget eros iaculis condimentum. Vestibulum ante ipsum primis in faucibus
orci luctus et ultrices posuere cubilia Curae; Maecenas et sapien purus, e
get porta turpis. Duis laoreet, sapien quis adipiscing egestas, felis
neque ornare quam, nec vulputate eros ipsum nec velit.

*/

/* END OF FILE */


