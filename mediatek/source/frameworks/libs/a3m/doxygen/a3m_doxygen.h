/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 **************************************************************************/
/** \file
 * Doxygen front page and top-level document structure (Text only, no code).
 */

/*************************************************************************
 * Opening Page
 * Explain what A3M is, and what it's for.
 *************************************************************************/

/** \mainpage Advanced 3D Middleware - A3M

\version Not yet released

\image html title_page_01.png "Advanced 3D Middleware"

A3M (Advanced 3D Middleware) is a compact and highly flexible 3D graphics engine
built on top of OpenGL ES 2.x.  It is specifically designed for use in embedded
environments.

A3M is an integral part of MAGE - Mediatek's Android Graphics Environment -
other parts of MAGE include the Android graphics system Ngin3D and the Digital
Content Creation Pipeline.

The DCC pipeline starts with the artist working on a tool such as 3dsMax, then
exporting the scene (auotomatically) through a series of optimisation tools
resulting in a file (in GLO format) suitable for an embedded environment.

A3M is not dedicated to Android and may be deployed on other OS's and platforms.
The android Java API overlays a native C++ API and A3M can be used in pure C++
environments.

A3M fully supports programmable-pipeline GPU graphics.


Link to \ref a3m

*/


/*************************************************************************
 * Top Level A3M
 *
 * Topmost group - must remain generic regardless of whether this is built
 * for Java API and/or C API, with or without Technical Manual
 *************************************************************************/

/** \defgroup a3m Contents

A3M is a modular graphics system with a number of different interfaces varying
according to application, platform, OS etc. This documentation is prepared
according to the modules and interfaces available to you.

The relevant modules are listed below:

*/


/* END OF FILE */


