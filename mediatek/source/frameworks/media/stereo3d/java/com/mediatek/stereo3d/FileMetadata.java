/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.stereo3d;

public class FileMetadata {
    private int mType = -1;
    private int mDisplay = -1;
    private int mLayout = -1;
    private int mHeightType = -1;
    private int mWidthType = -1;
    private int mFieldOrder = -1;

    /**
     * The method sets the image type according to the JPEG's header
     *
     * @param b the byte value
     * @hide
     */
    protected void setType(byte b) {
        if (b == (byte) 0x00) {
            mType = Stereo3DDisplay.MONOSCOPIC;
        } else if (b == (byte) 0x01) {
            mType = Stereo3DDisplay.STEREOSCOPIC;
        }
    }

    /**
     * The method sets the layout type according to the JPEG's header
     * Layout is only for stereoscopic
     *
     * @param b the byte value
     * @hide
     */
    protected void setLayout(byte b) {
        if (b == (byte) 0x01) {
            mLayout = Stereo3DDisplay.S3D_LAYOUT_INTERLEAVED;
        } else if (b == (byte) 0x02) {
            mLayout = Stereo3DDisplay.S3D_LAYOUT_SIDE_BY_SIDE;
        } else if (b == (byte) 0x03) {
            mLayout = Stereo3DDisplay.S3D_LAYOUT_TOP_AND_BOTTOM;
        } else if (b == (byte) 0x04) {
            mLayout = Stereo3DDisplay.S3D_LAYOUT_ANAGLYPH;
        }
    }

    /**
     * The method sets the display type according to the JPEG's header
     * Display is only for monoscopic
     *
     * @param b the byte value
     * @hide
     */
    protected void setDisplay(byte b) {
        if (b == (byte) 0x00) {
            mDisplay = Stereo3DDisplay.S3D_EYE_BOTH;
        } else if (b == (byte) 0x01) {
            mDisplay = Stereo3DDisplay.S3D_EYE_LEFT;
        } else if (b == (byte) 0x02) {
            mDisplay = Stereo3DDisplay.S3D_EYE_RIGHT;
        }
    }

    /**
     * The method sets the image dimension type according to the JPEG's header
     *
     * @param b the byte value
     * @hide
     */
    protected void setMiscFlags(byte b) {
        // height bit
        int bit = 1 & b >> 0;
        if (bit == 0) {
            mHeightType = Stereo3DDisplay.S3D_FULL_HEIGHT;
        } else {
            mHeightType = Stereo3DDisplay.S3D_HALF_HEIGHT;
        }

        // width bit
        bit = 1 & b >> 1;
        if (bit == 0) {
            mWidthType = Stereo3DDisplay.S3D_FULL_WIDTH;
        } else {
            mWidthType = Stereo3DDisplay.S3D_HALF_WIDTH;
        }

        // field order bit
        bit = 1 & b >> 2;
        if (bit == 0) {
            mFieldOrder = Stereo3DDisplay.S3D_RIGHT_FIELD_FIRST;
        } else {
            mFieldOrder = Stereo3DDisplay.S3D_LEFT_FIELD_FIRST;
        }
    }

    /**
     * The method retrieves the image type
     *
     * @return the image type
     * @hide
     */
    public int getType() {
        return mType;
    }

    /**
     * The method retrieves the image layout
     * Layout is only for stereoscopic
     *
     * @return the image layout
     * @hide
     */
    public int getLayout() {
        return mLayout;
    }

    /**
     * The method retrieves the display type of the image
     * Display is only for monoscopic
     *
     * @return the image display
     * @hide
     */
    public int getDisplay() {
        return mDisplay;
    }

    /**
     * The method retrieves the height type of the image
     *
     * @return the height type
     * @hide
     */
    public int getHeightType() {
        return mHeightType;
    }

    /**
     * The method retrieves the width type of the image
     *
     * @return the width type
     * @hide
     */
    public int getWidthType() {
        return mWidthType;
    }

    /**
     * The method retrieves the field order of the image
     *
     * @return the field order
     * @hide
     */
    public int getFieldOrder() {
        return mFieldOrder;
    }
}