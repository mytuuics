/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.stereo3d;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import com.mediatek.xlog.Xlog;

public class Stereo3DProcessInfo {
    private static final String TAG = "Stereo3DProcessInfo";
    private static final int SMALL_IMG_WIDTH = 40;
    private static final int SMALL_IMG_HEIGHT = 60;

    private final int mFormat;
    private final int mOutputImageWidth;
    private final int mOutputImageHeight;
    private int mLargeImageWidth;
    private int mLargeImageHeight;
    private int[] mLargeImage;  // rgba array
    private int[] mSmallImage;  // rgba array
    private int[] mOutputImage; // rgba array

    /**
    * The constructor of the Stereo3DProcessInfo
    *
    * @param format image format (rgba8888)
    * @param inputImage the 2D image to be converted into 3D image
    * @param outputImageWidth the expected width of the resulting image
    * @param outputImageHeight the expected height of the resulting image
    * @hide
    */
    public Stereo3DProcessInfo(int format, Bitmap inputImage, int outputImageWidth, int outputImageHeight) {
        mFormat = format;
        generateLargeImageColorArray(inputImage);
        generateSmallImageColorArray(inputImage);
        mOutputImageWidth = outputImageWidth;
        mOutputImageHeight = outputImageHeight;
    }

    /**
     * This method converts the image into RGBA array
     *
     * @param format image format (rgba8888)
     * @param bitmap the image to be converted into the pixel array
     * @param width the width of the image
     * @param height the height of the image
     * @param colorArray the array to store the pixel data in the bitmap
     */
    private void generateImageColorArray(Bitmap bitmap, int width, int height, int[] colorArray) {
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

        for (int i = 0; i < pixels.length; i++) {
            colorArray[i] = pixels[i];
        }

        Xlog.i(TAG, "colorArray size: " + colorArray.length);
    }

    /**
     * This method scales input image and produce a small image (W: 40 x H: 60) and then convert the
     * small image into RGBA array
     *
     * @param format image format (rgba8888)
     * @param inputImage the 2D image to be converted into small image pixel array
     */
    private void generateSmallImageColorArray(Bitmap inputImage) {
        // Scale input image and produce a small image (W: 40 x H: 60)
        Bitmap smallImage = scaleToExactImageSize(inputImage, SMALL_IMG_WIDTH, SMALL_IMG_HEIGHT);

        if (smallImage != null) {
            int smallImageWidth = smallImage.getWidth();
            int smallImageHeight = smallImage.getHeight();

            Xlog.i(TAG, "mSmallImageWidth: " + smallImageWidth);
            Xlog.i(TAG, "mSmallImageHeight: " + smallImageHeight);

            mSmallImage = allocateColorArray(smallImageWidth, smallImageHeight);
            generateImageColorArray(smallImage, smallImageWidth, smallImageHeight, mSmallImage);

            smallImage.recycle();
        }
    }

    /**
     * This method converts the large image into RGBA array
     *
     * @param format image format (rgba8888)
     * @param inputImage the 2D image to be converted into pixel array
     */
    private void generateLargeImageColorArray(Bitmap inputImage) {
        mLargeImageWidth = inputImage.getWidth();
        mLargeImageHeight = inputImage.getHeight();

        Xlog.i(TAG, "mLargeImageWidth: " + mLargeImageWidth);
        Xlog.i(TAG, "mLargeImageHeight: " + mLargeImageHeight);

        mLargeImage = allocateColorArray(mLargeImageWidth, mLargeImageHeight);
        generateImageColorArray(inputImage, mLargeImageWidth, mLargeImageHeight, mLargeImage);
    }

    /**
     * This method allocates the memory to store an image's pixel data
     *
     * @param format image format (rgba8888)
     * @param width the width of the image
     * @param height the height of the image
     * @return the allocated integer array
     */
    private int[] allocateColorArray(int width, int height) {
        int size = width * height; // rgba

        return new int[size];
    }

    /**
     * This method gets the format of the input image.
     *
     * @return the image format (rgba8888)
     * @hide
     */
    protected int getFormat() {
        return mFormat;
    }

    /**
    * This method gets the width of the input image.
    *
    * @return the image width
    * @hide
    */
    protected int getInputImageWidth() {
        return mLargeImageWidth;
    }

    /**
    * This method gets the height of the input image.
    *
    * @return the image height
    * @hide
    */
    protected int getInputImageHeight() {
        return mLargeImageHeight;
    }

    /**
    * This method gets the width of the output image.
    *
    * @return the image width
    * @hide
    */
    protected int getOutputImageWidth() {
        return mOutputImageWidth;
    }

    /**
    * This method gets the height of the output image.
    *
    * @return the image height
    * @hide
    */
    protected int getOutputImageHeight() {
        return mOutputImageHeight;
    }

    /**
    * This method gets the input large image in RGBA format.
    *
    * @return the color array
    * @hide
    */
    protected int[] getLargeImageColorArray() {
        return mLargeImage;
    }

    /**
    * This method gets the input small image in RGBA format.
    *
    * @return the color array
    * @hide
    */
    protected int[] getSmallImageColorArray() {
        return mSmallImage;
    }

    /**
    * This method gets the output image in RGBA format.
    *
    * @return the color array
    * @hide
    */
    protected int[] getOutputImageColorArray() {
        mOutputImage = allocateColorArray(mOutputImageWidth, mOutputImageHeight);

        return mOutputImage;
    }

    /**
    * This method gets the resulting output image
    *
    * @return the converted 3D image; side-by-side image
    * @hide
    */
    protected Bitmap getOutputImage() {
        Bitmap image = null;

        if (mOutputImage != null) {
            Xlog.i(TAG, "mOutputImageWidth: " + mOutputImageWidth);
            Xlog.i(TAG, "mOutputImageHeight: " + mOutputImageHeight);

            image = Bitmap.createBitmap(mOutputImage, mOutputImageWidth, mOutputImageHeight, Bitmap.Config.ARGB_8888);
        }

        return image;
    }

    /**
     * The method releases all arrays
     * @hide
     */
    protected void release() {
        mLargeImage = null;
        mSmallImage = null;
        mOutputImage = null;
    }

    /**
     * This method resizes the large image to the specified size
     *
     * @param oldBitmap the original bitmap
     * @param newWidth  the new width
     * @param newHeight the new height
     * @return the resized bitmap
     */
    private Bitmap scaleToExactImageSize(Bitmap oldBitmap, int newWidth, int newHeight) {
        int width = oldBitmap.getWidth();
        int height = oldBitmap.getHeight();

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(oldBitmap, 0, 0, width, height, matrix, true);
    }
}