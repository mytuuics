/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.stereo3d;

import android.graphics.Bitmap;

import com.mediatek.xlog.Xlog;

public class Stereo3DConversion {
    private static final String TAG = "Stereo3DConversion";

    public static final int TO3D_STATE_IDLE = 0;
    public static final int TO3D_STATE_STANDBY = 1;
    public static final int TO3D_STATE_INIT = 2;
    public static final int TO3D_STATE_PROC = 3;
    public static final int TO3D_STATE_READY = 4;

    // image format
    public static final int TO3D_IMAGE_FORMAT_RGBA8888 = 3;

    // scenarios
    public static final int TO3D_STILL_IMAGE_PLAYBACK = 0;
    public static final int TO3D_VIDEO_PLAYBACK = 1;

    // result code
    public static final int TO3D_OK = 0x00000000;
    public static final int TO3D_NEED_OVER_WRITE = 0x80000001;
    public static final int TO3D_NULL_OBJECT = 0x80000002;
    public static final int TO3D_WRONG_STATE = 0x80000003;
    public static final int TO3D_WRONG_CMD_ID = 0x80000004;
    public static final int TO3D_WRONG_CMD_PARAM = 0x80000005;
    public static final int TO3D_NOT_ENOUGH_MEM = 0x80000006;
    public static final int TO3D_INPUT_IMAGE_TOO_SMALL = 0x80000007;
    public static final int TO3D_ERR = 0x80000100;

    static {
        System.loadLibrary("to3d");
        System.loadLibrary("stereo3d");
    }

    private native int init3DConversion(int format, int scenario);

    private native int process3DConversion(int format, short largeImageWidth, short largeImageHeight,
                                           short outputImageWidth, short outputImageHeight, int[] largeImage, int[] smallImage,
                                           int[] outputImage);

    private native int close3DConversion();

    private native int get3DConversionState(int type);

    /**
     * This method converts 2D image into a side-by-side image used for 3D display.
     *
     * @param bitmap the 2D bitmap
     * @return the converted bitmap, i.e. side-by-side bitmap
     * @hide
     */
    public Bitmap perform2DTo3DConversion(Bitmap bitmap) {
        Xlog.i(TAG, "perform2DTo3DConversion");
        /* We divide output image height by 2 to keep the aspect ratio of the image.
         * For example, if input image is 160x120, we would like to get a left side image 80x60.
         * So the SBS output image will be 160x60*/
        Stereo3DProcessInfo processInfo = new Stereo3DProcessInfo(Stereo3DConversion.TO3D_IMAGE_FORMAT_RGBA8888,
                bitmap, bitmap.getWidth(), bitmap.getHeight() / 2);

        executeConversion(processInfo);

        Bitmap convertedBitmap = processInfo.getOutputImage();

        // finish conversion process, release Stereo3DProcessInfo resources
        processInfo.release();
        processInfo = null;

        return convertedBitmap;
    }

    /**
     * This method starts the process of 2D to 3D conversion.
     *
     * @param info the required information for the conversion.
     */
    private void executeConversion(Stereo3DProcessInfo info) {
        Xlog.i(TAG, "executeConversion");
        synchronized (Stereo3DConversion.class) {
            init3DConversion(Stereo3DConversion.TO3D_IMAGE_FORMAT_RGBA8888,
                             Stereo3DConversion.TO3D_STILL_IMAGE_PLAYBACK);
            process3DConversion(info);
            close3DConversion();
        }
    }

    /**
     * This method processes the 3D conversion.
     *
     * @param info the required information for the conversion.
     */
    private int process3DConversion(Stereo3DProcessInfo processInfo) {
        Xlog.i(TAG, "process3DConversion");
        int format = processInfo.getFormat();
        int[] largeImageColorArray = processInfo.getLargeImageColorArray();
        int[] smallImageColorArray = processInfo.getSmallImageColorArray();
        int[] OutputImageColorArray = processInfo.getOutputImageColorArray();
        short largeImageWidth = (short)processInfo.getInputImageWidth();
        short largeImageHeight = (short)processInfo.getInputImageHeight();
        short outputImageWidth = (short)(processInfo.getOutputImageWidth() / 2); // native requests left image width instead of the SBS width
        short outputImageHeight = (short)processInfo.getOutputImageHeight();

        return process3DConversion(format, largeImageWidth, largeImageHeight, outputImageWidth,
                                   outputImageHeight, largeImageColorArray, smallImageColorArray, OutputImageColorArray);
    }
}