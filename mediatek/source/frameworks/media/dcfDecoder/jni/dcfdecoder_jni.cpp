/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "DRM_DCF_DECODER_JNI"
#include <utils/Log.h>
#include <cutils/xlog.h>

#include <jni.h>

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>

#include <linux/serial.h>
#include <linux/fm.h>
#include <netinet/in.h>
#include <utils/Asset.h>
#include <utils/ResourceTypes.h>

#include "MediaHal.h"
#include "SkBitmap.h"
#include "GraphicsJNI.h"
#include "SkPixelRef.h"
#include "SkStream.h"
#include "SkImageDecoder.h"
#include "GraphicsJNI.h"

#include <drm_framework_common.h>
#include <DrmManagerClient.h>
#include <DrmMtkUtil.h>

using namespace android;

static const char *classPathName = "com/mediatek/dcfdecoder/DcfDecoder";
static jclass     dcfDecoder_class;
static jmethodID  dcfDecoder_constructorMethodID;
static jfieldID   dcfDecoder_nativeInstanceID;

jclass options_class;
jfieldID options_justBoundsFieldID;//
jfieldID options_sampleSizeFieldID;//
jfieldID options_preferSizeFieldID;//
jfieldID options_configFieldID;//
jfieldID options_ditherFieldID;//
jfieldID options_purgeableFieldID;//
jfieldID options_widthFieldID;//
jfieldID options_heightFieldID;//
jfieldID options_mimeFieldID;//
jfieldID options_mCancelID;//

static jclass make_globalref(JNIEnv* env, const char classname[]) {
    jclass c = env->FindClass(classname);
    SkASSERT(c);
    return (jclass)env->NewGlobalRef(c);
}

static jfieldID getFieldIDCheck(JNIEnv* env, jclass clazz,
        const char fieldname[], const char type[]) {
    jfieldID id = env->GetFieldID(clazz, fieldname, type);
    SkASSERT(id);
    return id;
}

jstring getMimeTypeString(JNIEnv* env, SkImageDecoder::Format format) {
    static const struct {
        SkImageDecoder::Format fFormat;
        const char*            fMimeType;
    } gMimeTypes[] = {
        { SkImageDecoder::kBMP_Format,  "image/bmp" },
        { SkImageDecoder::kGIF_Format,  "image/gif" },
        { SkImageDecoder::kICO_Format,  "image/x-ico" },
        { SkImageDecoder::kJPEG_Format, "image/jpeg" },
        { SkImageDecoder::kPNG_Format,  "image/png" },
        { SkImageDecoder::kWBMP_Format, "image/vnd.wap.wbmp" }
    };

    const char* cstr = NULL;
    for (size_t i = 0; i < SK_ARRAY_COUNT(gMimeTypes); i++) {
        if (gMimeTypes[i].fFormat == format) {
            cstr = gMimeTypes[i].fMimeType;
            break;
        }
    }

    jstring jstr = 0;
    if (NULL != cstr) {
        jstr = env->NewStringUTF(cstr);
    }
    return jstr;
}

static jobject decodeDcfSource(JNIEnv* env,jobject options,
                               DrmManagerClient *drmManagerClient,
                               sp<DecryptHandle> decryptHandle) {
    if (NULL == env) {
        XLOGE("decodeDcfSource:JNIEnv is NULL");
        return NULL;
    }
    if (NULL == drmManagerClient || decryptHandle == NULL) {
        XLOGE("decodeDcfSource:drmManagerClient or decryptHandle is NULL");
        return NULL;
    }
    int fileLength = decryptHandle->decryptInfo->decryptBufferLength;
    XLOGD("DcfDecoder:file length %d Bytes",fileLength);
    if (fileLength <= 0) {
        XLOGE("Find file length <= 0, return NULL");
        return NULL;
    }
    //allocate buffer to hold decrypted data
    unsigned char *decryptedBuf = (unsigned char*)malloc(fileLength);
    int readLength = drmManagerClient->pread(decryptHandle, decryptedBuf, fileLength, 0);
    XLOGD("DcfDecoder:pread read decrypted file length %d Bytes",readLength);
    if (readLength <= 0) {
        XLOGE("Read decrypted file length <= 0, return NULL");
        free(decryptedBuf);
        return NULL;
    }
    //as decrypted data are in buffer, wrap it into SkMemoryStream
    SkMemoryStream memStream(decryptedBuf, fileLength);

    int sampleSize = 1;
    int preferSize = 0;
    SkImageDecoder::Mode mode = SkImageDecoder::kDecodePixels_Mode;
    SkBitmap::Config prefConfig = SkBitmap::kNo_Config;
    bool doDither = true;
    bool isPurgeable = options != NULL && env->GetBooleanField(options, options_purgeableFieldID);

    if (NULL != options) {
        sampleSize = env->GetIntField(options, options_sampleSizeFieldID);
        preferSize = env->GetIntField(options, options_preferSizeFieldID);
        if (env->GetBooleanField(options, options_justBoundsFieldID)) {
            mode = SkImageDecoder::kDecodeBounds_Mode;
        }
        // initialize these, in case we fail later on
        env->SetIntField(options, options_widthFieldID, -1);
        env->SetIntField(options, options_heightFieldID, -1);
        env->SetObjectField(options, options_mimeFieldID, 0);

        jobject jconfig = env->GetObjectField(options, options_configFieldID);
        prefConfig = GraphicsJNI::getNativeBitmapConfig(env, jconfig);
        doDither = env->GetBooleanField(options, options_ditherFieldID);
    }

    SkImageDecoder* decoder = SkImageDecoder::Factory(&memStream);
    if (NULL == decoder) {
        XLOGE("SkImageDecoder-Factory() returned false");
        return NULL;
    }

    // To fix the race condition in case "requestCancelDecode"
    // happens earlier than AutoDecoderCancel object is added
    // to the gAutoDecoderCancelMutex linked list.
    if (NULL != options && env->GetBooleanField(options, options_mCancelID)) {
        XLOGE("Decoding is cancelled by requestCancelDecode");
        return NULL;
    }

    SkImageDecoder::Mode decodeMode = mode;
    if (isPurgeable) {
        decodeMode = SkImageDecoder::kDecodeBounds_Mode;
    }

    SkBitmap* bitmap = new SkBitmap;

    decoder->setSampleSize(sampleSize);
    decoder->setDitherImage(doDither);
    decoder->setPreferSize(preferSize);

    if (!decoder->decode(&memStream, bitmap, prefConfig, decodeMode)) {
        XLOGE("SkImageDecoder-decode() returned false");
        //free memory buffer
        free(decryptedBuf);
        return NULL;
    }

    //free memory buffer
    free(decryptedBuf);

    // update options (if any)
    if (NULL != options) {
        env->SetIntField(options, options_widthFieldID, bitmap->width());
        env->SetIntField(options, options_heightFieldID, bitmap->height());
        // TODO: set the mimeType field with the data from the codec.
        // but how to reuse a set of strings, rather than allocating new one
        // each time?
        env->SetObjectField(options, options_mimeFieldID,
                            getMimeTypeString(env, decoder->getFormat()));
    }

    // if we're in justBounds mode, return now (skip the java bitmap)
    if (SkImageDecoder::kDecodeBounds_Mode == mode) {
        return NULL;
    }

    if (bitmap != NULL) {
        XLOGV("createdBitmap is not NULL, wrap it into a Java Bitmap");
        return GraphicsJNI::createBitmap(env, bitmap, false, NULL);
    }
    else {
        XLOGW("createdBitamp is NULL");
        return NULL;
    }
}

static jobject nativeDecodeFile(JNIEnv* env, jobject clazz,
                                          jstring filePath,
                                          jobject options,
                                          jboolean consume) {
    if (NULL == env || NULL == filePath) {
        XLOGE("decodeDcfSource:JNIEnv or filePath is NULL");
        return NULL;
    }
    //get file name
    const char* pathname = env->GetStringUTFChars(filePath, NULL);
    XLOGI("DcfDecoder:decode file %s", pathname);
    if (NULL == pathname) {
        XLOGE("Can not get file name, return NULL");
        return NULL;
    }
//!!!!!! whether pathname needs to be freed when returned ?!!!!!!!!

    //get file description
    int fd = open(pathname,O_RDONLY);
    //after file is opened, release string resources.
//    (env)->RelaseStringUTFChars(&env,filePath,pathname);
    if (0 == fd) {
        XLOGE("Can not open fd of file %s, return NULL",pathname);
        return NULL;
    }
    XLOGV("DcfDecoder:open decryptSession...");
    //open decryptSession...
    //offset & length is not used currently
    int offset = 0;
    int length = 0;
    DrmManagerClient *drmManagerClient = new DrmManagerClient();
    if (NULL == drmManagerClient) {
        XLOGE("Can not create DrmManagerClient, return NULL");
        close(fd);
        return NULL;
    }
    sp<DecryptHandle> decryptHandle = drmManagerClient->openDecryptSession(
                                            fd, offset, length);
    XLOGV("DcfDecoder:decryptHandle=%d",decryptHandle.get());
    if (decryptHandle == NULL) {
        XLOGE("Can not openDecryptSession, return NULL");
        delete drmManagerClient;
        close(fd);
        return NULL;
    }
    jobject bmp = decodeDcfSource(env,options,drmManagerClient,decryptHandle);
    if(bmp != NULL && consume == JNI_TRUE ) { // consume rights
        drmManagerClient->consumeRights(decryptHandle, Action::DISPLAY, false);
        XLOGD("nativeDecodeDcfFile, consume rights");
    }
    drmManagerClient->closeDecryptSession(decryptHandle);
    delete  drmManagerClient;
    close(fd);
    return bmp;
}

static jobject nativeForceDecodeFile(JNIEnv* env, jobject clazz,
                                          jstring filePath,
                                          jobject options,
                                          jboolean consume) {
    if (NULL == env || NULL == filePath) {
        XLOGE("DcfDecoder:JNIEnv or filePath is NULL");
        return NULL;
    }
    //get file name
    const char* pathname = env->GetStringUTFChars(filePath, NULL);
    XLOGI("DcfDecoder:decode file %s", pathname);
    if (NULL == pathname) {
        XLOGE("Can not get file name, return NULL");
        return NULL;
    }
//!!!!!! whether pathname needs to be freed when returned ?!!!!!!!!

    //get file description
    int fd = open(pathname,O_RDONLY);
    //after file is opened, release string resources.
//    (env)->RelaseStringUTFChars(&env,filePath,pathname);
    if (0 == fd) {
        XLOGE("Can not open fd of file %s, return NULL",pathname);
        return NULL;
    }
    XLOGV("DcfDecoder:open decryptSession...");
    //open decryptSession...
    //offset & length is not used currently
    int offset = 0;
    int length = 0;
    DrmManagerClient *drmManagerClient = new DrmManagerClient();
    if (NULL == drmManagerClient) {
        XLOGE("Can not create DrmManagerClient, return NULL");
        close(fd);
        return NULL;
    }
    sp<DecryptHandle> decryptHandle = drmManagerClient->openDecryptSession(
                                            fd, offset, length);
    XLOGV("DcfDecoder:decryptHandle=%d",decryptHandle.get());
    if (decryptHandle == NULL) {
        XLOGE("Can not openDecryptSession, return NULL");
        delete drmManagerClient;
        close(fd);
        return NULL;
    }
    jobject bmp = decodeDcfSource(env,options,drmManagerClient,decryptHandle);
    if(bmp != NULL && consume == JNI_TRUE ) { // consume rights
        drmManagerClient->consumeRights(decryptHandle, Action::DISPLAY, false);
        XLOGD("nativeForceDecodeDcfFile, consume rights");
    }
    drmManagerClient->closeDecryptSession(decryptHandle);
    delete  drmManagerClient;
    close(fd);
    return bmp;
}

////////////////////////////////////////////////////////////////////////////////
//JNI register
////////////////////////////////////////////////////////////////////////////////

static JNINativeMethod methods[] = {
    { "nativeDecodeFile",
        "(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;",
        (void*)nativeDecodeFile },
    { "nativeForceDecodeFile",
        "(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;",
        (void*)nativeForceDecodeFile }
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;
    clazz = env->FindClass(className);
    if (clazz == NULL) {
        XLOGE("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        XLOGE("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }
    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
int registerDcfNatives(JNIEnv* env)
{
    if (!registerNativeMethods(env, classPathName, methods, sizeof(methods) / sizeof(methods[0]))) {
        return JNI_FALSE;
    }

    options_class = make_globalref(env, "android/graphics/BitmapFactory$Options");
    options_justBoundsFieldID = getFieldIDCheck(env, options_class, "inJustDecodeBounds", "Z");
    options_sampleSizeFieldID = getFieldIDCheck(env, options_class, "inSampleSize", "I");
    options_preferSizeFieldID = getFieldIDCheck(env, options_class, "inPreferSize", "I");
    options_configFieldID = getFieldIDCheck(env, options_class, "inPreferredConfig", "Landroid/graphics/Bitmap$Config;");
    options_ditherFieldID = getFieldIDCheck(env, options_class, "inDither", "Z");
    options_purgeableFieldID = getFieldIDCheck(env, options_class, "inPurgeable", "Z");
    options_widthFieldID = getFieldIDCheck(env, options_class, "outWidth", "I");
    options_heightFieldID = getFieldIDCheck(env, options_class, "outHeight", "I");
    options_mimeFieldID = getFieldIDCheck(env, options_class, "outMimeType", "Ljava/lang/String;");
    options_mCancelID = getFieldIDCheck(env, options_class, "mCancel", "Z");

    return JNI_TRUE;
}

// ----------------------------------------------------------------------------

int register_mediatek_DcfDecoder(JNIEnv* env)
{
    return registerDcfNatives(env);
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env = NULL;
    jint result = -1;

    if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
        XLOGE("ERROR: GetEnv failed\n");
        return result;
    }
    assert(env != NULL);

    if (register_mediatek_DcfDecoder(env) < 0) {
        XLOGE("ERROR: register_mediatek_DcfDecoder failed\n");
        return result;
    }
    result = JNI_VERSION_1_4;
    return result;
}
