# Copyright 2007-2008 The Android Open Source Project

ifneq (,$(filter CuHotLine,$(OPTR_APK)))

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := user


LOCAL_SRC_FILES := \
 	$(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := CuHotLine
LOCAL_CERTIFICATE := shared

include $(BUILD_PACKAGE)
endif
