# barrier for searching Android.mk
# Android.mk in out will be enumrated in mediatek/build/android



ifneq (,$(filter libminimapv320, $(OPTR_APK)))
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# your prebuilt file name
LOCAL_MODULE := libminimapv320.so
LOCAL_MODULE_TAGS := user
LOCAL_MODULE_CLASS := SHARED_LIBARARY
# your prebuilt file (must be relative directory )
LOCAL_SRC_FILES := $(LOCAL_MODULE)
# the path your prebuilt file will be installed   $(TARGET_OUT) is the system directory
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib
LOCAL_CERTIFICATE := platform
include $(BUILD_PREBUILT)

endif

