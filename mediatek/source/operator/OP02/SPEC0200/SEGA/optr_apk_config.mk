# we will  include optr_apk_config.mk into corresponding operator.mk
OPTR_APK := bootanim \
            Amap \
            libminimapv320 \
            libamapmapgv10 \
            libAisound \
            libtbt \
            BaiduSearch \
            libAudiofe_V1 \
            liblocSDK_2.2 \
            Dazhihui \
            Kaixin \
            QQBrowser \
            QQGame \
            QQIM \
            libmsfboot \
    				libcodecwrapper \
    				libaudiohelper \
    				libVideoCtrl \
    				libCommon \
    				liblbs \
    				libsnapcore \
    				libqqsecure \
            SogouInput \
            libhanvonhw_v30 \
            libsogouime_jni_v30 \
            libspeex_sogou_v30 \
            libthemeextractor_v30 \
            libkximager-jni \
            dmconfig \
            Wo3G \
            CuHotLine \
            APKInstaller \
            
