REWRITE_CIG_PY := $(MTK_ROOT_SOURCE_OPERATOR)/reconfigOptr.py
OPTR_MF := $(MTK_ROOT_SOURCE_OPERATOR)/OperatorConfig.mk
ifneq ($(OPTR_SPEC_SEG_DEF),NONE)
   OPTR_LIST := $(subst _, ,$(OPTR_SPEC_SEG_DEF))
   ifneq ($(words $(OPTR_LIST)),3)
      $(error OPTR_SPEC_SEG_DEF setting format is OPTR_SPEC_SEG_DEF = optr_spec_seg, but now setting is $(OPTR_SPEC_SEG_DEF))
   endif
   OPTR := $(word 1,$(OPTR_LIST))
   SPEC := $(word 2,$(OPTR_LIST))
   SEG  := $(word 3,$(OPTR_LIST))
   $(shell $(REWRITE_CIG_PY) --optr=$(OPTR) --spec=$(SPEC) --seg=$(SEG)) 
   include $(OPTR_MF)
   # in operator.mak, record the check rule or feature dependency 
   include $(MTK_ROOT_SOURCE_OPERATOR)/$(OPTR)/$(SPEC)/$(SEG)/operator.mak
else
   # all complier option in OperatorConfig.mk need to be set no,so check the correctness   
   $(shell $(REWRITE_CIG_PY))
   include $(OPTR_MF)
endif
