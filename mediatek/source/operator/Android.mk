ifdef OPTR_SPEC_SEG_DEF
   ifneq ($(OPTR_SPEC_SEG_DEF),NONE)
       OPTR     := $(word 1,$(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
       SPEC     := $(word 2,$(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
       SEG      := $(word 3,$(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
       OPTR_DIR := $(MTK_ROOT_SOURCE_OPERATOR)/$(OPTR)
       -include $(MTK_ROOT_SOURCE_OPERATOR)/$(OPTR)/$(SPEC)/$(SEG)/optr_apk_config.mk 
       -include $(call first-makefiles-under,$(OPTR_DIR))
   endif
endif
