#! /usr/bin/env python
import os,sys,re,getopt

def Usage():
    print """
Usage: mediatek/source/operator/reconfigOptr.py [option] ...
Options:
       -h,--help            print the help information and exit.
       --optr=OPERATOR      whatever the OPERATOR set, reset it to yes.
       --spec=SPEC          whatever the SPEC set, reset it to yes.
       --seg=SEGMENT        whatever the SEGMENT set, reset it to yes.
Attention:
       run this script under alps foler
    """
# parse the arguments
try:
    opts,args = getopt.getopt(sys.argv[1:],"h",["help","optr=","spec=","seg="])
except getopt.GetoptError:
    Usage()
    sys.exit(1)
if len(args) != 0:
    Usage()
    sys.exit(1)
for o,a in opts:
    if o in ("-h","--help"):
        Usage()
        sys.exit()
    elif o == "--optr":
        optr = a
    elif o == "--spec":
        spec = a
    elif o == "--seg":
        seg = a 

# re-config the OperatorConfig.mk
if 'optr' not in dir():
    optr = ""
if 'spec' not in dir():
    spec = ""
if 'seg' not in dir():
    seg = ""
patternOptr = "(%s)\s*=\s*\S+" % optr
patternSpec = "(%s)\s*=\s*\S+" % spec
patternSeg = "(%s)\s*=\s*\S+" % seg
patternGeneric = re.compile("(\S+)\s*=\s*\S+")
pattern = [ re.compile(patternOptr),re.compile(patternSpec),re.compile(patternSeg) ]
optrConfig = "mediatek/source/operator/OperatorConfig.mk"
os.system("chmod u+wr %s" % optrConfig)
fileRead = open(optrConfig,"r")
fileReadList = fileRead.readlines()
fileRead.close()
fileWriteList = []
for eachline in fileReadList:
    flag = True 
    if eachline.startswith("#"):
        fileWriteList.append(eachline)
        continue
    for p in pattern:
        match = p.match(eachline)
        if match:
            flag = False
            break
    if match:
        writeline = "%s = yes\n" % match.group(1)
        fileWriteList.append(writeline)
    elif flag and patternGeneric.match(eachline):
        writeline = "%s = no\n" % patternGeneric.match(eachline).group(1)
        fileWriteList.append(writeline)
    else:
        fileWriteList.append(eachline)
fileWrite = open(optrConfig,"w")
fileWrite.writelines(fileWriteList)
fileWrite.close()

    
   
    
