/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.syslogger;

import java.io.File;
import java.io.IOException;

import com.mediatek.engineermode.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.NumberKeyListener;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import android.os.SystemProperties;

public class TagLogService extends Service {

    private static final String TAG = "Syslog_taglog";

    private String tag = "";

    private boolean[] mLogToolStatus; // Status of MDLog/MobileLog/NetLog
    private String[] mLogToolPath; // Path of MDLog/MobileLog/NetLog
    private boolean mIsStartService;
    private String mLogPathInSDCard; // "/mnt/sdcard" or "/mnt/sdcard2"
    private String mExpDbPathInSDCard; // ex: "/mnt/sdcard/mtklog/aee_exp/db.00"

    private MyHandler mMyHandler;
    private SubHandler mSubHandler;
    
    private ProgressDialog mProgressDialog;
    private boolean mInClickProcess;
    
    private boolean mIsNormalExcep;

    private void init() {
        mIsStartService = false;
        mMyHandler = new MyHandler();
        new FuctionThread().start();
    }

    private void deInit() {
//        startOrStopAllLogTool(true);
        
//        if (!mIsNormalExcep) {
//            clearRedScreen();
//        }
        
        mIsStartService = false;
        
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        
        if (null != mMyHandler) {
            mMyHandler = null;
        }
        if (null != mSubHandler) {
            mSubHandler.getLooper().quit();
            mSubHandler = null;
        }
        
        mLogToolStatus = null;
        mLogToolPath = null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        Log.d(TAG, "TagLogService-->onCreate");
        init();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        Log.d(TAG, "TagLogService-->onStartCommand");
        if (null == intent) {
            Log.e(TAG, "Intent is null!!");
            return super.onStartCommand(intent, flags, startId);
        }
        
        if (mIsStartService) {
            Log.e(TAG, "The service is runing!");
            return super.onStartCommand(intent, flags, startId);
        } else {
            mIsStartService = true;
        }

        boolean[] logToolStatus = getLogToolStatus();
        if (!logToolStatus[0] && !logToolStatus[1] && !logToolStatus[2]) {
            mMyHandler.sendEmptyMessage(SysUtils.EVENT_UI_ALL_LOGTOOL_STOPED);
            return super.onStartCommand(intent, flags, startId);
        }
        
        mLogToolStatus = getLogToolStatus(); 
        mLogToolPath = getLogPath();

        Message msg = new Message();
        // msg.what = SysUtils.EVENT_FUNCTION_GET_EXCEPTION_TYPE;
        msg.what = SysUtils.EVENT_UI_CREATE_INPUTDIALOG;
        msg.setData(intent.getExtras());
        // mSubHandler.sendMessage(msg);
        mMyHandler.sendMessage(msg);
        mIsStartService = true;

        Log.d(TAG, "TagLogService-->onStartCommand End");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Log.d(TAG, "TagLogService-->onDestroy");
        deInit();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        Log.d(TAG, "TagLogService-->onBind");
        return null;
    }

    
    // --------------- -------- methods -------- ---------------

    private class MyHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            
            Log.d(TAG, " MyHandler handleMessage --> start " + msg.what);
            switch (msg.what) {
            case SysUtils.EVENT_UI_CREATE_INPUTDIALOG:               
                Bundle bundle = msg.getData();
                String exp_path = bundle.getString(SysUtils.EXTRA_KEY_EXP_PATH);
                String exp_file_name = bundle.getString(SysUtils.EXTRA_KEY_EXP_NAME);
                String zz_file_name = bundle.getString(SysUtils.EXTRA_KEY_EXP_ZZ);
                if (exp_path == null || exp_file_name == null || zz_file_name == null) {
                    Log.d(TAG, "params are not valid!");
                    TagLogService.this.stopSelf();
                    return;
                }
                mExpDbPathInSDCard = exp_path;
                String zz_file = exp_path + zz_file_name;
                ExceptionInfo exp_info = new ExceptionInfo();
                String exp_file = exp_path + exp_file_name;
                Log.e(TAG, "exp_path: " + exp_path);
                Log.e(TAG, "exp_file: " + exp_file);
                exp_info.setmPath(exp_file);
                try {
                    exp_info.initFieldsFromZZ(zz_file);
                } catch (IOException e) {
                    Log.e(TAG, "fail to init exception info:" + e.getMessage());
                    TagLogService.this.stopSelf();
                    return;
                }
                Log.i(TAG, "exp_info.getmType(): " + exp_info.getmType());
                Log.i(TAG, "exp_info.getmDiscription(): " + exp_info.getmDiscription());
                Log.i(TAG, "exp_info.getmProcess(): " + exp_info.getmProcess());
                
                // -- for change 1
//                if (exp_info.getmProcess().endsWith(SysUtils.EXCP_TAGLOG_PROCESS)) {
//                    mIsNormalExcep = false;
//                    createDialog(SysUtils.DIALOG_INPUT);
//                } else {
//                    mIsNormalExcep = true;
//                    TagLogService.this.stopSelf();
//                }
                
                createDialog(SysUtils.DIALOG_INPUT);
                // playTipSound(TagLogService.this, mWarningSoundUri);
                break;

            case SysUtils.SD_LOCK_OF_SPACE:
                createDialog(SysUtils.DIALOG_LOCK_OF_SDSPACE);
                break;
            case SysUtils.SD_NOT_EXIST:
                dismissProgressDialog();
                createDialog(SysUtils.SD_NOT_EXIST);
                break;
            case SysUtils.SD_NOT_WRITABLE:
                dismissProgressDialog();
                createDialog(SysUtils.SD_NOT_WRITABLE);
                break;
            case SysUtils.EVENT_UI_ALL_LOGTOOL_STOPED:
                createDialog(SysUtils.DIALOG_ALL_LOGTOOL_STOPED);
                break;
            case SysUtils.EVENT_UI_ZIP_LOG_SUCCESS:
                dismissProgressDialog();
                TagLogService.this.stopSelf();
                break;
            case SysUtils.EVENT_UI_ZIP_LOG_FAIL:
                dismissProgressDialog();
                createDialog(SysUtils.DIALOG_ZIP_LOG_FAIL);
                break;
            case SysUtils.DIALOG_START_PROGRESS:
                createProgressDialog();
                break;
            case SysUtils.DIALOG_END_PROGRESS:
                dismissProgressDialog();
                break;
            }
            Log.d(TAG, " MyHandler handleMessage --> end " + msg.what);
        }
    }

    private class SubHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            Log.d(TAG, " SubHandler handleMessage --> start " + msg.what);           
            switch (msg.what) {
            case SysUtils.EVENT_FUNCTION_GET_EXCEPTION_TYPE:
                Bundle bundle = msg.getData();
                String exp_path = bundle.getString(SysUtils.EXTRA_KEY_EXP_PATH);
                String exp_file_name = bundle.getString(SysUtils.EXTRA_KEY_EXP_NAME);
                String zz_file_name = bundle.getString(SysUtils.EXTRA_KEY_EXP_ZZ);
                if (exp_path == null || exp_file_name == null || zz_file_name == null) {
                    Log.d(TAG, "params are not valid!");
                    return;
                }
                
                String zz_file = exp_path + zz_file_name;
                ExceptionInfo exp_info = new ExceptionInfo();
                String exp_file = exp_path + exp_file_name;
                Log.e(TAG, "exp_path: " + exp_path);
                Log.e(TAG, "exp_file: " + exp_file);
                exp_info.setmPath(exp_file);
                try {
                    exp_info.initFieldsFromZZ(zz_file);
                } catch (IOException e) {
                    Log.e(TAG, "fail to init exception info:" + e.getMessage());
                    return;
                }
                if(null != exp_info.getmType())
                Log.i(TAG, "exp_info.getmType(): " + exp_info.getmType());
                if(null != exp_info.getmDiscription())
                Log.i(TAG, "exp_info.getmDiscription(): " + exp_info.getmDiscription());
                if(null != exp_info.getmProcess())
                Log.i(TAG, "exp_info.getmProcess(): " + exp_info.getmProcess());
                
                if (null != exp_info.getmProcess() && exp_info.getmProcess().endsWith(
                                SysUtils.EXCP_TAGLOG_PROCESS)) {
                    mMyHandler.sendEmptyMessage(SysUtils.EVENT_UI_CREATE_INPUTDIALOG);
                }             
                break;
            case SysUtils.EVENT_FUNCTION_ZIP_ALL_LOG:
                int SDCardSpaceStatus = SDCardUtils.checkSdCardSpace(mLogPathInSDCard, mLogToolPath);
                if (SDCardSpaceStatus != SysUtils.SD_NORMAL) {
                    mMyHandler.sendEmptyMessage(SDCardSpaceStatus);
                    return;
                }
                startOrStopAllLogTool(false); // stop all log tool.

                String tagLogFolderName = SDCardUtils.createTagLogFolder(
                        mLogPathInSDCard, tag);

                boolean isZipSuccess = zipAllLogAndDelete(tagLogFolderName);

                SDCardUtils.writeFolderToTagFolder(mExpDbPathInSDCard,
                        tagLogFolderName);

                startOrStopAllLogTool(true); // start all log tool
                if (isZipSuccess) {
                    mMyHandler.sendEmptyMessage(SysUtils.EVENT_UI_ZIP_LOG_SUCCESS);
                } else {
                    mMyHandler.sendEmptyMessage(SysUtils.EVENT_UI_ZIP_LOG_FAIL);
                }
                break;
            }
            Log.d(TAG, " SubHandler handleMessage --> end " + msg.what);
        }
    }
    
    
    class FuctionThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            super.run();
            Looper.prepare();
            mSubHandler = new SubHandler();
            Looper.loop();
        }       
    } 
    /**
     * 
     * @return Status of MDLog/MobileLog/NetLog
     */
    
    private boolean[] getLogToolStatus() {
        Log.i(TAG, "Check log tool status");
        boolean toolStatus[] = new boolean[3];
        String usermode = SystemProperties.get(SysUtils.PROP_MD);
        if (null != usermode && usermode.equalsIgnoreCase(SysUtils.PROP_ON)) {
            Log.i(TAG, "getLogToolStatus: ModemLog is running");
            toolStatus[0] = true;
        }
        usermode = SystemProperties.get(SysUtils.PROP_MOBILE);
        if (null != usermode && usermode.equalsIgnoreCase(SysUtils.PROP_ON)) {
            Log.i(TAG, "getLogToolStatus: MobileLog is running");
            toolStatus[1] = true;
        }

        usermode = SystemProperties.get(SysUtils.PROP_NETWORK);
        if (null != usermode && usermode.equalsIgnoreCase(SysUtils.PROP_ON)) {
            Log.i(TAG, "getLogToolStatus: NetLog is running");
            toolStatus[2] = true;
        }

//        usermode = SystemProperties.get(SysUtils.PROP_EXTMD);
//        if (null != usermode && usermode.equalsIgnoreCase(SysUtils.PROP_ON)) {
//            Log.i(TAG, "getLogToolStatus: ExtModemLog is running");
//            toolStatus[3] = true;
//        }
        return toolStatus;
    }
    
    /**
     * 
     * @return Path of MDLog/MobileLog/NetLog
     */
    private String[] getLogPath() {
        String[] logToolPath = new String[3];
        String usermode = SystemProperties.get(SysUtils.PER_LOG2SD);
        if (usermode.equals(SysUtils.EXTERNALSDPATH)) {
            mLogPathInSDCard = SysUtils.EXTERNALSDPATH;
            logToolPath[0] = SysUtils.EXTERNALSDPATH + File.separator
                    + SysUtils.MODEM_LOG_FOLDER;
            logToolPath[1] = SysUtils.EXTERNALSDPATH + File.separator
                    + SysUtils.MOBILE_LOG_FOLDER;
            logToolPath[2] = SysUtils.EXTERNALSDPATH + File.separator
                    + SysUtils.NET_LOG_FOLDER;
        } else {
            File sdCardFile = Environment.getExternalStorageDirectory();
            String sdCardState = Environment.getExternalStorageState();
            if (!Environment.MEDIA_MOUNTED.equals(sdCardState)
                    && !Environment.MEDIA_SHARED.equals(sdCardState)) {
                Log.e(TAG, "SD card not ready, current state=" + sdCardState);
                return null;
            }
            mLogPathInSDCard = sdCardFile.getAbsolutePath();
            logToolPath[0] = sdCardFile.getAbsolutePath() + File.separator
                    + SysUtils.MODEM_LOG_FOLDER;
            logToolPath[1] = sdCardFile.getAbsolutePath() + File.separator
                    + SysUtils.MOBILE_LOG_FOLDER;
            logToolPath[2] = sdCardFile.getAbsolutePath() + File.separator
                    + SysUtils.NET_LOG_FOLDER;
        }
        return logToolPath;
    }


    /**
     * 
     * @param isStart: true? start log tool:stop log tool
     * @param index: which log tool
     */
    private void startOrStopLogTool(boolean isStart, int index) {
        Intent intent = new Intent(SysUtils.BROADCAST_ACTION);
        intent.putExtra(SysUtils.BROADCAST_KEY_SELFTEST, SysUtils.BROADCAST_VAL_SELFTEST);
        intent.putExtra(SysUtils.BROADCAST_KEY_SRC_FROM, SysUtils.BROADCAST_VAL_SRC_HQ);
        
        if (isStart) {
            intent.putExtra(SysUtils.BROADCAST_KEY_COMMAND, SysUtils.BROADCAST_VAL_COMMAND_START);
        } else {
            intent.putExtra(SysUtils.BROADCAST_KEY_COMMAND, SysUtils.BROADCAST_VAL_COMMAND_STOP); 
        }
               
        switch (index) {
        case 0: // modem log
            intent.putExtra(SysUtils.BROADCAST_KEY_SRC_TO, SysUtils.BROADCAST_VAL_SRC_MD);  
            break;
        case 1: // mobile log
            intent.putExtra(SysUtils.BROADCAST_KEY_SRC_TO, SysUtils.BROADCAST_VAL_SRC_MOBILE);
            break;
        case 2: // net log
            intent.putExtra(SysUtils.BROADCAST_KEY_SRC_TO, SysUtils.BROADCAST_VAL_SRC_NETWORK);
            break;
        default:
            break;
        }        
        sendBroadcast(intent);              
    }
    
    private void startOrStopAllLogTool(boolean isStart) {
        if(!isStart){
            mLogToolStatus = getLogToolStatus();   
        }
        for (int i = 0; i < mLogToolStatus.length; i++) {
            if (mLogToolStatus[i]) {
                startOrStopLogTool(isStart,i);
            }
        }
//        if (isStart) { 
//            for (boolean temp : mLogToolStatus) {
//                temp = false;
//            }
//        }
    }
    
    private void startAllLogTool() {
        for (int i = 0; i < 3; i++) {
            startOrStopLogTool(true, i);
        }
    }
    
    private void createProgressDialog() {
        if (null == mProgressDialog) {
            mProgressDialog = new ProgressDialog(TagLogService.this);
            if (null != mProgressDialog) {
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                String text = "Compressing log files...";
                mProgressDialog.setMessage(text);
                mProgressDialog.setTitle("[Tag Log]");
                mProgressDialog.setCancelable(false);
                Window win = mProgressDialog.getWindow();
                win.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                mProgressDialog.show();
            } else {
                Log.e(TAG, "new mProgressDialog failed");
            }
        }
        mInClickProcess = true;
    }

    private void dismissProgressDialog() {
        if (null != mProgressDialog) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        mInClickProcess = false;
    }

    private void createDialog(int id) {
        Builder builder = new AlertDialog.Builder(TagLogService.this);
        switch (id) {
            case SysUtils.DIALOG_INPUT:  // input tag
            final EditText inputText = new EditText(TagLogService.this);
            inputText.setKeyListener(new NumberKeyListener() {

                public int getInputType() {
                    return InputType.TYPE_TEXT_FLAG_CAP_WORDS;
                }

                @Override
                protected char[] getAcceptedChars() {
                    char[] numberChars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                            'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0',
                            '1', '2', '3', '4', '5', '6', '7', '8', '9', '_',
                            ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                            'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                    return numberChars;
                }
            }); 
          builder.setTitle(SysUtils.TITLE_INPUT_TAG)
          .setView(inputText).setPositiveButton(android.R.string.ok,
                  new OnClickListener() {
                      public void onClick(DialogInterface dialog, int which) {
                          // TODO Auto-generated method stub
                          tag = inputText.getText().toString().trim();
                          if (tag.equals("")) {
                              tag = "taglog";
                          }
                          Log.i(TAG, "Input tag: " + tag);
                          mMyHandler.sendEmptyMessage(SysUtils.DIALOG_START_PROGRESS);
                          mSubHandler.sendEmptyMessage(SysUtils.EVENT_FUNCTION_ZIP_ALL_LOG);;
                      }
                  }).setNegativeButton(android.R.string.cancel,
                  new OnClickListener() {

                      public void onClick(DialogInterface dialog,
                              int which) {
                          // TODO Auto-generated method stub
                          dialog.dismiss();
                          TagLogService.this.stopSelf();
                      }
                  });
          
            break;
        case SysUtils.DIALOG_ALL_LOGTOOL_STOPED: // all log tool stoped
            builder.setTitle(SysUtils.TITLE_ALL_LOGTOOL_STOPED)
            .setMessage(SysUtils.MSG_ALL_LOGTOOL_STOPED)
            .setPositiveButton(android.R.string.ok,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // TODO Auto-generated method stub
                            startAllLogTool();
                            //startActivity(new Intent(TagLogService.this,SysCommon.class));
                            TagLogService.this.stopSelf();
                        }
                    }).setNegativeButton(android.R.string.cancel,
                    new OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            TagLogService.this.stopSelf();
                        }
                    });
            break;
        case SysUtils.DIALOG_LOCK_OF_SDSPACE:
            builder.setTitle(SysUtils.TITLE_LOCK_OF_SDSPACE)
            .setMessage(SysUtils.MSG_LOCK_OF_SDSPACE)
            .setPositiveButton(android.R.string.ok,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            TagLogService.this.stopSelf();
                        }
                    });
            break;
        case SysUtils.DIALOG_ZIP_LOG_FAIL:
            builder.setTitle(SysUtils.TITLE_ZIP_LOG_FAIL)
            .setMessage(SysUtils.MSG_ZIP_LOG_FAIL)
            .setPositiveButton(android.R.string.ok,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // TODO Auto-generated method stub
                            startOrStopAllLogTool(true);
                            dialog.dismiss();
                            TagLogService.this.stopSelf();
                        }
                    });
            break;
        case SysUtils.SD_NOT_EXIST:
            builder.setTitle(SysUtils.TITLE_WARNING)
            .setMessage(SysUtils.MSG_SD_NOT_EXIST)
            .setPositiveButton(android.R.string.ok,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            TagLogService.this.stopSelf();
                        }
                    });
            break;
        case SysUtils.SD_NOT_WRITABLE:
            builder.setTitle(SysUtils.TITLE_WARNING)
            .setMessage(SysUtils.MSG_SD_NOT_WRITABLE)
            .setPositiveButton(android.R.string.ok,
                    new OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            TagLogService.this.stopSelf();
                        }
                    });
            break;
        default:
            break;
        }        
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        Window win = dialog.getWindow();
        win.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();
    }

    private boolean zipAllLogAndDelete(String tagLogFolderName) {
        boolean zipStatus = true;
        if (mLogToolStatus[0]) {
            if (null == zipLogAndDelete(mLogToolPath[0], tagLogFolderName)) {
                zipStatus = false;
            } else {
//                SDCardUtils.deleteFolder(mLogToolPath[0]);
//                Log.d(TAG, "Delete folder: " + mLogToolPath[0]);
            }
        }
        if (mLogToolStatus[1]) {
            if (null == zipLogAndDelete(mLogToolPath[1], tagLogFolderName)) {
                zipStatus = false;
            } else {
//                SDCardUtils.deleteFolder(SysUtils.MOBILE_LOG_FOLDER);
//                Log.d(TAG, "Delete folder: " + mLogToolPath[1]);
            }
        }
        if (mLogToolStatus[2]) {
            if (null == zipLogAndDelete(mLogToolPath[2], tagLogFolderName)) {
                zipStatus = false;
            } else {
//                SDCardUtils.deleteFolder(SysUtils.NET_LOG_FOLDER);
//                Log.d(TAG, "Delete folder: " + mLogToolPath[2]);
            }
        }
        return zipStatus;
    }

    /**
     * Compress log, put the zip file in SD card
     * 
     * @return Compressed log file absolute path
     */
    private String zipLogAndDelete(String LogFolderPath, String tagLogFolderName) {
        Log.d(TAG, "-->zipLog()");
        String zipResultPath = null;
        if (null == LogFolderPath) {
            Log.e(TAG, "LogFolderPath is null!");
            return null;
        }
        
        File LogFolder = new File(LogFolderPath);
        if (!LogFolder.exists()) {
            return null;
        }
        File[] logFolderList = LogFolder.listFiles();
        if (logFolderList == null || logFolderList.length == 0) {
            Log.e(TAG, "Found no detail log in log folder");
            return null;
        }

        File neededLogFolder = getCurrentLogFolder_new(logFolderList);
        if (neededLogFolder != null) {
            String targetLogFileName = tagLogFolderName + File.separator
                    + neededLogFolder.getName() + SysUtils.ZIP_LOG_SUFFIX;
            Log.i(TAG, "targetLogFileName :" + targetLogFileName);
            boolean zipResult = ZipManager.zipFileOrFolder(neededLogFolder
                    .getAbsolutePath(), targetLogFileName);
            if (!zipResult) {
                Log.e(TAG, "Fail to zip log folder: "
                        + neededLogFolder.getAbsolutePath());
                return null;
            } else {
                Log.i(TAG, "Zip log success, target log file="
                        + targetLogFileName);
                zipResultPath = targetLogFileName;
                
                // -- for change 1
                //SDCardUtils.deleteFolder(neededLogFolder); // delete 
            }
        } else {
            Log.e(TAG, "Fail to get needed log folder");
        }

        Log.d(TAG, "<--zipLog(), zipResultPath=" + zipResultPath);
        return zipResultPath;
    }
    
    /**
     * Get the current using log folder, by lastModified time
     * Take the latest folder modified in the past 10 minute, and allow 10 second inaccuracy in the future,
     * as the log folder maybe modified in any time
     * @param logFolderList
     * @return return needed folder, if none found, return null
     */
    private File getCurrentLogFolder(File[] logFolderList) {
        Log.d(TAG, "-->getCurrentLogFolder()");
        if(null == logFolderList){
            Log.e(TAG, "-->Log folder list is null ");
            return null;
        }
        long timeAway = 10 * 60 * 1000;
        File neededFile = null;
        long nowTime = System.currentTimeMillis();
        Log.i(TAG, "Current time=" + ZipManager.translateTime(nowTime));
        for (File file : logFolderList) {
            Log.i(TAG, "Loop log folder:  name=" + file.getName()
                    + ", modified time="
                    + ZipManager.translateTime(file.lastModified()));
            long modifiedTime = file.lastModified();
            if (Math.abs(nowTime - modifiedTime) < timeAway) {
                timeAway = Math.abs(nowTime - modifiedTime);
                neededFile = file;
            }
        }
        if (neededFile != null) {
            Log.i(TAG, "Selected log folder name=[" + neededFile.getName()
                    + "], last modified time="
                    + ZipManager.translateTime(neededFile.lastModified()));
        } else {
            Log.w(TAG, "Could not get needed log folder.");                 
            // ---
            long temp = 0;
            for (File file : logFolderList) {
                long modifiedTime = file.lastModified();
                if(modifiedTime > temp && file.isDirectory()) {
                    temp = modifiedTime;
                    neededFile = file;
                }
            }
            Log.i(TAG, "Selected log folder name=[" + neededFile.getName()
                    + "], last modified time="
                    + ZipManager.translateTime(neededFile.lastModified()));
            // --
        }
        Log.d(TAG, "<--getCurrentLogFolder() end");
        return neededFile;
    }
    
    
    /**
     * Get the current using log folder, by lastModified time
     * Take the latest folder modified in the past 10 minute
     * @param logFolderList
     * @return return needed folder, if none found, return null
     */
    private File getCurrentLogFolder_new(File[] logFolderList) {
        Log.d(TAG, "-->getCurrentLogFolder()");
        long timeAway = 10*60*1000;
        File neededFile = null;
        long nowTime = System.currentTimeMillis();
        Log.i(TAG, "Current time="+ZipManager.translateTime(nowTime));
        for (File file : logFolderList) {
            long modifiedTime = getFolderLastModifyTime(file);
            Log.i(TAG, "Loop log folder:  name="+file.getName()+", modified time="+ZipManager.translateTime(modifiedTime));
            if(Math.abs(nowTime-modifiedTime)<timeAway){
                timeAway = Math.abs(nowTime-modifiedTime);
                neededFile = file;
            }
        }
        if (neededFile != null) {
            Log.i(TAG, "Selected log folder name=[" + neededFile.getName());
        } else {
            Log.e(TAG, "Could not get needed log folder.");
            // ---
            long temp = 0;
            for (File file : logFolderList) {
                long modifiedTime = file.lastModified();
                if(modifiedTime > temp && file.isDirectory()) {
                    temp = modifiedTime;
                    neededFile = file;
                }
            }
            Log.i(TAG, "Selected log folder name=[" + neededFile.getName()
                    + "], last modified time="
                    + ZipManager.translateTime(neededFile.lastModified()));
            // --
        }
        Log.d(TAG, "<--getCurrentLogFolder()");
        return neededFile;
    }
    
    
    /**
     * Since folder's last modify time seems can not work very well in Linux, 
     * we will loop each file in it to get the accurate time.
     * As file may be modified all the time, 10 second inaccuracy in the future will be allowed.
     * TODO Attention: This method may not work well if user modify system time, since file modified in future will be ignored
     * @return 0 if no valid file
     */
    private long getFolderLastModifyTime(File file) {
        Log.v(TAG, "-->getFolderLastModifyTime(), path="+(file==null?"NUll":file.getAbsoluteFile()));
        long result = 0;
        if(file==null || !file.exists()){
            Log.d(TAG, "Given file not exist.");
            return result;
        }
        long currentTime = System.currentTimeMillis();
        if(file.isFile()){
            Log.w(TAG, "You should give me a folder. But still can work here.");
            long time = file.lastModified();
            Log.v(TAG, file.getAbsolutePath()+" modified at "+ZipManager.translateTime(time));
            if(currentTime - time > -10*1000){//past or 10s in future
                result = time;
            }
        }else{
            File[] fileList = file.listFiles();
            for(File subFile : fileList){
                long time = 0;
                if(subFile.isFile()){
                    Log.v(TAG, subFile.getAbsolutePath()+" modified at "+ZipManager.translateTime(currentTime));
                    time = subFile.lastModified();
                    if(currentTime - time < -10*1000){//file in future
                        time = 0;
                    }
                }else{
                    time = getFolderLastModifyTime(subFile);
                }
                if(Math.abs(time-currentTime)<Math.abs(result-currentTime)){
                    result = time;
                }
            }
        }
        Log.v(TAG, "<--getFolderLastModifyTime(), time="+ZipManager.translateTime(result));
        return result;
    }
    
    private void clearRedScreen() {
        Log.d(TAG, "--> Clear Red Screen");
        try{
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("aee -c dal");
        } catch (IOException e){
            Log.e(TAG, "--> Clear Red Screen catch IOException");
            e.printStackTrace();
        } catch (SecurityException e) {
            Log.e(TAG, "--> Clear Red Screen catch SecurityException");
            e.printStackTrace();
        }
    }

    /**
     * Play tip sound
     * @param context
     * @param defaultUri
     */
    private void playTipSound(Context context, Uri defaultUri) {
        Log.d(TAG, "play sound tip.");
        if (defaultUri != null) {
            Ringtone mRingtone = RingtoneManager.getRingtone(context,
                    defaultUri);
            if (mRingtone != null) {
                mRingtone.play();
            }
        }
    }

}
