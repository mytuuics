/* 
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
/*
 * Definitions for cm3623 als/ps sensor chip.
 */
#ifndef __STK31xx_H__
#define __STK31xx_H__

#include <linux/ioctl.h>
#ifdef MT6577
#include <mach/mt6577_pm_ldo.h>
#endif

#define	POWER_IR_LED				MT65XX_POWER_LDO_VMC
#define STK31XX_I2C_SLAVE_ADDR	0x90

/*ALSPS REGS*/
#define ALS_CMD          0x01
#define ALS_DT1          0x02
#define ALS_DT2          0x03
#define ALS_THDH1        0x04
#define ALS_THDH2        0x05
#define ALS_THDL1        0x06
#define ALS_THDL2        0x07
#define ALSPS_STATUS     0x08
#define PS_CMD           0x09
#define PS_DT            0x0A
#define PS_THDH          0x0B
#define PS_THDL          0x0C

#define SW_RESET		  0x80
#define PS_GAIN          0x82


/*ALS Command. register 0x01 */
// ALS shutdown. 0: shutdown disable, that is ALS on; 1: ALS off
#define SD_ALS      (1      << 0)

// ALS int control. 0: disable; 1: enable.
#define INT_ALS     (1      << 1)

// als refresh time. 1/2/4/8T, relevant to 100/200/400/800 ms
#define IT_ALS      (0x03   << 2)

//#define THD_ALS     (0x03   << 4)
// ALS detect range: bit6 = 0: 57671 lux; bit6 = 1: 28836 lux
#define GAIN_ALS    (0x03   << 6)

// initial config data of ALS  0x45
#define	ALS_CMD_DATA	(0x49)

// for status register 0x08
// ID. read-only
#define	ALSPS_ID		(0x03 << 6)

// PS int flag.
#define PS_INT_FLAG	(0x01 << 5)

// ALS int flag.
#define ALS_INT_FLAG	(0x01 << 4)

/*Proximity sensor command register 0x09 */
// Ps shutdown. 0: PS on; 1: PS off
#define SD_PS       (1      << 0)

// PS int control. 0: disable; 1: enable
#define INT_PS      (1      << 1)

// PS refresh time: 1/1.5/2/2.5 T
#define IT_PS       (0x03   << 2)

// IR Led sinking current. 0: 100mA; 1: 200mA
#define DR_PS       (1      << 4)

// ps sleep time: 10/30/90/270 ms
#define SLP_PS      (0x03   << 5)

// 0: logic OR; 1: logic AND
#define INTM_PS     (1      << 7)

// Initial config data of PS  0x59	0x25
#define	PS_CMD_DATA	(0x35)

// Initial config data of register 0x82, [0x05, 0x09, 0x0d]
#define	PS_GAIN_4X		(0x05)
#define	PS_GAIN_8X		(0x09)
#define	PS_GAIN_16X	(0x0D)
#define	PS_GAIN_DATA	PS_GAIN_8X

#define CONFIG_STK_ALS_CHANGE_THRESHOLD		5

/* STK calibration */
//#define STK_AUTO_CT_CALI_SATU
//#define STK_MANUAL_GREYCARD_CALI
//#define STK_AUTO_CT_CALI_NO_SATU

#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI) || defined(STK_AUTO_CT_CALI_SATU) || defined(STK_AUTO_CT_CALI_NO_SATU))
#define STK_CALI_SAMPLE_NO		5	
#define STK_THD_MAX				255	
#define STK_MAX_PS_DIFF_AUTO		20

#define STK_HIGH_THD				0
#define STK_LOW_THD				1

#define STK_DATA_MAX				0
#define STK_DATA_AVE				1
#define STK_DATA_MIN				2

const static uint16_t cali_sample_time_table[4] = {20, 40, 100, 300};
#define STK_CALI_VER0			0x48
#define STK_CALI_VER1			0x02
#define STK_CALI_FILE "/data/stk_ps_cali.conf"
#define STK_CALI_FILE_SIZE 5
#endif	/*	#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI) || defined(STK_AUTO_CT_CALI_SATU) || defined(STK_AUTO_CT_CALI_NO_SATU))	*/

#if (defined(STK_MANUAL_CT_CALI) || defined(STK_AUTO_CT_CALI_NO_SATU) || defined(STK_AUTO_CT_CALI_SATU))
#define STK_MAX_PS_CROSSTALK		200
#define STK_THD_H_ABOVE_CT		30
#define STK_THD_L_ABOVE_CT		15
#endif

#if defined(STK_AUTO_CT_CALI_SATU)
#define STK_THD_H_BELOW_MAX_CT		15
#define STK_THD_L_BELOW_MAX_CT		30
#define STK_REDETECT_CT				50
#endif

#ifdef STK_MANUAL_GREYCARD_CALI
#define STK_MIN_GREY_PS_DATA			50
#define STK_DIFF_GREY_N_THD_H		15
#define STK_DIFF_GREY_N_THD_L		30
#endif	/*	#ifdef STK_MANUAL_GREYCARD_CALI	*/

#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI))
#define SITRONIX_PERMISSION_THREAD
#endif

#endif

