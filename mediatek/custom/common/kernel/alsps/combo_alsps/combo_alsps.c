/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/* drivers/hwmon/mt6516/amit/ISL29044.c - ISL29044 ALS/PS driver
 * 
 * Author: MingHsien Hsieh <minghsien.hsieh@mediatek.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/miscdevice.h>
#include <asm/uaccess.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/kobject.h>
#include <linux/earlysuspend.h>
#include <linux/platform_device.h>
#include <asm/atomic.h>
#include <linux/version.h>

#include <linux/hwmsensor.h>
#include <linux/hwmsen_dev.h>
#include <linux/hwmsen_helper.h>
#include <linux/sensors_io.h>
#include <linux/wakelock.h> 
#include <asm/io.h>
#include <cust_eint.h>
#include <cust_alsps.h>

#include "combo_alsps.h"

#ifdef MT6516
#include <mach/mt6516_devs.h>
#include <mach/mt6516_typedefs.h>
#include <mach/mt6516_gpio.h>
#include <mach/mt6516_pll.h>
#endif

#ifdef MT6573
#include <mach/mt6573_devs.h>
#include <mach/mt6573_typedefs.h>
#include <mach/mt6573_gpio.h>
#include <mach/mt6573_pll.h>
#endif

#ifdef MT6575
#include <mach/mt6575_devs.h>
#include <mach/mt6575_typedefs.h>
#include <mach/mt6575_gpio.h>
#include <mach/mt6575_pm_ldo.h>
#endif


#ifdef MT6577
#include <mach/mt6577_devs.h>
#include <mach/mt6577_typedefs.h>
#include <mach/mt6577_gpio.h>
#include <mach/mt6577_pm_ldo.h>
#endif


#define DRIVER_VERSION          "2.6"
#define STK_PS_POLLING_LOG

/*-------------------------MT6516&MT6573 define-------------------------------*/
#ifdef MT6516
#define POWER_NONE_MACRO MT6516_POWER_NONE
#endif

#ifdef MT6573
#define POWER_NONE_MACRO MT65XX_POWER_NONE
#endif
#ifdef MT6575
#define POWER_NONE_MACRO MT65XX_POWER_NONE
#endif
#ifdef MT6577
#define POWER_NONE_MACRO MT65XX_POWER_NONE
#endif

#define DEBUG  	1

#define APS_TAG                  "[ALSPS] "
#if DEBUG
#define APS_FUN(f)                printk(APS_TAG "%s\n", __FUNCTION__)
#define APS_ERR(fmt, args...)    printk(APS_TAG "%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define APS_LOG(fmt, args...)    printk(APS_TAG fmt, ##args)
#define APS_DBG(fmt, args...)    printk(APS_TAG fmt, ##args)      
#else
#define APS_FUN(f)
#define APS_ERR(fmt, args...)
#define APS_LOG(fmt, args...)
#define APS_DBG(fmt, args...)
#endif

#ifdef MT6573
extern void mt65xx_eint_unmask(unsigned int line);
extern void mt65xx_eint_mask(unsigned int line);
extern void mt65xx_eint_set_polarity(kal_uint8 eintno, kal_bool ACT_Polarity);
extern void mt65xx_eint_set_hw_debounce(kal_uint8 eintno, kal_uint32 ms);
extern kal_uint32 mt65xx_eint_set_sens(kal_uint8 eintno, kal_bool sens);
extern void mt65xx_eint_registration(kal_uint8 eintno, kal_bool Dbounce_En,
                                     kal_bool ACT_Polarity, void (EINT_FUNC_PTR)(void),
                                     kal_bool auto_umask);

#endif

#ifdef MT6575
extern void mt65xx_eint_unmask(unsigned int line);
extern void mt65xx_eint_mask(unsigned int line);
extern void mt65xx_eint_set_polarity(kal_uint8 eintno, kal_bool ACT_Polarity);
extern void mt65xx_eint_set_hw_debounce(kal_uint8 eintno, kal_uint32 ms);
extern kal_uint32 mt65xx_eint_set_sens(kal_uint8 eintno, kal_bool sens);
extern void mt65xx_eint_registration(kal_uint8 eintno, kal_bool Dbounce_En,
                                     kal_bool ACT_Polarity, void (EINT_FUNC_PTR)(void),
                                     kal_bool auto_umask);

#endif


#ifdef MT6577
extern void mt65xx_eint_unmask(unsigned int line);
extern void mt65xx_eint_mask(unsigned int line);
extern void mt65xx_eint_set_polarity(kal_uint8 eintno, kal_bool ACT_Polarity);
extern void mt65xx_eint_set_hw_debounce(kal_uint8 eintno, kal_uint32 ms);
extern kal_uint32 mt65xx_eint_set_sens(kal_uint8 eintno, kal_bool sens);
extern void mt65xx_eint_registration(kal_uint8 eintno, kal_bool Dbounce_En,
                                     kal_bool ACT_Polarity, void (EINT_FUNC_PTR)(void),
                                     kal_bool auto_umask);

#endif


// for isl29044
#if 1
#define ALSPS_REGISTER_INTERRPUT(intrrupt_handle_func)   \
{\
	mt65xx_eint_set_sens(CUST_EINT_ALS_NUM, CUST_EINT_EDGE_SENSITIVE); \
		mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY); \
		mt65xx_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN); \
		mt65xx_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, intrrupt_handle_func, 0); \
		mt65xx_eint_unmask(CUST_EINT_ALS_NUM);	\
}


/*----------------------------------------------------------------------------*/

#define ISL29044_DEV_NAME     "ISL29044"
/*----------------------------------------------------------------------------*/



/******************************************************************************
 * extern functions
*******************************************************************************/
/*----------------------------------------------------------------------------*/
static struct i2c_client *isl29044_i2c_client = NULL;
/*----------------------------------------------------------------------------*/
static const struct i2c_device_id isl29044_i2c_id[] = {{ISL29044_DEV_NAME,0},{}};

#if (LINUX_VERSION_CODE>=KERNEL_VERSION(3,0,0))
static struct i2c_board_info __initdata i2c_isl29044={ I2C_BOARD_INFO("isl29044", (ISL29044_I2C_SLAVE_ADDR>>1))};
#else
/*the adapter id & i2c address will be available in customization*/
static unsigned short isl29044_force[] = {0x00, ISL29044_I2C_SLAVE_ADDR, I2C_CLIENT_END, I2C_CLIENT_END};
static const unsigned short *const isl29044_forces[] = { isl29044_force, NULL };
static struct i2c_client_address_data isl29044_addr_data = { .forces = isl29044_forces,};
#endif

/*----------------------------------------------------------------------------*/
static int ISL29044_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id); 
static int ISL29044_i2c_remove(struct i2c_client *client);

#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
static int ISL29044_i2c_detect(struct i2c_client *client, int kind, struct i2c_board_info *info);
#endif

/*----------------------------------------------------------------------------*/
static int ISL29044_i2c_suspend(struct i2c_client *client, pm_message_t msg);
static int ISL29044_i2c_resume(struct i2c_client *client);


static struct ISL29044_priv *g_ISL29044_ptr = NULL;

/*----------------------------------------------------------------------------*/
typedef enum {
    ISL_TRC_ALS_DATA= 0x0001,
    ISL_TRC_PS_DATA = 0x0002,
    ISL_TRC_EINT    = 0x0004,
    ISL_TRC_IOCTL   = 0x0008,
    ISL_TRC_I2C     = 0x0010,
    ISL_TRC_CVT_ALS = 0x0020,
    ISL_TRC_CVT_PS  = 0x0040,
    ISL_TRC_DEBUG   = 0x8000,
} ISL_TRC;
/*----------------------------------------------------------------------------*/
typedef enum {
    ISL_BIT_ALS    = 1,
    ISL_BIT_PS     = 2,
} ISL_BIT;
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
struct ISL29044_priv {
    struct alsps_hw  *hw;
    struct i2c_client *client;
    struct delayed_work  eint_work;
    
    /*misc*/
    atomic_t    trace;
    atomic_t    i2c_retry;
    atomic_t    als_suspend;
    atomic_t    als_debounce;   /*debounce time after enabling als*/
    atomic_t    als_deb_on;     /*indicates if the debounce is on*/
    atomic_t    als_deb_end;    /*the jiffies representing the end of debounce*/
    atomic_t    ps_mask;        /*mask ps: always return far away*/
    atomic_t    ps_debounce;    /*debounce time after enabling ps*/
    atomic_t    ps_deb_on;      /*indicates if the debounce is on*/
    atomic_t    ps_deb_end;     /*the jiffies representing the end of debounce*/
    atomic_t    ps_suspend;


    /*data*/
    u16         als;
    u8          ps;
    u8          _align;
    u16         als_level_num;
    u16         als_value_num;
    u32         als_level[C_CUST_ALS_LEVEL-1];
    u32         als_value[C_CUST_ALS_LEVEL];

    bool    als_enable;    /*record current als status*/
	unsigned int    als_widow_loss; 
	
    bool    ps_enable;     /*record current ps status*/
    atomic_t   ps_thd_val;     /*the cmd value can't be read, stored in ram*/

	// The below two threshold are added by chu, zewei on 2012/12/17
	atomic_t   ps_threshold_high;	//	ps int high threshold
    atomic_t   ps_threshold_low;	//   ps int low threshold
    
    ulong       enable;         /*record HAL enalbe status*/
    ulong       pending_intr;   /*pending interrupt*/
    //ulong        first_read;   // record first read ps and als
    /*early suspend*/
#if defined(CONFIG_HAS_EARLYSUSPEND)
    struct early_suspend    early_drv;
#endif     
};
/*----------------------------------------------------------------------------*/
static struct i2c_driver isl29044_i2c_driver = {	
	.probe      = ISL29044_i2c_probe,
	.remove     = ISL29044_i2c_remove,
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	.detect     = ISL29044_i2c_detect,
#endif	
	.suspend    = ISL29044_i2c_suspend,
	.resume     = ISL29044_i2c_resume,
	.id_table   = isl29044_i2c_id,
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))		
	.address_data = &isl29044_addr_data,
#endif	
	.driver = {
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))		
		.owner          = THIS_MODULE,
#endif		
		.name           = ISL29044_DEV_NAME,
	},
};

static struct ISL29044_priv *ISL29044_obj = NULL;
static struct platform_driver ISL29044_alsps_driver;

static int ISL29044_get_ps_value(struct ISL29044_priv *obj, u8 ps);
static int ISL29044_get_als_value(struct ISL29044_priv *obj, u16 als);

/*----------------------------------------------------------------------------*/
static int hwmsen_read_byte_sr(struct i2c_client *client, u8 addr, u8 *data)
{
   u8 buf;
    int ret = 0;
	
   // client->addr = (client->addr & I2C_MASK_FLAG) | I2C_WR_FLAG |I2C_RS_FLAG;
    client->addr = (client->addr & I2C_MASK_FLAG);
    buf = addr;
	ret = i2c_master_send(client, (const char*)&buf,  1);
    //ret = i2c_master_send(client, (const char*)&buf, 1);
    if (ret !=1) {
        APS_ERR("send command error!!\n");
        return -EFAULT;
    }
	
	ret = i2c_master_recv(client, (const char*)&buf, 1);
	if (ret != 1) {
		APS_ERR("read command error!!\n");
		return -1;
	}

    *data = buf;
	//client->addr = client->addr& I2C_MASK_FLAG;
    return 0;
}

static void ISL29044_dumpReg(struct i2c_client *client)
{
  int i=0;
  u8 addr = 0x00;
  u8 regdata=0;
  for(i=0; i<15 ; i++)
  {
    //dump all
    hwmsen_read_byte_sr(client,addr,&regdata);
	APS_LOG("Reg addr=%x regdata=%x\n",addr,regdata);
	//snprintf(buf,1,"%c",regdata);
	addr++;
	if(addr > 0x0B)
		break;
  }
}

/*----------------------------------------------------------------------------*/
int ISL29044_get_timing(void)
{
return 200;
/*
	u32 base = I2C2_BASE; 
	return (__raw_readw(mt6516_I2C_HS) << 16) | (__raw_readw(mt6516_I2C_TIMING));
*/
}

int ISL29044_read_PS_data(struct i2c_client *client, u8 *data)
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);    
	int ret = 0;
	//u8 aps_data=0;
	//u8 addr = 0x00;

	//hwmsen_read_byte_sr(client,APS_BOTH_DATA,&aps_data);
	if(hwmsen_read_byte_sr(client,ISL_REG_DATA_PROX,data))
	{
		APS_ERR("reads aps data = %d\n", ret);
		return -EFAULT;
	}
	
	APS_LOG("data=%x \n",*data);
	if(atomic_read(&obj->trace) & ISL_TRC_PS_DATA)
	{
		APS_DBG("APS_PS:  0x%04X\n", (u32)(*data));
	}
	return 0;    
}

int ISL29044_read_ALS_data(struct i2c_client *client, u16 *data)
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);    
	u8 aps_data0=0, aps_data1=0;
	//u8 addr = 0x00;
	int ret = 0;

	//hwmsen_read_byte_sr(client,APS_BOTH_DATA,&aps_data);
	if(hwmsen_read_byte_sr(client,ISL_REG_DATA_LSB_ALS,&aps_data0))
	{
		APS_ERR("reads aps data = %d\n", ret);
		return -EFAULT;
	}
	if(hwmsen_read_byte_sr(client,ISL_REG_DATA_MSB_ALS,&aps_data1))
	{
		APS_ERR("reads aps data = %d\n", ret);
		return -EFAULT;
	}
	APS_LOG("aps_data0=%x,aps_data1=%x \n",aps_data0,aps_data1);
	*data = (aps_data0 | (aps_data1<<8));
	
	APS_LOG("aps_data=%x \n",*data);
	if(atomic_read(&obj->trace) & ISL_TRC_ALS_DATA)
	{
		APS_DBG("APS_ALS:  0x%04X\n", (u32)(*data));
	}
	return 0;    
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

int ISL29044_init_device(struct i2c_client *client)
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);        
	u8 buf =0;
	int i, err;
	u8 reg_dat[8], tmp;
	
	APS_LOG("ISL29044_init_device.........\r\n");
	
	//refor to datasheet
	if(hwmsen_write_byte(client,ISL_REG_CMD_1,ISL_REG_CMD_1_INIT))//b 1011 0110  ps_enable, ps_peroid 100ms ,110mA, als enable ,2000lux, visable spectrum
	{
	  return -EFAULT;
	}
	if(hwmsen_write_byte(client,ISL_REG_CMD_2,0x44))//b 0010 0010  ps_persist 4 ,als_persist 4, int_or
	{
	  return -EFAULT;
	}

	#if 0
    if(hwmsen_write_byte(client,ISL_REG_INT_LOW_PROX,0x20))//ps int low threshold
    {
      return -EFAULT;
    }
    if(hwmsen_write_byte(client,ISL_REG_INT_HIGH_PROX,0xE0))//ps int high threshold
    {
      return -EFAULT;
    }
	#else

	obj->hw = get_cust_alsps_hw(ISL29044_I2C_SLAVE_ADDR);
	
	if (1 == obj->hw->polling_mode_ps)
	{
		reg_dat[3] = 0x00;
		reg_dat[4] = 0xff;
	}
	else
	{
		reg_dat[3] = obj->hw->ps_threshold_low;
		reg_dat[4] = obj->hw->ps_threshold_high;
	}

	for (i = 3; i <= 4; i++)
	{
	    if((err = hwmsen_write_byte(client, i, reg_dat[i])))
	    {
			APS_ERR("Write %d times data failed: %d\n", i, err);
	      	return err;	
	    }
    }
	#endif
    if(hwmsen_write_byte(client,ISL_REG_INT_LOW_ALS,0x00)) //
    {
      return -EFAULT;
    }
    if(hwmsen_write_byte(client,ISL_REG_INT_LOW_HIGH_ALS,0xF0))
    {
      return -EFAULT;
    }
	//power up & enalbe both
	if(hwmsen_write_byte(client,ISL_REG_INT_HIGH_ALS,0xFF)) 
	{
	  return -EFAULT;
	}
	
	APS_LOG("ISL29044_init_device.........end \r\n");
	return 0;
}


/*----------------------------------------------------------------------------*/
static void ISL29044_power(struct alsps_hw *hw, unsigned int on) 
{
	static unsigned int power_on = 0;

	//APS_LOG("power %s\n", on ? "on" : "off");

	if(hw->power_id != POWER_NONE_MACRO)
	{
		if(power_on == on)
		{
			APS_LOG("ignore power control: %d\n", on);
		}
		else if(on)
		{
			if(!hwPowerOn(hw->power_id, hw->power_vol, "ISL29044")) 
			{
				APS_ERR("power on fails!!\n");
			}
		}
		else
		{
			if(!hwPowerDown(hw->power_id, "ISL29044")) 
			{
				APS_ERR("power off fail!!\n");   
			}
		}
	}

	//Turn on/off the VMC power to supply for IR LED
	#ifdef ISL29044_POWER_IR_LED
	if (on)
	{
		if(!hwPowerOn(ISL29044_POWER_IR_LED, VOL_2800, "isl29044")) 
		{
			APS_ERR("Turning on VMC fails!!\n");
		}
	}
	else
	{
		if(!hwPowerDown(ISL29044_POWER_IR_LED, "isl29044")) 
		{
			APS_ERR("power off fail!!\n");   
		}
	}
	#endif
	power_on = on;
}
/*----------------------------------------------------------------------------*/
static int ISL29044_enable_als(struct i2c_client *client, bool enable)
{
    APS_LOG(" ISL29044_enable_als %d \n",enable); 
	struct ISL29044_priv *obj = i2c_get_clientdata(client);
	int err=0;
	int trc = atomic_read(&obj->trace);
	u8 regdata=0;
	if(enable == obj->als_enable)
	{
	   return 0;
	}
	

	//if(hwmsen_read_byte_sr(client, ISL_REG_CMD_1, &regdata))
	//{
	//	APS_ERR("read ISL_REG_CMD_1 register err!\n");
	//	return -1;
	//}
#if 0
	//
    //APS_LOG(" ISL29044_enable_als 00h=%x \n",regdata); 
    ISL29044_enable_ps(client, true);
	//
#endif
	regdata=ISL_REG_CMD_1_INIT;
	//regdata &= 0b01111011; //first set bit7 for ps enable  , bit2 for als enable
	
	if(enable == TRUE)//enable als
	{
	     APS_LOG("first enable als!\n");
		 if(true == obj->ps_enable)
		 {
		   APS_LOG("ALS(1): enable both \n");
		   atomic_set(&obj->ps_deb_on, 1);
		   atomic_set(&obj->ps_deb_end, jiffies+atomic_read(&obj->ps_debounce)/(1000/HZ));
		   regdata |= 0b10000100; //enable both
		 }
		 if(false == obj->ps_enable)
		 {
		   APS_LOG("ALS(1): enable als only \n");
		   regdata |= 0b00000100; //only enable als
		 }
		 atomic_set(&obj->als_deb_on, 1);
		 atomic_set(&obj->als_deb_end, jiffies+atomic_read(&obj->als_debounce)/(1000/HZ));
		 set_bit(ISL_BIT_ALS,  &obj->pending_intr);
		 schedule_delayed_work(&obj->eint_work,230); //after enable the value is not accurate
		 APS_LOG("first enalbe als set pending interrupt %d\n",obj->pending_intr);
	}
	else
	{
		if(true == obj->ps_enable)
		 {
		   APS_LOG("ALS(0):enable ps only \n");
		   atomic_set(&obj->ps_deb_on, 1);
		   atomic_set(&obj->ps_deb_end, jiffies+atomic_read(&obj->ps_debounce)/(1000/HZ));
		   regdata |= 0b10000000;//only enable ps
		   set_bit(ISL_BIT_PS,  &obj->pending_intr);
		   schedule_delayed_work(&obj->eint_work,120);
		 }
		 if(false == obj->ps_enable)
		 {
		   APS_LOG("ALS(0): disable both \n");
		   regdata |= 0b00000000;//disable both
		 }
		 
		 regdata &= 0b11111011;//disable als only
		 //del_timer_sync(&obj->first_read_als_timer);
	}
	

	if(hwmsen_write_byte(client,ISL_REG_CMD_1,regdata))
	{
		APS_LOG("ISL29044_enable_als failed!\n");
		return -1;
	}
    obj->als_enable = enable;

#if 0
	if(hwmsen_read_byte_sr(client, ISL_REG_CMD_1, &regdata))
	{
		APS_ERR("read ISL_REG_CMD_1 register err!\n");
		return -1;
	}
	//
	APS_LOG(" after ISL29044_enable_als 00h=%x \n",regdata);
#endif

	if(trc & ISL_TRC_DEBUG)
	{
		APS_LOG("enable als (%d)\n", enable);
	}

	return err;
}
/*----------------------------------------------------------------------------*/
static int ISL29044_enable_ps(struct i2c_client *client, bool enable)
{
    APS_LOG(" ISL29044_enable_ps %d\n",enable); 
	struct ISL29044_priv *obj = i2c_get_clientdata(client);
	int err=0;
	int trc = atomic_read(&obj->trace);
	u8 regdata=0;
	if(enable == obj->ps_enable)
	{
	   return 0;
	}
	

	//if(hwmsen_read_byte_sr(client, ISL_REG_CMD_1, &regdata))
	//{
	//	APS_ERR("read ISL_REG_CMD_1 register err!\n");
	//	return -1;
	//}
	//regdata &= 0b01111011;
	regdata = ISL_REG_CMD_1_INIT;

#if 1
	APS_LOG(" ISL29044_enable_ps regdata=%x \n",regdata); 
#endif
	if(enable == TRUE)//enable ps
	{
	     APS_LOG("first enable ps!\n");
		 //if(true == obj->als_enable)
		 //{
		 //  regdata |= 0b10000100; //enable both
		 //  atomic_set(&obj->als_deb_on, 1);
		 //  atomic_set(&obj->als_deb_end, jiffies+atomic_read(&obj->als_debounce)/(1000/HZ));
		 //  APS_LOG("PS(1): enable ps both !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		 //}
		 //if(false == obj->als_enable)
		 //{
		   regdata |= 0b10000000; //only enable ps
		   APS_LOG("PS(1): enable ps only !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		 //}
		 atomic_set(&obj->ps_deb_on, 1);
		 atomic_set(&obj->ps_deb_end, jiffies+atomic_read(&obj->ps_debounce)/(1000/HZ));
		 set_bit(ISL_BIT_PS,  &obj->pending_intr);
		 schedule_delayed_work(&obj->eint_work,120);
		 APS_LOG("first enalbe ps set pending interrupt %d\n",obj->pending_intr);
	}
	else//disable ps
	{
		 if(true == obj->als_enable)
		 {
		   APS_LOG("PS(0): disable ps only enalbe als !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		   regdata &= 0b01111111;//only enable als
		   atomic_set(&obj->als_deb_on, 1);//add 
		   atomic_set(&obj->als_deb_end, jiffies+atomic_read(&obj->als_debounce)/(1000/HZ));
		   set_bit(ISL_BIT_ALS,  &obj->pending_intr);
		   schedule_delayed_work(&obj->eint_work,120);
		 }
		 if(false == obj->als_enable)
		 {
		   APS_LOG("PS(0): disable both !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		   regdata |= 0b01111011;//disable both
		 }
		 
		regdata &= 0b01111111;//disable Ps only
	}
	

	if(hwmsen_write_byte(client,ISL_REG_CMD_1,regdata))
	{
		APS_LOG("ISL29044_enable_als failed!\n");
		return -1;
	}
	obj->ps_enable = enable;

#if 0
	if(hwmsen_read_byte_sr(client, ISL_REG_CMD_1, &regdata))
	{
		APS_ERR("read ISL_REG_CMD_1 register err!\n");
		return -1;
	}
	//
	APS_LOG(" after ISL29044_enable_ps 00h=%x \n",regdata);
#endif
	
	if(trc & ISL_TRC_DEBUG)
	{
		APS_LOG("enable ps (%d)\n", enable);
	}

	return err;
}
/*----------------------------------------------------------------------------*/


static int ISL29044_check_intr(struct i2c_client *client) 
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);
	int err;
	u8 data=0;

	err = hwmsen_read_byte_sr(client,ISL_REG_CMD_2,&data);
	APS_LOG("INT flage: = %x  \n", data);

	if(err)
	{
		APS_ERR("WARNING: read int status: %d\n", err);
		return 0;
	}
    
	if(data & 0x08)//als
	{
		set_bit(ISL_BIT_ALS, &obj->pending_intr);
	}
	else
	{
	   clear_bit(ISL_BIT_ALS, &obj->pending_intr);
	}
	set_bit(ISL_BIT_PS,  &obj->pending_intr);
	
	if(atomic_read(&obj->trace) & ISL_TRC_DEBUG)
	{
		APS_LOG("check intr: 0x%08X\n", obj->pending_intr);
	}

	return 0;
}

/*----------------------------------------------------------------------------*/
void ISL29044_eint_func(void)
{
	struct ISL29044_priv *obj = g_ISL29044_ptr;
	APS_LOG("fwq interrupt fuc\n");
	if(!obj)
	{
		return;
	}
	
	schedule_delayed_work(&obj->eint_work,0);
	if(atomic_read(&obj->trace) & ISL_TRC_EINT)
	{
		APS_LOG("eint: als/ps intrs\n");
	}
}
/*----------------------------------------------------------------------------*/
int Ps_status =0;
static void ISL29044_eint_work(struct work_struct *work)
{
	struct ISL29044_priv *obj = (struct ISL29044_priv *)container_of(work, struct ISL29044_priv, eint_work);
	int err;
	hwm_sensor_data sensor_data;
	
	memset(&sensor_data, 0, sizeof(sensor_data));

	APS_LOG("ISL29044_eint_work\n");

	if(0 == atomic_read(&obj->ps_deb_on)) // first enable do not check interrupt
	{
	   err = ISL29044_check_intr(obj->client);
	}
	
	if(err)
	{
		APS_ERR("check intrs: %d\n", err);
	}

    APS_LOG("ISL29044_eint_work &obj->pending_intr =%d\n",obj->pending_intr);
	
	if((1<<ISL_BIT_ALS) & obj->pending_intr &(0 == obj->hw->polling_mode_als))
	{
	  //get raw data
	  APS_LOG("fwq als INT\n");
	  if(err = ISL29044_read_ALS_data(obj->client, &obj->als))
	  {
		 APS_ERR("ISL29044 read als data: %d\n", err);;
	  }
	  //map and store data to hwm_sensor_data
	  while(-1 == ISL29044_get_als_value(obj, obj->als))
	  {
		 ISL29044_read_ALS_data(obj->client, &obj->als);
		 msleep(50);
	  }
 	  sensor_data.values[0] = ISL29044_get_als_value(obj, obj->als);
	  sensor_data.value_divide = 1;
	  sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
	  //let up layer to know
	  if(err = hwmsen_get_interrupt_data(ID_LIGHT, &sensor_data))
	  {
		APS_ERR("call hwmsen_get_interrupt_data fail = %d\n", err);
	  }
	  
	}
	if((1<<ISL_BIT_PS) &  obj->pending_intr)
	{
	  //get raw data
	  APS_LOG("fwq ps INT\n");
	  if(err = ISL29044_read_PS_data(obj->client, &obj->ps))
	  {
		 APS_ERR("ISL29044 read ps data: %d\n", err);;
	  }
	  //map and store data to hwm_sensor_data
	  while(-1 == ISL29044_get_ps_value(obj, obj->ps))
	  {
		 ISL29044_read_PS_data(obj->client, &obj->ps);
		 msleep(50);
		 APS_LOG("ISL29044 read ps data delay\n");;
	  }
	  sensor_data.values[0] = ISL29044_get_ps_value(obj, obj->ps);
	  sensor_data.value_divide = 1;
	  sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
	  //let up layer to know
	  APS_LOG("ISL29044 read ps data  = %d  Ps_status =%d \n",sensor_data.values[0],Ps_status);


/*  Ps near: interrupt pin is low,  ps far away, pin is high
       ----        -------------
            |       |
            |       |
            |       |
             -----
*/
	  //interrupt pin initial state is low edge trigger
	  if(Ps_status != sensor_data.values[0])
	  {
	  	  if(sensor_data.values[0] == 0)  //now is close, next time should be high edge trigger
	  	  	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_POLARITY_HIGH);
		  else     //now is close, next time should be low  edge trigger
		  	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_POLARITY_LOW);
	  }

	  
	  Ps_status = sensor_data.values[0];   //record current status
	  
	  if(err = hwmsen_get_interrupt_data(ID_PROXIMITY, &sensor_data))
	  {
		APS_ERR("call hwmsen_get_interrupt_data fail = %d\n", err);
	  }
	}
	
	#ifdef MT6573
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);  
	#endif
	#ifdef MT6575
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
	#endif
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);  
}

/*----------------------------------------------------------------------------*/
int ISL29044_setup_eint(struct i2c_client *client)
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);        

	g_ISL29044_ptr = obj;
	/*configure to GPIO function, external interrupt*/

	
	mt_set_gpio_mode(GPIO_ALS_EINT_PIN, GPIO_ALS_EINT_PIN_M_EINT);
    mt_set_gpio_dir(GPIO_ALS_EINT_PIN, GPIO_DIR_IN);
	mt_set_gpio_pull_enable(GPIO_ALS_EINT_PIN, GPIO_PULL_ENABLE);
	mt_set_gpio_pull_select(GPIO_ALS_EINT_PIN, GPIO_PULL_UP);
	
	//mt_set_gpio_dir(GPIO_ALS_EINT_PIN, GPIO_DIR_IN);
	//mt_set_gpio_mode(GPIO_ALS_EINT_PIN, GPIO_ALS_EINT_PIN_M_EINT);
	//mt_set_gpio_pull_enable(GPIO_ALS_EINT_PIN, GPIO_PULL_ENABLE);
	//mt_set_gpio_pull_select(GPIO_ALS_EINT_PIN, GPIO_PULL_UP);

    //
#ifdef MT6573
	
    mt65xx_eint_set_sens(CUST_EINT_ALS_NUM, CUST_EINT_EDGE_SENSITIVE);
	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY);
	mt65xx_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
	mt65xx_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, ISL29044_eint_func, 0);
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);  
#endif  
	
#ifdef MT6575
			
	mt65xx_eint_set_sens(CUST_EINT_ALS_NUM, CUST_EINT_EDGE_SENSITIVE);
	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY);
	mt65xx_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
	mt65xx_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, ISL29044_eint_func, 0);
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);	
#endif 

	ALSPS_REGISTER_INTERRPUT(ISL29044_eint_func);

    return 0;
}

/*----------------------------------------------------------------------------*/
static int ISL29044_init_client(struct i2c_client *client)
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);
	int err=0;
	APS_LOG("ISL29044_init_client.........\r\n");

	if((err = ISL29044_setup_eint(client)))
	{
		APS_ERR("setup eint: %d\n", err);
		return err;
	}
	
	
	if((err = ISL29044_init_device(client)))
	{
		APS_ERR("init dev: %d\n", err);
		return err;
	}

	
	return err;
}
/******************************************************************************
 * Sysfs attributes
*******************************************************************************/
static ssize_t ISL29044_show_config(struct device_driver *ddri, char *buf)
{
	ssize_t res;
	
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	res = snprintf(buf, PAGE_SIZE, "(%d %d %d %d %d)\n", 
		atomic_read(&ISL29044_obj->i2c_retry), atomic_read(&ISL29044_obj->als_debounce), 
		atomic_read(&ISL29044_obj->ps_mask), atomic_read(&ISL29044_obj->ps_thd_val), atomic_read(&ISL29044_obj->ps_debounce));     
	return res;    
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_store_config(struct device_driver *ddri, char *buf, size_t count)
{
	int retry, als_deb, ps_deb, mask, thres;
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	if(5 == sscanf(buf, "%d %d %d %d %d", &retry, &als_deb, &mask, &thres, &ps_deb))
	{ 
		atomic_set(&ISL29044_obj->i2c_retry, retry);
		atomic_set(&ISL29044_obj->als_debounce, als_deb);
		atomic_set(&ISL29044_obj->ps_mask, mask);
		atomic_set(&ISL29044_obj->ps_thd_val, thres);        
		atomic_set(&ISL29044_obj->ps_debounce, ps_deb);
	}
	else
	{
		APS_ERR("invalid content: '%s', length = %d\n", buf, count);
	}
	return count;    
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_trace(struct device_driver *ddri, char *buf)
{
	ssize_t res;
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}

	res = snprintf(buf, PAGE_SIZE, "0x%04X\n", atomic_read(&ISL29044_obj->trace));     
	return res;    
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_store_trace(struct device_driver *ddri, char *buf, size_t count)
{
    int trace;
    if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	if(1 == sscanf(buf, "0x%x", &trace))
	{
		atomic_set(&ISL29044_obj->trace, trace);
	}
	else 
	{
		APS_ERR("invalid content: '%s', length = %d\n", buf, count);
	}
	return count;    
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_als(struct device_driver *ddri, char *buf)
{
	int res;
	u8 dat = 0;
	
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	if(res = ISL29044_read_ALS_data(ISL29044_obj->client, &ISL29044_obj->als))
	{
		return snprintf(buf, PAGE_SIZE, "ERROR: %d\n", res);
	}
	else
	{   
		dat = ISL29044_obj->als & 0x3f;
		return snprintf(buf, PAGE_SIZE, "0x%04X\n", dat);     
	}
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_ps(struct device_driver *ddri, char *buf)
{
	ssize_t res;
	u8 dat=0;
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	if(res = ISL29044_read_PS_data(ISL29044_obj->client, &ISL29044_obj->ps))
	{
		return snprintf(buf, PAGE_SIZE, "ERROR: %d\n", res);
	}
	else
	{
		// Modified by chu, zewei on 2012/12/17
	    //dat = ISL29044_obj->ps & 0x80;
	    dat = ISL29044_obj->ps;
		return snprintf(buf, PAGE_SIZE, "0x%04X\n", dat);     
	}
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_reg(struct device_driver *ddri, char *buf)
{
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	/*read*/
	ISL29044_dumpReg(ISL29044_obj->client);
	
	return 0;
}

/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_status(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;
	
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	if(ISL29044_obj->hw)
	{
	
		len += snprintf(buf+len, PAGE_SIZE-len, "CUST: %d, (%d %d)\n", 
			ISL29044_obj->hw->i2c_num, ISL29044_obj->hw->power_id, ISL29044_obj->hw->power_vol);
		
	}
	else
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "CUST: NULL\n");
	}


	len += snprintf(buf+len, PAGE_SIZE-len, "MISC: %d %d\n", atomic_read(&ISL29044_obj->als_suspend), atomic_read(&ISL29044_obj->ps_suspend));

	return len;
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_i2c(struct device_driver *ddri, char *buf)
{
/*	ssize_t len = 0;
	u32 base = I2C2_BASE;

  

	return len;*/
	return 0;
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_store_i2c(struct device_driver *ddri, char *buf, size_t count)
{
/*	int sample_div, step_div;
	unsigned long tmp;
	u32 base = I2C2_BASE;    

	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	else if(2 != sscanf(buf, "%d %d", &sample_div, &step_div))
	{
		APS_ERR("invalid format: '%s'\n", buf);
		return 0;
	}
	tmp  = __raw_readw(mt6516_I2C_TIMING) & ~((0x7 << 8) | (0x1f << 0));
	tmp  = (sample_div & 0x7) << 8 | (step_div & 0x1f) << 0 | tmp;
	__raw_writew(tmp, mt6516_I2C_TIMING);        

	return count;
	*/
	return 0;
}
/*----------------------------------------------------------------------------*/
#define IS_SPACE(CH) (((CH) == ' ') || ((CH) == '\n'))
/*----------------------------------------------------------------------------*/
static int ISL29044_read_int_from_buf(struct ISL29044_priv *obj, const char* buf, size_t count,
                             u32 data[], int len)
{
	int idx = 0;
	char *cur = (char*)buf, *end = (char*)(buf+count);

	while(idx < len)
	{
		while((cur < end) && IS_SPACE(*cur))
		{
			cur++;        
		}

		if(1 != sscanf(cur, "%d", &data[idx]))
		{
			break;
		}

		idx++; 
		while((cur < end) && !IS_SPACE(*cur))
		{
			cur++;
		}
	}
	return idx;
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_alslv(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;
	int idx;
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	for(idx = 0; idx < ISL29044_obj->als_level_num; idx++)
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "%d ", ISL29044_obj->hw->als_level[idx]);
	}
	len += snprintf(buf+len, PAGE_SIZE-len, "\n");
	return len;    
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_store_alslv(struct device_driver *ddri, char *buf, size_t count)
{
	struct ISL29044_priv *obj;
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	else if(!strcmp(buf, "def"))
	{
		memcpy(ISL29044_obj->als_level, ISL29044_obj->hw->als_level, sizeof(ISL29044_obj->als_level));
	}
	else if(ISL29044_obj->als_level_num != ISL29044_read_int_from_buf(ISL29044_obj, buf, count, 
			ISL29044_obj->hw->als_level, ISL29044_obj->als_level_num))
	{
		APS_ERR("invalid format: '%s'\n", buf);
	}    
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_show_alsval(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;
	int idx;
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	
	for(idx = 0; idx < ISL29044_obj->als_value_num; idx++)
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "%d ", ISL29044_obj->hw->als_value[idx]);
	}
	len += snprintf(buf+len, PAGE_SIZE-len, "\n");
	return len;    
}
/*----------------------------------------------------------------------------*/
static ssize_t ISL29044_store_alsval(struct device_driver *ddri, char *buf, size_t count)
{
	if(!ISL29044_obj)
	{
		APS_ERR("ISL29044_obj is null!!\n");
		return 0;
	}
	else if(!strcmp(buf, "def"))
	{
		memcpy(ISL29044_obj->als_value, ISL29044_obj->hw->als_value, sizeof(ISL29044_obj->als_value));
	}
	else if(ISL29044_obj->als_value_num != ISL29044_read_int_from_buf(ISL29044_obj, buf, count, 
			ISL29044_obj->hw->als_value, ISL29044_obj->als_value_num))
	{
		APS_ERR("invalid format: '%s'\n", buf);
	}    
	return count;
}

/*----------------------------------------------------------------------------*/
static DRIVER_ATTR(isl_als,     S_IWUSR | S_IRUGO, ISL29044_show_als,   NULL);
static DRIVER_ATTR(isl_ps,      S_IWUSR | S_IRUGO, ISL29044_show_ps,    NULL);
static DRIVER_ATTR(isl_config,  S_IWUSR | S_IRUGO, ISL29044_show_config,ISL29044_store_config);
static DRIVER_ATTR(isl_alslv,   S_IWUSR | S_IRUGO, ISL29044_show_alslv, ISL29044_store_alslv);
static DRIVER_ATTR(isl_alsval,  S_IWUSR | S_IRUGO, ISL29044_show_alsval,ISL29044_store_alsval);
static DRIVER_ATTR(isl_trace,   S_IWUSR | S_IRUGO, ISL29044_show_trace, ISL29044_store_trace);
static DRIVER_ATTR(isl_status,  S_IWUSR | S_IRUGO, ISL29044_show_status,  NULL);
static DRIVER_ATTR(isl_reg,     S_IWUSR | S_IRUGO, ISL29044_show_reg,   NULL);
static DRIVER_ATTR(isl_i2c,     S_IWUSR | S_IRUGO, ISL29044_show_i2c,   ISL29044_store_i2c);
/*----------------------------------------------------------------------------*/
static struct driver_attribute *ISL29044_attr_list[] = {
    &driver_attr_isl_als,
    &driver_attr_isl_ps,    
    &driver_attr_isl_trace,        /*trace log*/
    &driver_attr_isl_config,
    &driver_attr_isl_alslv,
    &driver_attr_isl_alsval,
    &driver_attr_isl_status,
    &driver_attr_isl_i2c,
    &driver_attr_isl_reg,
};
/*----------------------------------------------------------------------------*/
static int ISL29044_create_attr(struct driver_attribute *driver) 
{
	int idx, err = 0;
	int num = (int)(sizeof(ISL29044_attr_list)/sizeof(ISL29044_attr_list[0]));

	if (driver == NULL)
	{
		return -EINVAL;
	}

	for(idx = 0; idx < num; idx++)
	{
		if(err = driver_create_file(driver, ISL29044_attr_list[idx]))
		{            
			APS_ERR("driver_create_file (%s) = %d\n", ISL29044_attr_list[idx]->attr.name, err);
			break;
		}
	}    
	return err;
}
/*----------------------------------------------------------------------------*/
	static int ISL29044_delete_attr(struct device_driver *driver)
	{
	int idx ,err = 0;
	int num = (int)(sizeof(ISL29044_attr_list)/sizeof(ISL29044_attr_list[0]));

	if (!driver)
	return -EINVAL;

	for (idx = 0; idx < num; idx++) 
	{
		driver_remove_file(driver, ISL29044_attr_list[idx]);
	}
	
	return err;
}
/****************************************************************************** 
 * Function Configuration
******************************************************************************/
static int ISL29044_get_als_value(struct ISL29044_priv *obj, u16 als)
{
	int idx;
	int invalid = 0;
	//APS_LOG("als =%x \n",als);

	for(idx = 0; idx < obj->als_level_num; idx++)
	{
		if(als < obj->hw->als_level[idx])
		{
			//APS_LOG("idx =%d, level =%d\n",idx,obj->hw->als_level[idx] );
			break;
		}
	}
	
	if(idx >= obj->als_value_num)
	{
		APS_ERR("exceed range\n"); 
		idx = obj->als_value_num - 1;
	}
	
	if(1 == atomic_read(&obj->als_deb_on))
	{
		unsigned long endt = atomic_read(&obj->als_deb_end);
		if(time_after(jiffies, endt))
		{
			atomic_set(&obj->als_deb_on, 0);
			//clear_bit(ISL_BIT_ALS, &obj->first_read);
		}
		
		if(1 == atomic_read(&obj->als_deb_on))
		{
			invalid = 1;
		}
	}

	if(!invalid)
	{
		if (atomic_read(&obj->trace) & ISL_TRC_CVT_ALS)
		{
			APS_DBG("ALS: %05d => %05d\n", als, obj->hw->als_value[idx]);
		}
		
		return obj->hw->als_value[idx];
	}
	else
	{
		if(atomic_read(&obj->trace) & ISL_TRC_CVT_ALS)
		{
			APS_DBG("ALS: %05d => %05d (-1)\n", als, obj->hw->als_value[idx]);    
		}
		return -1;
	}
}
/*----------------------------------------------------------------------------*/

static int ISL29044_get_ps_value(struct ISL29044_priv *obj, u8 ps)
{
  
    int val= -1;
	int invalid = 0;

	// Modified by chu, zewei on 2012/12/17
	if(ps > atomic_read(&obj->ps_thd_val))
//	if (ps & 0x80)
	{
		val = 0;  /*close*/
	}
	else
	{
		val = 1;  /*far away*/
	}
	
	if(atomic_read(&obj->ps_suspend))
	{
		invalid = 1;
	}
	else if(1 == atomic_read(&obj->ps_deb_on))
	{
		unsigned long endt = atomic_read(&obj->ps_deb_end);
		if(time_after(jiffies, endt))
		{
			atomic_set(&obj->ps_deb_on, 0);
			//clear_bit(ISL_BIT_PS, &obj->first_read);
		}
		
		if (1 == atomic_read(&obj->ps_deb_on))
		{
			invalid = 1;
		}
	}

	if(!invalid)
	{
		if(unlikely(atomic_read(&obj->trace) & ISL_TRC_CVT_PS))
		{
		   APS_DBG("PS:  %05d => %05d\n", ps, val);
		}
		return val;
		
	}	
	else
	{
		if(unlikely(atomic_read(&obj->trace) & ISL_TRC_CVT_PS))
		{
			APS_DBG("PS:  %05d => %05d (-1)\n", ps, val);    
		}
		return -1;
	}	
	
}

/****************************************************************************** 
 * Function Configuration
******************************************************************************/
static int ISL29044_open(struct inode *inode, struct file *file)
{
	file->private_data = isl29044_i2c_client;

	if (!file->private_data)
	{
		APS_ERR("null pointer!!\n");
		return -EINVAL;
	}
	
	return nonseekable_open(inode, file);
}
/*----------------------------------------------------------------------------*/
static int ISL29044_release(struct inode *inode, struct file *file)
{
	file->private_data = NULL;
	return 0;
}
/*----------------------------------------------------------------------------*/
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
static long ISL29044_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
#else
static int ISL29044_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)      
#endif
{
	struct i2c_client *client = (struct i2c_client*)file->private_data;
	struct ISL29044_priv *obj = i2c_get_clientdata(client);  
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
	long err = 0;
#else
	int err = 0;
#endif
	void __user *ptr = (void __user*) arg;
	int dat;
	uint32_t enable;

	switch (cmd)
	{
		case ALSPS_SET_PS_MODE:
			
			if(copy_from_user(&enable, ptr, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			if(enable)
			{
				if(err = ISL29044_enable_ps(obj->client, true))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("enable ps fail: %ld\n", err); 
#else
					APS_ERR("enable ps fail: %d\n", err); 
#endif
					goto err_out;
				}
				
				set_bit(ISL_BIT_PS, &obj->enable);
			}
			else
			{
				if(err = ISL29044_enable_ps(obj->client, false))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("disable ps fail: %ld\n", err); 
#else
					APS_ERR("disable ps fail: %d\n", err); 
#endif
	
					goto err_out;
				}
				
				clear_bit(ISL_BIT_PS, &obj->enable);
			}
			break;

		case ALSPS_GET_PS_MODE:
			
			enable = test_bit(ISL_BIT_PS, &obj->enable) ? (1) : (0);
			if(copy_to_user(ptr, &enable, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			break;

		case ALSPS_GET_PS_DATA:    
			
			if(err = ISL29044_read_PS_data(obj->client, &obj->ps))
			{
				goto err_out;
			}
			dat = ISL29044_get_ps_value(obj, obj->ps);
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}  
			break;

		case ALSPS_GET_PS_RAW_DATA:    
			
			if(err = ISL29044_read_PS_data(obj->client, &obj->ps))
			{
				goto err_out;
			}
			dat = obj->ps;
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}  
			break;            

		case ALSPS_SET_ALS_MODE:
			
			if(copy_from_user(&enable, ptr, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			if(enable)
			{
				if(err = ISL29044_enable_als(obj->client, true))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("enable als fail: %ld\n", err); 
#else
					APS_ERR("enable als fail: %d\n", err); 
#endif

					goto err_out;
				}
				set_bit(ISL_BIT_ALS, &obj->enable);
			}
			else
			{
				if(err = ISL29044_enable_als(obj->client, false))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("disable als fail: %ld\n", err); 
#else
					APS_ERR("disable als fail: %d\n", err); 
#endif

					goto err_out;
				}
				clear_bit(ISL_BIT_ALS, &obj->enable);
			}
			break;

		case ALSPS_GET_ALS_MODE:
			
			enable = test_bit(ISL_BIT_ALS, &obj->enable) ? (1) : (0);
			if(copy_to_user(ptr, &enable, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			break;

		case ALSPS_GET_ALS_DATA: 
			
			if(err = ISL29044_read_ALS_data(obj->client, &obj->als))
			{
				goto err_out;
			}

			dat = ISL29044_get_als_value(obj, obj->als);
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}              
			break;

		case ALSPS_GET_ALS_RAW_DATA:    
			
			if(err = ISL29044_read_ALS_data(obj->client, &obj->als))
			{
				goto err_out;
			}
			dat = obj->als ;

			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}              
			break;

		default:
			APS_ERR("%s not supported = 0x%04x", __FUNCTION__, cmd);
			err = -ENOIOCTLCMD;
			break;
	}

	err_out:
	return err;    
}
/*----------------------------------------------------------------------------*/
static struct file_operations ISL29044_fops = {
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	.owner = THIS_MODULE,
#endif
	.open = ISL29044_open,
	.release = ISL29044_release,
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
	.unlocked_ioctl = ISL29044_unlocked_ioctl,
#else
	.unlocked_ioctl = ISL29044_ioctl,
#endif

};
/*----------------------------------------------------------------------------*/
static struct miscdevice ISL29044_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "als_ps",
	.fops = &ISL29044_fops,
};
/*----------------------------------------------------------------------------*/
static int ISL29044_i2c_suspend(struct i2c_client *client, pm_message_t msg) 
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);    
	int err;
	APS_FUN();    
/*	if(msg.event == PM_EVENT_SUSPEND)
	{   
		if(!obj)
		{
			APS_ERR("null pointer!!\n");
			return -EINVAL;
		}
		
		atomic_set(&obj->als_suspend, 1);
		if(err = ISL29044_enable_als(client, false))
		{
			APS_ERR("disable als: %d\n", err);
			return err;
		}

		atomic_set(&obj->ps_suspend, 1);
		if(err = ISL29044_enable_ps(client, false))
		{
			APS_ERR("disable ps:  %d\n", err);
			return err;
		}
		
		ISL29044_power(obj->hw, 0);
	}*/
	return 0;
}
/*----------------------------------------------------------------------------*/
static int ISL29044_i2c_resume(struct i2c_client *client)
{
	struct ISL29044_priv *obj = i2c_get_clientdata(client);        
	int err;
	APS_FUN();
/*
	if(!obj)
	{
		APS_ERR("null pointer!!\n");
		return -EINVAL;
	}

	ISL29044_power(obj->hw, 1);
	if(err = ISL29044_init_client(client))
	{
		APS_ERR("initialize client fail!!\n");
		return err;        
	}
	atomic_set(&obj->als_suspend, 0);
	if(test_bit(ISL_BIT_ALS, &obj->enable))
	{
		if(err = ISL29044_enable_als(client, true))
		{
			APS_ERR("enable als fail: %d\n", err);        
		}
	}
	atomic_set(&obj->ps_suspend, 0);
	if(test_bit(ISL_BIT_PS,  &obj->enable))
	{
		if(err = ISL29044_enable_ps(client, true))
		{
			APS_ERR("enable ps fail: %d\n", err);                
		}
	}
*/
	return 0;
}
/*----------------------------------------------------------------------------*/
static void ISL29044_early_suspend(struct early_suspend *h) 
{   /*early_suspend is only applied for ALS*/
	struct ISL29044_priv *obj = container_of(h, struct ISL29044_priv, early_drv);   
	int err;
	APS_FUN();    

	if(!obj)
	{
		APS_ERR("null pointer!!\n");
		return;
	}
	
	atomic_set(&obj->als_suspend, 1);    
	if(err = ISL29044_enable_als(obj->client, false))
	{
		APS_ERR("disable als fail: %d\n", err); 
	}
}
/*----------------------------------------------------------------------------*/
static void ISL29044_late_resume(struct early_suspend *h)
{   /*early_suspend is only applied for ALS*/
	struct ISL29044_priv *obj = container_of(h, struct ISL29044_priv, early_drv);         
	int err;
	APS_FUN();

	if(!obj)
	{
		APS_ERR("null pointer!!\n");
		return;
	}

	atomic_set(&obj->als_suspend, 0);
	if(test_bit(ISL_BIT_ALS, &obj->enable))
	{
		if(err = ISL29044_enable_als(obj->client, true))
		{
			APS_ERR("enable als fail: %d\n", err);        

		}
	}
}

int ISL29044_ps_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
	int err = 0;
	int value;
	hwm_sensor_data* sensor_data;
	struct ISL29044_priv *obj = (struct ISL29044_priv *)self;
	
	APS_FUN(f);
	switch (command)
	{
		case SENSOR_DELAY:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Set delay parameter error!\n");
				err = -EINVAL;
			}
			// Do nothing
			break;

		case SENSOR_ENABLE:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Enable sensor parameter error!\n");
				err = -EINVAL;
			}
			else
			{				
				value = *(int *)buff_in;
				if(value)
				{
					if(err = ISL29044_enable_ps(obj->client, true))
					{
						APS_ERR("enable ps fail: %d\n", err); 
						return -1;
					}
					set_bit(ISL_BIT_PS, &obj->enable);
				}
				else
				{
					if(err = ISL29044_enable_ps(obj->client, false))
					{
						APS_ERR("disable ps fail: %d\n", err); 
						return -1;
					}
					clear_bit(ISL_BIT_PS, &obj->enable);
				}
			}
			break;

		case SENSOR_GET_DATA:
			//APS_LOG("fwq get ps data !!!!!!\n");
			if((buff_out == NULL) || (size_out< sizeof(hwm_sensor_data)))
			{
				APS_ERR("get sensor data parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				sensor_data = (hwm_sensor_data *)buff_out;				
				
				if(err = ISL29044_read_PS_data(obj->client, &obj->ps))
				{
					err = -1;;
				}
				else
				{
				    while(-1 == ISL29044_get_ps_value(obj, obj->ps))
				    {
				      ISL29044_read_PS_data(obj->client, &obj->ps);
				      msleep(50);
				    }
				   
					sensor_data->values[0] = ISL29044_get_ps_value(obj, obj->ps);
					sensor_data->value_divide = 1;
					sensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;
					//APS_LOG("fwq get ps data =%d\n",sensor_data->values[0]);
				    
					
				}				
			}
			break;
		default:
			APS_ERR("proxmy sensor operate function no this parameter %d!\n", command);
			err = -1;
			break;
	}
	
	return err;
}

int ISL29044_als_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
	int err = 0;
	int value;
	hwm_sensor_data* sensor_data;
	struct ISL29044_priv *obj = (struct ISL29044_priv *)self;
	
	//APS_FUN(f);
	switch (command)
	{
		case SENSOR_DELAY:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Set delay parameter error!\n");
				err = -EINVAL;
			}
			// Do nothing
			break;

		case SENSOR_ENABLE:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Enable sensor parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				value = *(int *)buff_in;				
				if(value)
				{
					if(err = ISL29044_enable_als(obj->client, true))
					{
						APS_ERR("enable als fail: %d\n", err); 
						return -1;
					}
					set_bit(ISL_BIT_ALS, &obj->enable);
				}
				else
				{
					if(err = ISL29044_enable_als(obj->client, false))
					{
						APS_ERR("disable als fail: %d\n", err); 
						return -1;
					}
					clear_bit(ISL_BIT_ALS, &obj->enable);
				}
				
			}
			break;

		case SENSOR_GET_DATA:
			//APS_LOG("fwq get als data !!!!!!\n");
			if((buff_out == NULL) || (size_out< sizeof(hwm_sensor_data)))
			{
				APS_ERR("get sensor data parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				sensor_data = (hwm_sensor_data *)buff_out;
								
				if(err = ISL29044_read_ALS_data(obj->client, &obj->als))
				{
					err = -1;;
				}
				else
				{
				    while(-1 == ISL29044_get_als_value(obj, obj->als))
				    {
				      ISL29044_read_ALS_data(obj->client, &obj->als);
				      msleep(50);
				    }
					sensor_data->values[0] = ISL29044_get_als_value(obj, obj->als);
					sensor_data->value_divide = 1;
					sensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;
				}				
			}
			break;
		default:
			APS_ERR("light sensor operate function no this parameter %d!\n", command);
			err = -1;
			break;
	}
	
	return err;
}


/*----------------------------------------------------------------------------*/
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
static int ISL29044_i2c_detect(struct i2c_client *client, int kind, struct i2c_board_info *info) 
{    
	strcpy(info->type, ISL29044_DEV_NAME);
	return 0;
}
#endif
/*----------------------------------------------------------------------------*/
static int ISL29044_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    APS_FUN();
	struct ISL29044_priv *obj;
	struct hwmsen_object obj_ps, obj_als;
	int err = 0;

	if(!(obj = kzalloc(sizeof(*obj), GFP_KERNEL)))
	{
		err = -ENOMEM;
		goto exit;
	}
	memset(obj, 0, sizeof(*obj));
	ISL29044_obj = obj;

	obj->hw = get_cust_alsps_hw(ISL29044_I2C_SLAVE_ADDR);
	
	INIT_DELAYED_WORK(&obj->eint_work, ISL29044_eint_work);
	obj->client = client;
	i2c_set_clientdata(client, obj);	
	atomic_set(&obj->als_debounce, 1000);
	atomic_set(&obj->als_deb_on, 0);
	atomic_set(&obj->als_deb_end, 0);
	atomic_set(&obj->ps_debounce, 1000);
	atomic_set(&obj->ps_deb_on, 0);
	atomic_set(&obj->ps_deb_end, 0);
	atomic_set(&obj->ps_mask, 0);
	atomic_set(&obj->trace, 0x00);
	atomic_set(&obj->als_suspend, 0);

	obj->ps_enable = 0;
	obj->als_enable = 0;
	obj->enable = 0;
	obj->pending_intr = 0;
	obj->als_level_num = sizeof(obj->hw->als_level)/sizeof(obj->hw->als_level[0]);
	obj->als_value_num = sizeof(obj->hw->als_value)/sizeof(obj->hw->als_value[0]);   
	BUG_ON(sizeof(obj->als_level) != sizeof(obj->hw->als_level));
	memcpy(obj->als_level, obj->hw->als_level, sizeof(obj->als_level));
	BUG_ON(sizeof(obj->als_value) != sizeof(obj->hw->als_value));
	memcpy(obj->als_value, obj->hw->als_value, sizeof(obj->als_value));
	atomic_set(&obj->i2c_retry, 3);
    //pre set ps threshold
	atomic_set(&obj->ps_thd_val , (obj->hw->ps_threshold_high - 0x08));

	// The below two are added by chu, zewei on 2012/12/17
	atomic_set(&obj->ps_threshold_high, obj->hw->ps_threshold_high);
	atomic_set(&obj->ps_threshold_low, obj->hw->ps_threshold_low);
	
	//pre set window loss
    obj->als_widow_loss = obj->hw->als_window_loss;
	
	isl29044_i2c_client = client;

	
	if(err = ISL29044_init_client(client))
	{
		goto exit_init_failed;
	}
	
	if(err = misc_register(&ISL29044_device))
	{
		APS_ERR("ISL29044_device register failed\n");
		goto exit_misc_device_register_failed;
	}

	if(err = ISL29044_create_attr(&ISL29044_alsps_driver.driver))
	{
		APS_ERR("create attribute err = %d\n", err);
		goto exit_create_attr_failed;
	}
	obj_ps.self = ISL29044_obj;
	if(1 == obj->hw->polling_mode_ps)
	{
	  obj_ps.polling = 1;
	}
	else
	{
	  obj_ps.polling = 0;//interrupt mode
	}
	obj_ps.sensor_operate = ISL29044_ps_operate;
	if(err = hwmsen_attach(ID_PROXIMITY, &obj_ps))
	{
		APS_ERR("attach fail = %d\n", err);
		goto exit_create_attr_failed;
	}
	
	obj_als.self = ISL29044_obj;
	if(1 == obj->hw->polling_mode_als)
	{
	  obj_als.polling = 1;
	  APS_LOG("polling mode\n");
	}
	else
	{
	  obj_als.polling = 0;//interrupt mode
	  APS_LOG("interrupt mode\n");
	}
	obj_als.sensor_operate = ISL29044_als_operate;
	if(err = hwmsen_attach(ID_LIGHT, &obj_als))
	{
		APS_ERR("attach fail = %d\n", err);
		goto exit_create_attr_failed;
	}


#if defined(CONFIG_HAS_EARLYSUSPEND)
	obj->early_drv.level    = EARLY_SUSPEND_LEVEL_DISABLE_FB - 1,
	obj->early_drv.suspend  = ISL29044_early_suspend,
	obj->early_drv.resume   = ISL29044_late_resume,    
	register_early_suspend(&obj->early_drv);
#endif

	APS_LOG("%s: OK\n", __func__);
	return 0;

	exit_create_attr_failed:
	misc_deregister(&ISL29044_device);
	exit_misc_device_register_failed:
	exit_init_failed:
	//i2c_detach_client(client);
	exit_kfree:
	kfree(obj);
	exit:
	isl29044_i2c_client = NULL;           
	APS_ERR("%s: err = %d\n", __func__, err);
	return err;
}
/*----------------------------------------------------------------------------*/
static int ISL29044_i2c_remove(struct i2c_client *client)
{
	int err;	
	
	if(err = ISL29044_delete_attr(&isl29044_i2c_driver.driver))
	{
		APS_ERR("ISL29044_delete_attr fail: %d\n", err);
	} 

	if(err = misc_deregister(&ISL29044_device))
	{
		APS_ERR("misc_deregister fail: %d\n", err);    
	}
	
	isl29044_i2c_client = NULL;
	i2c_unregister_device(client);
	kfree(i2c_get_clientdata(client));

	return 0;
}
/*----------------------------------------------------------------------------*/
static int ISL29044_probe(struct platform_device *pdev) 
{
	struct alsps_hw *hw = get_cust_alsps_hw(ISL29044_I2C_SLAVE_ADDR);
	

	ISL29044_power(hw, 1);    
	//ISL29044_get_addr(hw, &addr);
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	isl29044_force[0] = hw->i2c_num;
	isl29044_force[1] = addr.init;
#endif
	if(i2c_add_driver(&isl29044_i2c_driver))
	{
		APS_ERR("add driver error\n");
		return -1;
	} 
	return 0;
}
/*----------------------------------------------------------------------------*/
static int ISL29044_remove(struct platform_device *pdev)
{
	struct alsps_hw *hw = get_cust_alsps_hw(ISL29044_I2C_SLAVE_ADDR);
	APS_FUN();    
	ISL29044_power(hw, 0);    
	i2c_del_driver(&isl29044_i2c_driver);
	return 0;
}
/*----------------------------------------------------------------------------*/
static struct platform_driver ISL29044_alsps_driver = {
	.probe      = ISL29044_probe,
	.remove     = ISL29044_remove,    
	.driver     = {
		.name  = "als_ps",
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
		.owner = THIS_MODULE,
#endif
	}
};

/*----------------------------------------------------------------------------*/
#if 0
static int __init ISL29044_init(void)
{
	APS_FUN();
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(3,0,0))	
	i2c_register_board_info(0, &i2c_isl29044, 1);
#endif
	if(platform_driver_register(&ISL29044_alsps_driver))
	{
		APS_ERR("failed to register driver");
		return -ENODEV;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
static void __exit ISL29044_exit(void)
{
	APS_FUN();
	platform_driver_unregister(&ISL29044_alsps_driver);
}
/*----------------------------------------------------------------------------*/

module_init(ISL29044_init);
module_exit(ISL29044_exit);

MODULE_AUTHOR("MingHsien Hsieh");
MODULE_DESCRIPTION("ALSPS ISL29044 driver");
MODULE_LICENSE("GPL");
#endif
#endif // isl29044

// for stk31xx
#if 1 
#define STK31XX_DEV_NAME     		"stk31xx"

/*----------------------------------------------------------------------------*/
static struct i2c_client *stk31xx_i2c_client = NULL;
/*----------------------------------------------------------------------------*/
static const struct i2c_device_id stk31xx_i2c_id[] = {{STK31XX_DEV_NAME,0},{}};

#if (LINUX_VERSION_CODE>=KERNEL_VERSION(3,0,0))	
static struct i2c_board_info __initdata i2c_stk31xx={ I2C_BOARD_INFO("stk31xx", (STK31XX_I2C_SLAVE_ADDR>>1))};
#else
/*the adapter id & i2c address will be available in customization*/
static unsigned short stk31xx_force[] = {0x00, STK31XX_I2C_SLAVE_ADDR, I2C_CLIENT_END, I2C_CLIENT_END};
static const unsigned short *const stk31xx_forces[] = { stk31xx_force, NULL };
static struct i2c_client_address_data stk31xx_addr_data = { .forces = stk31xx_forces,};
#endif
/*----------------------------------------------------------------------------*/
static int stk31xx_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id); 
static int stk31xx_i2c_remove(struct i2c_client *client);
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
static int stk31xx_i2c_detect(struct i2c_client *client, int kind, struct i2c_board_info *info);
#endif
/*----------------------------------------------------------------------------*/
static int stk31xx_i2c_suspend(struct i2c_client *client, pm_message_t msg);
static int stk31xx_i2c_resume(struct i2c_client *client);
static int stk31xx_set_als_int_thd(struct i2c_client *client, u16 als_data_reg);


static struct stk31xx_priv *g_stk31xx_ptr = NULL;
static int is_near;
//static unsigned int gain_setting;

/*----------------------------------------------------------------------------*/
typedef enum {
    STK_TRC_ALS_DATA= 0x0001,
    STK_TRC_PS_DATA = 0x0002,
    STK_TRC_EINT    = 0x0004,
    STK_TRC_IOCTL   = 0x0008,
    STK_TRC_I2C     = 0x0010,
    STK_TRC_CVT_ALS = 0x0020,
    STK_TRC_CVT_PS  = 0x0040,
    STK_TRC_DEBUG   = 0x8000,
} STK_TRC;
/*----------------------------------------------------------------------------*/
typedef enum {
    STK_BIT_ALS    = 1,
    STK_BIT_PS     = 2,
} STK_BIT;

/*----------------------------------------------------------------------------*/
struct stk31xx_i2c_addr {    /*define a series of i2c slave address*/
    u8  status;        /*Interrupt status*/
    u8  init;       /*device initialization */
    u8  als_cmd;    /*ALS command*/
    u8  als_dat1;   /*ALS MSB*/
    u8  als_dat0;   /*ALS LSB*/
    u8  ps_cmd;     /*PS command*/
    u8  ps_dat;     /*PS data*/
	u8	ps_gain;	/*PS gain*/
    u8  ps_high_thd;     /*PS INT threshold*/
	u8  ps_low_thd;     /*PS INT threshold*/
	u8  als_high_thd1;	/*ALS INT threshold high*/
	u8  als_high_thd2;	/*ALS INT threshold high*/
	u8  als_low_thd1;	/*ALS INT threshold low*/
	u8  als_low_thd2;	/*ALS INT threshold low*/
	u8  sw_reset;		/*software reset*/
};
/*----------------------------------------------------------------------------*/
struct stk31xx_priv {
    struct alsps_hw  *hw;
    struct i2c_client *client;
    struct delayed_work  eint_work;

    /*i2c address group*/
    struct stk31xx_i2c_addr  addr;
    
    /*misc*/
    atomic_t    trace;
    atomic_t    i2c_retry;
    atomic_t    als_suspend;
    atomic_t    als_debounce;   /*debounce time after enabling als*/
    atomic_t    als_deb_on;     /*indicates if the debounce is on*/
    atomic_t    als_deb_end;    /*the jiffies representing the end of debounce*/
    atomic_t    ps_mask;        /*mask ps: always return far away*/
    atomic_t    ps_debounce;    /*debounce time after enabling ps*/
    atomic_t    ps_deb_on;      /*indicates if the debounce is on*/
    atomic_t    ps_deb_end;     /*the jiffies representing the end of debounce*/
    atomic_t    ps_suspend;


    /*data*/
    u16         als;
    u8          ps;
    u8          _align;
    u16         als_level_num;
    u16         als_value_num;
    u32         als_level[C_CUST_ALS_LEVEL-1];
    u32         als_value[C_CUST_ALS_LEVEL];

    atomic_t    als_cmd_val;    /*the cmd value can't be read, stored in ram*/
    atomic_t    ps_cmd_val;     /*the cmd value can't be read, stored in ram*/
    atomic_t    ps_high_thd_val;     /*the cmd value can't be read, stored in ram*/
    atomic_t    ps_low_thd_val;     /*the cmd value can't be read, stored in ram*/
    ulong       enable;         /*enable mask*/
    ulong       pending_intr;   /*pending interrupt*/
	atomic_t	ps_enable;
	atomic_t	als_enable;
	atomic_t	recv_reg;
    /*early suspend*/
#if defined(CONFIG_HAS_EARLYSUSPEND)
    struct early_suspend    early_drv;
#endif     

};
/*----------------------------------------------------------------------------*/
static struct i2c_driver stk31xx_i2c_driver = {	
	.probe      = stk31xx_i2c_probe,
	.remove     = stk31xx_i2c_remove,
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	.detect     = stk31xx_i2c_detect,
#endif
	.suspend    = stk31xx_i2c_suspend,
	.resume     = stk31xx_i2c_resume,
	.id_table   = stk31xx_i2c_id,
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	.address_data = &stk31xx_addr_data,
#endif
	.driver = {
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
		.owner          = THIS_MODULE,
#endif
		.name           = STK31XX_DEV_NAME,
	},
};

static struct stk31xx_priv *stk31xx_obj = NULL;
static struct platform_driver stk31xx_alsps_driver;
static int stk31xx_get_ps_value(struct stk31xx_priv *obj, u16 ps);
static int stk31xx_get_als_value(struct stk31xx_priv *obj, u16 als);
static int stk31xx_read_als(struct i2c_client *client, u16 *data);
static int stk31xx_read_ps(struct i2c_client *client, u8 *data);

struct wake_lock ps_lock;

/*----------------------------------------------------------------------------*/
int stk31xx_get_addr(struct alsps_hw *hw, struct stk31xx_i2c_addr *addr)
{
	if(!hw || !addr)
	{
		return -EFAULT;
	}
	addr->status   = STK_ALSPS_STATUS; 
	addr->als_cmd   = STK_ALS_CMD;         
	addr->als_dat1  = STK_ALS_DT1;
	addr->als_dat0  = STK_ALS_DT2;
	addr->ps_cmd    = STK_PS_CMD;
	addr->ps_high_thd    = STK_PS_THDH;
	addr->ps_low_thd    = STK_PS_THDL;
	addr->ps_dat    = STK_PS_DT;
	addr->ps_gain = STK_PS_GAIN;
	addr->als_high_thd1 = STK_ALS_THDH1;
	addr->als_high_thd2 = STK_ALS_THDH2;
	addr->als_low_thd1 = STK_ALS_THDL1;
	addr->als_low_thd2 = STK_ALS_THDL2;	
	addr->sw_reset = STK_SW_RESET;	
	
	return 0;
}

/*----------------------------------------------------------------------------*/
int stk31xx_get_timing(void)
{
return 200;
/*
	u32 base = I2C2_BASE; 
	return (__raw_readw(mt6516_I2C_HS) << 16) | (__raw_readw(mt6516_I2C_TIMING));
*/
}
/*----------------------------------------------------------------------------*/
/*
int stk31xx_config_timing(int sample_div, int step_div)
{
	u32 base = I2C2_BASE; 
	unsigned long tmp;

	tmp  = __raw_readw(mt6516_I2C_TIMING) & ~((0x7 << 8) | (0x1f << 0));
	tmp  = (sample_div & 0x7) << 8 | (step_div & 0x1f) << 0 | tmp;

	return (__raw_readw(mt6516_I2C_HS) << 16) | (tmp);
}
*/
/*----------------------------------------------------------------------------*/
int stk31xx_master_recv(struct i2c_client *client, u16 addr, u8 *buf ,int count)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	//struct i2c_adapter *adap = client->adapter;
	//struct i2c_msg msg;
	int ret = 0, retry = 0;
	int trc = atomic_read(&obj->trace);
	int max_try = atomic_read(&obj->i2c_retry);

	while(retry++ < max_try)
	{
		ret = hwmsen_read_block(client, addr, buf, count);
		if(ret == 0)
            break;
		udelay(100);
	}

	if(unlikely(trc))
	{
/*
		if(trc & STK_TRC_I2C)
		{
			APS_LOG("(recv) %x %d %d %p [%02X]\n", msg.addr, msg.flags, msg.len, msg.buf, msg.buf[0]);    
		}
*/
		if((retry != 1) && (trc & STK_TRC_DEBUG))
		{
			APS_LOG("(recv) %d/%d\n", retry-1, max_try); 

		}
	}

	/* If everything went ok (i.e. 1 msg transmitted), return #bytes
	transmitted, else error code. */
	return (ret == 0) ? count : ret;
}
/*----------------------------------------------------------------------------*/
int stk31xx_master_send(struct i2c_client *client, u16 addr, u8 *buf ,int count)
{
	int ret = 0, retry = 0;
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	//struct i2c_adapter *adap=client->adapter;
	//struct i2c_msg msg;
	int trc = atomic_read(&obj->trace);
	int max_try = atomic_read(&obj->i2c_retry);


	while(retry++ < max_try)
	{
		ret = hwmsen_write_block(client, addr, buf, count);
		if (ret == 0)
		    break;
		udelay(100);
	}

	if(unlikely(trc))
	{
/*
		if(trc & STK_TRC_I2C)
		{
			APS_LOG("(send) %x %d %d %p [%02X]\n", msg.addr, msg.flags, msg.len, msg.buf, msg.buf[0]);    
		}
*/
		if((retry != 1) && (trc & STK_TRC_DEBUG))
		{
			APS_LOG("(send) %d/%d\n", retry-1, max_try);
		}
	}
	/* If everything went ok (i.e. 1 msg transmitted), return #bytes
	transmitted, else error code. */
	return (ret == 0) ? count : ret;
}


static void stk31xx_dump_reg(struct i2c_client *client)
{
  int i=0;
  u8 addr = 0x01;
  u8 regdata=0;
  for(i=1; i<15 ; i++)
  {
    //dump all
    //hwmsen_read_byte_sr(client,addr,&regdata);
    stk31xx_master_recv(client, addr, &regdata, 1);
	APS_LOG("Reg. 0x%x, data = 0x%x\n",addr,regdata);
	addr++;
	
	if(addr > 0x0C)
	{
		stk31xx_master_recv(client, 0x80, &regdata, 1);
		APS_LOG("Reg. 0x%x, data = 0x%x\n",0x80,regdata);
		
		stk31xx_master_recv(client, 0x82, &regdata, 1);
		APS_LOG("Reg. 0x%x, data = 0x%x\n",0x82,regdata);
		break;
	}
  }
}

/*----------------------------------------------------------------------------*/
int stk31xx_read_als(struct i2c_client *client, u16 *data)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);    
	int ret = 0;
	u8 buf[2];

	if(1 != (ret = stk31xx_master_recv(client, obj->addr.als_dat1, (char*)&buf[1], 1)))
	{
		APS_DBG("reads als data1 = %d\n", ret);
		return -EFAULT;
	}
	else if(1 != (ret = stk31xx_master_recv(client, obj->addr.als_dat0, (char*)&buf[0], 1)))
	{
		APS_DBG("reads als data2 = %d\n", ret);
		return -EFAULT;
	}
	
	*data = (buf[1] << 8) | (buf[0]);
	if(atomic_read(&obj->trace) & STK_TRC_ALS_DATA)
	{
		APS_DBG("stk31xx_read_als@@ALS_DATA: 0x%04x\n", (u32)(*data));
	}
	
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_write_als(struct i2c_client *client, u8 data)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);
	int ret = 0;
    
    ret = stk31xx_master_send(client, obj->addr.als_cmd, &data, 1);
	if(ret < 0)
	{
		APS_ERR("write als = %d\n", ret);
		return -EFAULT;
	}
	
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_read_ps(struct i2c_client *client, u8 *data)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);    
	int ret = 0;

	if(sizeof(*data) != (ret = stk31xx_master_recv(client, obj->addr.ps_dat, (char*)data, sizeof(*data))))
	{
		APS_ERR("reads ps data = %d\n", ret);
		return -EFAULT;
	} 

	if(atomic_read(&obj->trace) & STK_TRC_PS_DATA)
	{
		APS_DBG("stk31xx_read_ps@@PS_DATA:  0x%02x\n", (*data));
	}
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_write_ps(struct i2c_client *client, u8 data)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	int ret = 0;

    ret = stk31xx_master_send(client, obj->addr.ps_cmd, &data, 1);
	if (ret < 0)
	{
		APS_ERR("write ps = %d\n", ret);
		return -EFAULT;
	} 
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_read_ps_gain(struct i2c_client *client, u8 *data)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);    
	int ret = 0;

	if(sizeof(*data) != (ret = stk31xx_master_recv(client, obj->addr.ps_gain, (char*)data, sizeof(*data))))
	{
		APS_ERR("reads ps gain = %d\n", ret);
		return -EFAULT;
	} 

	APS_DBG("PS gain:  0x%04X\n", (u32)(*data));

	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_write_sw_reset(struct i2c_client *client)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	u8 buf = 0;	
	int ret = 0;

	// Do SW-Reset after power on by writing ANY value to the register 0x80
    ret = stk31xx_master_send(client, obj->addr.sw_reset, (char*)&buf, sizeof(buf));
	if (ret < 0)
	{
		APS_ERR("write software reset error = %d\n", ret);
		return -EFAULT;
	} 
	msleep(5);
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_write_ps_gain(struct i2c_client *client, u8 data)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	int ret = 0;

    ret = stk31xx_master_send(client, obj->addr.ps_gain, &data, 1);
	if (ret < 0)
	{
		APS_ERR("write ps gain = %d\n", ret);
		return -EFAULT;
	} 
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_write_ps_high_thd(struct i2c_client *client, u8 thd)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	u8 buf = thd;
	int ret = 0;

	if(sizeof(buf) != (ret = stk31xx_master_send(client, obj->addr.ps_high_thd, (char*)&buf, sizeof(buf))))
	{
		APS_ERR("write thd = %d\n", ret);
		return -EFAULT;
	} 
	return 0;    
}
/*----------------------------------------------------------------------------*/
int stk31xx_write_ps_low_thd(struct i2c_client *client, u8 thd)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        
	u8 buf = thd;
	int ret = 0;

	if(sizeof(buf) != (ret = stk31xx_master_send(client, obj->addr.ps_low_thd, (char*)&buf, sizeof(buf))))
	{
		APS_ERR("write thd = %d\n", ret);
		return -EFAULT;
	} 
	return 0;    
}
/*----------------------------------------------------------------------------*/
static void stk31xx_power(struct alsps_hw *hw, unsigned int on) 
{
	static unsigned int power_on = 0;

	//APS_LOG("power %s\n", on ? "on" : "off");

	if(hw->power_id != POWER_NONE_MACRO)
	{
		if(power_on == on)
		{
			APS_LOG("ignore power control: %d\n", on);
		}
		else if(on)
		{
			if(!hwPowerOn(hw->power_id, hw->power_vol, "stk31xx")) 
			{
				APS_ERR("power on fails!!\n");
			}
		}
		else
		{
			if(!hwPowerDown(hw->power_id, "stk31xx")) 
			{
				APS_ERR("power off fail!!\n");   
			}
		}
	}

	//Turn on/off the VMC power to supply for IR LED
	#ifdef STK31XX_POWER_IR_LED
	if (on)
	{
		if(!hwPowerOn(STK31XX_POWER_IR_LED, VOL_2800, "stk3111")) 
		{
			APS_ERR("Turning on VMC fails!\n");
		}
	}
	else
	{
		if(!hwPowerDown(STK31XX_POWER_IR_LED, "stk3111")) 
		{
			APS_ERR("Turning off fails!\n");   
		}
	}
	#endif
	power_on = on;
}

/*----------------------------------------------------------------------------*/
static int stk31xx_enable_als(struct i2c_client *client, u8 enable)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);
	int err, cur = 0, old = atomic_read(&obj->als_cmd_val);
	int trc = atomic_read(&obj->trace);
	u8 als_enable = atomic_read(&obj->als_enable);
	//hwm_sensor_data sensor_data;

	APS_LOG("%s: enable=%d\n", __func__, enable);
	//if (enable == (STK_BIT_ALS & obj->enable))
	if (enable == als_enable)
	{
		return 0;
	}
	
	if(enable)
	{
		cur = old & (~STK_SD_ALS);   
		set_bit(STK_BIT_ALS,  &obj->enable);
	}
	else
	{
		cur = old | (STK_SD_ALS); 
		clear_bit(STK_BIT_ALS,  &obj->enable);
	}
	
	if(trc & STK_TRC_DEBUG)
	{
		APS_LOG("%s: %08X, %08X, %d\n", __func__, cur, old, enable);
	}
	/*
	if(0 == (cur ^ old))
	{
		return 0;
	}
	*/
	
	if(0 == (err = stk31xx_write_als(client, cur))) 
	{
		atomic_set(&obj->als_cmd_val, cur);

		if (enable)
		{
			atomic_set(&obj->als_enable, 1);
		}
		else
		{
			atomic_set(&obj->als_enable, 0);
		}
	}
	
	if(enable)
	{
		atomic_set(&obj->als_deb_on, 1);
		atomic_set(&obj->als_deb_end, jiffies+atomic_read(&obj->als_debounce)/(1000/HZ));
		
		set_bit(STK_BIT_ALS,  &obj->pending_intr);
		schedule_delayed_work(&obj->eint_work, 230 /*360*HZ/1000*/); //after enable the value is not accurate				
	}

	if(trc & STK_TRC_DEBUG)
	{
		APS_LOG("enable als (%d)\n", enable);
	}

	return err;
}
/*----------------------------------------------------------------------------*/
static int stk31xx_enable_ps(struct i2c_client *client, u8 enable)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);
	int err, cur = 0, old = atomic_read(&obj->ps_cmd_val);
	int trc = atomic_read(&obj->trace);
	u8  ps_enable = atomic_read(&obj->ps_enable);
	
#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI))
	int16_t ps_thd_h = 150, ps_thd_l = 140;
	int16_t	thd_auto[2] = {150, 140};
	
	if(obj->first_boot)
	{		
		obj->first_boot = 0;
		err = stk31xx_get_ps_cali_thd(&ps_thd_h, &ps_thd_l);		
		if(err == 0)
		{			
			APS_LOG("%s : read PS calibration data from file, ps_thd_l=%d, ps_thd_h=%d\n", __func__, ps_thd_l, ps_thd_h);	
			thd_auto[STK_HIGH_THD] = ps_thd_h;
			thd_auto[STK_LOW_THD] = ps_thd_l;			
			stk31xx_set_ps_thd_to_driver(thd_auto);						
		}
		else
			APS_ERR("%s: threshold is too large!\n", __func__);
					
	}
#endif	//	#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI))	

#ifdef STK_AUTO_CT_CALI_SATU	
	if(stk31xx_obj->redetect_ct_en && enable)
	{
		stk31xx_set_ps_cali();
	}
#endif
	
	APS_LOG("%s: enable=%d\n", __func__, enable);	

	//if (enable == ((STK_BIT_PS & obj->enable) >> 1))
	if (enable == ps_enable)
	{
		return 0;
	}
	
	if(enable)
	{
		cur = old & (~STK_SD_PS);   
		set_bit(STK_BIT_PS, &obj->enable);
	}
	else
	{
		cur = old | (STK_SD_PS);
		clear_bit(STK_BIT_PS, &obj->enable);
	}
	
	if(trc & STK_TRC_DEBUG)
	{
		APS_LOG("%s: %08X, %08X, %d\n", __func__, cur, old, enable);
	}
	/*
	if(0 == (cur ^ old))
	{
		return 0;
	}
	*/
	
	if(0 == (err = stk31xx_write_ps(client, cur))) 
	{
		atomic_set(&obj->ps_cmd_val, cur);

		if (enable)
		{
			atomic_set(&obj->ps_enable, 1);
		}
		else
		{
			atomic_set(&obj->ps_enable, 0);
		}
	}
	
	if(enable)
	{
		atomic_set(&obj->ps_deb_on, 1);
		atomic_set(&obj->ps_deb_end, jiffies+atomic_read(&obj->ps_debounce)/(1000/HZ));
		
		set_bit(STK_BIT_PS,  &obj->pending_intr);
		schedule_delayed_work(&obj->eint_work, 110 /*110*HZ/1000*/);	// wait 110 ms
	}

	if(trc & STK_TRC_DEBUG)
	{
		APS_LOG("enable ps  (%d)\n", enable);
	}

	return err;
}
/*----------------------------------------------------------------------------*/
static int stk31xx_check_intr(struct i2c_client *client, u8 *status) 
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);
	int err;

	//if (mt_get_gpio_in(GPIO_ALS_EINT_PIN) == 1) /*skip if no interrupt*/  
	//    return 0;

    err = stk31xx_master_recv(client, obj->addr.status, status, 1);
	if (err < 0)
	{
		APS_ERR("WARNING: read status error: %d\n", err);
		return -EFAULT;
	}
	APS_LOG("stk31xx_check_intr: read status reg: 0x%x\n", *status);

	// Bit 4: ALS interrupt flag. = 1: interrupt is trggered; 0: Interrupt is cleared or not trggered yet
	if(*status & 0x10)
	{
		set_bit(STK_BIT_ALS, &obj->pending_intr);
	}
	else
	{
	   clear_bit(STK_BIT_ALS, &obj->pending_intr);
	}

	// Bit 5: PS interrupt flag.
	if(*status & 0x20)
	{
		set_bit(STK_BIT_PS,  &obj->pending_intr);
	}
	else
	{
	    clear_bit(STK_BIT_PS, &obj->pending_intr);
	}
	
	if(atomic_read(&obj->trace) & STK_TRC_DEBUG)
	{
		APS_LOG("check intr: 0x%02X => 0x%08lX\n", status, obj->pending_intr);
	}

	return 0;
}

static int stk31xx_clear_intr(struct i2c_client *client, u8 status, u8 disable_flag) 
{
    struct stk31xx_priv *obj = i2c_get_clientdata(client);
    int err;

	status &= (STK_PS_INT_FLAG | STK_ALS_INT_FLAG);		// 0x30;
    status &= (~disable_flag);
	
	APS_LOG("stk31xx_clear_intr: set status reg: 0x%x\n", status);
    err = stk31xx_master_send(client, obj->addr.status, &status, 1);    
    if (err < 0)
    {
		APS_ERR("ERROR: stk31xx_clear_intr clear intrrupt fail: %d\n", err);
		return -EFAULT;
	}

    return 0;
}

/*----------------------------------------------------------------------------*/
/*
static u16 stk31xx_lux2alscode(u16 input_lux)
{
	u32 ratio;
	u32 output_code;

	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	ratio = (stk31xx_obj->hw->als_value[10] << 10 ) / stk31xx_obj->hw->als_level[10];
	output_code = (input_lux<<10) / ratio;
	
	if (unlikely(output_code>=(1<<16)))
        output_code = (1<<16) -1;
	return (u16)output_code;
}
*/
/*----------------------------------------------------------------------------*/
static int stk31xx_set_als_int_thd(struct i2c_client *client, u16 als_data_reg) 
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);
	int err;
	u32 als_thd_h, als_thd_l;
	u8 buf[2] = {0,0};		
		
    als_thd_h = als_data_reg + STK_CONFIG_STK_ALS_CHANGE_THRESHOLD;
    als_thd_l = als_data_reg - STK_CONFIG_STK_ALS_CHANGE_THRESHOLD;
    if (als_thd_h >= (1<<16))
        als_thd_h = (1<<16) -1;
    if (als_thd_l <0)
        als_thd_l = 0;
	APS_LOG("stk31xx_set_als_int_thd:als_thd_h:%d,als_thd_l:%d\n", als_thd_h, als_thd_l);	
		
    buf[0] = (u8) ((0xFF00 & als_thd_h) >> 8);
    buf[1] = (u8) (0x00FF & als_thd_h);
	//APS_LOG("%s:als_thd_h, buf[0]=0x%x, buf[1]=0x%x\n", __func__, buf[0], buf[1]);
    err = stk31xx_master_send(client, obj->addr.als_high_thd1, &buf[0], 1);
	if (err < 0)
	{
		APS_ERR("WARNING: %s: %d\n", __func__, err);
		return 0;
	}

    err = stk31xx_master_send(client, obj->addr.als_high_thd2, &(buf[1]), 1);
	if (err < 0)
	{
		APS_ERR("WARNING: %s: %d\n", __func__, err);
		return 0;
	}
	
    buf[0] = (u8) ((0xFF00 & als_thd_l) >> 8);
    buf[1] = (u8) (0x00FF & als_thd_l);
	//APS_LOG("%s:als_thd_l, buf[0]=0x%x, buf[1]=0x%x\n", __func__, buf[0], buf[1]);	
    err = stk31xx_master_send(client, obj->addr.als_low_thd1, &buf[0], 1);
	if (err < 0)
	{
		APS_ERR("WARNING: %s: %d\n", __func__, err);
		return 0;
	}	

    err = stk31xx_master_send(client, obj->addr.als_low_thd2, &(buf[1]), 1);
	if (err < 0)
	{
		APS_ERR("WARNING: %s: %d\n", __func__, err);
		return 0;
	}	

	return 0;

}

/*----------------------------------------------------------------------------*/
void stk31xx_eint_func(void)
{
	struct stk31xx_priv *obj = g_stk31xx_ptr;
	APS_FUN();
	if(!obj)
	{
		return;
	}
	
	//schedule_work(&obj->eint_work);
	schedule_delayed_work(&obj->eint_work,0);
	if(atomic_read(&obj->trace) & STK_TRC_EINT)
	{
		APS_LOG("stk31xx_eint_func@@ als/ps intrs\n");
	}
}
/*----------------------------------------------------------------------------*/
static void stk31xx_eint_work(struct work_struct *work)
{
	struct stk31xx_priv *obj = g_stk31xx_ptr;
	int err;
	hwm_sensor_data sensor_data;
	u8 int_status,disable_flag = 0;

	memset(&sensor_data, 0, sizeof(sensor_data));

	APS_FUN();
	stk31xx_dump_reg(obj->client);
	if((err = stk31xx_check_intr(obj->client, &int_status)))
	{
		APS_ERR("check intrs fail: %d\n", err);
		msleep(30);
		#ifdef MT6516
		MT6516_EINTIRQUnmask(CUST_EINT_ALS_NUM);      
		#endif     
		#ifdef MT6573
		mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
		#endif
		#ifdef MT6575
		mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
		#endif			
		#ifdef MT6577
		mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
		#endif			
		return;
	}

    APS_LOG("stk31xx_eint_work@@ &obj->pending_intr = 0x%x\n",obj->pending_intr);
	
	if(((1<<STK_BIT_ALS) & obj->pending_intr) && (0 == obj->hw->polling_mode_als))
	{
		//get raw data
		APS_LOG("stk31xx_eint_work@@ALS_EINT\n");
		disable_flag |= STK_ALS_INT_FLAG;	// 0x10;
		if((err = stk31xx_read_als(obj->client, &obj->als)))
		{
			APS_ERR("stk31xx read als data: %d\n", err);
			msleep(30);
			#ifdef MT6516
			MT6516_EINTIRQUnmask(CUST_EINT_ALS_NUM);      
			#endif     
			#ifdef MT6573
			mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
			#endif
			#ifdef MT6575
			mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
			#endif			
			#ifdef MT6577
			mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
			#endif			 
			return;
		}
		stk31xx_set_als_int_thd(obj->client, obj->als);
		
		//map and store data to hwm_sensor_data
		sensor_data.values[0] = stk31xx_get_als_value(obj, obj->als);
		sensor_data.value_divide = 1;
		sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
		APS_LOG("%s:als raw 0x%x -> value 0x%x \n", __func__, obj->als,sensor_data.values[0]);
		//let up layer to know
		if((err = hwmsen_get_interrupt_data(ID_LIGHT, &sensor_data)))
		{
			APS_ERR("call hwmsen_get_interrupt_data fail = %d\n", err);
		}	  
	}
	
	if((1<<STK_BIT_PS) &  obj->pending_intr)
	{
	  	//get raw data
		APS_LOG("stk31xx_eint_work@@PS_EINT\n");
		disable_flag |= STK_PS_INT_FLAG;	// 0x20;
		if((err = stk31xx_read_ps(obj->client, &obj->ps)))
		{
			APS_ERR("stk31xx read ps data: %d\n", err);
			msleep(30);
			#ifdef MT6516
			MT6516_EINTIRQUnmask(CUST_EINT_ALS_NUM);      
			#endif     
			#ifdef MT6573
			mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
			#endif
			#ifdef MT6575
			 mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
			#endif			
			#ifdef MT6577
			mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
			#endif			 
			return;
		}
		//map and store data to hwm_sensor_data
		sensor_data.values[0] = stk31xx_get_ps_value(obj, obj->ps);
		sensor_data.value_divide = 1;
		sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
				
		//let up layer to know
		if((err = hwmsen_get_interrupt_data(ID_PROXIMITY, &sensor_data)))
		{	
			APS_ERR("call hwmsen_get_interrupt_data fail = %d\n", err);
		}
		APS_LOG("stk31xx_eint_work@@ ps raw 0x%x -> value 0x%x \n", obj->ps, sensor_data.values[0]);
	}
	
	if((err = stk31xx_clear_intr(obj->client, int_status, disable_flag)))
	{
		APS_ERR("stk31xx_clear_intr fail: %d\n", err);
		msleep(30);
	}	
	
	msleep(1);
	#ifdef MT6516
	MT6516_EINTIRQUnmask(CUST_EINT_ALS_NUM);      
	#endif     
	#ifdef MT6573
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
	#endif
	#ifdef MT6575
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
	#endif
	#ifdef MT6577
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);      
	#endif
}
/*----------------------------------------------------------------------------*/
int stk31xx_setup_eint(struct i2c_client *client)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);        

	g_stk31xx_ptr = obj;
	/*configure to GPIO function, external interrupt*/

  	printk("ALSPS interrupt pin = %d\n", GPIO_ALS_EINT_PIN);		
	
	mt_set_gpio_mode(GPIO_ALS_EINT_PIN, GPIO_ALS_EINT_PIN_M_EINT);
    mt_set_gpio_dir(GPIO_ALS_EINT_PIN, GPIO_DIR_IN);
	mt_set_gpio_pull_enable(GPIO_ALS_EINT_PIN, GPIO_PULL_ENABLE);
	mt_set_gpio_pull_select(GPIO_ALS_EINT_PIN, GPIO_PULL_UP);
	
	//mt_set_gpio_dir(GPIO_ALS_EINT_PIN, GPIO_DIR_IN);
	//mt_set_gpio_mode(GPIO_ALS_EINT_PIN, GPIO_ALS_EINT_PIN_M_EINT);
	//mt_set_gpio_pull_enable(GPIO_ALS_EINT_PIN, GPIO_PULL_ENABLE);
	//mt_set_gpio_pull_select(GPIO_ALS_EINT_PIN, GPIO_PULL_UP);

#ifdef MT6516

	MT6516_EINT_Set_Sensitivity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_SENSITIVE);
	MT6516_EINT_Set_Polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY);
	MT6516_EINT_Set_HW_Debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
	MT6516_EINT_Registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, stk31xx_eint_func, 0);
	MT6516_EINTIRQUnmask(CUST_EINT_ALS_NUM);  
#endif
    //
#ifdef MT6573
	
    mt65xx_eint_set_sens(CUST_EINT_ALS_NUM, CUST_EINT_ALS_SENSITIVE);
	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY);
	mt65xx_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
	mt65xx_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, stk31xx_eint_func, 0);
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);  
#endif  

#ifdef MT6575
	
    mt65xx_eint_set_sens(CUST_EINT_ALS_NUM, CUST_EINT_ALS_SENSITIVE);
	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY);
	mt65xx_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
	mt65xx_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, stk31xx_eint_func, 0);
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);  
#endif  

#ifdef MT6577
	
    mt65xx_eint_set_sens(CUST_EINT_ALS_NUM, CUST_EINT_ALS_SENSITIVE);
	mt65xx_eint_set_polarity(CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY);
	mt65xx_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
	mt65xx_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_POLARITY, stk31xx_eint_func, 0);
	mt65xx_eint_unmask(CUST_EINT_ALS_NUM);  
#endif  
	
    return 0;
	
}
/*----------------------------------------------------------------------------*/
static int stk31xx_init_client(struct i2c_client *client)
{
	struct stk31xx_priv *obj = i2c_get_clientdata(client);
	int err;
	u8 gain;
	u8 int_status;
	
	if((err = stk31xx_write_sw_reset(client)))
	{
		APS_ERR("software reset error, err=%d", err);
		return err;
	}
	
	if((err = stk31xx_setup_eint(client)))
	{
		APS_ERR("setup eint: %d\n", err);
		return err;
	}

	if((err = stk31xx_check_intr(client, &int_status)))
	{
		APS_ERR("check intr: %d\n", err);
		//    return err;
	}
	
	if((err = stk31xx_clear_intr(client, int_status, 0x30)))
	{
		APS_ERR("clear intr: %d\n", err);	
	}

	if((err = stk31xx_write_als(client, atomic_read(&obj->als_cmd_val))))
	{
		APS_ERR("write als: %d\n", err);
		return err;
	}
	
	if((err = stk31xx_write_ps(client, atomic_read(&obj->ps_cmd_val))))
	{
		APS_ERR("write ps: %d\n", err);
		return err;        
	}
	
	if((err = stk31xx_write_ps_high_thd(client, atomic_read(&obj->ps_high_thd_val))))
	{
		APS_ERR("write thd: %d\n", err);
		return err;        
	}
	
	if((err = stk31xx_write_ps_low_thd(client, atomic_read(&obj->ps_low_thd_val))))
	{
		APS_ERR("write thd: %d\n", err);
		return err;        
	}
	
	/*Read PS Gain Register*/
	if((err = stk31xx_read_ps_gain(client, &gain)))
	{
		APS_ERR("read ps gain: %d\n", err);
		return err;
	}
	
	//gain = (gain&0xF0)|gain_setting; //Setting PS Gain
	gain = (gain & 0xF0) | STK_PS_GAIN_DATA;
	printk("PS GAIN SETTING = 0x%x\n",STK_PS_GAIN_DATA);
	
	/*Write PS Gain into reg. 0x82*/
	if((err = stk31xx_write_ps_gain(client, gain)))
	{
		APS_ERR("write ps gain: %d\n", err);
		return err;
	}
	/*Set PS Gain END*/

	// Test: enable PS and ALS
	if ((err = stk31xx_enable_ps(client, 1)))
	{
		APS_ERR("enable PS failed: %d\n", err);
		return err;        
	}
	if ((err = stk31xx_enable_als(client, 1)))
	{
		APS_ERR("enable ALS failed: %d\n", err);
		return err;        
	}
	return 0;
}

/******************************************************************************
 * Sysfs attributes
*******************************************************************************/
static ssize_t stk31xx_show_config(struct device_driver *ddri, char *buf)
{
	ssize_t res;
	
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	res = snprintf(buf, PAGE_SIZE, "(%d %d %d %d %d %d)\n", 
		atomic_read(&stk31xx_obj->i2c_retry), atomic_read(&stk31xx_obj->als_debounce), 
		atomic_read(&stk31xx_obj->ps_mask), atomic_read(&stk31xx_obj->ps_high_thd_val),atomic_read(&stk31xx_obj->ps_low_thd_val), atomic_read(&stk31xx_obj->ps_debounce));     
	return res;    
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_store_config(struct device_driver *ddri, const char *buf, size_t count)
{
	int retry, als_deb, ps_deb, mask, hthres, lthres;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	if(6 == sscanf(buf, "%d %d %d %d %d %d", &retry, &als_deb, &mask, &hthres, &lthres, &ps_deb))
	{ 
		atomic_set(&stk31xx_obj->i2c_retry, retry);
		atomic_set(&stk31xx_obj->als_debounce, als_deb);
		atomic_set(&stk31xx_obj->ps_mask, mask);
		atomic_set(&stk31xx_obj->ps_high_thd_val, hthres);    
		atomic_set(&stk31xx_obj->ps_low_thd_val, lthres);        
		atomic_set(&stk31xx_obj->ps_debounce, ps_deb);
	}
	else
	{
		APS_ERR("invalid content: '%s', length = %d\n", buf, count);
	}
	return count;    
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_trace(struct device_driver *ddri, char *buf)
{
	ssize_t res;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}

	res = snprintf(buf, PAGE_SIZE, "0x%04X\n", atomic_read(&stk31xx_obj->trace));     
	return res;    
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_store_trace(struct device_driver *ddri, const char *buf, size_t count)
{
    int trace;
    if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	if(1 == sscanf(buf, "0x%x", &trace))
	{
		atomic_set(&stk31xx_obj->trace, trace);
	}
	else 
	{
		APS_ERR("invalid content: '%s', length = %d\n", buf, (int)count);
	}
	return count;    
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_als(struct device_driver *ddri, char *buf)
{
	int res;
	
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	if((res = stk31xx_read_als(stk31xx_obj->client, &stk31xx_obj->als)))
	{
		return snprintf(buf, PAGE_SIZE, "ERROR: %d\n", res);
	}
	else
	{
		return snprintf(buf, PAGE_SIZE, "0x%04X\n", stk31xx_obj->als);     
	}
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_ps(struct device_driver *ddri, char *buf)
{
	int res;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	if((res = stk31xx_read_ps(stk31xx_obj->client, &stk31xx_obj->ps)))
	{
		return snprintf(buf, PAGE_SIZE, "ERROR: %d\n", res);
	}
	else
	{
		return snprintf(buf, PAGE_SIZE, "0x%04X\n", stk31xx_obj->ps);     
	}
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_reg(struct device_driver *ddri, char *buf)
{
	u8 int_status;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	/*read*/
	stk31xx_check_intr(stk31xx_obj->client, &int_status);
	stk31xx_clear_intr(stk31xx_obj->client, int_status, 0x0);
	stk31xx_read_ps(stk31xx_obj->client, &stk31xx_obj->ps);
	stk31xx_read_als(stk31xx_obj->client, &stk31xx_obj->als);
	/*write*/
	stk31xx_write_als(stk31xx_obj->client, atomic_read(&stk31xx_obj->als_cmd_val));
	stk31xx_write_ps(stk31xx_obj->client, atomic_read(&stk31xx_obj->ps_cmd_val)); 
	stk31xx_write_ps_high_thd(stk31xx_obj->client, atomic_read(&stk31xx_obj->ps_high_thd_val));
	stk31xx_write_ps_low_thd(stk31xx_obj->client, atomic_read(&stk31xx_obj->ps_low_thd_val));
	return 0;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_send(struct device_driver *ddri, char *buf)
{
    return 0;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_store_send(struct device_driver *ddri, const char *buf, size_t count)
{
	int addr, cmd;
	u8 dat;

	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	else if(2 != sscanf(buf, "%x %x", &addr, &cmd))
	{
		APS_ERR("invalid format: '%s'\n", buf);
		return 0;
	}

	dat = (u8)cmd;
	APS_LOG("send(%02X, %02X) = %d\n", addr, cmd, 
	stk31xx_master_send(stk31xx_obj->client, (u16)addr, &dat, sizeof(dat)));
	
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_recv(struct device_driver *ddri, char *buf)
{
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	return snprintf(buf, PAGE_SIZE, "0x%04X\n", atomic_read(&stk31xx_obj->recv_reg));     	
  //  return 0;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_store_recv(struct device_driver *ddri, const char *buf, size_t count)
{
	int addr;
	u8 dat;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	else if(1 != sscanf(buf, "%x", &addr))
	{
		APS_ERR("invalid format: '%s'\n", buf);
		return 0;
	}

	APS_LOG("recv(%02X) = %d, 0x%02X\n", addr, 
	stk31xx_master_recv(stk31xx_obj->client, (u16)addr, (char*)&dat, sizeof(dat)), dat);
	atomic_set(&stk31xx_obj->recv_reg, dat);	
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_status(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;
	
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	if(stk31xx_obj->hw)
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "CUST: %d, (%d %d) (%02X %02X) (%02X %02X %02X) (%02X %02X %02X)\n", 
			stk31xx_obj->hw->i2c_num, stk31xx_obj->hw->power_id, stk31xx_obj->hw->power_vol, stk31xx_obj->addr.init, 
			stk31xx_obj->addr.status,stk31xx_obj->addr.als_cmd, stk31xx_obj->addr.als_dat0, stk31xx_obj->addr.als_dat1,
			stk31xx_obj->addr.ps_cmd, stk31xx_obj->addr.ps_dat, stk31xx_obj->addr.ps_high_thd);
	}
	else
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "CUST: NULL\n");
	}
	
	len += snprintf(buf+len, PAGE_SIZE-len, "REGS: %02X %02X %02X %02X %02lX %02lX\n", 
				atomic_read(&stk31xx_obj->als_cmd_val), atomic_read(&stk31xx_obj->ps_cmd_val), 
				atomic_read(&stk31xx_obj->ps_high_thd_val), atomic_read(&stk31xx_obj->ps_low_thd_val),stk31xx_obj->enable, stk31xx_obj->pending_intr);
	#ifdef MT6516
	len += snprintf(buf+len, PAGE_SIZE-len, "EINT: %d (%d %d %d %d)\n", mt_get_gpio_in(GPIO_ALS_EINT_PIN),
				CUST_EINT_ALS_NUM, CUST_EINT_ALS_POLARITY, CUST_EINT_ALS_DEBOUNCE_EN, CUST_EINT_ALS_DEBOUNCE_CN);

	len += snprintf(buf+len, PAGE_SIZE-len, "GPIO: %d (%d %d %d %d)\n",	GPIO_ALS_EINT_PIN, 
				mt_get_gpio_dir(GPIO_ALS_EINT_PIN), mt_get_gpio_mode(GPIO_ALS_EINT_PIN), 
				mt_get_gpio_pull_enable(GPIO_ALS_EINT_PIN), mt_get_gpio_pull_select(GPIO_ALS_EINT_PIN));
	#endif

	len += snprintf(buf+len, PAGE_SIZE-len, "MISC: %d %d\n", atomic_read(&stk31xx_obj->als_suspend), atomic_read(&stk31xx_obj->ps_suspend));

	return len;
}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define IS_SPACE(CH) (((CH) == ' ') || ((CH) == '\n'))
/*----------------------------------------------------------------------------*/
static int stk31xx_read_int_from_buf(struct stk31xx_priv *obj, const char* buf, size_t count,
                             u32 data[], int len)
{
	int idx = 0;
	char *cur = (char*)buf, *end = (char*)(buf+count);

	while(idx < len)
	{
		while((cur < end) && IS_SPACE(*cur))
		{
			cur++;        
		}

		if(1 != sscanf(cur, "%d", &data[idx]))
		{
			break;
		}

		idx++; 
		while((cur < end) && !IS_SPACE(*cur))
		{
			cur++;
		}
	}
	return idx;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_alslv(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;
	int idx;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	for(idx = 0; idx < stk31xx_obj->als_level_num; idx++)
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "%d ", stk31xx_obj->hw->als_level[idx]);
	}
	len += snprintf(buf+len, PAGE_SIZE-len, "\n");
	return len;    
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_store_alslv(struct device_driver *ddri, const char *buf, size_t count)
{
//	struct stk31xx_priv *obj;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	else if(!strcmp(buf, "def"))
	{
		memcpy(stk31xx_obj->als_level, stk31xx_obj->hw->als_level, sizeof(stk31xx_obj->als_level));
	}
	else if(stk31xx_obj->als_level_num != stk31xx_read_int_from_buf(stk31xx_obj, buf, count, 
			stk31xx_obj->hw->als_level, stk31xx_obj->als_level_num))
	{
		APS_ERR("invalid format: '%s'\n", buf);
	}    
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_show_alsval(struct device_driver *ddri, char *buf)
{
	ssize_t len = 0;
	int idx;
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	
	for(idx = 0; idx < stk31xx_obj->als_value_num; idx++)
	{
		len += snprintf(buf+len, PAGE_SIZE-len, "%d ", stk31xx_obj->hw->als_value[idx]);
	}
	len += snprintf(buf+len, PAGE_SIZE-len, "\n");
	return len;    
}
/*----------------------------------------------------------------------------*/
static ssize_t stk31xx_store_alsval(struct device_driver *ddri, const char *buf, size_t count)
{
	if(!stk31xx_obj)
	{
		APS_ERR("stk31xx_obj is null!!\n");
		return 0;
	}
	else if(!strcmp(buf, "def"))
	{
		memcpy(stk31xx_obj->als_value, stk31xx_obj->hw->als_value, sizeof(stk31xx_obj->als_value));
	}
	else if(stk31xx_obj->als_value_num != stk31xx_read_int_from_buf(stk31xx_obj, buf, count, 
			stk31xx_obj->hw->als_value, stk31xx_obj->als_value_num))
	{
		APS_ERR("invalid format: '%s'\n", buf);
	}    
	return count;
}

#if 1
/*----------------------------------------------------------------------------*/
static DRIVER_ATTR(stk_als,     S_IWUSR | S_IRUGO, stk31xx_show_als,   NULL);
static DRIVER_ATTR(stk_ps,      S_IWUSR | S_IRUGO, stk31xx_show_ps,    NULL);
static DRIVER_ATTR(stk_config,  S_IWUSR | S_IRUGO, stk31xx_show_config,stk31xx_store_config);
static DRIVER_ATTR(stk_alslv,   S_IWUSR | S_IRUGO, stk31xx_show_alslv, stk31xx_store_alslv);
static DRIVER_ATTR(stk_alsval,  S_IWUSR | S_IRUGO, stk31xx_show_alsval,stk31xx_store_alsval);
static DRIVER_ATTR(stk_trace,   S_IWUSR | S_IRUGO, stk31xx_show_trace, stk31xx_store_trace);
static DRIVER_ATTR(stk_status,  S_IWUSR | S_IRUGO, stk31xx_show_status,  NULL);
static DRIVER_ATTR(stk_send,    S_IWUSR | S_IRUGO, stk31xx_show_send,  stk31xx_store_send);
static DRIVER_ATTR(stk_recv,    S_IWUSR | S_IRUGO, stk31xx_show_recv,  stk31xx_store_recv);
static DRIVER_ATTR(stk_reg,     S_IWUSR | S_IRUGO, stk31xx_show_reg,   NULL);
//static DRIVER_ATTR(i2c,     S_IWUSR | S_IRUGO, stk31xx_show_i2c,   stk31xx_store_i2c);
/*----------------------------------------------------------------------------*/
static struct driver_attribute *stk31xx_attr_list[] = {
    &driver_attr_stk_als,
    &driver_attr_stk_ps,    
    &driver_attr_stk_trace,        /*trace log*/
    &driver_attr_stk_config,
    &driver_attr_stk_alslv,
    &driver_attr_stk_alsval,
    &driver_attr_stk_status,
    &driver_attr_stk_send,
    &driver_attr_stk_recv,
//    &driver_attr_i2c,
    &driver_attr_stk_reg,
};

/*----------------------------------------------------------------------------*/
static int stk31xx_create_attr(struct device_driver *driver) 
{
	int idx, err = 0;
	int num = (int)(sizeof(stk31xx_attr_list)/sizeof(stk31xx_attr_list[0]));
	if (driver == NULL)
	{
		return -EINVAL;
	}

	for(idx = 0; idx < num; idx++)
	{
		if((err = driver_create_file(driver, stk31xx_attr_list[idx])))
		{            
			APS_ERR("driver_create_file (%s) = %d\n", stk31xx_attr_list[idx]->attr.name, err);
			break;
		}
	}    
	return err;
}
/*----------------------------------------------------------------------------*/
	static int stk31xx_delete_attr(struct device_driver *driver)
	{
	int idx ,err = 0;
	int num = (int)(sizeof(stk31xx_attr_list)/sizeof(stk31xx_attr_list[0]));

	if (!driver)
	return -EINVAL;

	for (idx = 0; idx < num; idx++) 
	{
		driver_remove_file(driver, stk31xx_attr_list[idx]);
	}
	
	return err;
}
#endif

/****************************************************************************** 
 * Function Configuration
******************************************************************************/
static int stk31xx_get_als_value(struct stk31xx_priv *obj, u16 als)
{
	int idx;
	int invalid = 0;
	for(idx = 0; idx < obj->als_level_num; idx++)
	{
		if(als < obj->hw->als_level[idx])
		{
			break;
		}
	}
	
	if(idx >= obj->als_value_num)
	{
		APS_ERR("exceed range\n"); 
		idx = obj->als_value_num - 1;
	}
	
	if(1 == atomic_read(&obj->als_deb_on))
	{
		unsigned long endt = atomic_read(&obj->als_deb_end);
		if(time_after(jiffies, endt))
		{
			atomic_set(&obj->als_deb_on, 0);
		}
		
		if(1 == atomic_read(&obj->als_deb_on))
		{
			invalid = 1;
		}
	}

	if(!invalid)
	{
		if (atomic_read(&obj->trace) & STK_TRC_CVT_ALS)
		{
			APS_DBG("ALS: %05d => %05d\n", als, obj->hw->als_value[idx]);
		}
		
		return obj->hw->als_value[idx];
	}
	else
	{
		if(atomic_read(&obj->trace) & STK_TRC_CVT_ALS)
		{
			APS_DBG("ALS: %05d => %05d (-1)\n", als, obj->hw->als_value[idx]);    
		}
		return -1;
	}
}
/*----------------------------------------------------------------------------*/
static int stk31xx_get_ps_value(struct stk31xx_priv *obj, u16 ps)
{
	int val, mask = atomic_read(&obj->ps_mask);
	int invalid = 0;

	if( obj->hw->polling_mode_ps == 1 )	//For Polling Mode
	{	
		if( is_near != 1 && ps >= atomic_read(&obj->ps_high_thd_val))
		{
			val = 0;  /*close*/
			is_near = 1;
		}
		else if( is_near != 0 && ps <= atomic_read(&obj->ps_low_thd_val))
		{
			val = 1;  /*far away*/
			is_near = 0;
		}
		else
		{
			if(is_near==1)
				val = 0;//near
			else
				val = 1;//far
		}
	}
	else
	{
		if( ps >= atomic_read(&obj->ps_high_thd_val))
		{
			val = 0;  /*close*/	
		}
		else
		{
			val = 1;  /*far away*/
		}		
	}		
	
	if(atomic_read(&obj->ps_suspend))
	{
		invalid = 1;
	}
	else if(1 == atomic_read(&obj->ps_deb_on))
	{
		unsigned long endt = atomic_read(&obj->ps_deb_end);
		if(time_after(jiffies, endt))
		{
			atomic_set(&obj->ps_deb_on, 0);
		}
		
		if (1 == atomic_read(&obj->ps_deb_on))
		{
			invalid = 1;
		}
	}

	if(!invalid)
	{
		if(unlikely(atomic_read(&obj->trace) & STK_TRC_CVT_PS))
		{
			if(mask)
			{
				APS_DBG("PS:  %05d => %05d [M] \n", ps, val);
			}
			else
			{
				APS_DBG("PS:  %05d => %05d\n", ps, val);
			}
		}
		return val;
		
	}	
	else
	{
		if(unlikely(atomic_read(&obj->trace) & STK_TRC_CVT_PS))
		{
			APS_DBG("PS:  %05d => %05d (-1)\n", ps, val);    
		}
		return -1;
	}	
}
/****************************************************************************** 
 * Function Configuration
******************************************************************************/
static int stk31xx_open(struct inode *inode, struct file *file)
{
	file->private_data = stk31xx_i2c_client;

	if (!file->private_data)
	{
		APS_ERR("null pointer!!\n");
		return -EINVAL;
	}
	
	return nonseekable_open(inode, file);
}
/*----------------------------------------------------------------------------*/
static int stk31xx_release(struct inode *inode, struct file *file)
{
	file->private_data = NULL;
	return 0;
}
/*----------------------------------------------------------------------------*/
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))
static long stk31xx_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
#else
static int stk31xx_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
#endif
{
	struct i2c_client *client = (struct i2c_client*)file->private_data;
	struct stk31xx_priv *obj = i2c_get_clientdata(client);  
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
	long err = 0;
#else
	int err = 0;
#endif
	void __user *ptr = (void __user*) arg;
	int dat;
	uint32_t enable;

	switch (cmd)
	{
		case ALSPS_SET_PS_MODE:
			if(copy_from_user(&enable, ptr, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			if(enable)
			{
				if((err = stk31xx_enable_ps(obj->client, 1)))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("enable ps fail: %ld\n", err); 
#else
					APS_ERR("enable ps fail: %d\n", err); 
#endif
					goto err_out;
				}
				
				set_bit(STK_BIT_PS, &obj->enable);
			}
			else
			{
				if((err = stk31xx_enable_ps(obj->client, 0)))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("disable ps fail: %ld\n", err); 
#else
					APS_ERR("disable ps fail: %d\n", err); 
#endif
	
					goto err_out;
				}
				
				clear_bit(STK_BIT_PS, &obj->enable);
			}
			break;

		case ALSPS_GET_PS_MODE:
			enable = test_bit(STK_BIT_PS, &obj->enable) ? (1) : (0);
			if(copy_to_user(ptr, &enable, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			break;

		case ALSPS_GET_PS_DATA:    
			if((err = stk31xx_read_ps(obj->client, &obj->ps)))
			{
				goto err_out;
			}
			
			dat = stk31xx_get_ps_value(obj, obj->ps);
#ifdef STK_PS_POLLING_LOG	
			APS_LOG("%s:ps raw 0x%x -> value 0x%x \n",__func__, obj->ps, dat);			
#endif			
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}  
			break;

		case ALSPS_GET_PS_RAW_DATA:    
			if((err = stk31xx_read_ps(obj->client, &obj->ps)))
			{
				goto err_out;
			}
			
			dat = obj->ps;
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}  
			break;            

		case ALSPS_SET_ALS_MODE:
			if(copy_from_user(&enable, ptr, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			if(enable)
			{
				if((err = stk31xx_enable_als(obj->client, 1)))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("enable als fail: %ld\n", err); 
#else
					APS_ERR("enable als fail: %d\n", err); 
#endif

					goto err_out;
				}
				set_bit(STK_BIT_ALS, &obj->enable);
			}
			else
			{
				if((err = stk31xx_enable_als(obj->client, 0)))
				{
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
					APS_ERR("disable als fail: %ld\n", err); 
#else
					APS_ERR("disable als fail: %d\n", err); 
#endif

					goto err_out;
				}
				clear_bit(STK_BIT_ALS, &obj->enable);
			}
			break;

		case ALSPS_GET_ALS_MODE:
			enable = test_bit(STK_BIT_ALS, &obj->enable) ? (1) : (0);
			if(copy_to_user(ptr, &enable, sizeof(enable)))
			{
				err = -EFAULT;
				goto err_out;
			}
			break;

		case ALSPS_GET_ALS_DATA: 
			if((err = stk31xx_read_als(obj->client, &obj->als)))
			{
				goto err_out;
			}

			dat = stk31xx_get_als_value(obj, obj->als);
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}              
			break;

		case ALSPS_GET_ALS_RAW_DATA:    
			if((err = stk31xx_read_als(obj->client, &obj->als)))
			{
				goto err_out;
			}

			dat = obj->als;
			if(copy_to_user(ptr, &dat, sizeof(dat)))
			{
				err = -EFAULT;
				goto err_out;
			}              
			break;

#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI) || defined(STK_AUTO_CT_CALI_SATU) || defined(STK_AUTO_CT_CALI_NO_SATU))			
		case ALSPS_SET_PS_CALI:
			if((err = stk31xx_set_ps_cali()))
			{
				goto err_out;
			}
			break;	
#endif			
		default:
			APS_ERR("%s not supported = 0x%04x", __FUNCTION__, cmd);
			err = -ENOIOCTLCMD;
			break;
	}

	err_out:
	return err;    
}

/*----------------------------------------------------------------------------*/
static struct file_operations stk31xx_fops = {
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	.owner = THIS_MODULE,
#endif
	.open = stk31xx_open,
	.release = stk31xx_release,
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(2,6,36))	
	.unlocked_ioctl = stk31xx_unlocked_ioctl,
#else
	.ioctl = stk31xx_ioctl,
#endif

};
/*----------------------------------------------------------------------------*/
static struct miscdevice stk31xx_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "als_ps",
	.fops = &stk31xx_fops,
};
/*----------------------------------------------------------------------------*/
static int stk31xx_i2c_suspend(struct i2c_client *client, pm_message_t msg) 
{
	APS_FUN();    
/*
	if(msg.event == PM_EVENT_SUSPEND)
	{   
		if(!obj)
		{
			APS_ERR("null pointer!!\n");
			return -EINVAL;
		}
		
		atomic_set(&obj->als_suspend, 1);
		if((err = stk31xx_enable_als(client, 0)))
		{
			APS_ERR("disable als: %d\n", err);
			return err;
		}

		atomic_set(&obj->ps_suspend, 1);
		if((err = stk31xx_enable_ps(client, 0)))
		{
			APS_ERR("disable ps:  %d\n", err);
			return err;
		}
		
		stk31xx_power(obj->hw, 0);
	}
*/
	return 0;
}
/*----------------------------------------------------------------------------*/
static int stk31xx_i2c_resume(struct i2c_client *client)
{
	APS_FUN();
/*
	if(!obj)
	{
		APS_ERR("null pointer!!\n");
		return -EINVAL;
	}

	stk31xx_power(obj->hw, 1);
	if((err = stk31xx_init_client(client)))
	{
		APS_ERR("initialize client fail!!\n");
		return err;        
	}
	atomic_set(&obj->als_suspend, 0);
	if(test_bit(STK_BIT_ALS, &obj->enable))
	{
		if((err = stk31xx_enable_als(client, 1)))
		{
			APS_ERR("enable als fail: %d\n", err);        
		}
	}
	atomic_set(&obj->ps_suspend, 0);
	if(test_bit(STK_BIT_PS,  &obj->enable))
	{
		if((err = stk31xx_enable_ps(client, 1)))
		{
			APS_ERR("enable ps fail: %d\n", err);                
		}
	}
*/
	return 0;
}
/*----------------------------------------------------------------------------*/
static void stk31xx_early_suspend(struct early_suspend *h) 
{   /*early_suspend is only applied for ALS*/
	int err;
	struct stk31xx_priv *obj = container_of(h, struct stk31xx_priv, early_drv);   	
	APS_FUN();    

	if(!obj)
	{
		APS_ERR("null pointer!!\n");
		return;
	}
	
	atomic_set(&obj->als_suspend, 1);    
	if((err = stk31xx_enable_als(obj->client, 0)))
	{
		APS_ERR("disable als fail: %d\n", err); 
	}
}
/*----------------------------------------------------------------------------*/
static void stk31xx_late_resume(struct early_suspend *h)
{   /*early_suspend is only applied for ALS*/
	int err;
	hwm_sensor_data sensor_data;
	struct stk31xx_priv *obj = container_of(h, struct stk31xx_priv, early_drv);         
	
	memset(&sensor_data, 0, sizeof(sensor_data));
	APS_FUN();

	if(!obj)
	{
		APS_ERR("null pointer!!\n");
		return;
	}

	atomic_set(&obj->als_suspend, 0);
	if(test_bit(STK_BIT_ALS, &obj->enable))
	{
		if((err = stk31xx_enable_als(obj->client, 1)))
		{
			APS_ERR("enable als fail: %d\n", err);        
		}
	}
}

int stk31xx_ps_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
	int err = 0;
	int value;
	hwm_sensor_data* sensor_data;
	struct stk31xx_priv *obj = (struct stk31xx_priv *)self;
	
	//APS_FUN(f);
	switch (command)
	{
		case SENSOR_DELAY:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Set delay parameter error!\n");
				err = -EINVAL;
			}
			// Do nothing
			break;

		case SENSOR_ENABLE:			
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Enable sensor parameter error!\n");
				err = -EINVAL;
			}
			else
			{				
				value = *(int *)buff_in;
				
				if(value)
				{
					if((err = stk31xx_enable_ps(obj->client, 1)))
					{
						APS_ERR("enable ps fail: %d\n", err); 
						return -1;
					}
					set_bit(STK_BIT_PS, &obj->enable);
				}
				else
				{
					if((err = stk31xx_enable_ps(obj->client, 0)))
					{
						APS_ERR("disable ps fail: %d\n", err); 
						return -1;
					}
					clear_bit(STK_BIT_PS, &obj->enable);
				}
			}
			break;

		case SENSOR_GET_DATA:
			if((buff_out == NULL) || (size_out< sizeof(hwm_sensor_data)))
			{
				APS_ERR("get sensor data parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				sensor_data = (hwm_sensor_data *)buff_out;				
				
				if((err = stk31xx_read_ps(obj->client, &obj->ps)))
				{
					err = -1;
				}
				else
				{
					sensor_data->values[0] = stk31xx_get_ps_value(obj, obj->ps);
					sensor_data->value_divide = 1;
					sensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;
#ifdef STK_PS_POLLING_LOG						
					APS_LOG("%s:ps raw 0x%x -> value 0x%x \n",__func__, obj->ps, sensor_data->values[0]);					
#endif					
				}				
			}
			break;
		default:
			APS_ERR("proxmy sensor operate function no this parameter %d!\n", command);
			err = -1;
			break;
	}
	
	return err;
}

int stk31xx_als_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
	int err = 0;
	int value;
	hwm_sensor_data* sensor_data;
	struct stk31xx_priv *obj = (struct stk31xx_priv *)self;
	
	//APS_FUN(f);
	switch (command)
	{
		case SENSOR_DELAY:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Set delay parameter error!\n");
				err = -EINVAL;
			}
			// Do nothing
			break;

		case SENSOR_ENABLE:
			if((buff_in == NULL) || (size_in < sizeof(int)))
			{
				APS_ERR("Enable sensor parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				value = *(int *)buff_in;				
				if(value)
				{
					if((err = stk31xx_enable_als(obj->client, 1)))
					{
						APS_ERR("enable als fail: %d\n", err); 
						return -1;
					}
					set_bit(STK_BIT_ALS, &obj->enable);
				}
				else
				{
					if((err = stk31xx_enable_als(obj->client, 0)))
					{
						APS_ERR("disable als fail: %d\n", err); 
						return -1;
					}
					clear_bit(STK_BIT_ALS, &obj->enable);
				}
				
			}
			break;

		case SENSOR_GET_DATA:
			if((buff_out == NULL) || (size_out< sizeof(hwm_sensor_data)))
			{
				APS_ERR("get sensor data parameter error!\n");
				err = -EINVAL;
			}
			else
			{
				sensor_data = (hwm_sensor_data *)buff_out;
								
				if((err = stk31xx_read_als(obj->client, &obj->als)))
				{
					err = -1;
				}
				else
				{
					sensor_data->values[0] = stk31xx_get_als_value(obj, obj->als);
					sensor_data->value_divide = 1;
					sensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;
					APS_DBG("stk31xx_als_operate@@ value[0] = %d\n", sensor_data->values[0]);

					#if 0
					stk31xx_dump_reg(obj->client);
					
					stk31xx_read_ps(obj->client, &obj->ps);
					APS_DBG("stk31xx_als_operate@@ PS data = 0x%x\n", obj->ps);
					#endif
				}				
			}
			break;
		default:
			APS_ERR("light sensor operate function no this parameter %d!\n", command);
			err = -1;
			break;
	}
	
	return err;
}


/*----------------------------------------------------------------------------*/
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
static int stk31xx_i2c_detect(struct i2c_client *client, int kind, struct i2c_board_info *info) 
{    
	strcpy(info->type, STK31XX_DEV_NAME);
	return 0;
}
#endif
/*----------------------------------------------------------------------------*/
static int stk31xx_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct stk31xx_priv *obj;
	struct hwmsen_object obj_ps, obj_als;
	int err = 0;

    wake_lock_init(&ps_lock,WAKE_LOCK_SUSPEND,"ps wakelock");

	if(!(obj = kzalloc(sizeof(*obj), GFP_KERNEL)))
	{
		err = -ENOMEM;
		goto exit;
	}
	memset(obj, 0, sizeof(*obj));
	stk31xx_obj = obj;
	obj->hw = get_cust_alsps_hw(STK31XX_I2C_SLAVE_ADDR);
	stk31xx_get_addr(obj->hw, &obj->addr);

	INIT_DELAYED_WORK(&obj->eint_work, stk31xx_eint_work);
	obj->client = client;
	i2c_set_clientdata(client, obj);	
	atomic_set(&obj->als_debounce, 1000);
	atomic_set(&obj->als_deb_on, 0);
	atomic_set(&obj->als_deb_end, 0);
	atomic_set(&obj->ps_debounce, 1000);	// 100 --> 1000
	atomic_set(&obj->ps_deb_on, 0);
	atomic_set(&obj->ps_deb_end, 0);
	atomic_set(&obj->ps_mask, 0);
	atomic_set(&obj->trace, 0x00);			// 0x00
	atomic_set(&obj->als_suspend, 0);

#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI) || defined(STK_AUTO_CT_CALI_SATU) || defined(STK_AUTO_CT_CALI_NO_SATU))
	atomic_set(&obj->ps_cali_done, 0);
#endif	
#if (defined(STK_MANUAL_CT_CALI) || defined(STK_MANUAL_GREYCARD_CALI))		
	obj->first_boot = 1;	
#endif

	// als refresh timing is 400 ms. shutdown enable( ALS OFF)
	//atomic_set(&obj->als_cmd_val, 0x49);
	atomic_set(&obj->als_cmd_val, STK_ALS_CMD_DATA);

	// ps: sleep time is 30 ms; 100mA current sink. refresh time is 1T(0.2ms); shutdown enable (PS OFF).
	//atomic_set(&obj->ps_cmd_val, 0x21);
	atomic_set(&obj->ps_cmd_val, STK_PS_CMD_DATA);

	// 0x09: X8 gain. 0x0d: X16 gain.
	//gain_setting = 0x0D;		
	
	atomic_set(&obj->ps_high_thd_val, obj->hw->ps_threshold_high ); 
	atomic_set(&obj->ps_low_thd_val, obj->hw->ps_threshold_low ); 
	atomic_set(&obj->recv_reg, 0);  
	is_near = 2;
	
	if(obj->hw->polling_mode_ps == 0)
	{
	  	atomic_set(&obj->ps_cmd_val,  (STK_PS_CMD_DATA | STK_INT_PS));
	  	APS_LOG("enable PS interrupt\n");
	}
	if(obj->hw->polling_mode_als == 0)
	{
	  	atomic_set(&obj->als_cmd_val,  (STK_ALS_CMD_DATA | STK_INT_ALS));
	  	APS_LOG("enable ALS interrupt\n");
	}	
	APS_LOG("ps_cmd_val=0x%x\n", atomic_read(&obj->ps_cmd_val));
	APS_LOG("als_cmd_val=0x%x\n", atomic_read(&obj->als_cmd_val));
	
	APS_LOG("stk31xx_i2c_probe() OK!\n");
	obj->enable = 0;
	obj->pending_intr = 0;
	obj->als_level_num = sizeof(obj->hw->als_level)/sizeof(obj->hw->als_level[0]);
	obj->als_value_num = sizeof(obj->hw->als_value)/sizeof(obj->hw->als_value[0]);   
	BUG_ON(sizeof(obj->als_level) != sizeof(obj->hw->als_level));
	memcpy(obj->als_level, obj->hw->als_level, sizeof(obj->als_level));
	BUG_ON(sizeof(obj->als_value) != sizeof(obj->hw->als_value));
	memcpy(obj->als_value, obj->hw->als_value, sizeof(obj->als_value));
	atomic_set(&obj->i2c_retry, 3);
	
	if(!(atomic_read(&obj->als_cmd_val) & STK_SD_ALS))
	{
		set_bit(STK_BIT_ALS, &obj->enable);

		atomic_set(&obj->als_enable, 1);
	}
	else
	{
		clear_bit(STK_BIT_ALS, &obj->enable);

		atomic_set(&obj->als_enable, 0);
	}
	
	if(!(atomic_read(&obj->ps_cmd_val) & STK_SD_PS))
	{
		set_bit(STK_BIT_PS, &obj->enable);

		atomic_set(&obj->ps_enable, 1);
	}
	else
	{
		clear_bit(STK_BIT_PS, &obj->enable);

		atomic_set(&obj->ps_enable, 0);
	}
	
	stk31xx_i2c_client = client;	
	if((err = stk31xx_init_client(client)))
	{
		goto exit_init_failed;
	}
	
	if((err = misc_register(&stk31xx_device)))
	{
		APS_ERR("stk31xx_device register failed\n");
		goto exit_misc_device_register_failed;
	}
	#if 1
	if((err = stk31xx_create_attr(&stk31xx_alsps_driver.driver)))
	{
		APS_ERR("create attribute err = %d\n", err);
		goto exit_create_attr_failed;
	}
	#endif

	// attach ps and als into HWM layer.
	obj_ps.self = stk31xx_obj;
	if(1 == obj->hw->polling_mode_ps)
	{
	  obj_ps.polling = 1;
	}
	else
	{
	  obj_ps.polling = 0;//interrupt mode
	}
	obj_ps.sensor_operate = stk31xx_ps_operate;
	if((err = hwmsen_attach(ID_PROXIMITY, &obj_ps)))
	{
		APS_ERR("attach fail = %d\n", err);
		goto exit_create_attr_failed;
	}
	
	obj_als.self = stk31xx_obj;
	if(1 == obj->hw->polling_mode_als)
	{
	  obj_als.polling = 1;
	}
	else
	{
	  obj_als.polling = 0;//interrupt mode
	}
	obj_als.sensor_operate = stk31xx_als_operate;
	if((err = hwmsen_attach(ID_LIGHT, &obj_als)))
	{
		APS_ERR("attach fail = %d\n", err);
		goto exit_create_attr_failed;
	}


#if defined(CONFIG_HAS_EARLYSUSPEND)
	obj->early_drv.level    = EARLY_SUSPEND_LEVEL_DISABLE_FB - 1,
	obj->early_drv.suspend  = stk31xx_early_suspend,
	obj->early_drv.resume   = stk31xx_late_resume,    
	register_early_suspend(&obj->early_drv);
#endif

#ifdef STK_AUTO_CT_CALI_SATU
	stk31xx_obj->redetect_ct_en = 0;
#endif
#if (defined(STK_AUTO_CT_CALI_SATU) || defined(STK_AUTO_CT_CALI_NO_SATU))	
	stk31xx_set_ps_cali();
#endif	

	APS_LOG("%s: OK\n", __func__);
	return 0;

	exit_create_attr_failed:
	misc_deregister(&stk31xx_device);
	exit_misc_device_register_failed:
	exit_init_failed:
	//i2c_detach_client(client);
//	exit_kfree:
	kfree(obj);
	exit:
	stk31xx_i2c_client = NULL;           
	#ifdef MT6516        
	MT6516_EINTIRQMask(CUST_EINT_ALS_NUM);  /*mask interrupt if fail*/
	#endif
	#ifdef MT6573     
	mt65xx_eint_mask(CUST_EINT_ALS_NUM);  /*mask interrupt if fail*/
	#endif
	#ifdef MT6575    
	mt65xx_eint_mask(CUST_EINT_ALS_NUM);  /*mask interrupt if fail*/
	#endif
	#ifdef MT6577 
	mt65xx_eint_mask(CUST_EINT_ALS_NUM);  /*mask interrupt if fail*/
	#endif
	
	APS_ERR("%s: err = %d\n", __func__, err);
	return err;
}

/*----------------------------------------------------------------------------*/
static int stk31xx_i2c_remove(struct i2c_client *client)
{
	int err;	
	#if 1
	if((err = stk31xx_delete_attr(&stk31xx_i2c_driver.driver)))
	{
		APS_ERR("stk31xx_delete_attr fail: %d\n", err);
	} 
	#endif

	if((err = misc_deregister(&stk31xx_device)))
	{
		APS_ERR("misc_deregister fail: %d\n", err);    
	}
	
	stk31xx_i2c_client = NULL;
	i2c_unregister_device(client);
	kfree(i2c_get_clientdata(client));

	return 0;
}
/*----------------------------------------------------------------------------*/
static int stk31xx_probe(struct platform_device *pdev) 
{
	struct alsps_hw *hw = get_cust_alsps_hw(STK31XX_I2C_SLAVE_ADDR);
	struct stk31xx_i2c_addr addr;

	stk31xx_power(hw, 1);    
	stk31xx_get_addr(hw, &addr);
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
	stk31xx_force[0] = hw->i2c_num;
	stk31xx_force[1] = hw->i2c_addr[0];
#endif
	if(i2c_add_driver(&stk31xx_i2c_driver))
	{
		APS_ERR("add driver error\n");
		return -1;
	} 

	return 0;
}
/*----------------------------------------------------------------------------*/
static int stk31xx_remove(struct platform_device *pdev)
{
	struct alsps_hw *hw = get_cust_alsps_hw(STK31XX_I2C_SLAVE_ADDR);
	APS_FUN();    
	stk31xx_power(hw, 0);    
	i2c_del_driver(&stk31xx_i2c_driver);
	return 0;
}
/*----------------------------------------------------------------------------*/
static struct platform_driver stk31xx_alsps_driver = {
	.probe      = stk31xx_probe,
	.remove     = stk31xx_remove,    
	.driver     = {
		.name  = "als_ps",
#if (LINUX_VERSION_CODE<KERNEL_VERSION(3,0,0))	
		.owner = THIS_MODULE,
#endif
	}
};

#if 0
/*----------------------------------------------------------------------------*/
static int __init stk31xx_init(void)
{
	APS_FUN();
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(3,0,0))	
	i2c_register_board_info(0, &i2c_stk31xx, 1);
#endif
	if(platform_driver_register(&stk31xx_alsps_driver))
	{
		APS_ERR("failed to register driver");
		return -ENODEV;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
static void __exit stk31xx_exit(void)
{
	APS_FUN();
	platform_driver_unregister(&stk31xx_alsps_driver);
}
/*----------------------------------------------------------------------------*/

module_init(stk31xx_init);
module_exit(stk31xx_exit);

MODULE_AUTHOR("MingHsien Hsieh");
MODULE_DESCRIPTION("ALSPS stk31xx driver");
MODULE_LICENSE("GPL");
#endif
#endif

// COMBO configuration.
#if 1//def COMBO_ALSPS_SUPPORT
//#define COMBO_ALSPS_NUM	2

static struct i2c_board_info __initdata i2c_combo[COMBO_ALSPS_NUM] =
{
	{ I2C_BOARD_INFO(ISL29044_DEV_NAME, (ISL29044_I2C_SLAVE_ADDR>>1))},
	{ I2C_BOARD_INFO(STK31XX_DEV_NAME,  (STK31XX_I2C_SLAVE_ADDR>>1))}	
};


static struct platform_driver* g_alsps_driver = NULL;
static struct platform_driver* combo_alsps_driver[COMBO_ALSPS_NUM] =
{
	&ISL29044_alsps_driver,
	&stk31xx_alsps_driver	
};

static u8 combo_alsps_i2c_data[COMBO_ALSPS_NUM][2] =
{
	{ISL29044_I2C_SLAVE_ADDR, 0x01},
	{STK31XX_I2C_SLAVE_ADDR,  0x08}
};


// software i2c
#define	ALSPS_SCL			GPIO_I2C0_SCA_PIN
#define ALSPS_SDA 			GPIO_I2C0_SDA_PIN
#define ALSPS_I2C_DELAY	2
#define ALSPS_DELAY_MS	10

void custom_i2c_start(void)
{
	mt_set_gpio_mode(ALSPS_SCL, 0);
	mt_set_gpio_mode(ALSPS_SDA, 0);
	mt_set_gpio_dir(ALSPS_SCL, GPIO_DIR_OUT);
	mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_OUT);
	mt_set_gpio_out(ALSPS_SCL, 1);
	mt_set_gpio_out(ALSPS_SDA, 1);

	udelay(ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SDA, 0);
	udelay(ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SCL, 0);
	udelay(ALSPS_I2C_DELAY);
}


void custom_i2c_stop(void)
{
	mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_OUT);
	mt_set_gpio_out(ALSPS_SCL, 0);
	mt_set_gpio_out(ALSPS_SDA, 0);
	udelay(ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SCL, 1);
	udelay(4*ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SDA, 1);  
	udelay(4*ALSPS_I2C_DELAY);

	mt_set_gpio_mode(ALSPS_SCL, 1);
	mt_set_gpio_mode(ALSPS_SDA, 1);
}

// write into addr. or data
u8 custom_i2c_write_byte(kal_uint8 byte)
{
	u8  i, temp, ack = 0;
	
	mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_OUT);

	for (i = 0; i < 8; i++)
	{
		//temp = addr;    
		//temp=(temp&0x80);
		mt_set_gpio_out(ALSPS_SCL, 0);
        udelay(ALSPS_I2C_DELAY);
		if ((byte << i) & 0x80)
		{
			mt_set_gpio_out(ALSPS_SDA, 1); 
		}
		else
		{
			mt_set_gpio_out(ALSPS_SDA, 0); 
		}

		udelay(ALSPS_I2C_DELAY);
		mt_set_gpio_out(ALSPS_SCL, 1);
		udelay(ALSPS_I2C_DELAY);
	}

    // read ACK
    mt_set_gpio_out(ALSPS_SCL, 0);
	mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_IN);
	udelay(4 * ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SCL, 1);
	udelay(ALSPS_I2C_DELAY);

    ack = mt_get_gpio_in(ALSPS_SDA);
	udelay(ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SCL, 0);
    mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_OUT);  

	printk("i2c_write: (0x%x, 0x%x)\n", byte, ack);
    
    return ack;
}

u8 custom_i2c_read_byte(u8 ack)
{
	u8 count = 0x08;
	u8 data = 0x00;
	u8 temp = 0;

	mt_set_gpio_mode(ALSPS_SCL, GPIO_MODE_00);
	mt_set_gpio_dir(ALSPS_SCL, GPIO_DIR_OUT);
	while(count > 0) 
	{ 
		mt_set_gpio_out(ALSPS_SCL, 0);
		udelay(2*ALSPS_I2C_DELAY);
		if(count == 8)
		{
			mt_set_gpio_mode(ALSPS_SDA, GPIO_MODE_00);
			mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_IN);
		}
		udelay(ALSPS_I2C_DELAY);
		mt_set_gpio_out(ALSPS_SCL, 1);
		udelay(2*ALSPS_I2C_DELAY);
		temp = mt_get_gpio_in(ALSPS_SDA);
		data <<= 1;
		if (temp)
		{
			data |= 0x01;
		}

		udelay(ALSPS_I2C_DELAY);
		count--;
	} 

	mt_set_gpio_out(ALSPS_SCL, 0);
	udelay(2*ALSPS_I2C_DELAY);
	mt_set_gpio_dir(ALSPS_SDA, GPIO_DIR_OUT);
	udelay(ALSPS_I2C_DELAY);
	if(ack)
	{
		mt_set_gpio_out(ALSPS_SDA, 1);
	}
	else
	{
		mt_set_gpio_out(ALSPS_SDA, 0);
	}
	udelay(ALSPS_I2C_DELAY);
	mt_set_gpio_out(ALSPS_SCL, 1);
	udelay(2*ALSPS_I2C_DELAY);

	//*buffer = data;
	mt_set_gpio_out(ALSPS_SCL, 0);
	printk("i2c_read: 0x%x\n", data);
	return data;
}

int combo_alsps_write_reg(u8 chip_addr, u8 reg, u8 data)
{
	u8 ack;
	int ret = 0;
	
	custom_i2c_start();

	ack = custom_i2c_write_byte(chip_addr);
	if(ack == 1)
	{
		ret = -1;
		goto out;
	}

	ack = custom_i2c_write_byte(reg);
	if(ack == 1)
	{
		ret = -1;
		goto out;
	}

	ack = custom_i2c_write_byte(data);
	if(ack == 1)
	{
		ret = -1;
		goto out;
	}
	else
	{
		ret = ack;
	}

out:
	custom_i2c_stop();

	return ret;
}

// chip_addr : 8bits chip i2c address
int combo_alsps_read_reg(u8 chip_addr, u8 reg, u8* data)
{
	u8 tmp, ack;
	int ret = 0;

	custom_i2c_start();
	ack = custom_i2c_write_byte(chip_addr);
	if(ack == 1)
	{
		ret = -1;
		goto out;
	}
	ack = custom_i2c_write_byte(reg);

	if(ack == 1)
	{
		ret = -1;
		goto out;
	}

	custom_i2c_start();//restart   
	ack = custom_i2c_write_byte(chip_addr + 0x01);
	if(ack == 1)
	{
		ret = -1;
		goto out;
	}

	tmp = custom_i2c_read_byte(1);

	*data = tmp;

out:
	custom_i2c_stop();

	return ret;
}

int combo_alsps_identify(u8 chip_addr, u8 reg)
{
	//struct alsps_hw *hw = get_cust_alsps_hw(chip_addr);
	u8 data = 0;
	int ret = -1;
	
	APS_FUN();
	switch (chip_addr)
	{
	case ISL29044_I2C_SLAVE_ADDR:
		APS_LOG("combo_alsps_identify@@ isl29044 begin.\n");
		ret = combo_alsps_read_reg(ISL29044_I2C_SLAVE_ADDR, reg, &data);
		APS_LOG("combo_alsps_identify@@ isl29044. data = 0x%x, ret = %d\n", data, ret);
		break;

	case STK31XX_I2C_SLAVE_ADDR:
		APS_LOG("combo_alsps_identify@@ stk31xx begin.\n");
		ret = combo_alsps_read_reg(STK31XX_I2C_SLAVE_ADDR, reg, &data);
		APS_LOG("combo_alsps_identify@@ stk31xx. data = 0x%x, ret = %d\n", data, ret);
		break;

	default:
		ret = -1;
		break;
	}
	
	return ret;
}

static int __init combo_alsps_init(void)
{
	int i, k = -1;
	
	APS_FUN();

	for (i = 0; i < COMBO_ALSPS_NUM; i++)
	{
		if (0 == combo_alsps_identify(combo_alsps_i2c_data[i][0], combo_alsps_i2c_data[i][1]))
		{
			k = i;
			break;
		}
	}
	APS_LOG("combo_alsps_init@@ k = %d\n", k);
	if (k < 0)
	{
		return -ENODEV;
	}
	else
	{
		g_alsps_driver = combo_alsps_driver[k];
	}

combo_alsps_probe:
#if (LINUX_VERSION_CODE>=KERNEL_VERSION(3,0,0))	
	i2c_register_board_info(0, &i2c_combo[k], 1);
#endif		
	if(platform_driver_register(g_alsps_driver))
	{
		APS_ERR("failed to register driver\n");
		return -ENODEV;
		
	}
	else
	{
		APS_LOG("register alsps driver ok!");
		
	}
	return 0;	
}
/*----------------------------------------------------------------------------*/
static void __exit combo_alsps_exit(void)
{
	APS_FUN();
	if (g_alsps_driver)
		platform_driver_unregister(g_alsps_driver);
}

module_init(combo_alsps_init);
module_exit(combo_alsps_exit);

MODULE_AUTHOR("MingHsien Hsieh");
MODULE_DESCRIPTION("ALSPS sensor driver");
MODULE_LICENSE("GPL");
#endif

/*----------------------------------------------------------------------------*/


