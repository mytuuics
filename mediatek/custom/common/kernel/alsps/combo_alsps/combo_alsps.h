/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/* 
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
/*
 * Definitions for ISL29044 als/ps sensor chip.
 */
#ifndef __COMBO_ALSPS_H__
#define __COMBO_ALSPS_H__

#include <linux/ioctl.h>

#ifdef MT6573
#include <mach/mt6573_pll.h>
#endif
#ifdef MT6575
#include <mach/mt6575_pm_ldo.h>
#endif
#ifdef MT6577
#include <mach/mt6577_pm_ldo.h>
#endif

#define	ISL29044_POWER_IR_LED				MT65XX_POWER_LDO_VMC

// I2C 8-bits address of the chip isl29044.
#define ISL29044_I2C_SLAVE_ADDR 		0x88 //0x3A<-->SEL to VDD  0x38<-->SEL to GND

/* registers */
#define ISL29044_REG_VENDOR_REV  	0x06
#define ISL29044_VENDOR           	1
#define ISL29044_VENDOR_MASK      	0x0F
#define ISL29044_REV               	4
#define ISL29044_REV_SHIFT        	4
#define ISL29044_REG_DEVICE         	0x22
#define ISL29044_DEVICE             	22 

// Table 1: all i2c registers and bits per register
#define ISL_REG_CMD_1					0x01 // configure, range is reg 1 bit 1
#define ISL_REG_CMD_1_INIT 			0x5A
#define ISL_REG_CMD_2					0x02 // interrupt control

#define ISL_REG_INT_LOW_PROX			0x03 // 8 bits intr low thresh for prox
#define ISL_REG_INT_HIGH_PROX		0x04 // 8 bits intr high thresh for prox
#define ISL_REG_INT_LOW_ALS 			0x05 // 8 bits intr low thresh for ALS-IR
#define ISL_REG_INT_LOW_HIGH_ALS		0x06 // 8 bits(0-3,4-7) intr high/low thresh for ALS-IR
#define ISL_REG_INT_HIGH_ALS			0x07 // 8 bits intr high thresh for ALS-IR

#define ISL_REG_DATA_PROX				0x08 // 8 bits of PROX data
#define ISL_REG_DATA_LSB_ALS			0x09 // 8 bits of ALS data
#define ISL_REG_DATA_MSB_ALS			0x0A // 4 bits of ALS MSB data

#define ISL_TEST1 						0x0E // test write 0x00
#define ISL_TEST2 						0x0F // test write 0x00

#define ISL_MOD_MASK					0xE0
#define ISL_MOD_POWERDOWN				0

#define ISL_MOD_ALS_ONCE				1
#define ISL_MOD_IR_ONCE				2
#define ISL_MOD_PS_ONCE				3
#define ISL_MOD_RESERVED				4
#define ISL_MOD_ALS_CONT				5
#define ISL_MOD_IR_CONT				6
#define ISL_MOD_PS_CONT				7
#define ISL_MOD_DEFAULT				8

#define ISL_PROX_EN_MASK          	0x80 // prox sense on mask, 1=on, 0=off
#define ISL_PROX_CONT_MASK       	 	0x70 // prox sense contimnuous mask
//IR_CURRENT_MASK is now PROX_DR_MASK with just 0 or 1 settings
#define ISL_PROX_DR_MASK          	0x08 // prox drive pulse 220ma sink mask def=0 110ma
#define ISL_ALS_EN_MASK           	0x04 // prox sense enabled contimnuous mask
#define ISL_ALS_RANGE_HIGH_MASK   	0x02 // ALS range high LUX mask
#define ISL_ALSIR_MODE_SPECT_MASK 	0x01 // prox sense contimnuous mask

#define ISL_IR_CURRENT_MASK			0xC0
#define ISL_IR_FREQ_MASK				0x30
#define ISL_SENSOR_RANGE_MASK		0x03
#define ISL_RES_MASK					0x0C


// for stk31xx
#define	STK31XX_POWER_IR_LED			MT65XX_POWER_LDO_VMC

// I2C 8-bits address of the chip stk31xx.
#define STK31XX_I2C_SLAVE_ADDR	0x90

/*ALSPS REGS*/
#define STK_ALS_CMD          0x01
#define STK_ALS_DT1          0x02
#define STK_ALS_DT2          0x03
#define STK_ALS_THDH1        0x04
#define STK_ALS_THDH2        0x05
#define STK_ALS_THDL1        0x06
#define STK_ALS_THDL2        0x07
#define STK_ALSPS_STATUS     0x08
#define STK_PS_CMD           0x09
#define STK_PS_DT            0x0A
#define STK_PS_THDH          0x0B
#define STK_PS_THDL          0x0C

#define STK_SW_RESET		  0x80
#define STK_PS_GAIN          0x82


/*ALS Command. register 0x01 */
// ALS shutdown. 0: shutdown disable, that is ALS on; 1: ALS off
#define STK_SD_ALS      (1      << 0)

// ALS int control. 0: disable; 1: enable.
#define STK_INT_ALS     (1      << 1)

// als refresh time. 1/2/4/8T, relevant to 100/200/400/800 ms
#define STK_IT_ALS      (0x03   << 2)

//#define STK_THD_ALS     (0x03   << 4)
// ALS detect range: bit6 = 0: 57671 lux; bit6 = 1: 28836 lux
#define STK_GAIN_ALS    (0x03   << 6)

// initial config data of ALS  0x45
#define	STK_ALS_CMD_DATA	(0x49)

// for status register 0x08
// ID. read-only
#define	STK_ALSPS_ID		(0x03 << 6)

// PS int flag.
#define STK_PS_INT_FLAG	(0x01 << 5)

// ALS int flag.
#define STK_ALS_INT_FLAG	(0x01 << 4)

/*Proximity sensor command register 0x09 */
// Ps shutdown. 0: PS on; 1: PS off
#define STK_SD_PS       (1      << 0)

// PS int control. 0: disable; 1: enable
#define STK_INT_PS      (1      << 1)

// PS refresh time: 1/1.5/2/2.5 T
#define STK_IT_PS       (0x03   << 2)

// IR Led sinking current. 0: 100mA; 1: 200mA
#define STK_DR_PS       (1      << 4)

// ps sleep time: 10/30/90/270 ms
#define STK_SLP_PS      (0x03   << 5)

// 0: logic OR; 1: logic AND
#define STK_INTM_PS     (1      << 7)

// Initial config data of PS  0x59	0x25
#define	STK_PS_CMD_DATA	(0x35)

// Initial config data of register 0x82, [0x05, 0x09, 0x0d]
#define	STK_PS_GAIN_4X		(0x05)
#define	STK_PS_GAIN_8X		(0x09)
#define	STK_PS_GAIN_16X		(0x0D)
#define	STK_PS_GAIN_DATA		STK_PS_GAIN_8X

#define STK_CONFIG_STK_ALS_CHANGE_THRESHOLD		5

#endif

