package com.android.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.provider.Telephony;
import android.provider.Telephony.SIMInfo;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.settings.gemini.GeminiUtils;
import com.android.settings.wifi.WifiSettings;
import com.mediatek.CellConnService.CellConnMgr;
import com.mediatek.featureoption.FeatureOption;
import com.mediatek.telephony.TelephonyManagerEx;
import com.mediatek.xlog.Xlog;

public class WifiGprsSelector extends WifiSettings implements Preference.OnPreferenceChangeListener{
    private static final String TAG = "WifiGprsSelector";
    static final boolean DBG = true;
    private static final String KEY_APN_LIST = "apn_list";
    private static final String KEY_ADD_WIFI_NETWORK = "add_network";
    private static final String KEY_DATA_ENABLER = "data_enabler";
    private static final String KEY_DATA_ENABLER_GEMINI = "data_enabler_gemini";
    private static final String KEY_DATA_ENABLER_CATEGORY = "data_enabler_category";
    private static final int DIALOG_WAITING = 1001;
    
    //time out message event
    private static final int EVENT_DETACH_TIME_OUT = 2000;
    private static final int EVENT_ATTACH_TIME_OUT = 2001;
    //time out length
    private static final int DETACH_TIME_OUT_LENGTH = 10000;
    private static final int ATTACH_TIME_OUT_LENGTH = 30000;
    
    private static final int SIM_CARD_1 = 0;
    private static final int SIM_CARD_2 = 1;
    private static final int SIM_CARD_SINGLE = 2;
    private static final int SIM_CARD_UNDEFINED = -1;
    private static final int ID_INDEX = 0;
    private static final int NAME_INDEX = 1;
    private static final int APN_INDEX = 2;
    private static final int TYPES_INDEX = 3;
    private static final int SOURCE_TYPE_INDEX = 4;
    private static final String[] sProjection = new String[] {
        Telephony.Carriers._ID,     // 0
        Telephony.Carriers.NAME,    // 1
        Telephony.Carriers.APN,     // 2
        Telephony.Carriers.TYPE,    // 3
        Telephony.Carriers.SOURCE_TYPE,    // 4
    };
    
    private boolean mIsCallStateIdle = true;  
    private boolean mAirplaneModeEnabled = false; 

    private TelephonyManager mTelephonyManager; 
    private String mSelectedKey;

    private IntentFilter mMobileStateFilter;
    private int mSelectableApnCount = 0;
    private static final String TRANSACTION_START = "com.android.mms.transaction.START";
    private static final String TRANSACTION_STOP = "com.android.mms.transaction.STOP";
    private static final String MMS_TRANSACTION = "mms.transaction";
    private static boolean mScreenEnable = true;
    
    private static final Uri PREFERAPN_URI = Uri.parse(ApnSettings.PREFERRED_APN_URI);
    private static final Uri PREFERAPN_URI_GEMINI = Uri
            .parse(ApnSettings.PREFERRED_APN_URI_GEMINI);

    
    private int mSimId;
    private Uri mUri;
    private Uri mRestoreCarrierUri;
    
    private PreferenceCategory apnList;
    private Preference addWifiNetwork;
    private CheckBoxPreference dataEnabler;
    private Preference dataEnablerGemini;
    private ITelephony iTelephony;
    private boolean isSIMExist = true;
    private WifiManager mWifiManager;

	private static final int DISPLAY_NONE = 0;
	private static final int DISPLAY_FIRST_FOUR = 1;
	private static final int DISPLAY_LAST_FOUR = 2;	
    private static final int PIN1_REQUEST_CODE = 302;
    private Map<Long,SIMInfo> mSimMap;
    private List<Long> mSimMapKeyList = null;
    private TelephonyManagerEx mTelephonyManagerEx;
    private CellConnMgr mCellConnMgr;
    private int initValue;
    
    private final BroadcastReceiver mMobileStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction(); 
            if (action.equals(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED)) {
                String reason = intent.getStringExtra(Phone.STATE_CHANGE_REASON_KEY);
                Phone.DataState state = getMobileDataState(intent);
                Xlog.d(TAG, "catch data change, reason : " + reason + "  state = " + state + ";");
                if(reason==null)return;
                if(reason.equals(Phone.REASON_DATA_ENABLED) && (state==Phone.DataState.CONNECTED) && (isGprsSwitching)){
                    timeHandler.removeMessages(EVENT_ATTACH_TIME_OUT);
                    removeDialog(DIALOG_WAITING);
                    isGprsSwitching=false;
                    updateDataEnabler();
                }else if(reason.equals(Phone.REASON_DATA_DISABLED) && (state==Phone.DataState.DISCONNECTED) && (isGprsSwitching)){
                    timeHandler.removeMessages(EVENT_DETACH_TIME_OUT);
                    removeDialog(DIALOG_WAITING);
                    isGprsSwitching=false;
                    updateDataEnabler();
                }
            }
            else if(action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                mAirplaneModeEnabled = intent.getBooleanExtra("state", false);
                Xlog.d(TAG, "AIRPLANE_MODE state changed: " + mAirplaneModeEnabled + ";");
                apnList.setEnabled(!mAirplaneModeEnabled);
                updateDataEnabler();
            }
            else if(action.equals(TRANSACTION_START)) {
                Xlog.d(TAG, "ssr: TRANSACTION_START in ApnSettings" + ";");
                mScreenEnable = false;
                apnList.setEnabled(!mAirplaneModeEnabled && mScreenEnable);
            } 
            else if(action.equals(TRANSACTION_STOP)) {
                Xlog.d(TAG, "ssr: TRANSACTION_STOP in ApnSettings" + ";");
                mScreenEnable = true;
                apnList.setEnabled(!mAirplaneModeEnabled && mScreenEnable);
            } else if(action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)){
                handleWifiStateChanged(intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN));
            }
        }
    };

    ContentObserver GprsConnectObserver = new ContentObserver(new Handler()){
        @Override
        public void onChange(boolean selfChange) {
            Xlog.i(TAG, "Gprs connection changed");
            updateDataEnabler();
        }
    };
    private PhoneStateListener mPhoneStateListener = new PhoneStateListener(){

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);
            
             mIsCallStateIdle = mTelephonyManager.getCallState() == TelephonyManager.CALL_STATE_IDLE;
        }

    };
    
    private static Phone.DataState getMobileDataState(Intent intent) {
        String str = intent.getStringExtra(Phone.STATE_KEY);
        if (str != null) {
            return Enum.valueOf(Phone.DataState.class, str);
        } else {
            return Phone.DataState.DISCONNECTED;
        }
    }
    private Runnable mServiceComplete = new Runnable() {
        public void run() {

        }
    };
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Xlog.d(TAG, "onActivityCreated()");
        //addPreferencesFromResource(R.xml.wifi_access_points_and_gprs);
        apnList = (PreferenceCategory)findPreference(KEY_APN_LIST);
        addWifiNetwork=findPreference(KEY_ADD_WIFI_NETWORK);
        
        PreferenceCategory dataEnableCategory = (PreferenceCategory)findPreference(KEY_DATA_ENABLER_CATEGORY);
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            dataEnablerGemini = findPreference(KEY_DATA_ENABLER_GEMINI);
            dataEnableCategory.removePreference(findPreference(KEY_DATA_ENABLER));
        } else {
            dataEnabler = (CheckBoxPreference)findPreference(KEY_DATA_ENABLER);
            dataEnabler.setOnPreferenceChangeListener(this);
            dataEnableCategory.removePreference(findPreference(KEY_DATA_ENABLER_GEMINI));
        }



        initPhoneState();
        mMobileStateFilter = new IntentFilter(
                TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        mMobileStateFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED); 
        mMobileStateFilter.addAction(TRANSACTION_START);
        mMobileStateFilter.addAction(TRANSACTION_STOP);
        mMobileStateFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        getActivity().setTitle(R.string.wifi_gprs_selector_title);
        
        init();
        setHasOptionsMenu(false);
        //add for single card of CMCC
        //mSimId = SIM_CARD_SINGLE;
    }
    
    @Override
    public void onResume() {
        Xlog.d(TAG,"onResume");
        super.onResume();
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE);
        getActivity().registerReceiver(mMobileStateReceiver, mMobileStateFilter);
        mAirplaneModeEnabled =  AirplaneModeEnabler.isAirplaneModeOn(getActivity());
        
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        handleWifiStateChanged(mWifiManager.getWifiState());
        
        mScreenEnable = Settings.System.getInt(this.getContentResolver(),
                MMS_TRANSACTION, 0) == 0;
        
        fillList(mSimId);
        updateDataEnabler();
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            mCellConnMgr = new CellConnMgr(mServiceComplete);
            mCellConnMgr.register(getActivity());
            getContentResolver().registerContentObserver(Settings.System.getUriFor(Settings.System.GPRS_CONNECTION_SIM_SETTING),
	                    false, GprsConnectObserver);
        }
    }
    private boolean init(){         
        Xlog.d(TAG, "init()");
        iTelephony = ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
        if(null == iTelephony){
            return false;
        }
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            try{
                isSIMExist = iTelephony.isSimInsert(Phone.GEMINI_SIM_1)||iTelephony.isSimInsert(Phone.GEMINI_SIM_2);
            }catch(RemoteException e){
                Xlog.d(TAG, "RemoteException happens......");
                return false;
            }
        }else{
            try{
                isSIMExist = iTelephony.isSimInsert(Phone.GEMINI_SIM_1);
                Xlog.d(TAG, "Is SIM exist?"+isSIMExist + ";");
            }catch(RemoteException e){
                Xlog.d(TAG, "RemoteException happens......");
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void onPause() {
        Xlog.d(TAG,"onPause");
        super.onPause();
        getActivity().unregisterReceiver(mMobileStateReceiver);
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
	        mCellConnMgr.unregister();
            getContentResolver().unregisterContentObserver(GprsConnectObserver);
        }
    }
    @Override
    public void onDestroy(){
        timeHandler.removeMessages(EVENT_ATTACH_TIME_OUT);
        timeHandler.removeMessages(EVENT_DETACH_TIME_OUT);
        super.onDestroy();
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }
    private void initPhoneState() {
        Xlog.d(TAG, "initPhoneState()");
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            Intent it = getActivity().getIntent();
            mSimId = it.getIntExtra(Phone.GEMINI_SIM_ID_KEY, SIM_CARD_UNDEFINED);
            if (DBG) 
                Xlog.d(TAG, "GEMINI_SIM_ID_KEY = " + mSimId + ";");
            mTelephonyManagerEx = TelephonyManagerEx.getDefault();
            mSimMap = new HashMap<Long,SIMInfo>();
            initSimMap();
        } else {
            if (DBG) log("Not support GEMINI");
                mSimId = SIM_CARD_SINGLE;
        }
    }
    
    private void fillList(int simId) {
        apnList.removeAll();
        if(simId < 0)return;
        Xlog.d(TAG, "fillList(), simId="+simId + ";");
        String where;

        where = "numeric=\"" + getQueryWhere(simId) + "\"";
        Cursor cursor = getActivity().managedQuery(
                mUri, // Telephony.Carriers.CONTENT_URI,
                sProjection, where,
                Telephony.Carriers.DEFAULT_SORT_ORDER);

        ArrayList<Preference> mmsApnList = new ArrayList<Preference>();

        boolean keySetChecked = false;
        mSelectedKey = getSelectedApnKey();
        Xlog.d(TAG, "mSelectedKey = " + mSelectedKey + ";"); 
        
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String name = cursor.getString(NAME_INDEX);
            String apn = cursor.getString(APN_INDEX);
            String key = cursor.getString(ID_INDEX);
            String type = cursor.getString(TYPES_INDEX);
            int sourcetype = cursor.getInt(SOURCE_TYPE_INDEX);


            ApnPreference pref = new ApnPreference(getActivity());

            pref.setSimId(simId);   //set pre sim id info to the ApnEditor
            pref.setKey(key);
            pref.setTitle(name);
            pref.setSummary(apn);
            pref.setSourceType(sourcetype);
            pref.setPersistent(false);
            pref.setOnPreferenceChangeListener(this);

            boolean selectable = ((type == null) || !type.equals("mms"));
            pref.setSelectable(selectable);
            if (selectable) {
                if ((mSelectedKey != null) && mSelectedKey.equals(key)) {
                    setSelectedApnKey(key);
                    pref.setChecked();
                    keySetChecked = true;
                    Xlog.d(TAG, "apn key: " + key + " set." + ";"); 
                }
                Xlog.d(TAG, "key:  " + key + " added!" + ";"); 
                apnList.addPreference(pref);
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    pref.setDependency(KEY_DATA_ENABLER_GEMINI);
                } else {
                    pref.setDependency(KEY_DATA_ENABLER);
                }
            } else {
                mmsApnList.add(pref);
            }
            cursor.moveToNext();
        }
//        cursor.close();

        mSelectableApnCount = apnList.getPreferenceCount();
        //if no key selected, choose the 1st one.
        if(!keySetChecked && mSelectableApnCount > 0){
            ApnPreference apnPref = (ApnPreference) apnList.getPreference(0);
            if(apnPref != null){
                setSelectedApnKey(apnPref.getKey());
                apnPref.setChecked();
                Xlog.d(TAG, "Key does not match.Set key: " + apnPref.getKey() + "."); 
            }

        }
        
        mIsCallStateIdle = mTelephonyManager.getCallState() == TelephonyManager.CALL_STATE_IDLE;
        
        switch(mSimId){
            case SIM_CARD_1:
                boolean sim1Ready = TelephonyManager.SIM_STATE_READY == TelephonyManager.getDefault().getSimStateGemini(0);
                apnList.setEnabled(mScreenEnable && mIsCallStateIdle && !mAirplaneModeEnabled && sim1Ready);
                break;
            case SIM_CARD_2:
                boolean sim2Ready = TelephonyManager.SIM_STATE_READY == TelephonyManager.getDefault().getSimStateGemini(1);
                apnList.setEnabled(mScreenEnable && mIsCallStateIdle && !mAirplaneModeEnabled && sim2Ready);
                break;
            case SIM_CARD_SINGLE:
                boolean simReady = TelephonyManager.SIM_STATE_READY == TelephonyManager.getDefault().getSimState();
                apnList.setEnabled(mScreenEnable && mIsCallStateIdle && !mAirplaneModeEnabled && simReady);
                break;
        }
        
    }
    
    private String getQueryWhere(int simId) {
        String where = "";
        
        switch (simId) {
            case SIM_CARD_1:
                mUri = Telephony.Carriers.CONTENT_URI;
                where = SystemProperties.get(TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC, "-1");
                mRestoreCarrierUri = PREFERAPN_URI;
                break;

            case SIM_CARD_2:
                mUri = Telephony.Carriers.GeminiCarriers.CONTENT_URI;
                where = SystemProperties.get(TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC_2, "-1");
                mRestoreCarrierUri = PREFERAPN_URI_GEMINI;
                break;
            
            case SIM_CARD_SINGLE:
                mUri = Telephony.Carriers.CONTENT_URI;
                where = SystemProperties.get(TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC, "");
                mRestoreCarrierUri = PREFERAPN_URI;
                break;
            
            default:
                Toast.makeText(
                        getActivity(), 
                        "Can't get any valid SIM information",
                        Toast.LENGTH_SHORT).show();
                finish();
                break;
        }

        Xlog.d(TAG, "where = "+where + ";");
        Xlog.d(TAG, "mUri = "+mUri + ";");
        return where;
    }
    
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if(DBG)log("onPreferenceChange(): Preference - " + preference
                + ", newValue - " + newValue + ", newValue type - "
                + newValue.getClass());
        String key = (preference==null?"":preference.getKey());
        if(KEY_DATA_ENABLER.equals(key)){
            final boolean checked = ((Boolean)newValue).booleanValue();
            if(DBG)log("Data connection enabled?"+checked);
            dealWithConnChange(checked);
        }  else {
            if (newValue instanceof String) {
                setSelectedApnKey((String) newValue);
            }
        }

        return true;
    }
    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen screen, Preference preference) {
        String key = preference.getKey();
        if(KEY_ADD_WIFI_NETWORK.equals(key)){
            if (mWifiManager.isWifiEnabled()) {
                Xlog.d(TAG, "add network");
                super.onAddNetworkPressedForCmcc();
            }
        } else if(KEY_DATA_ENABLER_GEMINI.equals(key)){
            //connect data connection
            SimItem simitem;
            final List<SimItem> simItemList = new ArrayList<SimItem>();
            for (Long simid: mSimMapKeyList) {
            	SIMInfo siminfo = mSimMap.get(simid);

            	if (siminfo != null) {
                    simitem = new SimItem(siminfo);
                	int state = mTelephonyManagerEx.getSimIndicatorStateGemini(siminfo.mSlot);
                	simitem.mState = state;
                    simItemList.add(simitem);
                }
            }
            simitem = new SimItem (this.getString(R.string.gemini_default_sim_never), -1, 
                    Settings.System.GPRS_CONNECTION_SIM_SETTING_NEVER);
            simItemList.add(simitem);	
            final int simListSize = simItemList.size();
            Xlog.d(TAG,"simListSize = " + simListSize);
            initValue = (mSimId == -1)? simListSize-1:mSimId;
            Xlog.d(TAG,"initValue = " + initValue);

            SelectionListAdapter mAdapter = new SelectionListAdapter(simItemList); 
            AlertDialog dialog = new AlertDialog.Builder(getActivity())
            //.setCancelable(false)
            .setSingleChoiceItems( mAdapter, initValue, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Xlog.d(TAG,"which = " + which);
                        SimItem simItem = simItemList.get(which);
                        mSimId = simItem.mSlot;
                        Xlog.d(TAG,"mSimId = " + mSimId);
                        Xlog.d(TAG,"simItem.mIsSim="+simItem.mIsSim+",simItem.mState="+simItem.mState
                            +",Phone.SIM_INDICATOR_LOCKED="+Phone.SIM_INDICATOR_LOCKED);
                        if(mCellConnMgr==null)Xlog.d(TAG,"mCellConnMgr is 17=,.cdipsvwxz-null");
                        if((simItem.mIsSim == true)&&(mCellConnMgr != null)&&(simItem.mState == Phone.SIM_INDICATOR_LOCKED)){
                            Xlog.d(TAG,"mCellConnMgr.handleCellConn");
                            mCellConnMgr.handleCellConn(simItem.mSlot, PIN1_REQUEST_CODE);
                        } else {
                            switchGprsDefautlSIM((which == simListSize-1)? 0:which+1);
                        }
                        dialog.dismiss();
                    }
                })
            .setTitle(R.string.gemini_data_connection)
            .setNegativeButton(com.android.internal.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
            .create();
            dialog.show();
        } else {
            return super.onPreferenceTreeClick(screen, preference);
        }
        return true;
    }
    private void handleWifiStateChanged(int state) {
        Xlog.d(TAG, "handleWifiStateChanged(), new state="+state + ";");
        Xlog.d(TAG, "[0- stoping 1-stoped 2-starting 3-started 4-unknown]");
        if(state==WifiManager.WIFI_STATE_ENABLED){
            addWifiNetwork.setEnabled(true);
        }else{
            addWifiNetwork.setEnabled(false);
        }
    }

    private void setSelectedApnKey(String key) {
        mSelectedKey = key;
        ContentResolver resolver = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(ApnSettings.APN_ID, mSelectedKey);
        resolver.update(mRestoreCarrierUri, values, null, null);
    }

    private String getSelectedApnKey() {
        String key = null;
        Cursor cursor = getActivity().managedQuery(mRestoreCarrierUri, 
                                    new String[] {"_id"},
                                    null, 
                                    Telephony.Carriers.DEFAULT_SORT_ORDER);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            key = cursor.getString(ID_INDEX);
        }
//        cursor.close();
        return key;
    }
    
    private boolean isGprsSwitching = false;
    Handler timeHandler = new Handler(){
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case EVENT_ATTACH_TIME_OUT:
                    Xlog.d(TAG, "attach time out......");
                    removeDialog(DIALOG_WAITING);
                    isGprsSwitching = false;
                    updateDataEnabler();
                    //if(DBG)Toast.makeText(getActivity(), R.string.data_enabler_enable_timeout_message, Toast.LENGTH_SHORT).show();
                    break;
                case EVENT_DETACH_TIME_OUT:
                    Xlog.d(TAG, "detach time out......");
                    removeDialog(DIALOG_WAITING);
                    isGprsSwitching = false;
                    //if(DBG)Toast.makeText(getActivity(), R.string.data_enabler_disable_timeout_message, Toast.LENGTH_SHORT).show();
                    updateDataEnabler();
                default:
                    break;
            }
        };
    };
    
    private void updateDataEnabler(){
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
			long dataConnectId = (int)Settings.System.getLong(getContentResolver(),
                    Settings.System.GPRS_CONNECTION_SIM_SETTING,Settings.System.DEFAULT_SIM_NOT_SET);
            Xlog.d(TAG,"updateDataEnabler,dataConnectId="+dataConnectId);

            mSimId = (int)dataConnectId - 1;
            Xlog.d(TAG,"updateDataEnabler, mSimId="+mSimId);
            fillList(mSimId);
            dataEnablerGemini.setEnabled(isSIMExist && !mAirplaneModeEnabled); 
        } else {
            ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if(cm==null){
                Xlog.d(TAG, "Fail to get ConnectivityManager instance");
                return;
            }
            boolean enabled = cm.getMobileDataEnabled();
            if(DBG)log("updateDataEnabler(), current state="+enabled);
            dataEnabler.setChecked(enabled);
            Xlog.d(TAG,"single card dataEnabler, true");
            dataEnabler.setEnabled(isSIMExist && !mAirplaneModeEnabled);
        }
    }
    
    /**
     * To enable/disable data connection
     * @param enabled
     */
    private void dealWithConnChange(boolean enabled){
        if(FeatureOption.MTK_GEMINI_SUPPORT){
            Xlog.d(TAG, "only sigle SIM load support controling data connection from here till now");
            return;
        }
        if(DBG)Xlog.d(TAG, "dealWithConnChange(),new request state is enabled?"+enabled + ";");
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm==null){
            Xlog.d(TAG, "Fail to get ConnectivityManager instance");
            return;
        }
        cm.setMobileDataEnabled(enabled);
        showDialog(DIALOG_WAITING);
        isGprsSwitching = true;
        if(enabled){
            timeHandler.sendEmptyMessageDelayed(EVENT_ATTACH_TIME_OUT, ATTACH_TIME_OUT_LENGTH);
        }else{
            timeHandler.sendEmptyMessageDelayed(EVENT_DETACH_TIME_OUT, DETACH_TIME_OUT_LENGTH);
        }
    }
 
    @Override
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        if(id==DIALOG_WAITING){
            dialog.setMessage(getResources().getString(R.string.data_enabler_waiting_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            return dialog;
        } else {
            return super.onCreateDialog(id);
        }
    }
  
    private void log(String msg) {
        Xlog.d(TAG, msg);
    }
    private void initSimMap() {
    	
    	List<SIMInfo> simList = SIMInfo.getInsertedSIMList(getActivity());
    	mSimMap.clear();
    	Xlog.i(TAG, "sim number is "+simList.size());
    	for (SIMInfo siminfo:simList) {
    		mSimMap.put(Long.valueOf(siminfo.mSimId), siminfo);
    	}
        sortSimMap();
    }
	  
    private void sortSimMap(){
        if(mSimMap == null){
            return; 
        }
        mSimMapKeyList = (List<Long>)(new ArrayList(mSimMap.keySet()));
        if(mSimMap.size() < 2){
            return; 
        }
        Long a = mSimMapKeyList.get(0);
        Long b = mSimMapKeyList.get(1);
        if(mSimMap.get(a).mSlot > mSimMap.get(b).mSlot){
            mSimMapKeyList.clear();
            mSimMapKeyList.add(b);
            mSimMapKeyList.add(a);
        }
    } 
	/**
	 * switch data connection default SIM
	 * @param value: sim id of the new default SIM
	 */
	private void switchGprsDefautlSIM(long value) {

		if(value <0) {
			return;
		}
		
		long GprsValue = Settings.System.getLong(getContentResolver(),
				Settings.System.GPRS_CONNECTION_SIM_SETTING,Settings.System.DEFAULT_SIM_NOT_SET);
        Xlog.d(TAG,"value="+value+", GprsValue=" + GprsValue + ", valueOfNotSet"+Settings.System.DEFAULT_SIM_NOT_SET);    
		if(value == GprsValue) {
			return;
		}		
		Intent intent = new Intent(Intent.ACTION_DATA_DEFAULT_SIM_CHANGED);
		intent.putExtra("simid", value);
		getActivity().sendBroadcast(intent);
        Xlog.d(TAG,"send gprs switch broadcast");
        showDialog(DIALOG_WAITING);
        isGprsSwitching = true;
        if(value > 0){
            timeHandler.sendEmptyMessageDelayed(EVENT_ATTACH_TIME_OUT, ATTACH_TIME_OUT_LENGTH);
            Xlog.d(TAG,"set ATTACH_TIME_OUT");
        }else{
            timeHandler.sendEmptyMessageDelayed(EVENT_DETACH_TIME_OUT, DETACH_TIME_OUT_LENGTH);
            Xlog.d(TAG,"set DETACH_TIME_OUT");
        }
	}
    public int getSimColorResource(int color) {
	    if((color>=0)&&(color<=7)) {
		    return Telephony.SIMBackgroundRes[color];
	    } else {
		    return -1;
	    }
    }
    public int getStatusResource(int state) {
	    switch (state) {
	    case Phone.SIM_INDICATOR_RADIOOFF:
		    return com.mediatek.internal.R.drawable.sim_radio_off;
	    case Phone.SIM_INDICATOR_LOCKED:
		    return com.mediatek.internal.R.drawable.sim_locked;
	    case Phone.SIM_INDICATOR_INVALID:
		    return com.mediatek.internal.R.drawable.sim_invalid;
	    case Phone.SIM_INDICATOR_SEARCHING:
		    return com.mediatek.internal.R.drawable.sim_searching;
	    case Phone.SIM_INDICATOR_ROAMING:
		    return com.mediatek.internal.R.drawable.sim_roaming;
	    case Phone.SIM_INDICATOR_CONNECTED:
		    return com.mediatek.internal.R.drawable.sim_connected;
	    case Phone.SIM_INDICATOR_ROAMINGCONNECTED:
		    return com.mediatek.internal.R.drawable.sim_roaming_connected;
	    default:
		    return -1;
	    }
    }
	class SelectionListAdapter extends BaseAdapter {
		
		List<SimItem> mSimItemList;
		

		
		public SelectionListAdapter(List<SimItem> simItemList) {
			mSimItemList = simItemList;
		}
		
		public int getCount() {
			return mSimItemList.size();
		}

		public Object getItem(int position) {
			return mSimItemList.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView==null) {
                LayoutInflater mFlater = LayoutInflater.from(getActivity());	
                convertView=mFlater.inflate(R.layout.preference_sim_default_select, null);
                holder=new ViewHolder();
                setViewHolderId(holder,convertView);
                convertView.setTag(holder);
            }
            else {
                holder=(ViewHolder)convertView.getTag();
            }
            SimItem simItem = (SimItem)getItem(position);
            setNameAndNum(holder.textName,holder.textNum,simItem);
            setText3G(holder.text3G,simItem);
            setImageSim(holder.imageSim,simItem);
            setImageStatus(holder.imageStatus,simItem);
            setTextNumFormat(holder.textNumFormat,simItem);
            holder.ckRadioOn.setChecked(initValue == position);
            if(simItem.mState == Phone.SIM_INDICATOR_RADIOOFF){
                convertView.setEnabled(false);
                holder.textName.setEnabled(false);
                holder.textNum.setEnabled(false);
                holder.ckRadioOn.setEnabled(false);
            }else {
                convertView.setEnabled(true);
                holder.textName.setEnabled(true);
                holder.textNum.setEnabled(true);
                holder.ckRadioOn.setEnabled(true);
            }
            return convertView;
          }
		private void setTextNumFormat(TextView textNumFormat, SimItem simItem) {
		    if(simItem.mIsSim == true) {
		        if (simItem.mNumber!=null) {
		            switch (simItem.mDispalyNumberFormat) {
		            case DISPLAY_NONE: 
		                textNumFormat.setVisibility(View.GONE);
		                break;
		            case DISPLAY_FIRST_FOUR:
		                textNumFormat.setVisibility(View.VISIBLE);
                        if (simItem.mNumber.length()>=4) {
                            textNumFormat.setText(simItem.mNumber.substring(0, 4));
                        } else {
                            textNumFormat.setText(simItem.mNumber);
                        }
                        break;
                    case DISPLAY_LAST_FOUR:
                        textNumFormat.setVisibility(View.VISIBLE);
                        if (simItem.mNumber.length()>=4) {
                            textNumFormat.setText(simItem.mNumber.substring(simItem.mNumber.length()-4));
                        } else {
                            textNumFormat.setText(simItem.mNumber);
                        }
                        break;
		            }           
                }
		    }
            
        }
        private void setImageStatus(ImageView imageStatus, SimItem simItem) {
            if(simItem.mIsSim==true){
                int res = getStatusResource(simItem.mState);
                if(res == -1) {
                    imageStatus.setVisibility(View.GONE);
                }
                else {
                    imageStatus.setVisibility(View.VISIBLE);
                    imageStatus.setImageResource(res);
                }
            }
    		    
        }
        private void setImageSim(RelativeLayout imageSim, SimItem simItem) {
		    if(simItem.mIsSim == true) {
		        int resColor = getSimColorResource(simItem.mColor);
                if(resColor>=0) {
                    imageSim.setVisibility(View.VISIBLE);
                    imageSim.setBackgroundResource(resColor);
                }
		    }
		    else if (simItem.mColor == 8){
		        imageSim.setVisibility(View.VISIBLE);
                imageSim.setBackgroundResource(com.mediatek.internal.R.drawable.sim_background_sip);
		    }
		    else {
		        imageSim.setVisibility(View.GONE);
		    }
        }

        private void setText3G(TextView text3G, SimItem simItem) {
            boolean mNeed3GText = (Utils.opType == Utils.OpIndex.OP_CU)?true:false;
		    if((mNeed3GText == false)||(simItem.mSlot != Phone.GEMINI_SIM_1) || simItem.mColor == 8){
		        text3G.setVisibility(View.GONE);
		        }
		    else
		    {
		        text3G.setVisibility(View.VISIBLE);
		    }
        }

        private void setViewHolderId(ViewHolder holder, View convertView) {
		    holder.textName=(TextView)convertView.findViewById(R.id.simNameSel);
            holder.textNum=(TextView)convertView.findViewById(R.id.simNumSel);
            holder.imageStatus=(ImageView)convertView.findViewById(R.id.simStatusSel);
            holder.textNumFormat=(TextView)convertView.findViewById(R.id.simNumFormatSel);
            holder.text3G=(TextView)convertView.findViewById(R.id.sim3gSel);
            holder.ckRadioOn=(RadioButton)convertView.findViewById(R.id.Enable_select);
            holder.imageSim=(RelativeLayout)convertView.findViewById(R.id.simIconSel);
        }

        private void setNameAndNum(TextView textName,TextView textNum, SimItem simItem) {
		    if(simItem.mName != null) {
		        textName.setVisibility(View.VISIBLE);
		        textName.setText(simItem.mName);
		    }
            else
                textName.setVisibility(View.GONE);
		    
            if((simItem.mIsSim == true) &&((simItem.mNumber != null)&&(simItem.mNumber.length() != 0))) {
                textNum.setVisibility(View.VISIBLE);
                textNum.setText(simItem.mNumber);
            }
            else
                textNum.setVisibility(View.GONE);
        }
        class ViewHolder{
		    TextView textName;
		    TextView textNum;
		    RelativeLayout imageSim;
		    ImageView imageStatus;
		    TextView textNumFormat;
		    TextView text3G;
		    RadioButton ckRadioOn;
		    
		}
	}
    class SimItem {
        public boolean mIsSim = true;
        public String mName = null;
        public String mNumber = null;
        public int mDispalyNumberFormat = 0;
        public int mColor = -1;
        public int mSlot = -1;
        public long mSimID = -1;
        public int mState = Phone.SIM_INDICATOR_NORMAL;
        
        //Constructor for not real sim
        public SimItem (String name, int color,long simID) {
        	mName = name;
        	mColor = color;
        	mIsSim = false;
        	mSimID = simID;
        }
        //constructor for sim
        public SimItem (SIMInfo siminfo) {
        	mIsSim = true;
        	mName = siminfo.mDisplayName;
        	mNumber = siminfo.mNumber;
        	mDispalyNumberFormat = siminfo.mDispalyNumberFormat;
        	mColor = siminfo.mColor;
        	mSlot = siminfo.mSlot;
        	mSimID = siminfo.mSimId;
        }
    }
}
