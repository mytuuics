/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.gallery3d.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.os.storage.StorageManager;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
//import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.Files.FileColumns;
import android.provider.MediaStore;
import android.database.Cursor;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.android.gallery3d.R;
import com.android.gallery3d.app.Gallery;
import com.android.gallery3d.common.Utils;
import com.android.gallery3d.common.BitmapUtils;
import com.android.gallery3d.util.ThreadPool.Job;
import com.android.gallery3d.util.ThreadPool.JobContext;
import com.android.gallery3d.util.ThreadPool.CancelListener;
import com.android.gallery3d.data.DecodeUtils;
import com.android.gallery3d.data.Path;
import com.android.gallery3d.data.MediaItem;

import com.mediatek.mpo.MpoDecoder;
import com.mediatek.stereo3d.Stereo3DConversion;

//this class is add purely to support Mediatek Stereo Photo display feature.
//At present, there may be three kinds of stereo photo: MPO, JPS and PNS.
//For mpo image, different frames are stored in a series of JPEG sequence.
//For jps & pns, right and left eye frames are stored as right part and left
//part of a single frame.
//MPO images are roughly handled in MpoHelper.java and MpoDecoder.java.
//This class is mainly aimed for JPS & PNS: decode left and right eye frame,
//data base query clause, and so on

public class StereoHelper {
	
    private static final String TAG = "StereoHelper";

    public static final String JPS_EXTENSION = "jps";

    public static final String PNS_MIME_TYPE = "image/pns";
    public static final String JPS_MIME_TYPE = "image/x-jps";
    //this mime type is mainly to support un-common mime-types imposed by
    //other source
    public static final String JPS_MIME_TYPE2 = "image/jps";

    public static final int MIN_STEREO_INPUT_WIDTH = 40;
    public static final int MIN_STEREO_INPUT_HEIGHT = 60;

    public static final int STEREO_LAYOUT_NONE = 0;
    public static final int STEREO_LAYOUT_FULL_FRAME = 1 << 0; 
    public static final int STEREO_LAYOUT_LEFT_AND_RIGHT = 1 << 1;
    public static final int STEREO_LAYOUT_TOP_AND_BOTTOM = 1 << 2;

    public static final String STEREO_CONVERTED_TO_2D_FOLDER = "/.ConvertedTo2D/";
    public static final String STEREO_CONVERTED_TO_2D_FOLDER2 = "/.ConvertedTo2D";

    //invalid bucket is regarded as that bucket not within images
    //table. There are three candidate:
    //1, special value of String.hashCode(), we choose 0
    //2, hashCode of DCIM/.thumbnails 
    //3, hashCode of sdcard/.ConvertedTo2D
    //for simplicity, we choose 0 as invalide bucket id
    public static final int INVALID_BUCKET_ID = 0;
    public static final String INVALID_LOCAL_PATH_END = "/0";

    //stereo signature in Bundle
    public static final String STEREO_EXTRA = "onlyStereoMedia";
    public static final String INCLUDED_STEREO_IMAGE = "includedSteroImage";
    public static final String KEY_GET_NO_STEREO_IMAGE = "get_no_stereo_image";
    public static final String ATTATCH_WITHOUT_CONVERSION = "attachWithoutConversion";

    // Must preserve order between these indices and the order of the terms in
    // the following PROJECTION array.
    private static final int INDEX_ID = 0;
    private static final int INDEX_CAPTION = 1;
    private static final int INDEX_DISPLAY_NAME = 2;
    private static final int INDEX_MIME_TYPE = 3;
    private static final int INDEX_DATE_TAKEN = 4;
    private static final int INDEX_DATE_ADDED = 5;
    private static final int INDEX_DATE_MODIFIED = 6;
    private static final int INDEX_DATA = 7;
    private static final int INDEX_BUCKET_ID = 8;
    private static final int INDEX_SIZE_ID = 9;
    private static final int INDEX_WIDTH = 10;
    private static final int INDEX_HEIGHT = 11;

    static final String[] PROJECTION =  {
            ImageColumns._ID,           // 0
            ImageColumns.TITLE,         // 1
            Images.Media.DISPLAY_NAME,  // 2
            ImageColumns.MIME_TYPE,     // 3
            ImageColumns.DATE_TAKEN,    // 4
            ImageColumns.DATE_ADDED,    // 5
            ImageColumns.DATE_MODIFIED, // 6
            ImageColumns.DATA,          // 7
            ImageColumns.BUCKET_ID,     // 8
            ImageColumns.SIZE,          // 9
            // These should be changed to proper names after they are made public.
            "width", // ImageColumns.WIDTH,         // 10
            "height", // ImageColumns.HEIGHT         // 11
    };


    private static final boolean mIsMpoSupported = 
                                          MediatekFeature.isMpoSupported();
    private static final boolean mIsStereoDisplaySupported = 
                                          MediatekFeature.isStereoDisplaySupported();

    private static final boolean mConvertToJpeg = true;
    private static final String JPG_POST_FIX = ".jpg";

    private static StorageManager mStorageManager;
    private static Drawable sStereoOverlay = null;

    public static void drawImageTypeOverlay(Context context, Bitmap bitmap) {
        if (null == sStereoOverlay) {
            sStereoOverlay = context.getResources().getDrawable(R.drawable.ic_stereo_overlay);
        }
        int width = sStereoOverlay.getIntrinsicWidth();
        int height = sStereoOverlay.getIntrinsicHeight();
        Log.d(TAG, "original stereo overlay w=" + width + ", h=" + height);
        float aspectRatio = (float) width / (float) height;
        int bmpWidth = bitmap.getWidth();
        int bmpHeight = bitmap.getHeight();
        boolean heightSmaller = (bmpHeight < bmpWidth);
        int scaleResult = (heightSmaller ? bmpHeight : bmpWidth) / 5;
        if (heightSmaller) {
            height = scaleResult;
            width = (int)((float) scaleResult * aspectRatio);
        } else {
            width = scaleResult;
            height = (int)((float) width / aspectRatio);
        }
        Log.d(TAG, "scaled stereo overlay w=" + width + ", h=" + height);
        // 2 pixels' padding for both left and bottom
        int left = 2;   
        int bottom = bmpHeight - 2;
        int top = bottom - height;
        int right = width + left;
        Log.d(TAG, "stereo overlay drawing dimension=(" + left + ", " + top + ", " + right + ", " + bottom + ")");
        sStereoOverlay.setBounds(left, top, right, bottom);
        Canvas tmpCanvas = new Canvas(bitmap);
        sStereoOverlay.draw(tmpCanvas);
    }

    public static int convertToLocalLayout(int MediaStoreLayout) {
        switch (MediaStoreLayout) {
            case MediaStore.Video.Media.STEREO_TYPE_2D: 
                return STEREO_LAYOUT_NONE;
            case MediaStore.Video.Media.STEREO_TYPE_FRAME_SEQUENCE: 
                return STEREO_LAYOUT_FULL_FRAME; 
            case MediaStore.Video.Media.STEREO_TYPE_SIDE_BY_SIDE: 
                return STEREO_LAYOUT_LEFT_AND_RIGHT;
            case MediaStore.Video.Media.STEREO_TYPE_TOP_BOTTOM: 
                return STEREO_LAYOUT_TOP_AND_BOTTOM;
        }
        return STEREO_LAYOUT_NONE;
    }

    public static int getInclusionFromData(Bundle data) {
        //Log.v(TAG,"getInclusionFromData(data="+data+")");
        if (!mIsStereoDisplaySupported || null == data) {
            if (mIsMpoSupported) {
                return MediatekFeature.INCLUDE_MPO_MAV;
            }
            return 0;
        }
        int stereoInclusion = MediatekFeature.INCLUDE_STEREO_JPS;
        // stereoInclusion |= MediatekFeature.INCLUDE_STEREO_PNS;
        stereoInclusion |= MediatekFeature.INCLUDE_STEREO_VIDEO;
        boolean onlyStereoMedia = data.getBoolean(STEREO_EXTRA, false);
        boolean getAlbum = data.getBoolean(Gallery.KEY_GET_ALBUM, false);
        if (mIsMpoSupported) {
            if (onlyStereoMedia) {
                stereoInclusion |= MediatekFeature.INCLUDE_MPO_3D;
                stereoInclusion |= MediatekFeature.INCLUDE_MPO_3D_PAN;
                //exclude normal images/videos
                stereoInclusion |= MediatekFeature.EXCLUDE_DEFAULT_MEDIA;
            } else {
                stereoInclusion |= MediatekFeature.ALL_MPO_MEDIA;
            }
        }
        //if (!getAlbum && !onlyStereoMedia) {
        if (!onlyStereoMedia) {
            //Log.e(TAG,"getInclusionFromData:create virtual folder...");
            //create a virtual folder for stereo feature
            stereoInclusion |= MediatekFeature.INCLUDE_STEREO_FOLDER;
        }
        return stereoInclusion;
    }

    public static String getWhereClause(int mtkInclusion) {
        //Log.i(TAG,"getWhereClause(mtkInclusion="+mtkInclusion+")");
        String mpoWhereClause = null;//MpoHelper.getWhereClause(mtkInclusion);
        String whereClause = null;
        if ((mtkInclusion & MediatekFeature.EXCLUDE_DEFAULT_MEDIA) != 0) {
            //add mpo inclusion
            if (null != mpoWhereClause) {
                //Log.v(TAG,"getWhereClause:add where clause add mpo");
                whereClause = (null == whereClause) ? 
                          mpoWhereClause : whereClause + " OR " + mpoWhereClause;
            }
            if ((mtkInclusion & MediatekFeature.INCLUDE_STEREO_VIDEO) != 0) {
                //Log.v(TAG,"getWhereClause:add where clause add stereo video");
                whereClause = (null == whereClause) ? 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "!=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " AND " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NOT NULL)" :
                          whereClause + " OR " + 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "!=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " AND " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NOT NULL)";
            }
        } else {
            if (null != mpoWhereClause) {
                //Log.v(TAG,"getWhereClaus2:add where clause remove mpo");
                whereClause = (null == whereClause) ? 
                          mpoWhereClause : whereClause + " AND " + mpoWhereClause;
            }
            if ((mtkInclusion & MediatekFeature.INCLUDE_STEREO_VIDEO) == 0) {
                //Log.v(TAG,"getWhereClause:add where clause add stereo video");
                whereClause = (null == whereClause) ? 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " OR " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NULL)":
                          whereClause + " AND " + 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " OR " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NULL)";
            }
        }

        String directPath = StorageManager.getDefaultPath() +
                            STEREO_CONVERTED_TO_2D_FOLDER2;
        String dirWhereClause = ImageColumns.BUCKET_ID + " != " + 
                                directPath.toLowerCase().hashCode();
        whereClause = (null == whereClause) ? 
                      dirWhereClause :
                      "(" + whereClause +  ") AND " + dirWhereClause;
        //Log.v(TAG,"getWhereClause:whereClause = "+whereClause);
        return whereClause;
    }

    public static String getWhereClause(int mtkInclusion, boolean queryVideo) {
        //Log.i(TAG,"getWhereClause(mtkInclusion="+mtkInclusion+",queryVideo="+queryVideo+")");
        String mpoWhereClause = MpoHelper.getWhereClause(mtkInclusion);
        String whereClause = null;
        if ((mtkInclusion & MediatekFeature.EXCLUDE_DEFAULT_MEDIA) != 0) {
            if (!queryVideo) {
                //add mpo inclusion
                if (null != mpoWhereClause) {
                    //Log.v(TAG,"getWhereClause:add where clause add mpo");
                    whereClause = (null == whereClause) ? 
                          mpoWhereClause : whereClause + " OR " + mpoWhereClause;
                }
                if ((mtkInclusion & MediatekFeature.INCLUDE_STEREO_JPS) != 0) {
                    //Log.v(TAG,"getWhereClause:add where clause add jps");
                    whereClause = (null == whereClause) ? 
                          FileColumns.MIME_TYPE + "='" + JPS_MIME_TYPE + "'" :
                          whereClause + " OR " + 
                          FileColumns.MIME_TYPE + "='" + JPS_MIME_TYPE + "'";
                }
            } else {
                if ((mtkInclusion & MediatekFeature.INCLUDE_STEREO_VIDEO) != 0) {
                    //Log.v(TAG,"getWhereClause:add where clause add stereo video");
                    whereClause = (null == whereClause) ? 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "!=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " AND " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NOT NULL)" :
                          whereClause + " OR " + 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "!=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " AND " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NOT NULL)";
                }
            }
        } else {
            if (!queryVideo) {
                if (null != mpoWhereClause) {
                    //Log.v(TAG,"getWhereClaus2:add where clause remove mpo");
                    whereClause = (null == whereClause) ? 
                          mpoWhereClause : whereClause + " AND " + mpoWhereClause;
                }
                if ((mtkInclusion & MediatekFeature.INCLUDE_STEREO_JPS) == 0) {
                    //Log.v(TAG,"getWhereClause2:add where clause remove jps");
                    whereClause = (null == whereClause) ? 
                          FileColumns.MIME_TYPE + "!='" + JPS_MIME_TYPE + "'" :
                          whereClause + " AND " + 
                          FileColumns.MIME_TYPE + "!='" + JPS_MIME_TYPE + "'";
                }
            } else {
                if ((mtkInclusion & MediatekFeature.INCLUDE_STEREO_VIDEO) != 0) {
                    //Log.v(TAG,"getWhereClause:add where clause add stereo video");
                    whereClause = (null == whereClause) ? 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " OR " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NULL)":
                          whereClause + " AND " + 
                          "("+MediaStore.Video.Media.STEREO_TYPE + "=" + 
                          MediaStore.Video.Media.STEREO_TYPE_2D + " OR " +
                          MediaStore.Video.Media.STEREO_TYPE + " IS NULL)";
                }
            }
        }

        String directPath = StorageManager.getDefaultPath() +
                            STEREO_CONVERTED_TO_2D_FOLDER2;
        String dirWhereClause = ImageColumns.BUCKET_ID + " != " + 
                                directPath.toLowerCase().hashCode();
        whereClause = (null == whereClause) ? 
                      dirWhereClause :
                      "(" + whereClause +  ") AND " + dirWhereClause;
        //Log.v(TAG,"getWhereClause:whereClause = "+whereClause);
        return whereClause;
    }

    public static MediatekFeature.DataBundle
        generateSecondImage(Bitmap bitmap, MediatekFeature.Params params,
                            boolean recyleBitmap) {
        if (null == bitmap) return null;

        if (MIN_STEREO_INPUT_WIDTH > bitmap.getWidth() ||
            MIN_STEREO_INPUT_HEIGHT > bitmap.getHeight()) {
            Log.i(TAG,"generateSecondImage:image dimension too small");
            return null;
        }

        int originBitmapW = bitmap.getWidth();
        int originBitmapH = bitmap.getHeight();

        //be careful if one dimension is not even.
        int increaseX = originBitmapW % 2;
        int increaseY = originBitmapH % 2;

        Bitmap input = null;
        if (increaseX + increaseY > 0) {
            Log.d(TAG, "generateSecondImage:resize before convert");
            input = Bitmap.createBitmap(originBitmapW + increaseX, 
                                        originBitmapH + increaseY,
                                        Bitmap.Config.ARGB_8888);
            //draw original bitmap to input Bitmap
            Canvas tempCanvas = new Canvas(input);
            Rect src = new Rect(0, 0, originBitmapW, originBitmapH);
            RectF dst = new RectF(0, 0, originBitmapW, originBitmapH);
            tempCanvas.drawBitmap(bitmap, src, dst, null);
        } else {
            input = bitmap;
        }

        Bitmap stereo = convert2Dto3D(input);

        if (recyleBitmap) {
            bitmap.recycle();
        }
        if (bitmap != input) {
            input.recycle();
        }

        return retrieveDataBundle(stereo, params,originBitmapW, originBitmapH,
                                   increaseX, increaseY);
    }

    private static MediatekFeature.DataBundle
        retrieveDataBundle(Bitmap stereo, MediatekFeature.Params params,
            int originBitmapW, int originBitmapH, int increaseX, int increaseY) {
        if (null == stereo || null == params) {
            Log.w(TAG, "retrieveDataBundle: got null stereo or params");
            return null;
        }

        MediatekFeature.DataBundle dataBundle = 
                        new MediatekFeature.DataBundle();

        if (params.inFirstFrame) {
            dataBundle.firstFrame = retrieveStereoImage(stereo, true,
                            originBitmapW, originBitmapH, increaseX, increaseY);
        }
        if (params.inSecondFrame) {
            dataBundle.secondFrame =  retrieveStereoImage(stereo, false,
                            originBitmapW, originBitmapH, increaseX, increaseY);
        }

        //dataBundle.showInfo();//
        return dataBundle;
    }

    private static Bitmap retrieveStereoImage(Bitmap stereo, boolean first,
            int originBitmapW, int originBitmapH, int increaseX, int increaseY) {
        Bitmap temp = Bitmap.createBitmap(originBitmapW, 
                                          originBitmapH,
                                          Bitmap.Config.ARGB_8888);

        //draw first/second screen nail onto it
        Canvas canvas = new Canvas(temp);
        int x = first ?
                0 : stereo.getWidth()/2;
        int right = first ?
                stereo.getWidth()/2 : stereo.getWidth();
        Rect src = new Rect(x, 0, right - increaseX, 
                            stereo.getHeight() - increaseY);
        RectF dst = new RectF(0, 0, originBitmapW, originBitmapH);
        canvas.drawBitmap(stereo, src, dst, null);
        return temp;
    }

    public static Bitmap convert2Dto3D(Bitmap bitmap) {
        if (null == bitmap) return null;

        //perform dimension check

        //now conduct
        Stereo3DConversion conversion = new Stereo3DConversion();
        Bitmap stereo = conversion.perform2DTo3DConversion(bitmap);

        return stereo;
    }

    public static Uri convertTo2DUri(JobContext jc, Context context, MediaItem item) {
        if (null == jc || null == context || null == item) {
            Log.e(TAG,"Got null parameters");
            return null;
        }
        if (jc.isCancelled()) return null;
        Uri convertedUri = null;
        convertedUri = convertTo2D(jc, context,
                         item.getContentUri(), item.getMimeType());
        if (null == convertedUri) {
            Log.e(TAG,"failed to convert "+item.getContentUri());
            return null;
        }
        if (jc.isCancelled()) return null;
        return convertedUri;
    }

    public static Uri convertTo2D(JobContext jc, Context context, Uri origUri, String mimeType) {
        if (null == origUri) {
            Log.e(TAG,"convertTo2D:got null uri!");
            return null;
        }

        Uri imageUri = origUri;
        //change original uri into hashcode

        //for all kinds of uri other than content://media/..., we replace the 
        //original converted image if any and update the data base
        //
        //for content://media/... kind uri, we create a new file the first
        //time we convert, insert a record in media database including 
        //date_modified. next time we want to convert, we can firstly check 
        //if we can find a previously converted image:
        //    If we can, use it directly;
        //    If we can not, convert a image and insert database
        //The criteria we say we find a previously converted image is that:
        //    1,the image title matches the calculated new image title according
        //      to original uri's hashCode.
        //    2,bucket_id matches (saved in special directory)
        //    3,date_modified matches the original image's date_modified
        String fileName = String.valueOf(origUri.hashCode()); 
        String directPath = StorageManager.getDefaultPath() +
                            STEREO_CONVERTED_TO_2D_FOLDER;
        //Log.i(TAG,"convertTo2D:directPath="+directPath);
        if (origUri.toString().toLowerCase().startsWith("content://media/")) {
            //Log.v(TAG,"convertTo2D:for content media uri "+origUri);
            imageUri = convertTo2D(jc, context, origUri, directPath, fileName,
                                   mimeType, mConvertToJpeg, false);
        } else {
            //Log.v(TAG,"convertTo2D:for uri from other app:"+origUri);
            imageUri = convertTo2D(jc, context, origUri, directPath, fileName,
                                   mimeType, mConvertToJpeg, true);
        }

        //Log.i(TAG,"convertTo2D:returns "+imageUri);
        return imageUri;
    }

    private static Uri convertTo2D(JobContext jc, Context context, Uri origUri, String directPath, String fileName,
                                   String mimeType, boolean convertToJpeg, boolean forceReplace) {
        if (null == context || null == origUri || null == directPath ||
            null == fileName) {
            Log.e(TAG,"convertTo2D: invalid parameters");
            return null;
        }

        if (!convertToJpeg) {
            Log.w(TAG,"convertTo2D: should convert to JPEG");
            return null;
        }
        Uri imageUri = null;

        String displayName = fileName + JPG_POST_FIX;
        String outputPath = directPath + displayName;

        boolean matched = false;
        Cursor cursor = null;
        Cursor targetCursor = null;
        ContentResolver cr = context.getContentResolver();
        if (null == cr) {
            Log.e(TAG,"convertTo2D:got null ContentResolver!");
            return null;
        }

        if (!forceReplace) {
            cursor = cr.query(origUri, PROJECTION, null,null,null);
            if (null != cursor && !cursor.moveToNext()) {
                Log.w(TAG,"convertedTo2D:did not queried any data for " + origUri);
                cursor.close();
                cursor = null;
            }
        }

        targetCursor = cr.query(Images.Media.EXTERNAL_CONTENT_URI, 
                                PROJECTION, Images.Media.DATA +
                                "= ?", new String[]{ outputPath } ,null);
        //Log.i(TAG,"convertTo2D:got targetCursor:"+targetCursor);
        if (null != targetCursor && !targetCursor.moveToNext()) {
            Log.w(TAG,"convertedTo2D:did not queried any data for " + displayName);
            targetCursor.close();
            targetCursor = null;
        }

        boolean existing = targetCursor != null;
        Log.i(TAG,"convertedTo2D:existing="+existing);
        if (existing) {
            if (forceReplace) {
                //we should re-write the converted image and update DataBase
                //we delete any record that matches when we force refresh
                //Log.v(TAG,"convertedTo2d:delete existing...");
                cr.delete(Images.Media.EXTERNAL_CONTENT_URI, 
                      Images.Media._ID + "= ?", new String[]{ 
                      String.valueOf(targetCursor.getInt(INDEX_ID)) });
                imageUri = saveAndInsertLocalImage(jc, origUri,
                           cr, cursor, mimeType, true, 0,
                           directPath, fileName, displayName);
            } else {
                //we should check whether the databases matches. If matches,
                //directly returns the existing uri; If not, we follow 
                //forceReplace routin.
                if (null == cursor) {
                    //do nothing, and imageUri remains null
                } else if (true){//cursor.getLong(INDEX_DATE_MODIFIED) ==
                           //targetCursor.getLong(INDEX_SIZE_ID)) {
                    //Log.d(TAG,"convertTo2D:two cursor matched!");
                    imageUri = Uri.parse(Images.Media.EXTERNAL_CONTENT_URI +
                                     "/" + targetCursor.getInt(INDEX_ID));
                } else {
                    //we have to delete existing, and insert a new one
//Log.d(TAG,"convertTo2D:two cursors DO NOT matched!");
//Log.v(TAG,"convertTo2D:cursor.getLong(INDEX_DATE_MODIFIED)="+cursor.getLong(INDEX_DATE_MODIFIED));
//Log.w(TAG,"convertTo2D:targetCursor.getLong(INDEX_SIZE_ID)="+targetCursor.getLong(INDEX_SIZE_ID));
                    cr.delete(Images.Media.EXTERNAL_CONTENT_URI, 
                          Images.Media._ID + "= ?", new String[]{ 
                          String.valueOf(targetCursor.getInt(INDEX_ID)) });
                    imageUri = saveAndInsertLocalImage(jc, origUri,
                               cr, cursor, mimeType, true, 0,
                               directPath, fileName, displayName);
                }
            }
        } else {
            //if not found, we write the converted image and insert it to
            //DataBase
            imageUri = saveAndInsertLocalImage(jc, origUri,
                       cr, cursor, mimeType, true, 0,
                       directPath, fileName, displayName);
        }

        if (null != cursor) {
            cursor.close();
            cursor = null;
        }
        if (null != targetCursor) {
            targetCursor.close();
            targetCursor = null;
        }

        Log.v(TAG,"convertedTo2D:imageUri="+imageUri);
        return imageUri;
    }

    private static Uri saveAndInsertLocalImage(JobContext jc, Uri origUri,
                           ContentResolver cr, Cursor cursor,
                           String mimeType, boolean firstFrame, int targetSize,
                           String directPath, String fileName, String displayName) {
        Uri imageUri = null;
        Bitmap image = getStereoImage(jc, cr, origUri, mimeType, firstFrame,
                                      null, targetSize);
        //Log.v(TAG,"convertedTo2D:got image "+image);
        if (null != image) {
            File directory = new File(directPath);
            File saved = saveLocalImage(jc, image, directory, displayName);
            if (null != saved) {
                imageUri = insertLocalImage(jc, saved, fileName, cr,
                                      cursor, image.getWidth(), image.getHeight());
            } else {
                Log.w(TAG,"convertedTo2D:failed to save local image");
            }
            image.recycle();
        }
        return imageUri;
    }

    private static Uri insertLocalImage(JobContext jc, File image, String fileName,
                                ContentResolver cr, Cursor cursor, int width, int height) {
        //Log.i(TAG,"insertLocalImage(jc,image="+image+",fileName="+fileName+",cursor="+cursor+")");

        long now = System.currentTimeMillis() / 1000;
        ContentValues values = new ContentValues();
        values.put(Images.Media.TITLE, fileName);
        values.put(Images.Media.DISPLAY_NAME, fileName + JPG_POST_FIX);
        values.put(Images.Media.MIME_TYPE, "image/jpeg");
        values.put(Images.Media.DATA, image.getAbsolutePath());
        values.put(Images.Media.DATE_TAKEN, now);
        values.put(Images.Media.DATE_ADDED, now);
        values.put(Images.Media.DATE_MODIFIED, now);
        values.put("width", width);
        values.put("height", height);
        if (null != cursor) {
            //as date modifie field will be modified by MediaProvider when 
            //inserting, we use size field to hold data modified. a very
            //bad "workaround"
            values.put(Images.Media.SIZE, cursor.getLong(INDEX_DATE_MODIFIED));
        } else {
            values.put(Images.Media.SIZE, image.length());
        }
        Log.i(TAG,"insertLocalImage:values="+values);
        return cr.insert(
                Images.Media.EXTERNAL_CONTENT_URI, values);
    }


    private static boolean saveBitmapToOutputStream(
            JobContext jc, Bitmap bitmap, CompressFormat format, OutputStream os) {
        // We wrap the OutputStream so that it can be interrupted.
        final InterruptableOutputStream ios = new InterruptableOutputStream(os);
        if (null != jc) {
            jc.setCancelListener(new CancelListener() {
                    public void onCancel() {
                        ios.interrupt();
                    }
                });
        }
        try {
            bitmap.compress(format, 100, os);
            return !jc.isCancelled();
        } finally {
            jc.setCancelListener(null);
            Utils.closeSilently(os);
        }
    }

    private static File saveLocalImage(
            JobContext jc, Bitmap bitmap, File directory, String filename) {
        if ((!directory.exists()) && (!directory.mkdir())) {
            Log.e(TAG, "saveLocalImage: create directory file: " + directory.getPath()
                    + " failed");
            return null;
        }

        File candidate = null;
        candidate = new File(directory, filename);
        try {
            boolean created = candidate.createNewFile();
            if (!created) {
                Log.w(TAG,"saveLocalImage: error may happen when create new file");
            }
        } catch (IOException e) {
            Log.e(TAG, "saveLocalImage: fail to create new file: "
                    + candidate.getAbsolutePath(), e);
            return null;
        }

        if (!candidate.exists() || !candidate.isFile()) {
            throw new RuntimeException("cannot create file: " + filename);
        }

        candidate.setReadable(true, false);
        candidate.setWritable(true, false);

        try {
            FileOutputStream fos = new FileOutputStream(candidate);
            try {
                saveBitmapToOutputStream(jc, bitmap, CompressFormat.JPEG, fos);
            } finally {
                fos.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "saveLocalImage: fail to save image: "
                    + candidate.getAbsolutePath(), e);
            candidate.delete();
            return null;
        }

        if (null != jc && jc.isCancelled()) {
            candidate.delete();
            return null;
        }

        return candidate;
    }

    private static Bitmap getMpoFrame(JobContext jc, MpoDecoder mpoDecoder, 
                boolean firstFrame, Options options, int targetSize) {
        if (null == mpoDecoder) {
            Log.w(TAG,"getMpoFrame:null MpoDecoder");
            return null;
        }
        if (mpoDecoder.frameCount() < 1) {
            Log.w(TAG,"getMpoFrame:invalid frame count:"+
                      mpoDecoder.frameCount());
            return null;
        }
        if (null != jc && jc.isCancelled()) return null;

        int frameIndex = firstFrame ? 0 : 1;
        if (!firstFrame && 4 == mpoDecoder.frameCount()) {
            //sepecial workaround for sony 3d panorama, because it
            //contains 4 image, first two are left eyed, the last
            //two are right eyed.
            frameIndex = 2;
        }
        if (options == null) options = new Options();
        if (targetSize > 0) {
            mpoDecoder.frameBitmap(frameIndex, options);
            if (null != jc && jc.isCancelled()) return null;
            options.inSampleSize = BitmapUtils.computeSampleSizeLarger(
                   mpoDecoder.width(), mpoDecoder.height(), targetSize);
        } else {
            options.inSampleSize = 1;
        }
        return mpoDecoder.frameBitmap(frameIndex, options);
    }

    public static Bitmap getStereoImage(JobContext jc, ContentResolver cr, Uri uri,
                                        String mimeType, boolean firstFrame,
                                        Options options, int targetSize) {
        Bitmap bitmap = null;
        if (uri != null && null != mimeType) {
            if (options == null) options = new Options();

            if ("image/mpo".equals(mimeType)) {
                MpoDecoder mpoDecoder = MpoDecoder.decodeUri(cr, uri);
                if (null == mpoDecoder) {
                    Log.e(TAG,"getStereoImage:failed to decode mpo uri:"+ uri);
                    return null;
                }
                try {
                    bitmap = getMpoFrame(jc, mpoDecoder, firstFrame,
                                         options, targetSize);
                } finally {
                    mpoDecoder.close();
                }
            } else if (JPS_MIME_TYPE.equals(mimeType)) {
                ParcelFileDescriptor pfd = null;
                FileDescriptor fd = null;
                try {
                    pfd = cr.openFileDescriptor(uri, "r");
                    fd = pfd.getFileDescriptor();
                    Rect imageRect = generateRect(fd, mimeType, firstFrame, options);
                    Log.i(TAG,"getStereoImage:imageRect="+imageRect);
                    if (null != jc && jc.isCancelled()) return null;

                    if (targetSize > 0) {
                        options.inSampleSize = BitmapUtils.computeSampleSizeLarger(
                                    imageRect.right - imageRect.left, 
                                    imageRect.bottom - imageRect.top, targetSize);
                    } else {
                        options.inSampleSize = 1;
                    }
                    //Log.d(TAG,"getStereoImage:options.inSampleSize="+options.inSampleSize);

                    bitmap = getFlatternedImage(jc, fd, mimeType, firstFrame,
                                                options, imageRect);
                } catch (Exception ex) {
                    Log.w(TAG, ex);
                } finally {
                    Utils.closeSilently(pfd);
                }
            }
        } else {
            Log.e(TAG,"getStereoImage:get null local path");
        }
        if (null != bitmap) {
            Log.i(TAG,"getStereoImage:["+bitmap.getWidth()+"x"+bitmap.getHeight()+"]");
        }
        return bitmap;
    }

    public static Bitmap getStereoImage(JobContext jc, String localFilePath,
                                        String mimeType, boolean firstFrame,
                                        Options options, int targetSize) {
        Bitmap bitmap = null;
        if (localFilePath != null) {
            if (options == null) options = new Options();

            if (localFilePath.toLowerCase().endsWith(".mpo")) {
                MpoDecoder mpoDecoder = MpoDecoder.decodeFile(localFilePath);
                if (null == mpoDecoder) {
                    Log.e(TAG,"getStereoImage:failed to decode mpo file:"+
                              localFilePath);
                    return null;
                }
                try {
                    bitmap = getMpoFrame(jc, mpoDecoder, firstFrame,
                                         options, targetSize);
                } finally {
                    mpoDecoder.close();
                }
            } else if (localFilePath.toLowerCase().endsWith(".jps")) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(localFilePath);
                    FileDescriptor fd = fis.getFD();

                    Rect imageRect = generateRect(fd, mimeType, firstFrame, options);
                    Log.i(TAG,"getStereoImage:imageRect="+imageRect);
                    if (null != jc && jc.isCancelled()) return null;

                    options.inSampleSize = BitmapUtils.computeSampleSizeLarger(
                                imageRect.right - imageRect.left, 
                                imageRect.bottom - imageRect.top, targetSize);
                    //Log.w(TAG,"getStereoImage:options.inSampleSize="+options.inSampleSize);

                    bitmap = getFlatternedImage(jc, fd, mimeType, firstFrame,
                                                options, imageRect);
                } catch (Exception ex) {
                    Log.w(TAG, ex);
                } finally {
                    Utils.closeSilently(fis);
                }
            }
        } else {
            Log.e(TAG,"getStereoImage:get null local path");
        }
        //if (null != bitmap) {
        //    Log.i(TAG,"getStereoImage:["+bitmap.getWidth()+"x"+bitmap.getHeight()+"]");
        //}
        return bitmap;
    }

    //Stretch half of original video frame as a whole video frame
    public static Bitmap getStereoVideoImage(JobContext jc, Bitmap originFrame, 
                                        boolean firstFrame, int mediaStoreLayout) {
        if (null == originFrame || originFrame.getWidth() <= 0 ||
            originFrame.getHeight() <= 0) {
            Log.e(TAG, "getStereoVideoImage:got invalid original frame");
            return null;
        }
        int localLayout = convertToLocalLayout(mediaStoreLayout);
        //Log.i(TAG,"getStereoVideoImage:localLayout="+localLayout);

        if (STEREO_LAYOUT_NONE == localLayout ||
            STEREO_LAYOUT_FULL_FRAME == localLayout && !firstFrame) {
            Log.e(TAG, "getStereoVideoImage:can not retrieve second image!");
            return null;
        }

        boolean isLeftRight = (STEREO_LAYOUT_LEFT_AND_RIGHT == localLayout);
        Rect src = new Rect(0, 0, originFrame.getWidth(), originFrame.getHeight());
        //Log.v(TAG,"getStereoVideoImage:src="+src);
        //create a new bitmap that has the same dimension as the original video
        Bitmap bitmap = Bitmap.createBitmap(src.right - src.left, 
                                           src.bottom - src.top,
                                           Bitmap.Config.ARGB_8888);
        adjustRect(isLeftRight, firstFrame, src);
        //Log.d(TAG,"getStereoVideoImage:src="+src);
        Canvas canvas = new Canvas(bitmap);
        RectF dst = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
        canvas.drawBitmap(originFrame, src, dst, null);

        if (null != bitmap) {
            Log.i(TAG,"getStereoVideoImage:["+bitmap.getWidth()+"x"+bitmap.getHeight()+"]");
        }
        return bitmap;
    }

    public static Bitmap getFlatternedImage(JobContext jc, FileDescriptor fd, 
                             String mimeType, boolean firstFrame, Options options,
                             Rect imageRect) {
        //Log.i(TAG,"getFlatternedImage(fd="+fd+",imageRect="+imageRect+
        //                              ",firstFrame="+firstFrame+"..)");
        if (null == fd) {// && null == mimeType) {
            Log.e(TAG,"getFlatternedImage:invalid parameters");
        }

        if (options == null) options = new Options();

        Bitmap flatternedImage = null;

        BitmapRegionDecoder mRegionDecoder =
                                 DecodeUtils.requestCreateBitmapRegionDecoder(
                                                  null, fd, false);
        //Log.v(TAG, "getFlatternedImage:got mRegionDecoder="+mRegionDecoder);
        if (null != jc && jc.isCancelled()) return null;
        if (null != mRegionDecoder) {
            if (null == imageRect) {
                imageRect = generateRect(fd, mimeType, firstFrame, options);
                if (jc.isCancelled()) return null;
            }
            flatternedImage = safeDecodeImageRegion(jc, mRegionDecoder,
                                                    imageRect, options);
        } else {
            Log.w(TAG,"getFlatternedImage:we have to decode using BitmapFactory..");
        }

        return flatternedImage;        
    }

    public static Bitmap getFlatternedImage(JobContext jc, FileDescriptor fd,
                             String mimeType, boolean firstFrame, Options options) {
        //Log.i(TAG,"getFlatternedImage(fd="+fd+",mimeType="+mimeType+
        //                              ",firstFrame="+firstFrame+"..)");
        if (null == fd) {// && null == mimeType) {
            Log.e(TAG,"getFlatternedImage:invalid parameters");
        }

        if (options == null) options = new Options();

        Bitmap flatternedImage = null;

        BitmapRegionDecoder mRegionDecoder =
                                 DecodeUtils.requestCreateBitmapRegionDecoder(
                                                  null, fd, false);
        //Log.v(TAG, "getFlatternedImage:got mRegionDecoder="+mRegionDecoder);
        if (null != jc && jc.isCancelled()) return null;
        if (null != mRegionDecoder) {
            Rect imageRect = generateRect(fd, mimeType, firstFrame, options);
            if (null != jc && jc.isCancelled()) return null;
            flatternedImage = safeDecodeImageRegion(jc, mRegionDecoder,
                                                    imageRect, options);
        } else {
            Log.w(TAG,"getFlatternedImage:we have to decode using BitmapFactory..");
        }

        return flatternedImage;        
    }

    public static Bitmap getFlatternedImage(String filePath, String mimeType,
                                    boolean firstFrame, Options options) {
        //Log.i(TAG,"getFlatternedImage(filePath="+filePath+",mimeType="+mimeType+
        //                              ",firstFrame="+firstFrame+"..)");
        if (null == filePath && null == mimeType) {
            Log.e(TAG,"getFlatternedImage:invalid parameters");
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
            FileDescriptor fd = fis.getFD();
            return getFlatternedImage(null, fd, mimeType, firstFrame, options);
        } catch (Exception ex) {
            Log.w(TAG, ex);
            return null;
        } finally {
            Utils.closeSilently(fis);
        }
    }

    //this function return part of origin image as a Bitmap. It may shrink the
    //Bitmap size in case that out of memory happens
    public static Bitmap safeDecodeImageRegion(JobContext jc, BitmapRegionDecoder
                              regionDecoder, Rect imageRect, Options options) {
        if (null == regionDecoder || null == imageRect) {
            Log.e(TAG,"safeDecodeImageRegion:invalid region decoder or rect");
            return null;
        }
        if (options == null) options = new Options();
        if (options.inSampleSize < 1) options.inSampleSize = 1;

        Bitmap bmp = null;

        if (null != jc && jc.isCancelled()) return null;

        //add protection in case rect is not valid
        try {
            try {
                //Log.i(TAG,"safeDecodeImageRegion:decodeRegion(rect="+imageRect+"..)");
                bmp = regionDecoder.decodeRegion(imageRect, options);
                //Log.v(TAG,"safeDecodeImageRegion:decodeRegion() returns "+bmp);
            } catch (OutOfMemoryError e) {
                Log.w(TAG,"safeDecodeImageRegion:out of memory when decoding:"+e);
                bmp = null;
            }
            //As there is a chance no enough dvm memory for decoded Bitmap,
            //Skia will return a null Bitmap. In this case, we have to
            //downscale the decoded Bitmap by increase the options.inSampleSize
            if (null == bmp) {
                final int maxTryNum = 8;
                for (int i = 0; i < maxTryNum; i++) {
                    if (null != jc && jc.isCancelled()) return null;
                    //we increase inSampleSize to expect a smaller Bitamp
                    options.inSampleSize *= 2;
                    Log.w(TAG,"safeDecodeImageRegion:try for sample size " +
                              options.inSampleSize);
                    try {
                        //Log.i(TAG,"safeDecodeImageRegion:decodeRegion(rect="+imageRect+"..)");
                        bmp = regionDecoder.decodeRegion(imageRect, options);
                        //Log.v(TAG,"safeDecodeImageRegion:decodeRegion() returns "+bmp);
                    } catch (OutOfMemoryError e) {
                        Log.w(TAG,"safeDecodeImageRegion:out of memory when decoding:"+e);
                        bmp = null;
                    }
                    if (null != bmp) break;
                }
                if (null == bmp) {
                    Log.e(TAG,"safeDecodeImageRegion:failed to get a Bitmap");
                }
            }
        } catch (IllegalArgumentException e) {
            Log.w(TAG,"safeDecodeImageRegion:got exception:"+e);
        }
        if (null != bmp) {
            Log.i(TAG,"safeDecodeImageRegion:["+bmp.getWidth()+"x"+bmp.getHeight()+"]");
        }
        return bmp;
    }

    public static Rect generateRect(FileDescriptor fd, String mimeType,
                                    boolean firstFrame, Options options) {
        if (null == fd && null == mimeType) {
            Log.e(TAG,"generateRect:invalid parameters");
        }

        //actually, there is a "layout" tag in the specified JPS specification,
        //and we should parse the origion file to get this layout, and decide
        //which part of this image is dedicated firstFrame, and return its Rect.
        boolean isLeftRight = true;

        Rect imageRect = imageRect = decodeImageRect(fd, options);

        adjustRect(isLeftRight, firstFrame, imageRect);

        return imageRect;
    }

    public static Rect generateRect(String filePath, String mimeType,
                                    boolean firstFrame, Options options) {
        //Log.v(TAG,"generateRect(filePath="+filePath+",mimeType="+mimeType+")");
        if (null == filePath && null == mimeType) {
            Log.e(TAG,"generateRect:invalid parameters");
        }

        //actually, there is a "layout" tag in the specified JPS specification,
        //and we should parse the origion file to get this layout, and decide
        //which part of this image is dedicated firstFrame, and return its Rect.
        boolean isLeftRight = true;

        Rect imageRect = decodeImageRect(filePath, options);

        adjustRect(isLeftRight, firstFrame, imageRect);

        return imageRect;
    }

    private static void adjustRect(boolean isLeftRight,boolean firstFrame, 
                                  Rect imageRect) {
        //Log.i(TAG,"adjustRect:got imageRect: "+imageRect);
        if (null == imageRect) {
            Log.e(TAG,"adjustRect:got null image rect");
            return;
        }
        if (isLeftRight) {
            if (firstFrame) {
                imageRect.set(imageRect.left, imageRect.top,
                              (imageRect.left + imageRect.right) / 2, 
                              imageRect.bottom);
            } else {
                imageRect.set((imageRect.left + imageRect.right) / 2, 
                              imageRect.top, imageRect.right, imageRect.bottom);
            }
        } else {
            if (firstFrame) {
                imageRect.set(imageRect.left, imageRect.top,
                              imageRect.right, 
                              (imageRect.top + imageRect.bottom) / 2);
            } else {
                imageRect.set(imageRect.left, 
                              (imageRect.top + imageRect.bottom) / 2, 
                              imageRect.right, imageRect.bottom);
            }
        }
        //Log.d(TAG,"adjustRect:adjusted imageRect: "+imageRect);
    }

    public static int adjustDim(boolean dimX, int layout, int length) {
        if (dimX) {
            //for dimension x, we have to check left-and-right layout
            if (STEREO_LAYOUT_LEFT_AND_RIGHT == layout) {
                return length / 2;
            } else {
                return length;
            }
        } else {
            if (STEREO_LAYOUT_TOP_AND_BOTTOM == layout) {
                return length / 2;
            } else {
                return length;
            }
        }
    }

    public static Rect decodeImageRect(FileDescriptor fd, Options options) {
        if (null == fd) {
            Log.e(TAG,"decodeImageRect:got null FileDescriptor");
            return null;
        }

        if (options == null) options = new Options();

        //save value that we will modified
        boolean temp = options.inJustDecodeBounds;

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fd, null, options);

        //restore the value we changed
        options.inJustDecodeBounds = temp;

        return getRect(options);
    }

    public static Rect decodeImageRect(String filePath, Options options) {
        if (null == filePath) {
            Log.e(TAG,"decodeImageRect:got null filePath");
            return null;
        }

        if (options == null) options = new Options();

        //save value that we will modified
        boolean temp = options.inJustDecodeBounds;

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        //restore the value we changed
        options.inJustDecodeBounds = temp;

        return getRect(options);
    }

    private static Rect getRect(Options options) {
        if (options == null) {
            Log.e(TAG,"getRect:got null options");
            return null;
        }

        if (options.outWidth <= 0 || options.outHeight <= 0) {
            Log.e(TAG,"getRect:got invalid bitmap dimension!");
            return null;
        }
        //Log.i(TAG,"getRect:["+options.outWidth+" x "+options.outHeight+"]");

        return new Rect(0, 0, options.outWidth, options.outHeight);
    }

}
