// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Vibrator.java

package com.mediatek.factorymode.vibrator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class Vibrator extends Activity
	implements android.view.View.OnClickListener
{

	private Button mBtFailed;
	private Button mBtOk;
	private SharedPreferences mSp;
	private android.os.Vibrator mVibrator;
	private long pattern[];

	public Vibrator()
	{
		long al[] = new long[2];
		al[0] = 0L;
		al[1] = 1000L;
		pattern = al;
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.vibrator_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.vibrator);
		getWindow().addFlags(128);
		mSp = getSharedPreferences("FactoryMode", 0);
		mBtOk = (Button)findViewById(R.id.vibrator_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.vibrator_bt_failed);
		mBtFailed.setOnClickListener(this);
		mVibrator = (android.os.Vibrator)getSystemService("vibrator");
		mVibrator.vibrate(pattern, 0);
	}

	public void onDestroy()
	{
		super.onDestroy();
		mVibrator.cancel();
	}
}
