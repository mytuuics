// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   SDCard.java

package com.mediatek.factorymode.sdcard;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class SDCard extends Activity
	implements android.view.View.OnClickListener
{

	private Button mBtFailed;
	private Button mBtOk;
	private TextView mInfo;
	private SharedPreferences mSp;

	public SDCard()
	{
	}

	public void SDCardSizeTest()
	{
		if (Environment.getExternalStorageState().equals("mounted"))
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			long l = statfs.getBlockCount();
			long l1 = statfs.getBlockSize();
			long l2 = statfs.getAvailableBlocks();
			long l3 = (l * l1) / 1024L / 1024L;
			long l4 = (l2 * l1) / 1024L / 1024L;
			mInfo.setText((new StringBuilder()).append(getString(R.string.sdcard_tips_success)).append("\n\n").append(getString(R.string.sdcard_totalsize)).append(l3).append("MB").append("\n\n").append(getString(R.string.sdcard_freesize)).append(l4).append("MB").toString());
		} else
		{
			mInfo.setText(getString(R.string.sdcard_tips_failed));
		}
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.sdcard_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.sdcard);
		mSp = getSharedPreferences("FactoryMode", 0);
		mInfo = (TextView)findViewById(R.id.sdcard_info);
		mBtOk = (Button)findViewById(R.id.sdcard_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.sdcard_bt_failed);
		mBtFailed.setOnClickListener(this);
		SDCardSizeTest();
	}
}
