// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   HeadSet.java

package com.mediatek.factorymode.headset;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;
import com.mediatek.factorymode.VUMeter;

public class HeadSet extends Activity
	implements android.view.View.OnClickListener
{

	private static final int STATE_HEADSET_PLUG = 0;
	private static final int STATE_HEADSET_UNPLUG = 1;
	Handler h;
	private Button mBtFailed;
	private Button mBtOk;
	boolean mMicClick;
	private MediaPlayer mPlayer;
	BroadcastReceiver mReceiver;
	private Button mRecord;
	private MediaRecorder mRecorder;
	SharedPreferences mSp;
	boolean mSpkClick;
	VUMeter mVUMeter;
	Handler myHandler;
	Runnable ra;

	public HeadSet()
	{
		mRecorder = null;
		mPlayer = null;
		mMicClick = false;
		mSpkClick = false;
		h = new Handler();
		ra = new Runnable() {


			public void run()
			{
				mVUMeter.invalidate();
				h.postDelayed(this, 100L);
			}

			
		}
;
	myHandler = new Handler() {
		public void handleMessage(Message message)
		{
			super.handleMessage(message);
			switch(message.what){
			case 0:
				mBtOk.setEnabled(true);
				mRecord.setText(R.string.Mic_start);
				mRecord.setEnabled(true);
				break;
			
			case 1:
				if (mRecorder != null)
				{
					mRecorder.stop();
					mRecord.setTag("");
					mRecorder = null;
				}
				mVUMeter.SetCurrentAngle(0F);
				if (mPlayer != null)
				{
					mPlayer.stop();
					mPlayer.release();
					mPlayer = null;
				}
				mRecord.setText(R.string.HeadSet_tips);
				mRecord.setEnabled(false);
				break;
			}
		}
	}
	;

		mReceiver = new BroadcastReceiver() {

			public void onReceive(Context context, Intent intent)
			{
				if ("android.intent.action.HEADSET_PLUG".equals(intent.getAction()))
					if (intent.getIntExtra("state", 0) == 1)
						myHandler.sendEmptyMessage(0);
					else
						myHandler.sendEmptyMessage(1);
			}

		}
;
	}

	private void start()
	{
		h.post(ra);
		if (mPlayer != null)
			mPlayer.stop();
		if (!Environment.getExternalStorageState().equals("mounted"))
		{
			mRecord.setText(R.string.sdcard_tips_failed);
		} else
		{
			try
			{
				mRecorder = new MediaRecorder();
				mRecorder.setAudioSource(1);
				mRecorder.setOutputFormat(1);
				mRecorder.setAudioEncoder(3);
				mVUMeter.setRecorder(mRecorder);
				String s = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append(File.separator).append("test.mp3").toString();
				if (!(new File(s)).exists())
					(new File(s)).createNewFile();
				mRecorder.setOutputFile(s);
				mRecorder.prepare();
				mRecorder.start();
			}
			catch (Exception exception)
			{
				Toast.makeText(this, exception.getMessage(), 0);
			}
			mRecord.setTag("ing");
			mRecord.setText(R.string.Mic_stop);
		}
	}

	private void stopAndSave()
	{
		h.removeCallbacks(ra);
		mRecord.setText(R.string.Mic_start);
		mRecord.setTag("");
		mVUMeter.SetCurrentAngle(0F);
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		try{
			mPlayer = new MediaPlayer();
			mPlayer.setAudioStreamType(3);
			mPlayer.setDataSource("/sdcard/test.mp3");
			mPlayer.prepare();
			mPlayer.start();
	    }
	    catch (IllegalArgumentException localIllegalArgumentException)
	    {
	        localIllegalArgumentException.printStackTrace();
	    }
	    catch (IllegalStateException localIllegalStateException)
	    {
	        localIllegalStateException.printStackTrace();
	    }
	    catch (IOException localIOException)
	    {
	        localIOException.printStackTrace();
	    }
	}

	public void onClick(View view)
	{
		if (view.getId() == mRecord.getId())
			if (mRecord.getTag() == null || !mRecord.getTag().equals("ing"))
				start();
			else
				stopAndSave();
		if (view.getId() != mBtOk.getId()){
			if (view.getId() == mBtFailed.getId())
			{
				Utils.SetPreferences(this, mSp, R.string.headset_name, "failed");
				finish();
			}
		}else{
			Utils.SetPreferences(this, mSp, R.string.headset_name, "success");
			finish();
		}

	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.headset);
		mSp = getSharedPreferences("FactoryMode", 0);
		mRecord = (Button)findViewById(R.id.mic_bt_start);
		mRecord.setOnClickListener(this);
		mRecord.setEnabled(false);
		mBtOk = (Button)findViewById(R.id.bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.bt_failed);
		mBtFailed.setOnClickListener(this);
		mVUMeter = (VUMeter)findViewById(R.id.uvMeter);
		IntentFilter intentfilter = new IntentFilter("android.intent.action.HEADSET_PLUG");
		intentfilter.setPriority(999);
		registerReceiver(mReceiver, intentfilter);
	}

	protected void onDestroy()
	{
		super.onDestroy();
		(new File("/sdcard/test.mp3")).delete();
		if (mPlayer != null)
			mPlayer.stop();
		if (mRecorder != null)
			mRecorder.stop();
		unregisterReceiver(mReceiver);
		h.removeCallbacks(ra);
	}





/*
	static MediaRecorder access$202(HeadSet headset, MediaRecorder mediarecorder)
	{
		headset.mRecorder = mediarecorder;
		return mediarecorder;
	}

*/



/*
	static MediaPlayer access$302(HeadSet headset, MediaPlayer mediaplayer)
	{
		headset.mPlayer = mediaplayer;
		return mediaplayer;
	}

*/
}
