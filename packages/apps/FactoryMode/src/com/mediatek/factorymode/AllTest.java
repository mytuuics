// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   AllTest.java

package com.mediatek.factorymode;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.net.wifi.ScanResult;
import android.os.*;
import com.mediatek.factorymode.gps.GPSThread;
import com.mediatek.factorymode.wifi.WiFiTools;
import java.util.List;

// Referenced classes of package com.mediatek.factorymode:
//			Utils

public class AllTest extends Activity
{
	class BlueHandler extends Handler
	{

		//final AllTest this$0;

		public void handleMessage(Message message)
		{
			super.handleMessage(message);
		}

		public BlueHandler()
		{
			//this$0 = AllTest.this;
			//super();
		}

		public BlueHandler(Looper looper)
		{
			//this$0 = AllTest.this;
			//super(looper);
		}
	}

	class WifiHandler extends Handler
	{

		//final AllTest this$0;

		public void handleMessage(Message message)
		{
			super.handleMessage(message);
		}

		public WifiHandler()
		{
			//this$0 = AllTest.this;
			//super();
		}

		public WifiHandler(Looper looper)
		{
			//this$0 = AllTest.this;
			//super(looper);
		}
	}


	Runnable bluerunnable;
	boolean isregisterReceiver;
	private BluetoothAdapter mAdapter;
	boolean mBlueFlag;
	BlueHandler mBlueHandler;
	boolean mBlueResult;
	boolean mBlueStatus;
	HandlerThread mBlueThread;
	GPSThread mGPS;
	boolean mOtherOk;
	BroadcastReceiver mReceiver;
	boolean mSdCardResult;
	SharedPreferences mSp;
	boolean mWifiConReslut;
	WifiHandler mWifiHandler;
	List mWifiList;
	boolean mWifiResult;
	boolean mWifiStatus;
	HandlerThread mWifiThread;
	WiFiTools mWifiTools;
	Message msg;
	Runnable wifirunnable;

	public AllTest()
	{
		mWifiList = null;
		mWifiConReslut = false;
		mWifiResult = false;
		mWifiStatus = false;
		mOtherOk = false;
		mBlueResult = false;
		mBlueFlag = false;
		mBlueStatus = false;
		mSdCardResult = false;
		msg = null;
		mAdapter = null;
		isregisterReceiver = false;
		mBlueThread = new HandlerThread("blueThread");
		mWifiThread = new HandlerThread("wifiThread");
		mGPS = null;
		wifirunnable = new Runnable() {

			//final AllTest this$0;

			public void run()
			{
				if (!mWifiStatus)
				{
					if (WifiInit())
						mWifiStatus = true;
					mWifiHandler.postDelayed(this, 3000L);
				} else
				if (mWifiTools.IsConnection().booleanValue())
				{
					mWifiResult = true;
					mWifiTools.closeWifi();
				} else
				{
					mWifiHandler.postDelayed(this, 3000L);
				}
			}

			/*
			{
				this$0 = AllTest.this;
				super();
			}*/
		}
;
		mReceiver = new BroadcastReceiver() {

			//final AllTest this$0;

			public void onReceive(Context context, Intent intent)
			{
				if ("android.bluetooth.device.action.FOUND".equals(intent.getAction()) && ((BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE")).getBondState() != 12)
				{
					mBlueResult = true;
					if (isregisterReceiver)
					{
						unregisterReceiver(mReceiver);
						isregisterReceiver = false;
					}
					mAdapter.disable();
				}
			}

			/*
			{
				this$0 = AllTest.this;
				super();
			}*/
		}
;
		bluerunnable = new Runnable() {

			//final AllTest this$0;

			public void run()
			{
				BlueInit();
			}

			/*
			{
				this$0 = AllTest.this;
				super();
			}*/
		}
;
	}

	public void BackstageDestroy()
	{
		mWifiTools.closeWifi();
		mBlueHandler.removeCallbacks(bluerunnable);
		mWifiHandler.removeCallbacks(wifirunnable);
		if (isregisterReceiver)
			unregisterReceiver(mReceiver);
		mAdapter.disable();
		mGPS.closeLocation();
	}

	public void BlueInit()
	{
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mAdapter.enable();
		if (mAdapter.isEnabled())
		{
			StartReciver();
			for (; !mAdapter.startDiscovery(); mAdapter.startDiscovery());
		} else
		{
			mBlueHandler.postDelayed(bluerunnable, 3000L);
		}
	}

	public void OnFinish()
	{
		Utils.SetPreferences(this, mSp, R.string.memory_name, "success");
		SharedPreferences sharedpreferences = mSp;
		String s;
		SharedPreferences sharedpreferences1;
		String s1;
		SharedPreferences sharedpreferences2;
		String s2;
		if (mGPS.isSuccess())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.gps_name, s);
		sharedpreferences1 = mSp;
		if (mWifiResult)
			s1 = "success";
		else
			s1 = "failed";
		Utils.SetPreferences(this, sharedpreferences1, R.string.wifi_name, s1);
		sharedpreferences2 = mSp;
		if (mBlueResult)
			s2 = "success";
		else
			s2 = "failed";
		Utils.SetPreferences(this, sharedpreferences2, R.string.bluetooth_name, s2);
		finish();
	}

	public void SdCardInit()
	{
		if (Environment.getExternalStorageState().equals("mounted"))
			mSdCardResult = true;
	}

	public void StartReciver()
	{
		IntentFilter intentfilter = new IntentFilter("android.bluetooth.device.action.FOUND");
		registerReceiver(mReceiver, intentfilter);
		isregisterReceiver = true;
	}

	public boolean WifiInit()
	{
		boolean flag = false;
		
		mWifiTools.openWifi();
		mWifiList = mWifiTools.scanWifi();	
		
		if (mWifiList != null && mWifiList.size() > 0)
		{
			for (int i = 0; i < mWifiList.size(); i++)
			{
				ScanResult scanresult = (ScanResult)mWifiList.get(i);
				if (!scanresult.capabilities.equals("[WPS]") && !scanresult.capabilities.equals(""))
					continue;
				
				mWifiConReslut = mWifiTools.addWifiConfig(mWifiList, scanresult, "");
				if (!mWifiConReslut)
					continue;
				
				flag = true;
				break;
			}
		}
		
		return flag;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Intent intent1;
		char c;
		intent1 = new Intent();
		intent1.setFlags(0x14000000);
		c = '\uFFFF';
		
		if(requestCode == 16)
		{
			if(resultCode == 1)
			{
				finish();
				return;
			}
			
			intent1.setClassName(this, "com.mediatek.factorymode.KeyCode");
			c = '\u0200';
		}
		
		if (requestCode == 512)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.lcd.LCD");
			c = '\u0190';
		}
		if (requestCode == 400)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.touchscreen.LineTest");
			c = ' ';
		}
		if (requestCode == 32)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.backlight.BackLight");
			c = '\u0100';
		}
		if (requestCode == 256)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.sdcard.SDCard");
			c = '\u0180';
		}
		if (requestCode == 384)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.vibrator.Vibrator");
			c = '\200';
		}
		if (requestCode == 128)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.signal.Signal");
			c = '\220';
		}
		if (requestCode == 144)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.earphone.Earphone");
			c = '\u0170';
		}
		if (requestCode == 368)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.microphone.MicRecorder");
			c = '\u0202';
		}
		if (requestCode == 514)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.headset.HeadSet");
			c = '\u0120';
		}
		if (requestCode == 288)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.fmradio.FMRadio");
			c = '\u0201';
		}
		if (requestCode == 513)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.sensor.GSensor");
			c = '\u0130';
		}
		if (requestCode == 304)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.sensor.MSensor");
			c = '\u0140';
		}
		if (requestCode == 320)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.sensor.PSensor");
			c = '\u0160';
		}
		if (requestCode == 352)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.sensor.LSensor");
			c = '\u0150';
		}
		if (requestCode == 336)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.simcard.SimCard");
			c = '\u0203';
		}
		if (requestCode == 515)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.camera.SubCamera");
			c = '\u0204';
		}
		if (requestCode == 516)
		{
			intent1.setClassName(this, "com.mediatek.factorymode.camera.CameraTest");
			c = '@';
		}
		if (requestCode == 64)
			OnFinish();
		else
			startActivityForResult(intent1, c);
		
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.alltest);
		mSp = getSharedPreferences("FactoryMode", 0);
		mWifiTools = new WiFiTools(this);
		mWifiThread.start();
		mWifiHandler = new WifiHandler(mWifiThread.getLooper());
		mWifiHandler.post(wifirunnable);
		mBlueThread.start();
		mBlueHandler = new BlueHandler(mBlueThread.getLooper());
		mBlueHandler.post(bluerunnable);
		mGPS = new GPSThread(this);
		mGPS.start();
		Intent intent = new Intent();
		intent.setClassName(this, "com.mediatek.factorymode.BatteryLog");
		startActivityForResult(intent, 16);
	}

	public void onDestroy()
	{
		super.onDestroy();
		BackstageDestroy();
	}

}
