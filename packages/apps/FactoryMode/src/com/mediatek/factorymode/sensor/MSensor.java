// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   MSensor.java

package com.mediatek.factorymode.sensor;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class MSensor extends Activity
	implements SensorListener
{

	private android.view.View.OnClickListener cl;
	private Button mBtFailed;
	private Button mBtOk;
	private float mDegressQuondam;
	private ImageView mImgCompass;
	private RotateAnimation mMyAni;
	private TextView mOrientText;
	private TextView mOrientValue;
	private SensorManager mSm;
	private SharedPreferences mSp;

	public MSensor()
	{
		mImgCompass = null;
		mOrientText = null;
		mOrientValue = null;
		mSm = null;
		mMyAni = null;
		mDegressQuondam = 0F;
		cl = new android.view.View.OnClickListener() {


			public void onClick(View view)
			{
				android.content.Context context = getApplicationContext();
				SharedPreferences sharedpreferences = mSp;
				String s;
				if (view.getId() == mBtOk.getId())
					s = "success";
				else
					s = "failed";
				Utils.SetPreferences(context, sharedpreferences, R.string.msensor_name, s);
				finish();
			}

		}
;
	}

	private void AniRotateImage(float f)
	{
		if (Math.abs(f - mDegressQuondam) >= 1F)
		{
			mMyAni = new RotateAnimation(mDegressQuondam, f, 1, 0.5F, 1, 0.5F);
			mMyAni.setDuration(200L);
			mMyAni.setFillAfter(true);
			mImgCompass.startAnimation(mMyAni);
			mDegressQuondam = f;
		}
	}

	public void onAccuracyChanged(int i, int j)
	{
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.msensor);
		mSp = getSharedPreferences("FactoryMode", 0);
		mOrientText = (TextView)findViewById(R.id.OrientText);
		mImgCompass = (ImageView)findViewById(R.id.ivCompass);
		mOrientValue = (TextView)findViewById(R.id.OrientValue);
		mBtOk = (Button)findViewById(R.id.msensor_bt_ok);
		mBtOk.setOnClickListener(cl);
		mBtFailed = (Button)findViewById(R.id.msensor_bt_failed);
		mBtFailed.setOnClickListener(cl);
		getWindow().setFlags(128, 128);
	}

	public void onDestroy()
	{
		mSm.unregisterListener(this);
		super.onDestroy();
	}

	protected void onResume()
	{
		super.onResume();
		mSm.registerListener(this, 1, 0);
	}

	public void onSensorChanged(int i, float af[])
	{
		if(i ==1){
			try{
				if(mOrientText != null && mOrientValue != null && mImgCompass != null && Math.abs(af[0] - mDegressQuondam) >= 0x3f80){
						switch((int)af[0]){
						case 0:
							mOrientText.setText(R.string.MSensor_North);
							break;
						case 90:
							mOrientText.setText(R.string.MSensor_East);
							break;
						case 180:
							mOrientText.setText(R.string.MSensor_South);
							break;
						case 270:
							mOrientText.setText(R.string.MSensor_West);
							break;
						default:
							int j = (int)af[0];
							if (j > 0 && j < 90)
								mOrientText.setText((new StringBuilder()).append(getString(R.string.MSensor_north_east)).append(j).toString());
							if (j > 90 && j < 180)
							{
								j = 180 - j;
								mOrientText.setText((new StringBuilder()).append(getString(R.string.MSensor_south_east)).append(j).toString());
							}
							if (j > 180 && j < 270)
							{
								j -= 180;
								mOrientText.setText((new StringBuilder()).append(getString(R.string.MSensor_south_west)).append(j).toString());
							}
							if (j > 270 && j < 360)
							{
								int k = 360 - j;
								mOrientText.setText((new StringBuilder()).append(getString(R.string.MSensor_north_west)).append(k).toString());
							}
							
							mOrientValue.setText(String.valueOf(af[0]));
							if (mDegressQuondam != -af[0])
								AniRotateImage(-af[0]);
						}
				}
			}catch(Exception e){
				
			}
		}
	}

	public void onStart()
	{
		super.onStart();
		mSm = (SensorManager)getSystemService("sensor");
	}

	protected void onStop()
	{
		mSm.unregisterListener(this);
		super.onStop();
	}


}
