// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   GSensor.java

package com.mediatek.factorymode.sensor;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class GSensor extends Activity
	implements android.view.View.OnClickListener
{

	private static final int OFFSET = 2;
	private ImageView ivimg;
	SensorEventListener lsn;
	private Button mBtFailed;
	private Button mBtOk;
	Sensor mGravitySensor;
	SensorManager mSm;
	SharedPreferences mSp;

	public GSensor()
	{
		mSm = null;
		lsn = new SensorEventListener() {

			public void onAccuracyChanged(Sensor sensor, int i)
			{
			}

			public void onSensorChanged(SensorEvent sensorevent)
			{
				if (sensorevent.sensor == mGravitySensor){
					int i;
					int j;
					int k;
					i = (int)sensorevent.values[0];
					j = (int)sensorevent.values[1];
					k = (int)sensorevent.values[2];
					if (i - 2 <= j || i - 2 <= k){
						if (j - 2 > i && j - 2 > k)
							ivimg.setBackgroundResource(R.drawable.gsensor_y);
						else
						if (k - 2 > i && k - 2 > j)
							ivimg.setBackgroundResource(R.drawable.gsensor_z);
						else
						if (i < -2)
							ivimg.setBackgroundResource(R.drawable.gsensor_x_2);
					}else{
						ivimg.setBackgroundResource(R.drawable.gsensor_x);
					}
				}

			}

		}
;
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.gsensor_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.gsensor);
		mSp = getSharedPreferences("FactoryMode", 0);
		ivimg = (ImageView)findViewById(R.id.gsensor_iv_img);
		mBtOk = (Button)findViewById(R.id.gsensor_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.gsensor_bt_failed);
		mBtFailed.setOnClickListener(this);
		mSm = (SensorManager)getSystemService("sensor");
		mGravitySensor = mSm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSm.registerListener(lsn, mGravitySensor, 1);
	}

	protected void onDestroy()
	{
		mSm.unregisterListener(lsn);
		super.onDestroy();
	}

}
