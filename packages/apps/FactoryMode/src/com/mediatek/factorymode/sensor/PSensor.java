// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   PSensor.java

package com.mediatek.factorymode.sensor;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class PSensor extends Activity
	implements SensorEventListener, android.view.View.OnClickListener
{

	public static final String LOG_TAG = "Sensor";
	private static int mCount = 0;
	private int mAllPsensor[];
	private int mAverage;
	private Button mBtOk;
	CountDownTimer mCountDownTimer;
	private Button mFailed;
	private int mPrePsensor;
	private TextView mPsensor;
	SharedPreferences mSp;
	private int mSumPsensor;
	private char mWrint[];
	private Handler myHandler;
	public Runnable myRunnable;
	private SensorManager sensorManager;

	public PSensor()
	{
		mAllPsensor = new int[1000];
		mPrePsensor = 0;
		mAverage = 0;
		mWrint = new char[1];
		mSumPsensor = 0;
		myRunnable = new Runnable() {

			public void run()
			{
				String[] pathes = getResources().getStringArray(R.array.ps_dev_path);
				File file = null;
				for (int i = 0; i < pathes.length; i++) {
					File f = new File(pathes[i]);
					if (f.exists()) {
						file = f;
						break;
					}

				}
				if(file != null){
					String s = null;
					try {
						s = readFile(file);
						//mPrePsensor = Integer.parseInt(s.trim());
						PSensor.mCount++;
						PSensor.this.mPsensor.setText(PSensor.this.getResources().getString(R.string.proximity) + " " + s);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{

					PSensor.this.mPsensor.setText(PSensor.this.getResources().getString(R.string.proximity) + " not exists");
					return;
				}
				for (int i = 0; i < PSensor.mCount; i++)
				{
					mSumPsensor = mSumPsensor + PSensor.this.mAllPsensor[i];
					PSensor.this.mAllPsensor[i] = 0;
				}				
				if (PSensor.mCount > 0)
				{
					mAverage = 1 + PSensor.this.mSumPsensor / PSensor.mCount;
					PSensor.this.mWrint[0] = (char)mAverage;
				}
				mCount = 0;
				mSumPsensor = 0;
				myHandler.post(myRunnable);
			}

		}
;
	}

	private static String readFile(File file) throws IOException
	{
		FileReader filereader = null;
		FileReader filereader1 = null;

		try{
			filereader1 = new FileReader(file);
			String s = null;
			char ac[];
			s = "";
			ac = new char[200];
			int length;
			String s1;
			do{
				String s2;
				length = filereader1.read(ac, 0, 200);
				if(length < 0){
					if(filereader1 ==null){
						filereader1.close();
					}
					break;
				}
				s2 = (new StringBuilder()).append(s).append(String.valueOf(ac, 0, length)).toString();
				s = s2;
			}while(length > 0);
			
			return s;
		}catch(IOException e){
			
		}
		finally{
			if(filereader1 ==null){
				filereader1.close();
			}
		}
		return null;
	}

	public void onAccuracyChanged(Sensor sensor, int i)
	{
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.psensor_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		sensorManager = (SensorManager)getSystemService("sensor");
		setContentView(R.layout.psensor);
		mSp = getSharedPreferences("FactoryMode", 0);
		mBtOk = (Button)findViewById(R.id.psensor_bt_ok);
		mBtOk.setOnClickListener(this);
		mFailed = (Button)findViewById(R.id.psensor_bt_failed);
		mFailed.setOnClickListener(this);
		mPsensor = (TextView)findViewById(R.id.proximity);
		myHandler = new Handler();
		myHandler.post(myRunnable);
	}

	protected void onDestroy()
	{
		super.onDestroy();
		myHandler.removeCallbacks(myRunnable);
	}

	protected void onResume()
	{
		super.onResume();
		Sensor sensor;
		for (Iterator iterator = sensorManager.getSensorList(-1).iterator(); iterator.hasNext(); sensorManager.registerListener(this, sensor, 3))
			sensor = (Sensor)iterator.next();

	}

	public void onSensorChanged(SensorEvent sensorevent)
	{
	}

	protected void onStop()
	{
		super.onStop();
		if (mCountDownTimer != null)
			mCountDownTimer.cancel();
		sensorManager.unregisterListener(this);
	}

}
