// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   LSensor.java

package com.mediatek.factorymode.sensor;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class LSensor extends Activity
	implements SensorEventListener
{

	public android.view.View.OnClickListener cl;
	TextView mAccuracyView;
	Button mBtFailed;
	Button mBtOk;
	Sensor mLightSensor;
	SensorManager mSensorManager;
	SharedPreferences mSp;
	TextView mValueX;

	public LSensor()
	{
		mSensorManager = null;
		mLightSensor = null;
		mAccuracyView = null;
		mValueX = null;
		cl = new android.view.View.OnClickListener() {

			public void onClick(View view)
			{
				android.content.Context context = getApplicationContext();
				SharedPreferences sharedpreferences = mSp;
				String s;
				if (view.getId() == mBtOk.getId())
					s = "success";
				else
					s = "failed";
				Utils.SetPreferences(context, sharedpreferences, R.string.lsensor_name, s);
				finish();
			}

		}
;
	}

	public void onAccuracyChanged(Sensor sensor, int i)
	{
		if (sensor.getType() == 5)
			mAccuracyView.setText((new StringBuilder()).append(getString(R.string.LSensor_accuracy)).append(i).toString());
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.lsensor);
		mSp = getSharedPreferences("FactoryMode", 0);
		mSensorManager = (SensorManager)getSystemService("sensor");
		mLightSensor = mSensorManager.getDefaultSensor(5);
		mAccuracyView = (TextView)findViewById(R.id.lsensor_accuracy);
		mValueX = (TextView)findViewById(R.id.lsensor_value);
		mBtOk = (Button)findViewById(R.id.lsensor_bt_ok);
		mBtOk.setOnClickListener(cl);
		mBtFailed = (Button)findViewById(R.id.lsensor_bt_failed);
		mBtFailed.setOnClickListener(cl);
	}

	protected void onPause()
	{
		super.onPause();
		mSensorManager.unregisterListener(this, mLightSensor);
	}

	protected void onResume()
	{
		super.onResume();
		mSensorManager.registerListener(this, mLightSensor, 3);
	}

	public void onSensorChanged(SensorEvent sensorevent)
	{
		if (sensorevent.sensor.getType() == 5)
		{
			float af[] = sensorevent.values;
			mValueX.setText((new StringBuilder()).append(getString(R.string.LSensor_value)).append(af[0]).toString());
		}
	}
}
