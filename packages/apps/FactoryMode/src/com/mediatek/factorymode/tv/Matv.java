package com.mediatek.factorymode.tv;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Matv extends Activity implements OnClickListener
{

	static Cursor mCursor = null;
	SharedPreferences mSp;
	Handler myHandler;
    Button btnSuccess = null;
    Button btnFail = null;

	public Matv()
	{
	}


	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
        setContentView(R.layout.only_two_button);
        btnSuccess = (Button) findViewById(R.id.btn_success);
        btnFail = (Button) findViewById(R.id.but_failed);
        btnSuccess.setOnClickListener(this);
        btnFail.setOnClickListener(this);
		mSp = getSharedPreferences("FactoryMode", 0);
		Intent i = new Intent();
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//i.setPackage("com.mediatek.app.mtv");
        i.setClassName("com.mediatek.app.mtv", "com.mediatek.app.mtv.ChannelListActivity");
		startActivity(i);
	}

    @Override
    public void onClick(View v) {
        if (v == btnSuccess) {

		    Utils.SetPreferences(getApplicationContext(), mSp, R.string.tv_name, "success");
        } else if (v == btnFail) {

			Utils.SetPreferences(getApplicationContext(), mSp, R.string.tv_name, "failed");
        }
        finish();
    }
}
