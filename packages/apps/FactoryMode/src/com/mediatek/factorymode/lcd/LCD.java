// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   LCD.java

package com.mediatek.factorymode.lcd;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class LCD extends Activity
{

	private int mNum;
	SharedPreferences mSp;
	private View mTestColorView=null;
	Handler myHandler;
	private Timer timer;

	public LCD()
	{
		mNum = 0;
		myHandler = new Handler() {

			public void handleMessage(Message message)
			{
				if (message.what == 0)
				{
					if (mNum >= 4)
					{
						timer.cancel();
						myHandler.removeMessages(0);
						android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(LCD.this);
						builder.setTitle(R.string.FMRadio_notice);
						builder.setPositiveButton(R.string.Success, new android.content.DialogInterface.OnClickListener() {


							public void onClick(DialogInterface dialoginterface, int i)
							{
								Utils.SetPreferences(getApplicationContext(), mSp, R.string.lcd_name, "success");
								finish();
							}

						}
);
						builder.setNegativeButton(getResources().getString(R.string.Failed), new android.content.DialogInterface.OnClickListener() {


							public void onClick(DialogInterface dialoginterface, int i)
							{
								Utils.SetPreferences(getApplicationContext(), mSp, R.string.lcd_name, "failed");
								finish();
							}

						}
);
						builder.create().show();
					} else
					{
						changeColor(mNum);
					}
					mNum++;
				}
			}

		}
;
	}

	private void changeColor(int i)
	{
		switch(i%4){
		
		case 0:
			mTestColorView.setBackgroundColor(0xffff0000);
			break;
		case 1:
			mTestColorView.setBackgroundColor(0xff00ff00);
			break;
		case 2:
			mTestColorView.setBackgroundColor(0xff0000ff);
			break;
		case 3:
			mTestColorView.setBackgroundColor(-1);
			break;
		
		default:
		}
	}

	private void initView()
	{
		mTestColorView = findViewById(R.id.test_color_view);
		timer.schedule(new TimerTask() {

			public void run()
			{
				Message message = new Message();
				message.what = 0;
				myHandler.sendMessage(message);
			}

		}
, 1000L, 1000L);
	}

	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		mSp = getSharedPreferences("FactoryMode", 0);
		setContentView(R.layout.lcd);
		timer = new Timer();
		initView();
	}

	protected void onDestroy()
	{
		super.onDestroy();
		timer.cancel();
	}



/*
	static int access$008(LCD lcd)
	{
		int i = lcd.mNum;
		lcd.mNum = i + 1;
		return i;
	}

*/


}
