// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   BackLight.java

package com.mediatek.factorymode.backlight;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.ShellExe;
import com.mediatek.factorymode.Utils;

public class BackLight extends Activity
	implements android.view.View.OnClickListener
{

	private final int ERR_ERR = 1;
	private final int ERR_OK = 0;
	private String lcdCmdOFF;
	private String lcdCmdON;
	private Button mBtFailed;
	private Button mBtOk;
	private Button mBtnLcdOFF;
	private Button mBtnLcdON;
	private SharedPreferences mSp;

	public BackLight()
	{
		lcdCmdON = "echo 250 > /sys/class/leds/lcd-backlight/brightness";
		lcdCmdOFF = "echo 50 > /sys/class/leds/lcd-backlight/brightness";
	}

	private void setLastError(int i)
	{
		System.out.print(i);
	}

	public void onClick(View view)
	{
		try
		{
			if (view.getId() == mBtnLcdON.getId())
			{
				String as1[] = new String[3];
				as1[0] = "/system/bin/sh";
				as1[1] = "-c";
				as1[2] = lcdCmdON;
				if (ShellExe.execCommand(as1) == 0)
					setLastError(0);
				else
					setLastError(1);
				return;
			}
			if (view.getId() == mBtnLcdOFF.getId())
			{
				String as[] = new String[3];
				as[0] = "/system/bin/sh";
				as[1] = "-c";
				as[2] = lcdCmdOFF;
				if (ShellExe.execCommand(as) == 0)
					setLastError(0);
				else
					setLastError(1);
			} else
			if (view.getId() == mBtOk.getId())
			{
				Utils.SetPreferences(this, mSp, R.string.backlight_name, "success");
				finish();
			} else
			if (view.getId() == mBtFailed.getId())
			{
				Utils.SetPreferences(this, mSp, R.string.backlight_name, "failed");
				finish();
			}
		}
		catch(Exception e)
		{
			setLastError(1);
			return;
		}
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.backlight);
		mBtnLcdON = (Button)findViewById(R.id.Display_lcd_on);
		mBtnLcdOFF = (Button)findViewById(R.id.Display_lcd_off);
		mBtOk = (Button)findViewById(R.id.display_bt_ok);
		mBtFailed = (Button)findViewById(R.id.display_bt_failed);
		mBtnLcdON.setOnClickListener(this);
		mBtnLcdOFF.setOnClickListener(this);
		mBtOk.setOnClickListener(this);
		mBtFailed.setOnClickListener(this);
		mSp = getSharedPreferences("FactoryMode", 0);
	}
}
