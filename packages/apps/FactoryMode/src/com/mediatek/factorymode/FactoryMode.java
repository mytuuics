// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   FactoryMode.java

package com.mediatek.factorymode;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.view.*;
import android.widget.*;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.mediatek.factorymode:
//			Report

public class FactoryMode extends Activity implements android.widget.AdapterView.OnItemClickListener {
    public class MyAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        // final FactoryMode this$0;

        public int getCount() {
            int i;
            if (mListData == null)
                i = 0;
            else
                i = mListData.size();
            return i;
        }

        public Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            View view1 = mInflater.inflate(R.layout.main_grid, null);
            TextView textview = (TextView) view1.findViewById(R.id.factor_button);
            textview.setText((CharSequence) mListData.get(i));
            SetColor(textview);
            return view1;
        }

        public MyAdapter(Context context) {
            /*
             * this$0 = FactoryMode.this; super();
             */
            mInflater = LayoutInflater.from(context);
        }

        public MyAdapter(FactoryMode factorymode1, int i) {
            /*
             * this$0 = FactoryMode.this; super();
             */
        }
    }

    public android.view.View.OnClickListener cl;

    int itemString[];

    private MyAdapter mAdapter;

    // private Button mBtAll;

    private Button mBtAuto;

    private GridView mGrid;

    private List mListData;

    private SharedPreferences mSp;

    public FactoryMode() {
        mSp = null;
        cl = new android.view.View.OnClickListener() {

            // final FactoryMode this$0;

            public void onClick(View view) {
                Intent intent = new Intent();
                char c = '\uFFFF';
                if (view.getId() == mBtAuto.getId()) {
                    intent.setClassName("com.mediatek.factorymode",
                            "com.mediatek.factorymode.AutoTest");
                    c = '\u1000';
                }
                // if (view.getId() == mBtAll.getId()) {
                // intent.setClassName("com.mediatek.factorymode",
                // "com.mediatek.factorymode.AllTest");
                // c = '\u2000';
                // }
                startActivityForResult(intent, c);
            }

            /*
             * { this$0 = FactoryMode.this; super(); }
             */
        };
    }

    private void SetColor(TextView textview) {
        mSp = getSharedPreferences("FactoryMode", 0);
        int i = 0;
        while (i < itemString.length) {
            if (getResources().getString(itemString[i]).equals(textview.getText().toString())) {
                String s = mSp.getString(getString(itemString[i]), null);
                if (s.equals("success"))
                    textview.setTextColor(getApplicationContext().getResources().getColor(
                            R.color.Blue));
                else if (s.equals("default"))
                    textview.setTextColor(getApplicationContext().getResources().getColor(
                            R.color.black));
                else if (s.equals("failed"))
                    textview.setTextColor(getApplicationContext().getResources().getColor(
                            R.color.Red));
            }
            i++;
        }
    }

    private List getData() {
        ArrayList arraylist = new ArrayList();
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        for (int i = 0; i < itemString.length; i++)
            if (sharedpreferences.getBoolean(getString(itemString[i]), true))
                arraylist.add(getString(itemString[i]));

        return arraylist;
    }

    private void init() {
        mSp = getSharedPreferences("FactoryMode", 0);
        android.content.SharedPreferences.Editor editor = mSp.edit();
        for (int i = 0; i < itemString.length; i++)
            editor.putString(getString(itemString[i]), "default");

        editor.putString(getString(R.string.headsethook_name), "default");
        editor.commit();
    }

    protected void onActivityResult(int i, int j, Intent intent) {
        System.gc();
        startActivity(new Intent(this, Report.class));
    }

    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        setContentView(R.layout.main);
        super.onCreate(bundle);
        TestActivityUtil.init(this);
        int len = TestActivityUtil.getTestCount();
        itemString = new int[len];
        for (int i = 0; i < len; i++) {
            itemString[i] = TestActivityUtil.getNameIDFromIndex(i);
        }
        init();
        mBtAuto = (Button) findViewById(R.id.main_bt_autotest);
        mBtAuto.setOnClickListener(cl);
        // mBtAll = (Button) findViewById(R.id.main_bt_alltest);
        // mBtAll.setOnClickListener(cl);
        mGrid = (GridView) findViewById(R.id.main_grid);
        mListData = getData();
        mAdapter = new MyAdapter(this);
    }

    protected void onDestroy() {
        super.onDestroy();
        Process.killProcess(Process.myPid());
    }

    public void onItemClick(AdapterView adapterview, View v, int position, long id) {
        Intent intent;
        String s = null;
        String s1 = null;
        try {
            intent = new Intent();
            intent.setFlags(0x14000000);
            s = (String) mListData.get(position);
            int len = TestActivityUtil.getTestCount();

            for (int i = 0; i < len; i++) {
                String tem = getString(TestActivityUtil.getNameIDFromIndex(i));
                if (tem.equals(s)) {
                    s1 = TestActivityUtil.getPckNameFromIndex(i);
                    break;
                }
            }
            // s1 = null;
            // if (getString(R.string.speaker_name) == s) {
            // s1 = "com.mediatek.factorymode.audio.AudioTest";
            // } else if (getString(R.string.battery_name) == s)
            // s1 = "com.mediatek.factorymode.BatteryLog";
            // else if (getString(R.string.touchscreen_name) == s)
            // s1 = "com.mediatek.factorymode.touchscreen.LineTest";
            // else if (getString(R.string.camera_name) == s)
            // s1 = "com.mediatek.factorymode.camera.CameraTest";
            // else if (getString(R.string.wifi_name) == s)
            // s1 = "com.mediatek.factorymode.wifi.WiFiTest";
            // else if (getString(R.string.bluetooth_name) == s)
            // s1 = "com.mediatek.factorymode.bluetooth.Bluetooth";
            // else if (getString(R.string.headset_name) == s)
            // s1 = "com.mediatek.factorymode.headset.HeadSet";
            // else if (getString(R.string.earphone_name) == s)
            // s1 = "com.mediatek.factorymode.earphone.Earphone";
            // else if (getString(R.string.vibrator_name) == s)
            // s1 = "com.mediatek.factorymode.vibrator.Vibrator";
            // else if (getString(R.string.telephone_name) == s)
            // s1 = "com.mediatek.factorymode.signal.Signal";
            // else if (getString(R.string.gps_name) == s)
            // s1 = "com.mediatek.factorymode.gps.GPS";
            // else if (getString(R.string.backlight_name) == s)
            // s1 = "com.mediatek.factorymode.backlight.BackLight";
            // else if (getString(R.string.memory_name) == s)
            // s1 = "com.mediatek.factorymode.memory.Memory";
            // else if (getString(R.string.microphone_name) == s)
            // s1 = "com.mediatek.factorymode.microphone.MicRecorder";
            // else if (getString(R.string.gsensor_name) == s)
            // s1 = "com.mediatek.factorymode.sensor.GSensor";
            // else if (getString(R.string.msensor_name) == s)
            // s1 = "com.mediatek.factorymode.sensor.MSensor";
            // else if (getString(R.string.lsensor_name) == s)
            // s1 = "com.mediatek.factorymode.sensor.LSensor";
            // else if (getString(R.string.psensor_name) == s)
            // s1 = "com.mediatek.factorymode.sensor.PSensor";
            // else if (getString(R.string.sdcard_name) == s)
            // s1 = "com.mediatek.factorymode.sdcard.SDCard";
            // else if (getString(R.string.fmradio_name) == s)
            // s1 = "com.mediatek.factorymode.fmradio.FMRadio";
            // else if (getString(R.string.KeyCode_name) == s)
            // s1 = "com.mediatek.factorymode.KeyCode";
            // else if (getString(R.string.lcd_name) == s)
            // s1 = "com.mediatek.factorymode.lcd.LCD";
            // else if (getString(R.string.sim_name) == s)
            // s1 = "com.mediatek.factorymode.simcard.SimCard";
            // else if (getString(R.string.subcamera_name) == s)
            // s1 = "com.mediatek.factorymode.camera.SubCamera";

            intent.setClassName(this, s1);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle(R.string.PackageIerror);
            builder.setMessage(R.string.Packageerror);
            builder.setPositiveButton("OK", null);
            builder.create().show();
        }
    }

    protected void onResume() {
        super.onResume();
        mGrid.setAdapter(mAdapter);
        mGrid.setOnItemClickListener(this);
    }

}
