// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   AppDefine.java

package com.mediatek.factorymode;


public abstract class AppDefine
{

	public static final int FT_ALLTESTID = 8192;
	public static final int FT_AUDIOID = 48;
	public static final int FT_AUTOTESTID = 4096;
	public static final int FT_BACKLIGHTID = 256;
	public static final int FT_BATTERYID = 16;
	public static final int FT_BLUETOOTHID = 96;
	public static final int FT_CAMERAID = 64;
	public static final int FT_CAMERASETID = 1;
	public static final String FT_DEFAULT = "default";
	public static final int FT_EARPHONEID = 368;
	public static final String FT_FAILED = "failed";
	public static final int FT_FMRADIOID = 513;
	public static final int FT_FMRADIOSETID = 3;
	public static final int FT_GPSID = 112;
	public static final int FT_GPSSETID = 2;
	public static final int FT_GSENSORID = 304;
	public static final int FT_HEADSETHOOKID = 517;
	public static final int FT_HEADSETID = 288;
	public static final int FT_HOOKSETID = 5;
	public static final int FT_KEYCODEID = 512;
	public static final int FT_LCDID = 400;
	public static final int FT_LSENSORID = 336;
	public static final int FT_MEMORYID = 272;
	public static final int FT_MICROPHONEID = 514;
	public static final int FT_MICROPHONESETID = 4;
	public static final int FT_MSENSORID = 320;
	public static final int FT_PSENSORID = 352;
	public static final int FT_SDCARDID = 384;
	public static final int FT_SIGNALID = 144;
	public static final int FT_SIMCARDID = 515;
	public static final int FT_SUBCAMERAID = 516;
	public static final String FT_SUCCESS = "success";
	public static final int FT_TOUCHSCREENID = 32;
	public static final int FT_VIBRATORID = 128;
	public static final int FT_WIFIID = 80;

	public AppDefine()
	{
	}
}
