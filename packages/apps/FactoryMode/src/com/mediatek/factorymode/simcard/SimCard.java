// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   SimCard.java

package com.mediatek.factorymode.simcard;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;


import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;
import com.android.internal.telephony.gemini.GeminiPhone;
import com.android.internal.telephony.PhoneFactory;

public class SimCard extends Activity
{

	private boolean Sim1State;
	private boolean Sim2State;
	private GeminiPhone mGeminiPhone;
	private String mSimStatus;
	SharedPreferences mSp;

	public SimCard()
	{
		Sim1State = false;
		Sim2State = false;
		mSimStatus = "";
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		mSp = getSharedPreferences("FactoryMode", 0);
		mGeminiPhone = (GeminiPhone)PhoneFactory.getDefaultPhone();
		Sim1State = mGeminiPhone.isSimInsert(0) & mGeminiPhone.isRadioOnGemini(0);
		Sim2State = mGeminiPhone.isSimInsert(1) & mGeminiPhone.isRadioOnGemini(1);
		android.app.AlertDialog.Builder builder;
		if (Sim1State)
			mSimStatus = (new StringBuilder()).append(mSimStatus).append(getString(R.string.sim1_info_ok)).append("\n").toString();
		else
			mSimStatus = (new StringBuilder()).append(mSimStatus).append(getString(R.string.sim1_info_failed)).append("\n").toString();
		if (Sim2State)
			mSimStatus = (new StringBuilder()).append(mSimStatus).append(getString(R.string.sim2_info_ok)).append("\n").toString();
		else
			mSimStatus = (new StringBuilder()).append(mSimStatus).append(getString(R.string.sim2_info_failed)).append("\n").toString();
		builder = new android.app.AlertDialog.Builder(this);
		builder.setTitle(R.string.FMRadio_notice);
		builder.setMessage(mSimStatus);
		builder.setPositiveButton(R.string.Success, new android.content.DialogInterface.OnClickListener() {


			public void onClick(DialogInterface dialoginterface, int i)
			{
				Utils.SetPreferences(SimCard.this, mSp, R.string.sim_name, "success");
				finish();
			}
		}
);
		builder.setNegativeButton(getResources().getString(R.string.Failed), new android.content.DialogInterface.OnClickListener() {


			public void onClick(DialogInterface dialoginterface, int i)
			{
				Utils.SetPreferences(SimCard.this, mSp, R.string.sim_name, "failed");
				finish();
			}

		}
);
		builder.create().show();
	}
}
