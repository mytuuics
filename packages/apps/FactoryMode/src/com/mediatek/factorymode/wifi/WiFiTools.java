// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   WiFiTools.java

package com.mediatek.factorymode.wifi;

import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.mediatek.factorymode.R;

public class WiFiTools
{

	public static final int WIFI_STATE_DISABLED = 1;
	public static final int WIFI_STATE_DISABLING = 0;
	public static final int WIFI_STATE_ENABLED = 3;
	public static final int WIFI_STATE_ENABLING = 2;
	static boolean mResult = false;
	private static WifiManager mWifiManager = null;
	Context context;
	String info;
	WifiInfo wifiinfo;

	public WiFiTools(Context context1)
	{
		info = "";
		context = context1;
		mWifiManager = (WifiManager)context1.getSystemService("wifi");
		wifiinfo = mWifiManager.getConnectionInfo();
		mWifiManager.startScan();
	}

	public String GetState()
	{
		if (!mWifiManager.isWifiEnabled()){
			mWifiManager.setWifiEnabled(true);
		    switch (mWifiManager.getWifiState()){
		    case 0:
		    	info = context.getString(R.string.WiFi_info_closeing);
		    	break;
		    case 1:
		    	info = context.getString(R.string.WiFi_info_close);
		    	break;
		    case 2:
		    	info = context.getString(R.string.WiFi_info_opening);
		    	break;
		    case 3:
		    	info = context.getString(R.string.WiFi_info_open);
		    	break;
		    	
		    default:
		    	info = context.getString(R.string.WiFi_info_unknown);
		    	break;
		    }
		}else{
			info = context.getString(R.string.WiFi_info_open);
		}
		return info;
	}

	public WifiInfo GetWifiInfo()
	{
		wifiinfo = mWifiManager.getConnectionInfo();
		return wifiinfo;
	}

	public Boolean IsConnection()
	{
		Boolean boolean1;
		if (((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED)
			boolean1 = Boolean.valueOf(true);
		else
			boolean1 = Boolean.valueOf(false);
		return boolean1;
	}

	public boolean addWifiConfig(List list, ScanResult scanresult, String s)
	{
		WifiConfiguration wificonfiguration = new WifiConfiguration();
		wificonfiguration.SSID = (new StringBuilder()).append("\"").append(scanresult.SSID).append("\"").toString();
		wificonfiguration.allowedKeyManagement.set(0);
		wificonfiguration.status = 2;
		wificonfiguration.networkId = mWifiManager.addNetwork(wificonfiguration);
		return mWifiManager.enableNetwork(wificonfiguration.networkId, true);
	}

	public void closeWifi()
	{
		if (mWifiManager.isWifiEnabled())
			mWifiManager.setWifiEnabled(false);
	}

	public boolean openWifi()
	{
		boolean flag;
		if (!mWifiManager.isWifiEnabled())
			flag = mWifiManager.setWifiEnabled(true);
		else
			flag = true;
		return flag;
	}

	public List scanWifi()
	{
		return mWifiManager.getScanResults();
	}

}
