// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   WiFiTest.java

package com.mediatek.factorymode.wifi;

import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

// Referenced classes of package com.mediatek.factorymode.wifi:
//			WiFiTools

public class WiFiTest extends Activity
	implements android.view.View.OnClickListener
{
	class WifiHandler extends Handler
	{


		public void handleMessage(Message message)
		{
			super.handleMessage(message);
		}

		public WifiHandler()
		{
			super();
		}

		public WifiHandler(Looper looper)
		{
			super(looper);
		}
	}


	public static final int TIMEOUT = 15;
	private static final int WIFI_CONNECTED = 2;
	private static final int WIFI_CONNECTING = 3;
	private static final int WIFI_FAILED = 4;
	private static final int WIFI_LIST = 1;
	private static final int WIFI_NOTFOUND_OPENAP = 5;
	private static final int WIFI_STATE = 0;
	public static final int WIFI_STATE_DISABLED = 1;
	public static final int WIFI_STATE_DISABLING = 0;
	public static final int WIFI_STATE_ENABLED = 3;
	public static final int WIFI_STATE_ENABLING = 2;
	Handler UiHandler;
	private Button mBtFailed;
	private Button mBtOk;
	private int mCount;
	boolean mFlag;
	WifiHandler mHandler;
	boolean mListFlag;
	private String mNetWorkName;
	boolean mResult;
	private SharedPreferences mSp;
	private TextView mTvCon;
	private TextView mTvInfo;
	private TextView mTvResInfo;
	private TextView mTvResult;
	private List mWifiList;
	boolean mWifiScan;
	HandlerThread mWifiThread;
	private WiFiTools mWifiTools;
	Runnable wifirunnable;

	public WiFiTest()
	{
		mTvInfo = null;
		mTvResult = null;
		mTvCon = null;
		mTvResInfo = null;
		mBtOk = null;
		mBtFailed = null;
		mWifiList = null;
		mSp = null;
		mNetWorkName = "";
		mResult = false;
		mFlag = false;
		mListFlag = false;
		mWifiScan = false;
		mCount = 0;
		mWifiThread = new HandlerThread("wifiThread");
		wifirunnable = new Runnable(){
			public void run(){
			      if (mCount >= 15)
			      {
			        UiHandler.sendEmptyMessage(4);
			        mWifiTools.closeWifi();
			        mHandler.removeCallbacks(this);
			      }
			      if (!mFlag)
			      {
			        boolean bool = StartWifi();
			        if ((!bool) && (mListFlag == true))
			        {
			          UiHandler.sendEmptyMessage(5);
			          mHandler.removeCallbacks(this);
			          return;
			        }
			        if (bool)
			        {
			          mFlag = true;
			          UiHandler.sendEmptyMessage(3);
			        }
			        mHandler.postDelayed(this, 3000L);
			      }
			      if (WiFiTest.this.mWifiTools.IsConnection().booleanValue() != false){
			    	  mHandler.postDelayed(this, 3000L);
			      }else{
			    	  WiFiTest.this.UiHandler.sendEmptyMessage(2);
			      }
			      
			      mCount ++;
			    }
		};
		UiHandler = new Handler() {
			public void handleMessage(Message message)
			{
				switch(message.what){
				
				case 0:
					mTvInfo.setText(mWifiTools.GetState());
					mTvResult.setText(R.string.WiFi_scaning);
					break;
				case 1:
					mTvResult.setText(mNetWorkName);
					break;
				case 2:
					mTvResInfo.setText(mWifiTools.GetWifiInfo().toString());
					mTvCon.setText(getString(R.string.WiFi_success));
					break;
				case 3:
					mTvCon.setText(getString(R.string.WiFi_connecting));
					mTvCon.setTextColor(getResources().getColor(R.color.Green));
					break;
				case 4:
					mTvCon.setText(getString(R.string.WiFi_failed));
					mTvCon.setTextColor(getResources().getColor(R.color.Red));
					break;
				case 5:
					//mTvCon.setText(getString(R.string.WiFi_notfound_openap));
					//mTvCon.setTextColor(getResources().getColor(R.color.Red));
					break;
					
				default:
					
				}

			}

		}
;
	}

	public boolean StartWifi()
	{
		UiHandler.sendEmptyMessage(0);
		mWifiList = mWifiTools.scanWifi();
		mNetWorkName = "";
		
		boolean flag = false;
		if (mWifiList != null && mWifiList.size() > 0){
			if (mWifiList.size() > 0)
			{
				for (int i = 0; i < mWifiList.size(); i++)
				{
					ScanResult scanresult1 = (ScanResult)mWifiList.get(i);
					mNetWorkName = (new StringBuilder()).append(mNetWorkName).append(scanresult1.SSID).append("\n").toString();
				}

				UiHandler.sendEmptyMessage(1);
				for (int j = 0; j < mWifiList.size(); j++)
				{
					ScanResult scanresult = (ScanResult)mWifiList.get(j);
					if (!scanresult.capabilities.equals("[WPS]") && !scanresult.capabilities.equals(""))
						continue;
					mResult = mWifiTools.addWifiConfig(mWifiList, scanresult, "");
					if (!mResult)
						continue;
					flag = true;
					continue; /* Loop/switch isn't completed */
				}

				mListFlag = true;
				flag = false;
			} else
			{
				flag = false;
			}
		}
		
		return flag;
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.wifi_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.wifi_test);
		mSp = getSharedPreferences("FactoryMode", 0);
		mTvInfo = (TextView)findViewById(R.id.wifi_state_id);
		mTvResult = (TextView)findViewById(R.id.wifi_result_id);
		mTvCon = (TextView)findViewById(R.id.wifi_con_id);
		mTvResInfo = (TextView)findViewById(R.id.wifi_resinfo_id);
		mBtOk = (Button)findViewById(R.id.wifi_bt_ok);
		mBtFailed = (Button)findViewById(R.id.wifi_bt_failed);
		mBtOk.setOnClickListener(this);
		mBtFailed.setOnClickListener(this);
		mWifiTools = new WiFiTools(this);
		mWifiTools.openWifi();
		mWifiThread.start();
		mHandler = new WifiHandler(mWifiThread.getLooper());
		mHandler.post(wifirunnable);
	}

	public void onDestroy()
	{
		super.onDestroy();
		mWifiTools.closeWifi();
		mHandler.removeCallbacks(wifirunnable);
	}



/*
	static int access$008(WiFiTest wifitest)
	{
		int i = wifitest.mCount;
		wifitest.mCount = i + 1;
		return i;
	}

*/






}
