// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   VUMeter.java

package com.mediatek.factorymode;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.media.MediaRecorder;
import android.util.AttributeSet;
import android.view.View;

public class VUMeter extends View
{

	static final long ANIMATION_INTERVAL = 70L;
	static final float DROPOFF_STEP = 0.18F;
	static final float PIVOT_RADIUS = 3.5F;
	static final float PIVOT_Y_OFFSET = 10F;
	static final float SHADOW_OFFSET = 2F;
	static final float SURGE_STEP = 0.35F;
	private float mCurrentAngle;
	Paint mPaint;
	MediaRecorder mRecorder;
	Paint mShadow;

	public VUMeter(Context context)
	{
		super(context);
		init(context);
	}

	public VUMeter(Context context, AttributeSet attributeset)
	{
		super(context, attributeset);
		init(context);
	}

	public float GetCurrentAngle()
	{
		return mCurrentAngle;
	}

	public void SetCurrentAngle(float f)
	{
		mCurrentAngle = f;
	}

	void init(Context context)
	{
		setBackgroundDrawable(context.getResources().getDrawable(R.drawable.vumeter));
		mPaint = new Paint(1);
		mPaint.setColor(-1);
		mShadow = new Paint(1);
		mShadow.setColor(Color.argb(60, 0, 0, 0));
		mRecorder = null;
		mCurrentAngle = 0F;
	}

	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		float f = 0.3926991F;
		if (mRecorder != null)
			f += (2.356195F * (float)mRecorder.getMaxAmplitude()) / 32768F;
		float f1;
		float f2;
		float f3;
		float f4;
		float f5;
		float f6;
		float f7;
		float f8;
		if (f > mCurrentAngle)
			mCurrentAngle = f;
		else
			mCurrentAngle = Math.max(f, mCurrentAngle - 0.18F);
		mCurrentAngle = Math.min(2.748894F, mCurrentAngle);
		f1 = getWidth();
		f2 = getHeight();
		f3 = f1 / 2F;
		f4 = f2 - 3.5F - 10F;
		f5 = (4F * f2) / 5F;
		f6 = (float)Math.sin(mCurrentAngle);
		f7 = f3 - f5 * (float)Math.cos(mCurrentAngle);
		f8 = f4 - f5 * f6;
		canvas.drawLine(f7 + 2F, f8 + 2F, f3 + 2F, f4 + 2F, mShadow);
		canvas.drawCircle(2F + f3, 2F + f4, 3.5F, mShadow);
		canvas.drawLine(f7, f8, f3, f4, mPaint);
		canvas.drawCircle(f3, f4, 3.5F, mPaint);
	}

	public void setRecorder(MediaRecorder mediarecorder)
	{
		mRecorder = mediarecorder;
		invalidate();
	}
}
