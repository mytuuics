// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Earphone.java

package com.mediatek.factorymode.earphone;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class Earphone extends Activity
	implements android.view.View.OnClickListener
{

	private AudioManager mAudioManager;
	private Button mBtFailed;
	private Button mBtOk;
	private MediaPlayer mPlayer;
	private SharedPreferences mSp;

	public Earphone()
	{
	}

	private void initMediaPlayer()
	{
		mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.tada);
		mPlayer.setLooping(true);
		mPlayer.start();
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.earphone_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.audio_test);
		mSp = getSharedPreferences("FactoryMode", 0);
		mBtOk = (Button)findViewById(R.id.audio_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.audio_bt_failed);
		mBtFailed.setOnClickListener(this);
		mAudioManager = (AudioManager)getSystemService("audio");
		mAudioManager.setRingerMode(2);
		//mAudioManager.setStreamVolume(3, mAudioManager.getStreamMaxVolume(3), 4);
		mAudioManager.setSpeakerphoneOn(false);
		setVolumeControlStream(0);
		mAudioManager.setMode(2);
		initMediaPlayer();
	}

	protected void onDestroy()
	{
		mPlayer.stop();
		mAudioManager.setMode(0);
		super.onDestroy();
	}

	protected void onResume()
	{
		super.onResume();
		mBtOk = (Button)findViewById(R.id.audio_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.audio_bt_failed);
		mBtFailed.setOnClickListener(this);
	}
}
