// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   ShellExe.java

package com.mediatek.factorymode;

import java.io.*;

public class ShellExe
{

	public static String ERROR = "ERROR";
	private static StringBuilder sb = new StringBuilder("");

	public ShellExe()
	{
	}

	public static int execCommand(String command[])
		throws IOException
	{
		byte byte0;
		try
		{
			Process process = Runtime.getRuntime().exec(command);
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(process.getInputStream()));			
			
			sb.delete(0, sb.length());			
			String s;
			if (process.waitFor() == 0)
			{
				s = bufferedreader.readLine();
				InterruptedException interruptedexception;
				if (s != null)
				{
					sb.append(s);
					do
					{
						String s1 = bufferedreader.readLine();
						if (s1 == null)
						{
							byte0 = 0;
							break;
						}
						sb.append('\n');
						sb.append(s1);
					} while (true);
				} else
				{
					byte0 = 0;
				}				
			}
			else
			{
				sb.append(ERROR);
				byte0 = -1;
			}
			
		}catch(InterruptedException e)
		{		
			sb.append(ERROR);
			byte0 = -1;
		}
		return byte0;
	}

	public static String getOutput()
	{
		return sb.toString();
	}

}
