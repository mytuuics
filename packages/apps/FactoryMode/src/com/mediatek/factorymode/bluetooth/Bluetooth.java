// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Bluetooth.java

package com.mediatek.factorymode.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class Bluetooth extends Activity
implements android.view.View.OnClickListener
{
	class BlueHandler extends Handler
	{

		//final Bluetooth this$0;

		public void handleMessage(Message message)
		{
			super.handleMessage(message);
		}
		
		public BlueHandler()
		{		
			super();
		}

		public BlueHandler(Looper looper)
		{		
			super(looper);
		}
	}


	Runnable bluerunnable;
	private BluetoothAdapter mAdapter;
	private boolean mBlueFlag;
	BlueHandler mBlueHandler;
	HandlerThread mBlueThread;
	private Button mBtFailed;
	private Button mBtOk;
	private Handler mHandler;
	private String mNameList;
	BroadcastReceiver mReceiver;
	private SharedPreferences mSp;
	private TextView mTvCon;
	private TextView mTvInfo;
	private TextView mTvResult;
	Message msg;
	Runnable myRunnable;

	public Bluetooth()
	{
		mAdapter = null;
		mTvInfo = null;
		mTvResult = null;
		mTvCon = null;
		mNameList = "";
		mBlueFlag = false;
		mBlueThread = new HandlerThread("blueThread");
		msg = null;
		mHandler = new Handler() {

			//final Bluetooth this$0;

			public void handleMessage(Message message)
			{
				super.handleMessage(message);
				mTvInfo.setText(R.string.Bluetooth_open);
				mTvResult.setText(R.string.Bluetooth_scaning);
				mBlueHandler.removeCallbacks(bluerunnable);
				IntentFilter intentfilter = new IntentFilter("android.bluetooth.device.action.FOUND");
				registerReceiver(mReceiver, intentfilter);
				IntentFilter intentfilter1 = new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
				registerReceiver(mReceiver, intentfilter1);
				mBlueFlag = true;
				for (; !mAdapter.startDiscovery(); mAdapter.startDiscovery());
			}

			/*
			{
				this$0 = Bluetooth.this;
				super();
			}*/
		};
		myRunnable = new Runnable() {

			//final Bluetooth this$0;

			public void run()
			{
				if (mAdapter.enable())
				{
					mHandler.removeCallbacks(myRunnable);
					init();
				} else
				{
					mHandler.post(myRunnable);
				}
			}

			/*
			{
				this$0 = Bluetooth.this;
				super();
			}*/
		};

		mReceiver = new BroadcastReceiver()
		{
			public void onReceive(Context paramContext, Intent paramIntent)
			{
				String str = paramIntent.getAction();
				if ("android.bluetooth.device.action.FOUND".equals(str))
				{
					BluetoothDevice localBluetoothDevice = (BluetoothDevice)paramIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
					if (localBluetoothDevice.getBondState() != 12)
					{
						Bluetooth.getBlueToothName(Bluetooth.this, localBluetoothDevice.getName() + "--" + Bluetooth.this.getString(R.string.Bluetooth_mac) + localBluetoothDevice.getAddress() + "\n");
						mTvResult.setText(Bluetooth.this.mNameList);
					}
				}
				else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(str))
				{
					mTvCon.setText(R.string.Bluetooth_scan_success);
				}
			}
		};

		bluerunnable = new Runnable() {
			public void run()
			{
				init();
			}			
		};
	}

	private void init()
	{
		mAdapter.enable();
		if (mAdapter.isEnabled())
		{
			msg = mHandler.obtainMessage();
			msg.sendToTarget();
		} else
		{
			mBlueHandler.postDelayed(bluerunnable, 3000L);
		}
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.bluetooth_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.ble_test);
		mTvInfo = (TextView)findViewById(R.id.ble_state_id);
		mTvResult = (TextView)findViewById(R.id.ble_result_id);
		mTvCon = (TextView)findViewById(R.id.ble_con_id);
		mBtOk = (Button)findViewById(R.id.ble_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.ble_bt_failed);
		mBtFailed.setOnClickListener(this);
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mBlueThread.start();
		mBlueHandler = new BlueHandler(mBlueThread.getLooper());
		mBlueHandler.post(bluerunnable);
		mTvInfo.setText(R.string.Bluetooth_opening);
		mSp = getSharedPreferences("FactoryMode", 0);
	}

	protected void onDestroy()
	{
		super.onDestroy();
		if (mBlueFlag)
			unregisterReceiver(mReceiver);
		mAdapter.disable();
	}




	/*
	static boolean access$202(Bluetooth bluetooth, boolean flag)
	{
		bluetooth.mBlueFlag = flag;
		return flag;
	}

	 */







	static String getBlueToothName(Bluetooth bluetooth, Object obj)
	{
		String s = (new StringBuilder()).append(bluetooth.mNameList).append(obj).toString();
		bluetooth.mNameList = s;
		return s;
	}

}
