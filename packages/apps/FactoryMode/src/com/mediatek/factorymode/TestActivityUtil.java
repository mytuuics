
package com.mediatek.factorymode;

import android.content.Context;

public class TestActivityUtil {
    String pckName = null;

    int nameID;

    public static String[] mTestItems = null;

    public TestActivityUtil(String pckName, int nameID) {
        this.pckName = pckName;
        this.nameID = nameID;
    }

    static final TestActivityUtil[] array = new TestActivityUtil[] {
            new TestActivityUtil("com.mediatek.factorymode.audio.AudioTest", R.string.speaker_name),
            new TestActivityUtil("com.mediatek.factorymode.BatteryLog", R.string.battery_name),
            new TestActivityUtil("com.mediatek.factorymode.touchscreen.LineTest",
                    R.string.touchscreen_name),
            new TestActivityUtil("com.mediatek.factorymode.camera.CameraTest", R.string.camera_name),
            new TestActivityUtil("com.mediatek.factorymode.wifi.WiFiTest", R.string.wifi_name),
            new TestActivityUtil("com.mediatek.factorymode.bluetooth.Bluetooth",
                    R.string.bluetooth_name),
            new TestActivityUtil("com.mediatek.factorymode.headset.HeadSet", R.string.headset_name),
            new TestActivityUtil("com.mediatek.factorymode.earphone.Earphone",
                    R.string.earphone_name),
            new TestActivityUtil("com.mediatek.factorymode.vibrator.Vibrator",
                    R.string.vibrator_name),
            new TestActivityUtil("com.mediatek.factorymode.signal.Signal", R.string.telephone_name),
            new TestActivityUtil("com.mediatek.factorymode.gps.GPS", R.string.gps_name),
            new TestActivityUtil("com.mediatek.factorymode.backlight.BackLight",
                    R.string.backlight_name),
            new TestActivityUtil("com.mediatek.factorymode.memory.Memory", R.string.memory_name),
            new TestActivityUtil("com.mediatek.factorymode.microphone.MicRecorder",
                    R.string.microphone_name),
            new TestActivityUtil("com.mediatek.factorymode.sensor.GSensor", R.string.gsensor_name),
            new TestActivityUtil("com.mediatek.factorymode.sensor.MSensor", R.string.msensor_name),
            new TestActivityUtil("com.mediatek.factorymode.sensor.LSensor", R.string.lsensor_name),
            new TestActivityUtil("com.mediatek.factorymode.sensor.PSensor", R.string.psensor_name),
            new TestActivityUtil("com.mediatek.factorymode.sdcard.SDCard", R.string.sdcard_name),
            new TestActivityUtil("com.mediatek.factorymode.fmradio.FMRadio", R.string.fmradio_name),
            new TestActivityUtil("com.mediatek.factorymode.KeyCode", R.string.KeyCode_name),
            new TestActivityUtil("com.mediatek.factorymode.lcd.LCD", R.string.lcd_name),
            new TestActivityUtil("com.mediatek.factorymode.simcard.SimCard", R.string.sim_name),
            new TestActivityUtil("com.mediatek.factorymode.camera.SubCamera",
                    R.string.subcamera_name),
            new TestActivityUtil("com.mediatek.factorymode.tv.Matv", R.string.tv_name),
    };

    public static void init(Context context) {
        if (mTestItems == null) {
            mTestItems = context.getResources().getStringArray(R.array.TestActivitys);
        }
    }

    public static int getTestCount() {
        return mTestItems.length;
    }

    public static int getNameIDFromIndex(int index) {
        if (index >= 0 && index < mTestItems.length) {
            return getNameID(mTestItems[index]);
        } else
            return -1;
    }

    public static String getPckNameFromIndex(int index) {
        if (index >= 0 && index < mTestItems.length) {
            return mTestItems[index];
        } else
            return null;
    }

    public static int getNameID(String pckName) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].pckName.equals(pckName)) {
                return array[i].nameID;
            }
        }
        return -1;
    }

    public static String getPckName(int nameID) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].nameID == nameID) {
                return array[i].pckName;
            }
        }
        return null;
    }
}
