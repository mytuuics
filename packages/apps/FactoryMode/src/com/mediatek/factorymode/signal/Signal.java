// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Signal.java

package com.mediatek.factorymode.signal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class Signal extends Activity
	implements android.view.View.OnClickListener
{

	private Button mBtFailed;
	private Button mBtOk;
	SharedPreferences mSp;

	public Signal()
	{
	}

	protected void onActivityResult(int i, int j, Intent intent)
	{
		android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
		builder.setTitle(R.string.FMRadio_notice);
		builder.setMessage(R.string.HeadSet_hook_message);
		builder.setPositiveButton(R.string.Success, new android.content.DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialoginterface, int k)
			{
				Utils.SetPreferences(Signal.this, mSp, R.string.headsethook_name, "success");
			}

		}
);
		builder.setNegativeButton(R.string.Failed, new android.content.DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialoginterface, int k)
			{
				Utils.SetPreferences(Signal.this, mSp, R.string.headsethook_name, "failed");
			}

		}
);
		builder.create().show();
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.telephone_name, s);
		finish();
	}

	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.signal);
		mSp = getSharedPreferences("FactoryMode", 0);
		mBtOk = (Button)findViewById(R.id.signal_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.signal_bt_failed);
		mBtFailed.setOnClickListener(this);
		startActivityForResult(new Intent("android.intent.action.CALL_PRIVILEGED", Uri.fromParts("tel", "112", null)), 5);
	}
}
