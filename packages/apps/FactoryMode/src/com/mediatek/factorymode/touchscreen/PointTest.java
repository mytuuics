// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   PointTest.java

package com.mediatek.factorymode.touchscreen;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class PointTest extends Activity
	implements android.view.View.OnTouchListener
{
	public class MyView extends View
	{



		protected void onDraw(Canvas canvas)
		{
			for (int i = 0; i < mDrawList.size(); i++)
			{
				CircleEntity circleentity = (CircleEntity)mDrawList.get(i);
				canvas.drawCircle(circleentity.dx, circleentity.dy, 20F, circleentity.mPaint);
			}

		}

		protected void onMeasure(int i, int j)
		{
			super.onMeasure(i, j);
		}

		public MyView(Context context)
		{
			super(context);
		}
	}

	public class CircleEntity
	{

		int dx;
		int dy;
		boolean isselect;
		Paint mPaint;

		CircleEntity(int i, int j)
		{
			super();
			dx = i;
			dy = j;
			isselect = false;
			mPaint = new Paint();
			mPaint.setAntiAlias(true);
			mPaint.setARGB(255, 255, 0, 0);
			mPaint.setStyle(android.graphics.Paint.Style.STROKE);
			mPaint.setStrokeWidth(2F);
		}
	}


	private static final int radius = 20;
	public Point PrePoint;
	private int dx;
	private int dy;
	public Bitmap mBitmap;
	public int mBitmapPad;
	private int mCount;
	private List mDrawList;
	private List mList;
	MyView mMyView;
	public double mPointError;
	private int mRectHeight;
	private int mRectWidth;
	public boolean mRun;
	private SharedPreferences mSp;
	public Random rand;

	public PointTest()
	{
		mMyView = null;
		mRun = false;
		mPointError = 0D;
		mBitmapPad = 0;
		dx = 20;
		dy = 20;
		mList = null;
		mDrawList = null;
		mCount = 0;
	}

	public boolean IsCollision(int i, int j, int k)
	{
		boolean flag;
		if (k < 9)
		{
			Rect rect = (Rect)mList.get(k);
			if (rect.left < i && rect.right > i && rect.top < j && rect.bottom > j)
				flag = true;
			else
				flag = false;
		} else
		{
			flag = false;
		}
		return flag;
	}

	public void initCircle(int i, int j)
	{
		CircleEntity circleentity = new CircleEntity(i, j);
		mDrawList.add(circleentity);
	}

	public void initRect(int i, int j, int k, int l)
	{
		Rect rect = new Rect();
		rect.left = i;
		rect.right = j;
		rect.top = k;
		rect.bottom = l;
		mList.add(rect);
	}

	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		getWindow().setFlags(1024, 1024);
		requestWindowFeature(1);
		new DisplayMetrics();
		mSp = getSharedPreferences("FactoryMode", 0);
		DisplayMetrics displaymetrics = getApplicationContext().getResources().getDisplayMetrics();
		mRectWidth = displaymetrics.widthPixels;
		mRectHeight = displaymetrics.heightPixels;
		MyView myview = new MyView(this);
		mMyView = myview;
		setContentView(myview);
		mMyView.setOnTouchListener(this);
		mList = new ArrayList();
		initRect(0, 2 * dx, 0, 2 * dy);
		initRect(mRectWidth / 2 - dx, mRectWidth / 2 + dx, 0, 2 * dy);
		initRect(mRectWidth - 2 * dx, mRectWidth, 0, 2 * dy);
		initRect(0, 2 * dx, mRectHeight / 2 - dy, mRectHeight / 2 + dy);
		initRect(mRectWidth / 2 - dx, mRectWidth / 2 + dx, mRectHeight / 2 - dy, mRectHeight / 2 + dy);
		initRect(mRectWidth - 2 * dx, mRectWidth, mRectHeight / 2 - dy, mRectHeight / 2 + dy);
		initRect(0, 2 * dx, mRectHeight - 2 * dy, mRectHeight);
		initRect(mRectWidth / 2 - dx, mRectWidth / 2 + dx, mRectHeight - 2 * dy, mRectHeight);
		initRect(mRectWidth - 2 * dx, mRectWidth, mRectHeight - 2 * dy, mRectHeight);
		mDrawList = new ArrayList();
		initCircle(dx, dy);
		initCircle(mRectWidth / 2, dy);
		initCircle(mRectWidth - dx, dy);
		initCircle(dx, mRectHeight / 2);
		initCircle(mRectWidth / 2, mRectHeight / 2);
		initCircle(mRectWidth - dx, mRectHeight / 2);
		initCircle(dx, mRectHeight - dy);
		initCircle(mRectWidth / 2, mRectHeight - dy);
		initCircle(mRectWidth - dx, mRectHeight - dy);
	}

	public boolean onKeyDown(int i, KeyEvent keyevent)
	{
		if (i == 4)
		{
			Utils.SetPreferences(this, mSp, R.string.touchscreen_name, "failed");
			finish();
		}
		return true;
	}

	public boolean onTouch(View view, MotionEvent motionevent)
	{
		boolean flag = false;
		if (motionevent.getAction() == 0){
			boolean flag1;
			flag1 = IsCollision((int)motionevent.getX(), (int)motionevent.getY(), mCount);
			Log.i("hewei", "onTouch");
			if (flag1){
				((CircleEntity)mDrawList.get(mCount)).mPaint.setARGB(255, 0, 255, 0);
				if (mCount >= 8){
					Intent intent = new Intent();
					intent.setClassName(this, "com.mediatek.factorymode.touchscreen.LineTest");
					startActivity(intent);
					finish();
					flag = true;
					return flag;
				}else{
					mCount = 1 + mCount;
					mMyView.invalidate();
					flag = true;
					return flag;
				}
			}
		}
		mMyView.invalidate();
		flag = true;
		return flag;
	}

}
