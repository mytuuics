// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   LineTest.java

package com.mediatek.factorymode.touchscreen;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class LineTest extends Activity
{
	class CanvasView extends View
	{

		private Paint mLinePaint;
		private Paint mOkPaint;
		private Rect mRect;
		private Paint mRectPaint;
		private Paint mTextPaint;

		private void showDialog(String s, String s1, boolean flag, boolean flag1)
		{
			android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(LineTest.this);
			builder.setTitle(s).setMessage(s1);
			if (flag)
				builder.setPositiveButton(getString(R.string.Again), new android.content.DialogInterface.OnClickListener() {


					public void onClick(DialogInterface dialoginterface, int i)
					{
						mInput.clear();
						mTemPoints.clear();
						mSuccess = true;
						invalidate();
					}

				}
);
			if (flag1)
				builder.setNegativeButton(getString(R.string.GoOn), new android.content.DialogInterface.OnClickListener() {


					public void onClick(DialogInterface dialoginterface, int i)
					{
					}

				}
);
			builder.show();
		}

		public void onDraw(Canvas canvas)
		{
			canvas.drawColor(0xffffff);
			int i = mRectHeight / 4;
			canvas.drawRect(mRect, mRectPaint);
			mLinePaint.setARGB(255, 0, 0, 255);
			for (int j = 0; j < mPts1.size(); j++)
			{
				List list1 = (List)mPts1.get(j);
				Point point4 = (Point)list1.get(0);
				Point point5 = (Point)list1.get(1);
				canvas.drawLine(point4.x, point4.y, point5.x, point5.y, mLinePaint);
			}

			mLinePaint.setARGB(255, 255, 0, 0);
			for (int k = 0; k < mTemPoints.size() - 2; k++)
			{
				Point point2 = (Point)mTemPoints.get(k);
				Point point3 = (Point)mTemPoints.get(k + 1);
				canvas.drawLine(point2.x, point2.y, point3.x, point3.y, mLinePaint);
			}

			for (int l = 0; l < mInput.size(); l++)
			{
				List list = (List)mInput.get(l);
				for (int i1 = 0; i1 < list.size() - 1; i1++)
				{
					Point point = (Point)list.get(i1);
					Point point1 = (Point)list.get(i1 + 1);
					canvas.drawLine(point.x, point.y, point1.x, point1.y, mLinePaint);
				}

			}

			if (!mResultString.equals(""))
				canvas.drawText((new StringBuilder()).append(getResources().getString(R.string.Offset)).append(mResultString).toString(), 20 * mZoom, i, mTextPaint);
			canvas.drawCircle(mRectWidth / 2, mRectHeight / 2, 50F, mOkPaint);
			canvas.drawText(getResources().getString(R.string.Result), mRectWidth / 2 - 40, mRectHeight / 2 - 55, mOkPaint);
		}

		public boolean onTouchEvent(MotionEvent motionevent)
		{
			int i;
			int j;
			i = (int)motionevent.getX();
			j = (int)motionevent.getY();
			switch(motionevent.getAction()){
			
			case 0:
				if (mSuccess && (i >= 50 + mRectWidth / 2 || i <= mRectWidth / 2 - 50 || j >= 50 + mRectHeight / 2 || j <= mRectHeight / 2 - 50))
					mSuccess = isBeginOrEndPoint(i, j);
				break;
			case 1:
				if (i < 50 + mRectWidth / 2 && i > mRectWidth / 2 - 50 && j < 50 + mRectHeight / 2 && j > mRectHeight / 2 - 50)
				{
					mTemPoints.clear();
					CalculateDiversity();
				} else
				{
					if (mSuccess)
						mSuccess = isBeginOrEndPoint(i, j);
					if (!mSuccess)
						showDialog(getString(R.string.Error), getString(R.string.DrawError), true, true);
					mInput.add(mTemPoints);
					mTemPoints = new ArrayList();
				}
				break;
			case 2:
				mTemPoints.add(new Point(i, j));
				break;
				
			default:
				
			}
			invalidate();
			return true;

		}

		CanvasView(Context context)
		{
			super(context);
			mLinePaint = null;
			mTextPaint = null;
			mRectPaint = null;
			mOkPaint = null;
			mRect = null;
			mLinePaint = new Paint();
			mLinePaint.setAntiAlias(true);
			mLinePaint.setStrokeCap(android.graphics.Paint.Cap.ROUND);
			mLinePaint.setStrokeWidth(8F);
			mTextPaint = new Paint();
			mTextPaint.setAntiAlias(true);
			mTextPaint.setTextSize(12F * (float)mZoom);
			mTextPaint.setARGB(255, 0, 0, 0);
			mRect = new Rect(0, 0, mRectWidth, mRectHeight);
			mRectPaint = new Paint();
			mRectPaint.setARGB(255, 255, 255, 255);
			mOkPaint = new Paint();
			mOkPaint.setTextSize(20F);
			mOkPaint.setARGB(255, 0, 0, 255);
			mOkPaint.setAntiAlias(true);
			mOkPaint.setStyle(android.graphics.Paint.Style.STROKE);
		}
	}


	public static final int CALCULATE_ID = 1;
	public static final int NEXTLINE_ID = 2;
	private static final int SFVALUE = 30;
	private int flags;
	public double mDiversity;
	public List mInput;
	public List mInputBottom;
	public List mInputLeft;
	public List mInputRight;
	public List mInputTop;
	private int mPadding;
	public List mPts1;
	private int mRectHeight;
	private int mRectWidth;
	private String mResultString;
	public boolean mRun;
	private SharedPreferences mSp;
	private boolean mSuccess;
	private List mSuperPts;
	private List mTemPoints;
	private int mZoom;

	public LineTest()
	{
		mRun = false;
		mSuccess = true;
		mDiversity = 0D;
		mPts1 = new ArrayList();
		mTemPoints = new ArrayList();
		mInput = new ArrayList();
		mSuperPts = new ArrayList();
		mInputLeft = new ArrayList();
		mInputTop = new ArrayList();
		mInputRight = new ArrayList();
		mInputBottom = new ArrayList();
		mZoom = 1;
		mPadding = 20;
		flags = 0;
		mResultString = "";
	}

	private void distributeAllPoint(List list)
	{
		int i = 0;
		do
		{
			if (i >= list.size())
				break;
			List list1 = (List)list.get(i);
			int j = 0;
			while (j < list1.size()) 
			{
				Point point = (Point)list1.get(j);
				if (point.x < mRectWidth / 2)
				{
					if (point.y < mRectHeight / 2)
					{
						if (point.x > point.y)
							mInputTop.add(point);
						else
							mInputLeft.add(point);
					} else
					if (point.x > mRectHeight - point.y)
						mInputBottom.add(point);
					else
						mInputLeft.add(point);
				} else
				if (point.y < mRectHeight / 2)
				{
					if (mRectWidth - point.x > point.y)
						mInputTop.add(point);
					else
						mInputRight.add(point);
					if (mRectWidth - point.x > mRectHeight - point.y)
						mInputBottom.add(point);
					else
						mInputRight.add(point);
				}
				j++;
			}
			i++;
		} while (true);
	}

	private boolean isBeginOrEndPoint(int i, int j)
	{
		boolean flag;
		if ((i < 4 * mPadding || i > mRectWidth - 4 * mPadding) && (j < 4 * mPadding || j > mRectHeight - 4 * mPadding))
			flag = true;
		else
			flag = false;
		return flag;
	}

	private void readLine()
	{
		ArrayList arraylist = new ArrayList();
		arraylist.add(new Point(mPadding, mPadding));
		arraylist.add(new Point(mPadding, mRectHeight - mPadding));
		mPts1.add(arraylist);
		ArrayList arraylist1 = new ArrayList();
		arraylist1.add(new Point(mPadding, mPadding));
		arraylist1.add(new Point(mRectWidth - mPadding, mPadding));
		mPts1.add(arraylist1);
		ArrayList arraylist2 = new ArrayList();
		arraylist2.add(new Point(mRectWidth - mPadding, mPadding));
		arraylist2.add(new Point(mRectWidth - mPadding, mRectHeight - mPadding));
		mPts1.add(arraylist2);
		ArrayList arraylist3 = new ArrayList();
		arraylist3.add(new Point(mPadding, mRectHeight - mPadding));
		arraylist3.add(new Point(mRectWidth - mPadding, mRectHeight - mPadding));
		mPts1.add(arraylist3);
	}

	public void CalculateDiversity()
	{
		new Point(0, 0);
		if (mInput.size() != 0)
		{
			distributeAllPoint(mInput);
			double d = 0D;
			for (int i = 0; i < mInputLeft.size(); i++)
				d += Math.abs(((Point)mInputLeft.get(i)).x - mPadding);

			for (int j = 0; j < mInputTop.size(); j++)
				d += Math.abs(((Point)mInputTop.get(j)).y - mPadding);

			for (int k = 0; k < mInputRight.size(); k++)
				d += Math.abs(((Point)mInputRight.get(k)).x - (mRectWidth - mPadding));

			for (int l = 0; l < mInputBottom.size(); l++)
				d += Math.abs(((Point)mInputBottom.get(l)).y - (mRectHeight - mPadding));

			mDiversity = d / (double)(mInputLeft.size() + mInputTop.size() + mInputRight.size() + mInputBottom.size());
			mResultString = String.valueOf(mDiversity);
			if ((int)Float.parseFloat(mResultString) > 30)
				flags = 1;
			if (mInputLeft.size() < 5 || mInputTop.size() < 5 || mInputRight.size() < 5 || mInputBottom.size() < 5)
				mSuccess = false;
			Context context = getApplicationContext();
			SharedPreferences sharedpreferences = mSp;
			String s;
			if (mSuccess)
			{
				if (flags == 1)
					s = "failed";
				else
					s = "success";
			} else
			{
				s = "failed";
			}
			Utils.SetPreferences(context, sharedpreferences, R.string.touchscreen_name, s);
			finish();
			mDiversity = 0D;
		}
	}

	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		getWindow().setFlags(1024, 1024);
		requestWindowFeature(1);
		mSp = getSharedPreferences("FactoryMode", 0);
		new DisplayMetrics();
		DisplayMetrics displaymetrics = getApplicationContext().getResources().getDisplayMetrics();
		mRectWidth = displaymetrics.widthPixels;
		mRectHeight = displaymetrics.heightPixels;
		if (480 == mRectWidth && 800 == mRectHeight || 800 == mRectWidth && 480 == mRectHeight)
			mZoom = 2;
		readLine();
		setContentView(new CanvasView(this));
	}

	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 1, 0, "Calculate");
		menu.add(0, 2, 1, "NextLine");
		return true;
	}

	protected void onDestroy()
	{
		releaseList();
		super.onDestroy();
	}

	public void readPoints()
	{
		ArrayList arraylist = new ArrayList();
		for (int i = 0; i < mRectHeight - 2 * mPadding; i++)
			arraylist.add(new Point(mPadding, i + mPadding));

		mSuperPts.add(arraylist);
		ArrayList arraylist1 = new ArrayList();
		for (int j = 0; j < mRectWidth - 2 * mPadding; j++)
			arraylist1.add(new Point(j + mPadding, mPadding));

		mSuperPts.add(arraylist1);
		ArrayList arraylist2 = new ArrayList();
		for (int k = 0; k < mRectHeight - 2 * mPadding; k++)
			arraylist2.add(new Point(mRectWidth - mPadding, k + mPadding));

		mSuperPts.add(arraylist2);
		ArrayList arraylist3 = new ArrayList();
		for (int l = 0; l < mRectWidth - 2 * mPadding; l++)
			arraylist3.add(new Point(l + mPadding, mRectHeight - mPadding));

		mSuperPts.add(arraylist3);
		new ArrayList();
	}

	public void releaseList()
	{
		mPts1 = null;
		mTemPoints = null;
		mSuperPts = null;
		mInput = null;
		mInputLeft = null;
		mInputTop = null;
		mInputRight = null;
		mInputBottom = null;
	}






/*
	static List access$302(LineTest linetest, List list)
	{
		linetest.mTemPoints = list;
		return list;
	}

*/




/*
	static boolean access$502(LineTest linetest, boolean flag)
	{
		linetest.mSuccess = flag;
		return flag;
	}

*/

}
