// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   FMRadio.java

package com.mediatek.factorymode.fmradio;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class FMRadio extends Activity implements OnClickListener
{

	static Cursor mCursor = null;
	SharedPreferences mSp;
	Handler myHandler;
    Button btnSuccess = null;
    Button btnFail = null;

	public FMRadio()
	{
	}

	//protected void onActivityResult(int i, int j, Intent intent)
	//{

	//	Log.d("tttt","onActivityResult i="+i+",j="+j);
	//	Message message = new Message();
	//	message.what = 0;
	//	myHandler.sendMessage(message);
	//}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
        setContentView(R.layout.only_two_button);
        btnSuccess = (Button) findViewById(R.id.btn_success);
        btnFail = (Button) findViewById(R.id.but_failed);
        btnSuccess.setOnClickListener(this);
        btnFail.setOnClickListener(this);
		//myHandler = new Handler() {
		//	public void handleMessage(Message message)
		//	{

		//		android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(FMRadio.this);
		//		builder.setTitle(R.string.FMRadio_notice);
		//		builder.setPositiveButton(R.string.Success, new android.content.DialogInterface.OnClickListener() {
		//			public void onClick(DialogInterface dialoginterface, int i){
		//				Utils.SetPreferences(getApplicationContext(), mSp, R.string.fmradio_name, "success");
		//				finish();
		//			}

		//		}
		//		);
		//		builder.setNegativeButton(getResources().getString(R.string.Failed), new android.content.DialogInterface.OnClickListener() {


		//			public void onClick(DialogInterface dialoginterface, int i){
		//				Utils.SetPreferences(getApplicationContext(), mSp, R.string.fmradio_name, "failed");
		//				finish();
		//			}

		//		}
		//		);
		//		builder.create().show();
		//	}
		//};
		mSp = getSharedPreferences("FactoryMode", 0);
		Intent i = new Intent();
		i.putExtra("test", "test");
        i.setClassName("com.mediatek.FMRadio", "com.mediatek.FMRadio.FMRadioActivity");
		startActivity(i);
	}

    @Override
    public void onClick(View v) {
        if (v == btnSuccess) {

		    Utils.SetPreferences(getApplicationContext(), mSp, R.string.fmradio_name, "success");
        } else if (v == btnFail) {

			Utils.SetPreferences(getApplicationContext(), mSp, R.string.fmradio_name, "failed");
        }
        finish();
    }
}
