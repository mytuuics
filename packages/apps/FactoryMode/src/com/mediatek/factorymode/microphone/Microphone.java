// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Microphone.java

package com.mediatek.factorymode.microphone;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioRecord;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class Microphone extends Activity
	implements android.view.View.OnClickListener
{
	public class RecordThread extends Thread
	{

		private static final int SAMPLE_RATE_IN_HZ = 44100;
		private AudioRecord ar;
		private int bs;
		private boolean isRun;

		public void pause()
		{
			isRun = false;
		}

		public void run()
		{
			super.run();
			ar.startRecording();
			byte abyte0[] = new byte[bs];
			int i = ar.read(abyte0, 0, bs);
			for (isRun = true; isRun;)
			{
				int j = 0;
				for (int k = 0; k < abyte0.length; k++)
					j += abyte0[k] * abyte0[k];

				Message message = new Message();
				message.arg1 = (int)((float)j / (float)i);
				h.sendMessage(message);
			}

			ar.stop();
		}

		public void start()
		{
			if (!isRun)
				super.start();
		}

		public RecordThread()
		{
			super();
			isRun = false;
			bs = AudioRecord.getMinBufferSize(44100, 2, 2);
			ar = new AudioRecord(1, 44100, 2, 2, bs);
		}
	}


	private Button btfailed;
	private Button btok;
	public Handler h;
	RecordThread rt;
	private SharedPreferences sp;
	private TextView tvstatus;

	public Microphone()
	{
		h = new Handler() {

			public void handleMessage(Message message)
			{
				super.handleMessage(message);
				tvstatus.setText(String.valueOf(message.arg1));
			}

		}
;
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = sp;
		String s;
		if (view.getId() == btok.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.Microphone, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.microphone);
		sp = getSharedPreferences("FactoryMode", 0);
		tvstatus = (TextView)findViewById(R.id.mic_tv_status);
		btok = (Button)findViewById(R.id.mic_bt_ok);
		btok.setOnClickListener(this);
		btfailed = (Button)findViewById(R.id.mic_bt_failed);
		btfailed.setOnClickListener(this);
		rt = new RecordThread();
	}

	public void onDestroy()
	{
		super.onDestroy();
		rt.destroy();
	}

	public void onPause()
	{
		super.onPause();
		rt.pause();
	}

	public void onResume()
	{
		super.onResume();
		rt.start();
	}

}
