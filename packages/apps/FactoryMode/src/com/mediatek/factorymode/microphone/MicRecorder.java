// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   MicRecorder.java

package com.mediatek.factorymode.microphone;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;
import com.mediatek.factorymode.VUMeter;

public class MicRecorder extends Activity
	implements android.view.View.OnClickListener
{

	Handler h;
	private Button mBtMicFailed;
	private Button mBtMicOk;
	private Button mBtSpkFailed;
	private Button mBtSpkOk;
	boolean mMicClick;
	private MediaPlayer mPlayer;
	private Button mRecord;
	private MediaRecorder mRecorder;
	SharedPreferences mSp;
	boolean mSpkClick;
	VUMeter mVUMeter;
	Runnable ra;

	public MicRecorder()
	{
		mMicClick = false;
		mSpkClick = false;
		h = new Handler();
		ra = new Runnable() {

			public void run()
			{
				mVUMeter.invalidate();
				h.postDelayed(this, 100L);
			}
		}
;
	}

	private void start()
	{
		h.post(ra);
		if (mPlayer != null)
			mPlayer.stop();
		if (!Environment.getExternalStorageState().equals("mounted"))
		{
			mRecord.setText(R.string.sdcard_tips_failed);
		} else
		{
			try
			{
				mRecorder = new MediaRecorder();
				mRecorder.setAudioSource(1);
				mRecorder.setOutputFormat(1);
				mRecorder.setAudioEncoder(3);
				mVUMeter.setRecorder(mRecorder);
				String s = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append(File.separator).append("test.mp3").toString();
				if (!(new File(s)).exists())
					(new File(s)).createNewFile();
				mRecorder.setOutputFile(s);
				mRecorder.prepare();
				mRecorder.start();
			}
			catch (Exception exception)
			{
				Toast.makeText(this, exception.getMessage(), 0);
			}
			mRecord.setTag("ing");
			mRecord.setText(R.string.Mic_stop);
		}
	}

	private void stopAndSave()
	{
		h.removeCallbacks(ra);
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		mRecord.setText(R.string.Mic_start);
		mRecord.setTag("");
		mVUMeter.SetCurrentAngle(0F);
		try{
			mPlayer = new MediaPlayer();
			mPlayer.setDataSource("/sdcard/test.mp3");
			mPlayer.prepare();
			mPlayer.start();
		}
	    catch (IllegalArgumentException localIllegalArgumentException)
	    {
	        localIllegalArgumentException.printStackTrace();
	    }
	    catch (IllegalStateException localIllegalStateException)
	    {
	        localIllegalStateException.printStackTrace();
	    }
	    catch (IOException localIOException)
	    {
	        localIOException.printStackTrace();
	    }
	}

	public void isFinish()
	{
		if (mMicClick && mSpkClick)
			finish();
	}

	public void onClick(View view)
	{
		if (view.getId() == mRecord.getId())
			if (mRecord.getTag() == null || !mRecord.getTag().equals("ing"))
				start();
			else
				stopAndSave();
		if (view.getId() == mBtMicOk.getId())
		{
			mMicClick = true;
			mBtMicFailed.setBackgroundColor(getResources().getColor(R.color.gray));
			mBtMicOk.setBackgroundColor(getResources().getColor(R.color.Green));
			Utils.SetPreferences(this, mSp, R.string.microphone_name, "success");
		} else
		if (view.getId() == mBtMicFailed.getId())
		{
			mMicClick = true;
			mBtMicOk.setBackgroundColor(getResources().getColor(R.color.gray));
			mBtMicFailed.setBackgroundColor(getResources().getColor(R.color.Red));
			Utils.SetPreferences(this, mSp, R.string.microphone_name, "failed");
		}
		if (view.getId() == mBtSpkOk.getId())
		{
			mSpkClick = true;
			mBtSpkFailed.setBackgroundColor(getResources().getColor(R.color.gray));
			mBtSpkOk.setBackgroundColor(getResources().getColor(R.color.Green));
			Utils.SetPreferences(this, mSp, R.string.speaker_name, "success");
		} else
		if (view.getId() == mBtSpkFailed.getId())
		{
			mSpkClick = true;
			mBtSpkOk.setBackgroundColor(getResources().getColor(R.color.gray));
			mBtSpkFailed.setBackgroundColor(getResources().getColor(R.color.Red));
			Utils.SetPreferences(this, mSp, R.string.speaker_name, "failed");
		}
		isFinish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.micrecorder);
		mSp = getSharedPreferences("FactoryMode", 0);
		mRecord = (Button)findViewById(R.id.mic_bt_start);
		mRecord.setOnClickListener(this);
		mBtMicOk = (Button)findViewById(R.id.mic_bt_ok);
		mBtMicOk.setOnClickListener(this);
		mBtMicFailed = (Button)findViewById(R.id.mic_bt_failed);
		mBtMicFailed.setOnClickListener(this);
		mBtSpkOk = (Button)findViewById(R.id.speaker_bt_ok);
		mBtSpkOk.setOnClickListener(this);
		mBtSpkFailed = (Button)findViewById(R.id.speaker_bt_failed);
		mBtSpkFailed.setOnClickListener(this);
		mVUMeter = (VUMeter)findViewById(R.id.uvMeter);
	}

	protected void onDestroy()
	{
		super.onDestroy();
		(new File("/sdcard/test.mp3")).delete();
		if (mPlayer != null)
			mPlayer.stop();
		if (mRecorder != null)
			mRecorder.stop();
	}
}
