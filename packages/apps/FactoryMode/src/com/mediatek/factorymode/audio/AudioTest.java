// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   AudioTest.java

package com.mediatek.factorymode.audio;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class AudioTest extends Activity
	implements android.view.View.OnClickListener
{

	private Button mBtFailed;
	private Button mBtOk;
	private MediaPlayer mPlayer;
	private SharedPreferences mSp;

	public AudioTest()
	{
	}

	private void initMediaPlayer()
	{
		mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.tada);
		mPlayer.setLooping(true);
		mPlayer.start();
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.speaker_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.audio_test);
		mBtOk = (Button)findViewById(R.id.audio_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.audio_bt_failed);
		mBtFailed.setOnClickListener(this);
		AudioManager audiomanager = (AudioManager)getSystemService("audio");
		audiomanager.setRingerMode(2);
		//audiomanager.setStreamVolume(3, audiomanager.getStreamMaxVolume(3), 4);
		initMediaPlayer();
	}

	protected void onDestroy()
	{
		super.onDestroy();
		mPlayer.stop();
	}

	public boolean onKeyDown(int i, KeyEvent keyevent)
	{
		if (i == 4 && keyevent.getRepeatCount() == 0)
			finish();
		return true;
	}

	protected void onResume()
	{
		super.onResume();
		mSp = getSharedPreferences("FactoryMode", 0);
		mBtOk = (Button)findViewById(R.id.audio_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.audio_bt_failed);
		mBtFailed.setOnClickListener(this);
	}
}
