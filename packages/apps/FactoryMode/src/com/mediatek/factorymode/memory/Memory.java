// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Memory.java

package com.mediatek.factorymode.memory;

import java.io.IOException;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.ShellExe;
import com.mediatek.factorymode.Utils;

public class Memory extends Activity
	implements android.view.View.OnClickListener
{

	private TextView mBtFailed;
	private TextView mBtOk;
	private TextView mCommInfo;
	SharedPreferences mSp;

	public Memory()
	{
	}

	private String getInfo(String s)
	{
		String s1;
		try
		{
			String as[] = new String[3];
			as[0] = "/system/bin/sh";
			as[1] = "-c";
			as[2] = s;
			if (ShellExe.execCommand(as) == 0)
				s1 = ShellExe.getOutput();
			else
				s1 = "Can not get flash info.";
		}
		catch (IOException ioexception)
		{
			s1 = ioexception.toString();
		}
		return s1;
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.memory_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.memory);
		mSp = getSharedPreferences("FactoryMode", 0);
		mBtOk = (TextView)findViewById(R.id.memory_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (TextView)findViewById(R.id.memory_bt_failed);
		mBtFailed.setOnClickListener(this);
		mCommInfo = (TextView)findViewById(R.id.comm_info);
		mCommInfo.setText(getInfo("cat /proc/driver/nand"));
	}
}
