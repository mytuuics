// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   BatteryLog.java

package com.mediatek.factorymode;

import android.app.Activity;
import android.content.*;
import android.os.*;
import android.text.format.DateUtils;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;
import java.io.*;

// Referenced classes of package com.mediatek.factorymode:
//			Utils

public class BatteryLog extends Activity
	implements android.view.View.OnClickListener
{
	class runThread extends Thread
	{
		//final BatteryLog this$0;

		public void run()
		{
			while (mStart) 
			{
				File file = new File("/sys/devices/platform/mt6573-battery/power_supply/battery/BatteryAverageCurrent");
				File file1 = new File("/sys/devices/platform/mt6573-battery/FG_Battery_CurrentConsumption");
				if (file1.exists() && file.exists())
					mHandler.obtainMessage(2, Integer.parseInt(BatteryLog.readFile(file).trim()), Integer.parseInt(BatteryLog.readFile(file1).trim())).sendToTarget();
				try
				{
					sleep(1000L);
				}
				catch (InterruptedException interruptedexception) { }
			}
		}

		/*
		runThread()
		{
			this$0 = BatteryLog.this;
			super();
		}*/
	}


	private static final int EVENT_TICK = 1;
	private static final int EVENT_UPDATE = 2;
	String flag;
	private Button mBtFailed;
	private Button mBtOK;
	private TextView mCurrent;
	private TextView mCurrentConsumption;
	private Handler mHandler;
	private TextView mHealth;
	private IntentFilter mIntentFilter;
	private BroadcastReceiver mIntentReceiver;
	private TextView mLevel;
	private TextView mScale;
	private SharedPreferences mSp;
	boolean mStart;
	private TextView mStatus;
	private TextView mTechnology;
	private TextView mTemperature;
	private TextView mUptime;
	private TextView mVoltage;

	public BatteryLog()
	{
		flag = "";
		mStart = false;
		mHandler = new Handler() {

			//final BatteryLog this$0;

			private void updateBatteryStats()
			{
				long l = SystemClock.elapsedRealtime();
				mUptime.setText(DateUtils.formatElapsedTime(l / 1000L));
			}

			public void handleMessage(Message message)
			{
				switch(message.what)
				{
				case 29:
					updateBatteryStats();
					sendEmptyMessageDelayed(1, 1000L);
					break;
				case 45:
					mCurrent.setText((new StringBuilder()).append(message.arg1).append("mA").toString());
					mCurrentConsumption.setText((new StringBuilder()).append(message.arg2).append("mA").toString());
					break;
				}
			}

			/*
			{
				this$0 = BatteryLog.this;
				super();
			}*/
		}
;
		mIntentReceiver = new BroadcastReceiver() {

			//final BatteryLog this$0;

			public void onReceive(Context context, Intent intent)
			{
				if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED"))
				{
					int i = intent.getIntExtra("plugged", 0);
					mLevel.setText((new StringBuilder()).append("").append(intent.getIntExtra("level", 0)).toString());
					mScale.setText((new StringBuilder()).append("").append(intent.getIntExtra("scale", 0)).toString());
					mVoltage.setText((new StringBuilder()).append("").append(intent.getIntExtra("voltage", 0)).append(" ").append(getString(R.string.battery_info_voltage_units)).toString());
					mTemperature.setText((new StringBuilder()).append("").append(tenthsToFixedString(intent.getIntExtra("temperature", 0))).append(getString(R.string.battery_info_temperature_units)).toString());
					mTechnology.setText((new StringBuilder()).append("").append(intent.getStringExtra("technology")).toString());
					String s;
					switch(intent.getIntExtra("status", 1))
					{
					case 2:
						s = getString(R.string.battery_info_status_charging);
						if (i > 0)
						{
							StringBuilder stringbuilder = (new StringBuilder()).append(s).append(" ");
							BatteryLog batterylog = BatteryLog.this;
							int j;
							if (i == 1)
								j = R.string.battery_info_status_charging_ac;
							else
								j = R.string.battery_info_status_charging_usb;
							s = stringbuilder.append(batterylog.getString(j)).toString();
						}
						break;
					case 3:
						s = getString(R.string.battery_info_status_discharging);
						break;
					case 4:
						s = getString(R.string.battery_info_status_not_charging);
						break;
					case 5:
						s = getString(R.string.battery_info_status_full);
						break;
					default:
						s = getString(R.string.battery_info_status_unknown);
					}
					mStatus.setText(s);
					switch(intent.getIntExtra("health", 1))
					{
					case 2:
						s = getString(R.string.battery_info_health_good);
						break;
					case 3:
						s = getString(R.string.battery_info_health_overheat);
						break;
					case 4:
						s = getString(R.string.battery_info_health_dead);
						break;
					case 5:
						s = getString(R.string.battery_info_health_over_voltage);
						break;
					case 6:
						s = getString(R.string.battery_info_health_unspecified_failure);
						break;
					default:
						s = getString(R.string.battery_info_health_unknown);
					}
					
					mHealth.setText(s);
	
				}
			}

		}
;
	}

	private static String readFile(File fn)
	{
		String s = "0";
		try
		{
			FileReader freader = new FileReader(fn);
			
			if(freader != null)
			{
				char ac[] = new char[20];
				StringBuilder sb = new StringBuilder();
				
				int i;
				
				while((i = freader.read(ac, 0, ac.length)) > 0 )
				{
					sb.append(String.valueOf(ac, 0, i));
				}
				
				s = sb.toString();				
			}			
		}
		catch(IOException e)
		{
			s = "0";
		}
		
		
		return s;
	}

	private final String tenthsToFixedString(int i)
	{
		int j = i / 10;
		return new String((new StringBuilder()).append("").append(j).append(".").append(i - j * 10).toString());
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOK.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.battery_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.battery_info);
		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
	}

	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 1, 1, R.string.menu_exit);
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem menuitem)
	{
		setResult(1);
		finish();
		return true;
	}

	public void onPause()
	{
		super.onPause();
		mHandler.removeMessages(1);
		mStart = false;
		unregisterReceiver(mIntentReceiver);
	}

	public void onResume()
	{
		super.onResume();
		mStatus = (TextView)findViewById(R.id.status);
		mLevel = (TextView)findViewById(R.id.level);
		mScale = (TextView)findViewById(R.id.scale);
		mHealth = (TextView)findViewById(R.id.health);
		mTechnology = (TextView)findViewById(R.id.technology);
		mVoltage = (TextView)findViewById(R.id.voltage);
		mTemperature = (TextView)findViewById(R.id.temperature);
		mUptime = (TextView)findViewById(R.id.uptime);
		mCurrent = (TextView)findViewById(R.id.current);
		mCurrentConsumption = (TextView)findViewById(R.id.current_consumption);
		mBtOK = (Button)findViewById(R.id.battery_bt_ok);
		mBtOK.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.battery_bt_failed);
		mBtFailed.setOnClickListener(this);
		mSp = getSharedPreferences("FactoryMode", 0);
		mHandler.sendEmptyMessageDelayed(1, 1000L);
		mStart = true;
		registerReceiver(mIntentReceiver, mIntentFilter);
		(new runThread()).start();
	}













}
