// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   CameraTest.java

package com.mediatek.factorymode.camera;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class CameraTest extends Activity
	implements android.view.View.OnClickListener
{

	android.hardware.Camera.AutoFocusCallback autofocusCallback;
	private ImageView mAutoFocus;
	private boolean mAutoFocusFlag;
	private Button mBtFailed;
	private Button mBtFinish;
	private Button mBtOk;
	private Camera mCamera;
	private boolean mIsPreview;
	SharedPreferences mSp;
	android.hardware.Camera.PictureCallback pictureCallback;
	android.view.SurfaceHolder.Callback surfaceCallback;
	private SurfaceHolder surfaceHolder;
	private SurfaceView surfaceView;

	public CameraTest()
	{
		mIsPreview = false;
		mAutoFocusFlag = true;
		pictureCallback = new android.hardware.Camera.PictureCallback() {

			//final CameraTest this$0;

			public void onPictureTaken(byte abyte0[], Camera camera)
			{
				mAutoFocus.setImageDrawable(null);
			}

			/*
			{
				this$0 = CameraTest.this;
				super();
			}*/
		}
;
		autofocusCallback = new android.hardware.Camera.AutoFocusCallback() {

			//final CameraTest this$0;

			public void onAutoFocus(boolean flag, Camera camera)
			{
				if (flag)
				{
					mAutoFocus.setImageResource(R.drawable.focus_focused);
					mCamera.takePicture(null, null, pictureCallback);
				} else
				{
					mAutoFocus.setImageResource(R.drawable.focus_focus_failed);
				}
			}

			/*
			{
				this$0 = CameraTest.this;
				super();
			}*/
		}
;
		surfaceCallback = new android.view.SurfaceHolder.Callback() {

			//final CameraTest this$0;

			public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k)
			{
				if (mCamera == null)
				{
					try
					{
						Thread.sleep(1000L);
					}
					catch (InterruptedException interruptedexception)
					{
						interruptedexception.printStackTrace();
					}
					Utils.SetPreferences(getApplicationContext(), mSp, R.string.camera_name, "failed");
					finish();
				} else
				{
					android.hardware.Camera.Parameters parameters = mCamera.getParameters();
					parameters.setPictureFormat(256);
					parameters.setFlashMode("on");
					android.hardware.Camera.Size size;
					List list;
					android.hardware.Camera.Size size1;
					if (parameters.getSupportedFocusModes().contains("auto"))
					{
						parameters.setFocusMode("auto");
						mAutoFocusFlag = true;
					} else
					{
						mAutoFocusFlag = false;
					}
					size = parameters.getPictureSize();
					list = parameters.getSupportedPreviewSizes();
					size1 = null;
					if (size != null && size.height != 0)
						size1 = getOptimalPreviewSize(list, (double)size.width / (double)size.height);
					if (size1 != null)
						parameters.setPreviewSize(size1.width, size1.height);
					mCamera.setParameters(parameters);
					mCamera.startPreview();
					mIsPreview = true;
				}
			}

			public void surfaceCreated(SurfaceHolder surfaceholder)
			{
				android.hardware.Camera.CameraInfo camerainfo = new android.hardware.Camera.CameraInfo();
				int i = Camera.getNumberOfCameras();
				int j = 0;
				while (j < i) 
				{
					Camera.getCameraInfo(j, camerainfo);
					if (camerainfo.facing == 0)
						try
						{
							mCamera = Camera.open(j);
							mCamera.setPreviewDisplay(surfaceholder);
						}
						catch (RuntimeException runtimeexception)
						{
							runtimeexception.printStackTrace();
						}
						catch (IOException ioexception)
						{
							ioexception.printStackTrace();
						}
					j++;
				}
			}

			public void surfaceDestroyed(SurfaceHolder surfaceholder)
			{
				if (mCamera != null && mIsPreview)
				{
					mCamera.stopPreview();
					mCamera.release();
					mCamera = null;
				}
			}

			/*
			{
				this$0 = CameraTest.this;
				super();
			}*/
		}
;
	}

	private android.hardware.Camera.Size getOptimalPreviewSize(List list, double d)
	{
		android.hardware.Camera.Size size1;
		if (list == null)
		{
			size1 = null;
		} else
		{
			android.hardware.Camera.Size size = null;
			double d1 = 1.7976931348623157E+308D;
			Display display = getWindowManager().getDefaultDisplay();
			int i = Math.min(display.getHeight(), display.getWidth());
			if (i <= 0)
				i = ((WindowManager)getSystemService("window")).getDefaultDisplay().getHeight();
			Iterator iterator = list.iterator();
			do
			{
				if (!iterator.hasNext())
					break;
				android.hardware.Camera.Size size3 = (android.hardware.Camera.Size)iterator.next();
				if (Math.abs((double)size3.width / (double)size3.height - d) <= 0.050000000000000003D && (double)Math.abs(size3.height - i) < d1)
				{
					size = size3;
					d1 = Math.abs(size3.height - i);
				}
			} while (true);
			if (size == null)
			{
				double d2 = 1.7976931348623157E+308D;
				Iterator iterator1 = list.iterator();
				do
				{
					if (!iterator1.hasNext())
						break;
					android.hardware.Camera.Size size2 = (android.hardware.Camera.Size)iterator1.next();
					if ((double)Math.abs(size2.height - i) < d2)
					{
						size = size2;
						d2 = Math.abs(size2.height - i);
					}
				} while (true);
			}
			size1 = size;
		}
		return size1;
	}

	private void setupViews()
	{
		surfaceView = (SurfaceView)findViewById(R.id.camera_view);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(surfaceCallback);
		surfaceHolder.setType(3);
		mBtFinish = (Button)findViewById(R.id.camera_take);
		mBtFinish.setOnClickListener(this);
		mBtOk = (Button)findViewById(R.id.camera_btok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.camera_btfailed);
		mBtFailed.setOnClickListener(this);
		mAutoFocus = (ImageView)findViewById(R.id.autofocus_img);
	}

	private void takePic()
	{
		mBtFinish.setEnabled(false);
		if (mAutoFocusFlag)
		{
			mAutoFocus.setImageResource(R.drawable.focus_focusing);
			mCamera.autoFocus(autofocusCallback);
		} else
		{
			mCamera.takePicture(null, null, null);
		}
	}

	public void onClick(View view)
	{
		if (view.getId() == mBtFinish.getId())
		{
			takePic();
		}
		else if (view.getId() == mBtOk.getId())
		{
			Utils.SetPreferences(getApplicationContext(), mSp, R.string.camera_name, "success");
			finish();
		} else if (view.getId() == mBtFailed.getId())
		{
			Utils.SetPreferences(getApplicationContext(), mSp, R.string.camera_name, "failed");
			finish();
		}
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		requestWindowFeature(1);
		getWindow().setFlags(1024, 1024);
		setContentView(R.layout.camera);
		mSp = getSharedPreferences("FactoryMode", 0);
		setupViews();
	}

	public void onDestory()
	{
		mCamera.startPreview();
		super.onDestroy();
	}

	public boolean onKeyDown(int i, KeyEvent keyevent)
	{
		boolean flag;
		if (i == 27 || i == 84)
		{
			takePic();
			flag = true;
		} else
		{
			flag = super.onKeyDown(i, keyevent);
		}
		return flag;
	}




/*
	static Camera access$102(CameraTest cameratest, Camera camera)
	{
		cameratest.mCamera = camera;
		return camera;
	}

*/


/*
	static boolean access$202(CameraTest cameratest, boolean flag)
	{
		cameratest.mAutoFocusFlag = flag;
		return flag;
	}

*/




/*
	static boolean access$402(CameraTest cameratest, boolean flag)
	{
		cameratest.mIsPreview = flag;
		return flag;
	}

*/
}
