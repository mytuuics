// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   SubCamera.java

package com.mediatek.factorymode.camera;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

public class SubCamera extends Activity
	implements android.view.View.OnClickListener
{

	android.hardware.Camera.CameraInfo cameraInfo;
	private Button mBtFailed;
	private Button mBtFinish;
	private Button mBtOk;
	private Camera mCamera;
	SharedPreferences mSp;
	android.hardware.Camera.PictureCallback pictureCallback;
	android.view.SurfaceHolder.Callback surfaceCallback;
	private SurfaceHolder surfaceHolder;
	private SurfaceView surfaceView;

	public SubCamera()
	{
		pictureCallback = new android.hardware.Camera.PictureCallback() {

			//final SubCamera this$0;

			public void onPictureTaken(byte abyte0[], Camera camera)
			{
			}

			/*
			{
				this$0 = SubCamera.this;
				super();
			}*/
		}
;
		surfaceCallback = new SurfaceHolder.Callback() {

			public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k)
			{
				if (mCamera == null)
				{
					try
					{
						Thread.sleep(1000L);
					}
					catch (InterruptedException interruptedexception)
					{
						interruptedexception.printStackTrace();
					}
					Utils.SetPreferences(getApplicationContext(), mSp, R.string.subcamera_name, "failed");
					finish();
				} else
				{
					android.hardware.Camera.Parameters parameters = mCamera.getParameters();
					android.hardware.Camera.Size size = parameters.getPictureSize();
					List list = parameters.getSupportedPreviewSizes();
					android.hardware.Camera.Size size1 = getOptimalPreviewSize(list, (double)size.width / (double)size.height);
					if (size1 != null)
					{
						int l = cameraInfo.orientation;
						if (l == 0 || l == 180)
							parameters.setPreviewSize(size1.height, size1.width);
						else
							parameters.setPreviewSize(size1.width, size1.height);
					}
					mCamera.setParameters(parameters);
					mCamera.startPreview();
				}
			}

			public void surfaceCreated(SurfaceHolder surfaceholder)
			{
				int i = getWindowManager().getDefaultDisplay().getRotation();
				int j = 0;
				
				switch(i)
				{
				case 0:
					j = 0;
					break;
				case 1:
					j = 90;
					break;
				case 2:
					j = 180;
					break;
				case 3:
					j = 270;
					break;					
				}
				cameraInfo = new android.hardware.Camera.CameraInfo();
				int k = Camera.getNumberOfCameras();
				int l = 0;
				while (l < k) 
				{
					Camera.getCameraInfo(l, cameraInfo);
					if (cameraInfo.facing == 1)
					{
						int i1 = (360 - (j + cameraInfo.orientation) % 360) % 360;
						try
						{
							mCamera = Camera.open(l);
							mCamera.setDisplayOrientation(i1);
							mCamera.setPreviewDisplay(surfaceholder);
						}
						catch (RuntimeException runtimeexception)
						{
							runtimeexception.printStackTrace();
						}
						catch (IOException ioexception)
						{
							ioexception.printStackTrace();
						}
					}
					l++;
				}
			}

			public void surfaceDestroyed(SurfaceHolder surfaceholder)
			{
				if (mCamera != null)
				{
					mCamera.stopPreview();
					mCamera.release();
					mCamera = null;
				}
			}

			/*
			{
				this$0 = SubCamera.this;
				super();
			}*/
		};
	}

	private android.hardware.Camera.Size getOptimalPreviewSize(List list, double d)
	{
		android.hardware.Camera.Size size1;
		if (list == null)
		{
			size1 = null;
		} else
		{
			android.hardware.Camera.Size size = null;
			double d1 = 1.7976931348623157E+308D;
			Display display = getWindowManager().getDefaultDisplay();
			int i = Math.min(display.getHeight(), display.getWidth());
			if (i <= 0)
				i = ((WindowManager)getSystemService("window")).getDefaultDisplay().getHeight();
			Iterator iterator = list.iterator();
			do
			{
				if (!iterator.hasNext())
					break;
				android.hardware.Camera.Size size4 = (android.hardware.Camera.Size)iterator.next();
				if (i <= size4.height && Math.abs((double)size4.width / (double)size4.height - d) <= 0.050000000000000003D && (double)Math.abs(size4.height - i) < d1)
				{
					size = size4;
					d1 = Math.abs(size4.height - i);
				}
			} while (true);
			if (size == null)
			{
				Iterator iterator2 = list.iterator();
				do
				{
					if (!iterator2.hasNext())
						break;
					android.hardware.Camera.Size size3 = (android.hardware.Camera.Size)iterator2.next();
					if (Math.abs((double)size3.width / (double)size3.height - d) <= 0.050000000000000003D && (double)Math.abs(size3.height - i) < d1)
					{
						size = size3;
						d1 = Math.abs(size3.height - i);
					}
				} while (true);
			}
			if (size == null)
			{
				double d2 = 1.7976931348623157E+308D;
				Iterator iterator1 = list.iterator();
				do
				{
					if (!iterator1.hasNext())
						break;
					android.hardware.Camera.Size size2 = (android.hardware.Camera.Size)iterator1.next();
					if ((double)Math.abs(size2.height - i) < d2)
					{
						size = size2;
						d2 = Math.abs(size2.height - i);
					}
				} while (true);
			}
			size1 = size;
		}
		return size1;
	}

	private void setupViews()
	{
		surfaceView = (SurfaceView)findViewById(R.id.camera_view);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(surfaceCallback);
		surfaceHolder.setType(3);
		mBtFinish = (Button)findViewById(R.id.camera_take);
		mBtFinish.setOnClickListener(this);
		mBtOk = (Button)findViewById(R.id.camera_btok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.camera_btfailed);
		mBtFailed.setOnClickListener(this);
	}

	private void takePic()
	{
		mBtFinish.setEnabled(false);
		mCamera.takePicture(null, null, pictureCallback);
	}

	public void onClick(View view)
	{
		if (view.getId() == mBtFinish.getId())
		{
			takePic();
		}
		else if (view.getId() == mBtOk.getId())
		{
			Utils.SetPreferences(getApplicationContext(), mSp, R.string.subcamera_name, "success");
			finish();
		} else if (view.getId() == mBtFailed.getId())
		{
			Utils.SetPreferences(getApplicationContext(), mSp, R.string.subcamera_name, "failed");
			finish();
		}
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		requestWindowFeature(1);
		getWindow().setFlags(1024, 1024);
		setContentView(R.layout.camera);
		mSp = getSharedPreferences("FactoryMode", 0);
		setupViews();
	}

	public boolean onKeyDown(int i, KeyEvent keyevent)
	{
		boolean flag;
		if (i == 27 || i == 84)
		{
			takePic();
			flag = true;
		} else
		{
			flag = super.onKeyDown(i, keyevent);
		}
		return flag;
	}



/*
	static Camera access$002(SubCamera subcamera, Camera camera)
	{
		subcamera.mCamera = camera;
		return camera;
	}

*/

}
