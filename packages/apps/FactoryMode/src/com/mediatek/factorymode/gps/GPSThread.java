// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   GPSThread.java

package com.mediatek.factorymode.gps;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

// Referenced classes of package com.mediatek.factorymode.gps:
//			GpsUtil

public class GPSThread
{

	private Context mContext;
	private GpsUtil mGpsUtil;
	private Handler mHandler;
	private boolean mIsSuccess;

	public GPSThread(Context context)
	{
		mIsSuccess = false;
		mHandler = new Handler() {

			public void handleMessage(Message message)
			{
				switch(message.what){
				
				case 0:
					getSatelliteInfo();
				}
			}
		}
;
		mContext = context;
		mGpsUtil = new GpsUtil(mContext);
	}

	private void getSatelliteInfo()
	{
		if (mGpsUtil.getSatelliteNumber() > 2)
		{
			mHandler.removeMessages(0);
			mIsSuccess = true;
			closeLocation();
		} else
		{
			mHandler.sendEmptyMessageDelayed(0, 2000L);
		}
	}

	public void closeLocation()
	{
		mGpsUtil.closeLocation();
	}

	public int getSatelliteNum()
	{
		return mGpsUtil.getSatelliteNumber();
	}

	public String getSatelliteSignals()
	{
		return mGpsUtil.getSatelliteSignals();
	}

	public boolean isSuccess()
	{
		return mIsSuccess;
	}

	public void start()
	{
		getSatelliteInfo();
	}

}
