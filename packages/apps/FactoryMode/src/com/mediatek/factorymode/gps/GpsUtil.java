// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   GpsUtil.java

package com.mediatek.factorymode.gps;

import android.content.Context;
import android.location.*;
import android.os.Bundle;
import java.util.*;

public class GpsUtil
{

	private Context mContext;
	private int mGpsCurrentStatus;
	private android.location.GpsStatus.Listener mGpsStatusListener;
	private LocationListener mLocationListener;
	private LocationManager mLocationManager;
	private String mProvider;
	private int mSatelliteNum;
	private List mSatelliteSignal;

	public GpsUtil(Context context)
	{
		mSatelliteSignal = new ArrayList();
		mLocationListener = new LocationListener() {

			public void onLocationChanged(Location location)
			{
			}

			public void onProviderDisabled(String s)
			{
			}

			public void onProviderEnabled(String s)
			{
			}

			public void onStatusChanged(String s, int i, Bundle bundle)
			{
			}

		}
;
		mGpsStatusListener = new android.location.GpsStatus.Listener() {

			public void onGpsStatusChanged(int i)
			{
				GpsStatus gpsstatus = mLocationManager.getGpsStatus(null);
				updateGpsStatus(i, gpsstatus);
			}

		}
;
		mContext = context;
		mLocationManager = (LocationManager)mContext.getSystemService("location");
		mProvider = "gps";
		openGPS();
		mLocationManager.getLastKnownLocation(mProvider);
		mLocationManager.requestLocationUpdates(mProvider, 2000L, 0F, mLocationListener);
		mLocationManager.addGpsStatusListener(mGpsStatusListener);
		updateGpsStatus(0, null);
	}

	private void openGPS()
	{
		if (!mLocationManager.isProviderEnabled(mProvider))
			openGPSSetting();
	}

	private void openGPSSetting()
	{
		android.provider.Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(), mProvider, true);
	}

	private void updateGpsStatus(int i, GpsStatus gpsstatus)
	{
		if (gpsstatus == null)
			mSatelliteNum = 0;
		else
		if (i == 4)
		{
			Iterator iterator = gpsstatus.getSatellites().iterator();
			while (iterator.hasNext()) 
			{
				float f = ((GpsSatellite)iterator.next()).getSnr();
				if (f >= 30F)
				{
					mSatelliteSignal.add(Float.valueOf(f));
					mSatelliteNum = 1 + mSatelliteNum;
				}
			}
		}
		mGpsCurrentStatus = i;
	}

	public void closeLocation()
	{
		mLocationManager.removeGpsStatusListener(mGpsStatusListener);
		mLocationManager.removeUpdates(mLocationListener);
	}

	public int getGpsCurrentStatus()
	{
		return mGpsCurrentStatus;
	}

	public LocationManager getLocationManager()
	{
		return mLocationManager;
	}

	public int getSatelliteNumber()
	{
		return mSatelliteNum;
	}

	public String getSatelliteSignals()
	{
		String s = mSatelliteSignal.toString();
		String s1;
		if (mSatelliteSignal.size() <= 0)
			s1 = "";
		else
			s1 = s.substring(1, s.length() - 1);
		return s1;
	}


}
