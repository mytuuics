// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   GPS.java

package com.mediatek.factorymode.gps;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import com.mediatek.factorymode.R;
import com.mediatek.factorymode.Utils;

// Referenced classes of package com.mediatek.factorymode.gps:
//			GpsUtil

public class GPS extends Activity
	implements android.view.View.OnClickListener
{

	private Button mBtFailed;
	private Button mBtOk;
	private GpsUtil mGpsUtil;
	private Handler mHandler;
	private TextView mResultView;
	private TextView mSatelliteNumView;
	private TextView mSignalView;
	private SharedPreferences mSp;
	private TextView mStView;
	private Chronometer mTimeView;

	public GPS()
	{
		mHandler = new Handler() {

			public void handleMessage(Message message)
			{
				switch(message.what){
				case 0:
					getSatelliteInfo();
				}
			}

		}
;
	}

	public void getSatelliteInfo()
	{
		int i = mGpsUtil.getSatelliteNumber();
		if (i >= 2)
		{
			mHandler.removeMessages(0);
			mTimeView.stop();
			mResultView.setText(R.string.GPS_Success);
		} else
		{
			mHandler.sendEmptyMessageDelayed(0, 3000L);
		}
		mSatelliteNumView.setText((new StringBuilder()).append(getString(R.string.GPS_satelliteNum)).append(i).toString());
		mSignalView.setText((new StringBuilder()).append(getString(R.string.GPS_Signal)).append(mGpsUtil.getSatelliteSignals()).toString());
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.gps_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.gps);
		mGpsUtil = new GpsUtil(this);
		mSp = getSharedPreferences("FactoryMode", 0);
		mStView = (TextView)findViewById(R.id.gps_state_id);
		mSatelliteNumView = (TextView)findViewById(R.id.gps_satellite_id);
		mSignalView = (TextView)findViewById(R.id.gps_signal_id);
		mResultView = (TextView)findViewById(R.id.gps_result_id);
		mTimeView = (Chronometer)findViewById(R.id.gps_time_id);
		mBtOk = (Button)findViewById(R.id.gps_bt_ok);
		mBtFailed = (Button)findViewById(R.id.gps_bt_failed);
		mBtOk.setOnClickListener(this);
		mBtFailed.setOnClickListener(this);
		mTimeView.setFormat(getResources().getString(R.string.GPS_time));
		mStView.setText(R.string.GPS_connect);
		mTimeView.start();
		getSatelliteInfo();
	}

	protected void onDestroy()
	{
		mGpsUtil.closeLocation();
		super.onDestroy();
	}
}
