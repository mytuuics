// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Report.java

package com.mediatek.factorymode;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Report extends Activity {

    final int itemString[];

    private TextView mDefault;

    private List mDefaultList;

    private TextView mFailed;

    private List mFailedList;

    private List mOkList;

    private SharedPreferences mSp;

    private TextView mSuccess;

    public Report() {
        int len = TestActivityUtil.getTestCount();
        itemString = new int[len];
        for (int i = 0; i < len; i++) {
            itemString[i] = TestActivityUtil.getNameIDFromIndex(i);
        }
    }

    protected void ShowInfo() {
        String s = (new StringBuilder()).append("\n").append(getString(R.string.report_ok))
                .append("\n").toString();
        for (int i = 0; i < mOkList.size(); i++)
            s = (new StringBuilder()).append(s).append((String) mOkList.get(i)).append(" | ")
                    .toString();

        mSuccess.setText(s);
        String s1 = (new StringBuilder()).append("\n").append(getString(R.string.report_failed))
                .append("\n").toString();
        for (int j = 0; j < mFailedList.size(); j++)
            s1 = (new StringBuilder()).append(s1).append((String) mFailedList.get(j)).append(" | ")
                    .toString();

        mFailed.setText(s1);
        String s2 = (new StringBuilder()).append("\n").append(getString(R.string.report_notest))
                .append("\n").toString();
        for (int k = 0; k < mDefaultList.size(); k++)
            s2 = (new StringBuilder()).append(s2).append((String) mDefaultList.get(k))
                    .append(" | ").toString();

        mDefault.setText(s2);
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.report);
        mSp = getSharedPreferences("FactoryMode", 0);
        mSuccess = (TextView) findViewById(R.id.report_success);
        mFailed = (TextView) findViewById(R.id.report_failed);
        mDefault = (TextView) findViewById(R.id.report_default);
        mOkList = new ArrayList();
        mFailedList = new ArrayList();
        mDefaultList = new ArrayList();
        int i = 0;
        while (i < itemString.length) {
            if (mSp.getString(getString(itemString[i]), null).equals("success"))
                mOkList.add(getString(itemString[i]));
            else if (mSp.getString(getString(itemString[i]), null).equals("failed"))
                mFailedList.add(getString(itemString[i]));
            else
                mDefaultList.add(getString(itemString[i]));
            i++;
        }
        ShowInfo();
    }
}
