// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   KeyCode.java

package com.mediatek.factorymode;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.mediatek.factorymode:
//			Utils, FactoryMode

public class KeyCode extends Activity
	implements android.view.View.OnClickListener
{
	public class MyAdapter extends BaseAdapter
	{

		private LayoutInflater mInflater;
		//final KeyCode this$0;

		public int getCount()
		{
			int i;
			if (mListData == null)
				i = 0;
			else
				i = mListData.size();
			return i;
		}

		public Object getItem(int i)
		{
			return Integer.valueOf(i);
		}

		public long getItemId(int i)
		{
			return (long)i;
		}

		public View getView(int i, View view, ViewGroup viewgroup)
		{
			View view1 = mInflater.inflate(R.layout.keycode_grid, null);
			((ImageView)view1.findViewById(R.id.imgview)).setBackgroundResource(((Integer)mListData.get(i)).intValue());
			return view1;
		}

		public MyAdapter(Context context)
		{
			/*this$0 = KeyCode.this;
			super();*/
			mInflater = LayoutInflater.from(context);
		}

		public MyAdapter(FactoryMode factorymode, int i)
		{
			/*
			this$0 = KeyCode.this;
			super();*/
		}
	}


	final int imgString[];
	Button mBtFailed;
	Button mBtOk;
	private GridView mGrid;
	TextView mInfo;
	String mKeycode;
	private List mListData;
	SharedPreferences mSp;

	public KeyCode()
	{
		mKeycode = "";
		int ai[] = new int[8];
		ai[0] = R.drawable.home;
		ai[1] = R.drawable.menu;
		ai[2] = R.drawable.vldown;
		ai[3] = R.drawable.vlup;
		ai[4] = R.drawable.back;
		ai[5] = R.drawable.search;
		ai[6] = R.drawable.camera;
		ai[7] = R.drawable.unknown;
		imgString = ai;
	}

	public void onAttachedToWindow()
	{
		super.onAttachedToWindow();
	}

	public void onClick(View view)
	{
		SharedPreferences sharedpreferences = mSp;
		String s;
		if (view.getId() == mBtOk.getId())
			s = "success";
		else
			s = "failed";
		Utils.SetPreferences(this, sharedpreferences, R.string.KeyCode_name, s);
		finish();
	}

	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.keycode);
		mSp = getSharedPreferences("FactoryMode", 0);
		onAttachedToWindow();
		mInfo = (TextView)findViewById(R.id.keycode_info);
		mBtOk = (Button)findViewById(R.id.keycode_bt_ok);
		mBtOk.setOnClickListener(this);
		mBtFailed = (Button)findViewById(R.id.keycode_bt_failed);
		mBtFailed.setOnClickListener(this);
		mListData = new ArrayList();
		mGrid = (GridView)findViewById(R.id.keycode_grid);
	}

	public boolean onKeyDown(int i, KeyEvent keyevent)
	{
		boolean flag = true;
		switch(i)
		{
		case 3:
			if (mKeycode.indexOf("HOME") >= 0)
			{
				flag = false;				
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("HOME\n").toString();
				mListData.add(Integer.valueOf(imgString[0]));
			}
			break;
			
		case 4:
			if (mKeycode.indexOf("BACK") >= 0)
			{
				flag = false;			
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("BACK\n").toString();
				mListData.add(Integer.valueOf(imgString[4]));
			}
			
			break;
		case 24:
			if (mKeycode.indexOf("VLUP") >= 0)
			{
				flag = false;			
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("VLUP\n").toString();
				mListData.add(Integer.valueOf(imgString[3]));
			}
			break;
		case 25:
			if (mKeycode.indexOf("VLDOWN") >= 0)
			{
				flag = false;				
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("VLDOWN\n").toString();
				mListData.add(Integer.valueOf(imgString[2]));
			}
			break;
		case 27:
			if (mKeycode.indexOf("CAMERA") >= 0)
			{
				flag = false;				
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("CAMERA\n").toString();
				mListData.add(Integer.valueOf(imgString[6]));
			}
			break;
			
		case 82:
			if (mKeycode.indexOf("MENU") >= 0)
			{
				flag = false;
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("MENU\n").toString();
				mListData.add(Integer.valueOf(imgString[1]));
			}
			break;
			
		case 84:
			if (mKeycode.indexOf("SEARCH") >= 0)
			{
				flag = false;
			}
			else
			{
				mKeycode = (new StringBuilder()).append(mKeycode).append("SEARCH\n").toString();
				mListData.add(Integer.valueOf(imgString[5]));
			}
			break;
			
		default:
			mListData.add(Integer.valueOf(imgString[7]));
			break;
		}
		
		mGrid.setAdapter(new MyAdapter(this));
		
		return flag;
	}
}
