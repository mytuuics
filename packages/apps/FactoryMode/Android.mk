LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := FactoryMode
LOCAL_CERTIFICATE := platform


# Should not use this if need to use customized SDK. Disabled.
# LOCAL_SDK_VERSION := current

include $(BUILD_PACKAGE)
