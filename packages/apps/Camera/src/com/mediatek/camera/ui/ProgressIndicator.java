package com.mediatek.camera.ui;

import com.android.camera.R;
import com.android.camera.ui.Rotatable;
import com.android.camera.ui.RotateLayout;

import android.app.Activity;
import android.view.View;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mediatek.xlog.Xlog;

public class ProgressIndicator {
	
	private static final String TAG = "ProgressIndicator";
	public static final int TYPE_MAV = 1;
	public static final int TYPE_PANO = 2;

	public static final int BLOCK_NUMBERS = 9;

	//those sizes are designed for mdpi in pixels, you need to change progress_indicator.xml when change the values here.
	private final int mPanoBlockSizes[] = {17, 15, 13, 12, 11, 12, 13, 15, 17};
	private final int mMavBlockSizes[] =  {11, 12, 13, 15, 17, 15, 13, 12, 11};
	
	public static final int MAV_CAPTURE_NUM = 15;
	public static final int PANORAMA_CAPTURE_NUM = 9;

	private int mBlockPadding = 4;

	private View mProgressView;
	private View mOkBtn;
	private View mCancelBtn;
	private ImageView mProgressBars;
	private final int mOrientation;
	
	public ProgressIndicator(Activity activity, int indicatorType) {
		mProgressView = activity.findViewById(R.id.progress_indicator);
		mProgressBars = (ImageView)activity.findViewById(R.id.progress_bars);
		mOrientation = activity.getRequestedOrientation();
		
		Resources res = activity.getResources();
		final float scale = res.getDisplayMetrics().density;
		if (indicatorType == TYPE_MAV) {
			if (scale != 1.0f) {
				mBlockPadding = (int)(mBlockPadding * scale + 0.5f);
				for (int i = 0; i < BLOCK_NUMBERS; i++) {
					mMavBlockSizes[i] = (int)(mMavBlockSizes[i] * scale + 0.5f);
				}
			}
			mProgressBars.setImageDrawable(new ProgressBarDrawable(activity,mProgressBars,mMavBlockSizes
				,mBlockPadding));
		} else if (indicatorType == TYPE_PANO) {
			if (scale != 1.0f) {
				mBlockPadding = (int)(mBlockPadding * scale + 0.5f);
				for (int i = 0; i < BLOCK_NUMBERS; i++) {
					mPanoBlockSizes[i] = (int)(mPanoBlockSizes[i] * scale + 0.5f);
					Xlog.i(TAG, "mPanoBlockSizes[i]: " + mPanoBlockSizes[i]);
				}
			}		
			mProgressBars.setImageDrawable(new ProgressBarDrawable(activity,mProgressBars,mPanoBlockSizes
				,mBlockPadding));
			mOkBtn = activity.findViewById(R.id.pano_ok);			
			mOkBtn.setVisibility(View.VISIBLE);
			mCancelBtn = activity.findViewById(R.id.pano_cancel);
			mCancelBtn.setVisibility(View.VISIBLE);
			enableOkCancelButton(false);
		}
	}

    public void setVisibility(int visibility) {
		mProgressView.setVisibility(visibility);
	}

	public void setOkCancelClickListener(View.OnClickListener ok_listener,View.OnClickListener cancel_listener) {
		mOkBtn.setOnClickListener(ok_listener);
		mCancelBtn.setOnClickListener(cancel_listener);
	}

	public void enableOkCancelButton(boolean enable) {
		mOkBtn.setEnabled(enable);
		mCancelBtn.setEnabled(enable);		
	}
	
	public void setProgress(int progress) {
		Xlog.i(TAG, "setProgress: " + progress);
		mProgressBars.setImageLevel(progress);
	}
}
